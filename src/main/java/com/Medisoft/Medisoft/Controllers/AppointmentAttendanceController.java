package com.Medisoft.Medisoft.Controllers;


import com.Medisoft.Medisoft.Entities.AppointmentManagement.EventoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AppointmentService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Exceptions.InvalidAppointmentStateActionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/turno/asistencia")
public class AppointmentAttendanceController {

    @Autowired
    private AppointmentService appointmentService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Turno> asistirTurno(@RequestParam long idTurno) {
        Turno turno = appointmentService.findById(idTurno);
        if (turno.esAsistible()) {
            turno.actualizarEstado(EventoTurno.REGISTRAR_ASISTENCIA);
            log.info("Asistencia registrada");
            return ResponseEntity.ok(appointmentService.save(turno));
        } else {
            throw new InvalidAppointmentStateActionException();
        }
    }
}
