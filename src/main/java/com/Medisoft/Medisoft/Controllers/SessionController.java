package com.Medisoft.Medisoft.Controllers;


import com.Medisoft.Medisoft.Entities.SessionManagement.DTO.SessionUserDTO;
import com.Medisoft.Medisoft.Entities.SessionManagement.DTO.UserDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/sesion")
public class SessionController {

    @Autowired
    private SessionService sessionService;


    @PostMapping(value = "/inicio", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> login(@RequestBody SessionUserDTO sessionUserDto) {
        UserDTO userDTO = sessionService.login(sessionUserDto);
        return ResponseEntity.ok(userDTO);
    }

    @PostMapping(value = "/cierre", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> logout(@RequestParam int sessionId) {
        sessionService.logout(sessionId);
        return ResponseEntity.ok().build();
    }
}
