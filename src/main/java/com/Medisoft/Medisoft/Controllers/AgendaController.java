package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.AgendaCheckedDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.AgendaDto;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.AgendaService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalProfileDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(value = "/usuario/{professionalUserId}/profesional/perfilProfesional/agenda",
        produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class AgendaController {

    @Autowired
    private AgendaService agendaService;


    @PostMapping(value = "/check")
    public ResponseEntity<Set<Long>> checkAndSaveAgenda(@PathVariable int professionalUserId,
                                                        @RequestBody AgendaDto agendaDto) {
        Set<Long> affectedAppointments = agendaService.checkAndSaveAgenda(professionalUserId, agendaDto);
        return ResponseEntity.ok(affectedAppointments);
    }

    @PostMapping
    public ResponseEntity<ProfessionalProfileDTO> saveAgenda(@PathVariable int professionalUserId,
                                                             @RequestBody AgendaCheckedDTO agendaCheckedDTO) {
        User updatedUser = agendaService.saveAgenda(professionalUserId, agendaCheckedDTO);
        return ResponseEntity.ok(new ProfessionalProfileDTO(updatedUser));
    }
}
