package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO.CategoriaTipoAtencionDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO.ProfessionalAttentionTypeDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.Services.ProfessionalAttentionTypeService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalProfileDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;


@RestController
@RequestMapping(value = "/user/{professionalUserId}/professional/attention-types", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProfessionalAttentionTypeController {

    @Autowired
    private ProfessionalAttentionTypeService professionalAttentionTypeService;


    @GetMapping
    public ResponseEntity<Set<CategoriaTipoAtencionDTO>> getAttentionTypesByPatient(
            @PathVariable int professionalUserId,
            @RequestParam("patientUserId") int patientUserId) {
        return ResponseEntity.ok(professionalAttentionTypeService.getAttentionTypesByPatient(professionalUserId, patientUserId));
    }

    @GetMapping("/all")
    public ResponseEntity<Set<CategoriaTipoAtencionDTO>> getAllAttentionTypes(@PathVariable int professionalUserId) {
        return ResponseEntity.ok(professionalAttentionTypeService.getAllAttentionTypes(professionalUserId));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfessionalProfileDTO> postProfessionalAttentionTypes(@PathVariable int professionalUserId,
                                                                                 @RequestBody Set<ProfessionalAttentionTypeDTO> attentions) {

        return ResponseEntity.ok(professionalAttentionTypeService.createProfessionalAttentionTypes(professionalUserId, attentions));
    }
}
