package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.StatisticsManagement.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/users/{professionalUserId}/professional/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProfessionalStatisticsController {

    @Autowired
    private ProfessionalStatisticsService professionalStatisticsService;


    @GetMapping(value = "/appointments-by-state")
    public ResponseEntity<List<AppointmentByState>> getAppointmentsByState(@PathVariable int professionalUserId,
                                                                  @RequestParam(name = "stateName") List<String> stateNames,
                                                                  @RequestParam(name = "fromTimestamp", defaultValue = "0") long fromTimestamp,
                                                                  @RequestParam(name = "toTimestamp", defaultValue = "9223372036854775807") long toTimestamp) {
        if (CollectionUtils.isEmpty(stateNames)){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(professionalStatisticsService.getAppointmentsByState(professionalUserId, stateNames, fromTimestamp, toTimestamp));
    }

    @GetMapping(value = "/appointments-treated-by-health-insurance")
    public ResponseEntity<List<AppointmentByHealthInsurance>> getAppointmentsTreatedByHealthInsurance(@PathVariable int professionalUserId,
                                                                                                       @RequestParam(name = "fromTimestamp", defaultValue = "0") long fromTimestamp,
                                                                                                       @RequestParam(name = "toTimestamp", defaultValue = "9223372036854775807") long toTimestamp) {
        return ResponseEntity.ok(professionalStatisticsService.getAppointmentsTreatedByHealthInsurance(professionalUserId, fromTimestamp, toTimestamp));
    }

    @GetMapping(value = "/payments-by-payment-type")
    public ResponseEntity<List<PaymentByPaymentType>> getPaymentsByPaymentType(@PathVariable int professionalUserId,
                                                                               @RequestParam(name = "fromTimestamp", defaultValue = "0") long fromTimestamp,
                                                                               @RequestParam(name = "toTimestamp", defaultValue = "9223372036854775807") long toTimestamp) {
        return ResponseEntity.ok(professionalStatisticsService.getPaymentsByPaymentType(professionalUserId, fromTimestamp, toTimestamp));
    }

    @GetMapping(value = "/appointments-treated-by-month")
    public ResponseEntity<List<AppointmentByMonth>> getAppointmentsTreatedByMonth(@PathVariable int professionalUserId,
                                                                                   @RequestParam(name = "fromTimestamp", defaultValue = "0") long fromTimestamp,
                                                                                   @RequestParam(name = "toTimestamp", defaultValue = "9223372036854775807") long toTimestamp) {
        return ResponseEntity.ok(professionalStatisticsService.getAppointmentsTreatedByMonth(professionalUserId, fromTimestamp, toTimestamp));
    }

    @GetMapping(value = "/appointments-treated-by-day")
    public ResponseEntity<List<AppointmentByDay>> getAppointmentsTreatedByDay(@PathVariable int professionalUserId,
                                                                               @RequestParam(name = "fromTimestamp", defaultValue = "0") long fromTimestamp,
                                                                               @RequestParam(name = "toTimestamp", defaultValue = "9223372036854775807") long toTimestamp) {
        return ResponseEntity.ok(professionalStatisticsService.getAppointmentsTreatedByDay(professionalUserId, fromTimestamp, toTimestamp));
    }
}
