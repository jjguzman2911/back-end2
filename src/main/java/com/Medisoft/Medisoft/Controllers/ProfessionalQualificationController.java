package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification.QualificationDetailedViewDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification.QualificationListViewDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification.QualificationRequestDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Services.ProfessionalQualificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/professionals/qualification", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProfessionalQualificationController {

    @Autowired
    private ProfessionalQualificationService qualificationService;


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> requestQualificationAsProfessional(@RequestBody QualificationRequestDTO qualificationRequest){
        qualificationService.requestQualificationAsProfessional(qualificationRequest);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<QualificationListViewDTO>> getAllQualificationRequests(){
        return ResponseEntity.ok(qualificationService.getAllQualificationRequests());
    }

    @GetMapping("/{qualificationRequestId}")
    public ResponseEntity<QualificationDetailedViewDTO> getQualificationRequest(@PathVariable("qualificationRequestId") int requestId){
        return ResponseEntity.ok(qualificationService.getQualificationRequestById(requestId));
    }

    @PutMapping("/{qualificationRequestId}/accept")
    public ResponseEntity<Void> acceptQualificationRequest(@PathVariable("qualificationRequestId") int requestId){
        qualificationService.acceptQualificationRequest(requestId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{qualificationRequestId}/decline")
    public ResponseEntity<Void> declineQualificationRequest(@PathVariable("qualificationRequestId") int requestId){
        qualificationService.declineQualificationRequest(requestId);
        return ResponseEntity.noContent().build();
    }
}
