package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO.AppointmentDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO.ConsultaTurnosProfesionalDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO.PatientAppointmentDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO.ProfessionalAppointmentDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.PatientAppointmentCheckService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.ProfessionalAppointmentCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.Response;
import java.util.List;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class AppointmentCheckController {

    @Autowired
    private PatientAppointmentCheckService patientAppointmentCheckService;

    @Autowired
    private ProfessionalAppointmentCheckService professionalAppointmentCheckService;

    /**
     * @param future         indicates if the appointments should be future or not.
     * @param maxSize        indicates the max size of appointments to retrieve.
     * @param ascendingOrder indicates that appointments are ordered from closest to farest
     */
    @GetMapping(value = "/appointments")
    public ResponseEntity<List<PatientAppointmentDTO>> getAppointmentsForPatient(
            @RequestParam("patientUserId") int patientUserId,
            @RequestParam(value = "professionalUserId", required = false) Integer professionalUserId,
            @RequestParam(value = "future", defaultValue = "false") boolean future,
            @RequestParam(value = "maxSize", defaultValue = "100") int maxSize,
            @RequestParam(value = "ascendingOrder", defaultValue = "true") boolean ascendingOrder) {
        return professionalUserId != null ?
                ResponseEntity.ok(patientAppointmentCheckService.getAllPatientAppointmentsForProfessional(
                        patientUserId, professionalUserId, future, maxSize, ascendingOrder)) :
                ResponseEntity.ok(patientAppointmentCheckService.getAllPatientAppointments(
                        patientUserId, future, maxSize, ascendingOrder));
    }


    @GetMapping(value = "/users/{professionalUserId}/professional/appointments/available")
    public ResponseEntity<List<AppointmentDTO>> getAvailableAppointments(@PathVariable int professionalUserId) {
        return ResponseEntity.ok(professionalAppointmentCheckService.getAvailableAppointments(professionalUserId));
    }


    @GetMapping(value = "/usuario/{professionalUserId}/profesional/turno/proximos")
    public Response consultarProximosTurnosProfesional(@PathVariable int professionalUserId) {
        ConsultaTurnosProfesionalDTO consultaTurnosProfesionalDTO = professionalAppointmentCheckService.getNextAppointments(professionalUserId);
        return Response.ok(consultaTurnosProfesionalDTO).build();
    }

    @GetMapping(value = "/professionals/{professionalUserId}/appointments", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProfessionalAppointmentDTO>> getFutureAppointmentsForProfessional(
            @PathVariable("professionalUserId") int professionalUserId,
            @RequestParam(value = "maxSize", defaultValue = "100") int maxSize) {
        return ResponseEntity.ok(professionalAppointmentCheckService.getFutureAppointments(professionalUserId, maxSize));
    }

}
