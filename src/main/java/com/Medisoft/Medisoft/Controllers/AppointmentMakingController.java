package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.AppointmentMakingDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AppointmentMakingService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/usuario/{professionalUserId}/profesional/turno")
public class AppointmentMakingController {

    @Autowired
    private AppointmentMakingService appointmentMakingService;


    @PostMapping(value = "/asignacion", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Turno> makeAppointment(@PathVariable int professionalUserId,
                                                 @RequestBody AppointmentMakingDTO appointmentMakingDto) {
        Turno appointment = appointmentMakingService.makeAppointment(professionalUserId, appointmentMakingDto);
        return ResponseEntity.ok(appointment);
    }

}