package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.PaymentManagement.DTO.AccessTokenDTO;
import com.Medisoft.Medisoft.Entities.PaymentManagement.DTO.MercadopagoCredentialsDTO;
import com.Medisoft.Medisoft.Entities.PaymentManagement.Services.MercadopagoCredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;


@RestController
@RequestMapping("/mercadopago-credentials/{professionalUserId}")
public class MercadopagoCredentialsController {

    @Autowired
    private MercadopagoCredentialsService credentialsService;


    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createCredentials(@PathVariable int professionalUserId,
                                                  @RequestBody MercadopagoCredentialsDTO credentials){
        credentialsService.createCredentials(professionalUserId, credentials);
        return ResponseEntity.created(URI.create("/mercadopago-credentials/" + professionalUserId)).build();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccessTokenDTO> getCredentials(@PathVariable int professionalUserId){
        return ResponseEntity.ok(credentialsService.getCredentials(professionalUserId));
    }

    @DeleteMapping
    public ResponseEntity<Void> disableCredentials(@PathVariable int professionalUserId){
        credentialsService.disableCredentials(professionalUserId);
        return ResponseEntity.ok().build();
    }
}
