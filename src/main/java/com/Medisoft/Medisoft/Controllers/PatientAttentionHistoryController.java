package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.AttentionDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Services.PatientAttentionHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/users/{patientUserId}/patient/attention-history", produces = MediaType.APPLICATION_JSON_VALUE)
public class PatientAttentionHistoryController {

    @Autowired
    private PatientAttentionHistoryService patientAttentionHistoryService;


    @GetMapping
    public ResponseEntity<List<AttentionDTO>> getPatientAttentionHistoryByProfessional(
            @PathVariable int patientUserId,
            @RequestParam("professionalUserId") int professionalUserId) {
        return ResponseEntity.ok(patientAttentionHistoryService.getPatientAttentionHistoryByProfessional(patientUserId, professionalUserId));
    }

    @GetMapping("/all")
    public ResponseEntity<List<AttentionDTO>> getAllAttentions(
            @PathVariable int patientUserId,
            @RequestParam(value = "size", defaultValue = "100") int size) {
        return ResponseEntity.ok(patientAttentionHistoryService.getAllAttentions(patientUserId, size));
    }

}
