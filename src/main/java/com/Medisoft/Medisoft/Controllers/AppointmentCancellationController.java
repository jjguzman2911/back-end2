package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AppointmentCancellationService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/appointments/{appointmentId}")
public class AppointmentCancellationController {

    @Autowired
    private AppointmentCancellationService appointmentCancellationService;


    @PostMapping(value = "/cancellation", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Turno>> cancelAppointment(@PathVariable long appointmentId) {
        return ResponseEntity.ok(appointmentCancellationService.cancelAppointment(appointmentId));
    }

    @PutMapping(value = "/nullification")
    public ResponseEntity<Void> nullifyAppointment(@PathVariable long appointmentId) {
        appointmentCancellationService.nullify(appointmentId);
        return ResponseEntity.noContent().build();
    }

}
