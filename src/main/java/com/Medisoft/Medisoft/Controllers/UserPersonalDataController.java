package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.DAO.JPA.ISexoDAO;
import com.Medisoft.Medisoft.DAO.JPA.ITipoDocumentoDAO;
import com.Medisoft.Medisoft.Entities.UserManagement.Person.PersonalDataDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/personal-data")
public class UserPersonalDataController {

    @Autowired
    private ISexoDAO sexoDAO;

    @Autowired
    private ITipoDocumentoDAO tipoDocumentoDAO;


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonalDataDTO> getPersonalData() {
        PersonalDataDTO personalDataDTO = new PersonalDataDTO(sexoDAO.findAll(), tipoDocumentoDAO.findAll());
        return ResponseEntity.ok(personalDataDTO);
    }
}
