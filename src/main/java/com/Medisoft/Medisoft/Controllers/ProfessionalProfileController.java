package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ProfessionalProfileReviewDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalProfileDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalProfileForPatientDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class ProfessionalProfileController {

    @Autowired
    private ProfessionalProfileService professionalProfileService;


    @GetMapping("/usuario/{professionalUserId}/profesional/perfilProfesional")
    public ResponseEntity<ProfessionalProfileDTO> getProfessionalProfile(@PathVariable int professionalUserId) {
        return ResponseEntity.ok(professionalProfileService.getProfessionalProfile(professionalUserId));
    }

    @GetMapping("/usuario/{professionalUserId}/profesional/perfilProfesional/vistaPaciente")
    public ResponseEntity<ProfessionalProfileForPatientDTO> getProfessionalProfileForPatient(@PathVariable int professionalUserId) {
        return ResponseEntity.ok(professionalProfileService.getProfessionalProfileForPatient(professionalUserId));
    }

    @GetMapping("/users/{professionalUserId}/professional/reviewProfile")
    public ResponseEntity<ProfessionalProfileReviewDTO> getProfessionalReviewProfile(@PathVariable int professionalUserId) {
        return ResponseEntity.ok(professionalProfileService.findProfessionalReview(professionalUserId));
    }
}
