package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.AttentionManagement.Services.PatientProfessionalHistoryService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalForPatientHistoryDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalSetGroupedBySpecialty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(value = "/users/{patientUserId}/patient/professional-history", produces = MediaType.APPLICATION_JSON_VALUE)
public class PatientProfessionalHistoryController {

    @Autowired
    private PatientProfessionalHistoryService patientProfessionalHistoryService;


    @GetMapping
    public ResponseEntity<Set<ProfessionalSetGroupedBySpecialty>> getProfessionalHistoryByPatient(@PathVariable int patientUserId) {
        Set<ProfessionalSetGroupedBySpecialty> professionalForPatientHistoryDTOSet =
                patientProfessionalHistoryService.getProfessionalHistoryByPatientGroupedBySpecialty(patientUserId);
        return ResponseEntity.ok(professionalForPatientHistoryDTOSet);
    }

    @GetMapping("/all")
    public ResponseEntity<Set<ProfessionalForPatientHistoryDTO>> getAllProfessionals(@PathVariable int patientUserId,
                                                                                     @RequestParam(value = "size", defaultValue = "100") int size) {
        return ResponseEntity.ok(patientProfessionalHistoryService.getAllProfessionals(patientUserId, size));
    }

}
