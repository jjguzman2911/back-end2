package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ReviewDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ReviewProfileDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @PostMapping(value = "/attentions/{attentionId}/review", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createReview(@PathVariable("attentionId") long attentionId,
                                             @RequestBody ReviewDTO reviewDTO) {
        reviewService.createReview(attentionId, reviewDTO);
        return ResponseEntity.created(URI.create("/attentions/" + attentionId + "/review")).build();
    }

    @GetMapping(value = "/reviews", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReviewProfileDTO> getReviewProfile(@RequestParam("professionalUserId") int professionalUserId) {
        return ResponseEntity.ok(reviewService.getReviewProfile(professionalUserId));
    }
}
