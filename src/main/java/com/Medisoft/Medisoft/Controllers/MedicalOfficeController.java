package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice.Consultorio;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice.MedicalOfficeService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalProfileDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/usuario/{professionalUserId}/profesional/perfilProfesional/consultorio",
        produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class MedicalOfficeController {

    @Autowired
    private MedicalOfficeService medicalOfficeService;


    @PostMapping
    public ResponseEntity<ProfessionalProfileDTO> postMedicalOffice(@PathVariable int professionalUserId,
                                                                    @RequestBody Consultorio medicalOffice) {
        User professionalUserUpdated = medicalOfficeService.postMedicalOffice(professionalUserId, medicalOffice);
        return ResponseEntity.ok(new ProfessionalProfileDTO(professionalUserUpdated));
    }
}
