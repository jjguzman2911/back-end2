package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.PatientManagement.Paciente;
import com.Medisoft.Medisoft.Entities.SessionManagement.VerificationToken;
import com.Medisoft.Medisoft.Entities.UserManagement.DTO.BasicUserDataDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.DTO.UserProfileDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.EventManagement.OnUserSignUpEvent;
import com.Medisoft.Medisoft.Entities.UserManagement.TokenService;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Entities.UserManagement.UserState;
import com.Medisoft.Medisoft.Exceptions.InvalidDataException;
import com.Medisoft.Medisoft.Exceptions.UserDataIntegrityViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private TokenService tokenService;


    @PostMapping(value = "/sign-up", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> postUser(@RequestBody UserProfileDTO userDTO) {
        String email = userDTO.getDatosUsuario().getEmail();
        String documento = userDTO.getDatosPersonales().getDocumento();
        validateUserEmailAndDocumento(email, documento);

        User user = User.builder().email(email)
                .contrasenia(userDTO.getDatosUsuario().getContrasenia())
                .fotoUrl(userDTO.getDatosUsuario().getFotoUrl())
                .persona(userDTO.getDatosPersonales())
                .paciente(new Paciente())
                .actualState(UserState.NON_VERIFIED).build();
        String validate = user.validate();
        if (!StringUtils.isEmpty(validate)) {
            throw new InvalidDataException(validate);
        }

        User savedUser = userService.save(user);
        eventPublisher.publishEvent(new OnUserSignUpEvent(savedUser));

        return ResponseEntity.ok(savedUser);
    }


    @GetMapping(value = "/sign-up", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> confirmSignUp(@RequestParam("token") String token) {
        VerificationToken verificationToken = tokenService.findByToken(token);

        User user = verificationToken.getUser();
        user.updateState(UserState.VERIFIED);

        return ResponseEntity.ok(userService.save(user));
    }


    @GetMapping(value = "/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserProfileDTO> getUserData(@PathVariable("userId") int userId) {
        User user = userService.findById(userId);

        return ResponseEntity.ok(buildUserProfileDTO(user));
    }


    @PostMapping(value = "/{userId}", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserProfileDTO> putUserData(@PathVariable("userId") int userId,
                                                      @RequestBody UserProfileDTO userProfileDTO) {
        User user = userService.findById(userId);
        String email = userProfileDTO.getDatosUsuario().getEmail();
        String documento = userProfileDTO.getDatosPersonales().getDocumento();
        if (!user.getEmail().equals(email)) {
            validateEmail(email);
        }
        if (!(user.getDocumento().equals(documento))) {
            validateDocumento(documento);
        }

        user.setPersona(userProfileDTO.getDatosPersonales());
        user.setContrasenia(userProfileDTO.getDatosUsuario().getContrasenia());
        user.setEmail(email);
        user.setFotoUrl(userProfileDTO.getDatosUsuario().getFotoUrl());

        String validate = user.validate();
        if (!StringUtils.isEmpty(validate)) {
            throw new InvalidDataException(validate);
        }

        User savedUser = userService.save(user);
        return ResponseEntity.ok(buildUserProfileDTO(savedUser));
    }


    private UserProfileDTO buildUserProfileDTO(User user) {
        BasicUserDataDTO basicUserDataDTO = BasicUserDataDTO.builder().contrasenia(user.getContrasenia())
                .email(user.getEmail())
                .fotoUrl(user.getFotoUrl()).build();

        return UserProfileDTO.builder().datosPersonales(user.getPersona())
                .datosUsuario(basicUserDataDTO).build();
    }

    private void validateUserEmailAndDocumento(String email, String documento) {
        validateEmail(email);
        validateDocumento(documento);
    }

    private void validateEmail(String email) {
        boolean existsEmail = userService.existsEmail(email);
        if (existsEmail) {
            throw new UserDataIntegrityViolationException("email", email);
        }
    }

    private void validateDocumento(String documento) {
        boolean existsDocumento = userService.existsDocumento(documento);
        if (existsDocumento) {
            throw new UserDataIntegrityViolationException("documento", documento + "");
        }
    }
}
