package com.Medisoft.Medisoft.Controllers;


import com.Medisoft.Medisoft.Entities.PaymentManagement.AppointmentPayment;
import com.Medisoft.Medisoft.Entities.PaymentManagement.DTO.OnSiteAppointmentPaymentDTO;
import com.Medisoft.Medisoft.Entities.PaymentManagement.DTO.PaymentInitDTO;
import com.Medisoft.Medisoft.Entities.PaymentManagement.Services.OnSitePaymentService;
import com.Medisoft.Medisoft.Entities.PaymentManagement.Services.OnlinePaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class AppointmentPaymentController {

    @Autowired
    private OnlinePaymentService onlinePaymentService;

    @Autowired
    private OnSitePaymentService onSitePaymentService;


    @PostMapping(value = "/turno/cobro/notificacion-pago/{appointmentId}")
    public ResponseEntity<Void> processOnlinePayment(@RequestParam(value = "topic", required = false) String topic,
                                                     @RequestParam(value = "id", required = false) String paymentId,
                                                     @RequestParam(value = "type", required = false) String type,
                                                     @RequestParam(value = "data.id", required = false) String dataId,
                                                     @PathVariable("appointmentId") long appointmentId) {
        if ((paymentId == null || topic == null) && (dataId == null || type == null)){
            return ResponseEntity.badRequest().build();
        }
        if (paymentId != null && topic != null){
            onlinePaymentService.processPayment(topic, paymentId, appointmentId);
        }
        else {
            onlinePaymentService.processPayment(type, dataId, appointmentId);
        }
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/turno/cobro/cobro-presencial", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AppointmentPayment> payOnSite(@RequestBody OnSiteAppointmentPaymentDTO onSiteAppointmentPaymentDTO) {
        AppointmentPayment payment = onSitePaymentService.payOnSite(onSiteAppointmentPaymentDTO);
        return ResponseEntity.ok(payment);
    }

    @GetMapping(value = "appointment/payment")
    public ResponseEntity<PaymentInitDTO> getPaymentInit(@RequestParam("appointmentId") long appointmentId) {
        PaymentInitDTO paymentInit = onSitePaymentService.getPaymentInit(appointmentId);
        return ResponseEntity.ok(paymentInit);
    }
}