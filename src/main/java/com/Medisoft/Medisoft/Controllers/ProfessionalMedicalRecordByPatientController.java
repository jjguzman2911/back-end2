package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.AttentionManagement.Services.PatientMedicalRecordService;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("user/{professionalUserId}/professional/patient-medical-record")
public class ProfessionalMedicalRecordByPatientController {

    @Autowired
    private PatientMedicalRecordService patientMedicalRecordService;


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Section>> getNewestMedicalRecordStructureByPatient(@PathVariable("professionalUserId") int professionalUserId,
                                                                                  @RequestParam("patientUserId") int patientUserId) {
        return ResponseEntity.ok(patientMedicalRecordService.getNewestPatientMedicalRecord(professionalUserId, patientUserId));
    }
}
