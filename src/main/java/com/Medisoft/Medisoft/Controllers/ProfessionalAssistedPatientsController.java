package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.PatientManagement.PatientAssistedDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.Services.ProfessionalAssistedPatientsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/user/{professionalUserId}/professional", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProfessionalAssistedPatientsController {

    @Autowired
    private ProfessionalAssistedPatientsService professionalAssistedPatientsService;


    @GetMapping(value = "/patients-assisted")
    public ResponseEntity<List<PatientAssistedDTO>> getAssistedPatients(@PathVariable int professionalUserId,
                                                                        @RequestParam(defaultValue = "100") int maxSize) {
        return ResponseEntity.ok(professionalAssistedPatientsService.getPatientsAssistedDTO(professionalUserId, maxSize));
    }
}
