package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.PatientManagement.AtencionDisponible;
import com.Medisoft.Medisoft.Entities.PatientManagement.AtencionesDisponiblesPaciente;
import com.Medisoft.Medisoft.Entities.PatientManagement.AvailableAttentionTypeDTO;
import com.Medisoft.Medisoft.Entities.PatientManagement.PatientAttentionTypeService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO.CategoriaTipoAtencionDTO;
import com.Medisoft.Medisoft.Exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;


@RestController
@RequestMapping("/paciente/{idUsuarioPaciente}")
public class PatientAttentionTypeController {

    @Autowired
    private PatientAttentionTypeService patientAttentionTypeService;


    /**
     * This method is just for populate combo box on Registrar Turno page
     */
    @GetMapping(value = "/categoria-tipo-atencion-disponible", produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public ResponseEntity<Set<CategoriaTipoAtencionDTO>> getCategoriasYTipoAtencionDisponible(
            @PathVariable("idUsuarioPaciente") int idUsuarioPaciente,
            @RequestParam int idUsuarioProfesional) {
        AtencionesDisponiblesPaciente atencionesDisponiblesPaciente =
                patientAttentionTypeService.findAvailableAttentionTypes(idUsuarioPaciente, idUsuarioProfesional);
        return ResponseEntity.ok(patientAttentionTypeService
                .createDtoSet(atencionesDisponiblesPaciente.getAvailableAttentions()));
    }


    /**
     * @param availableAttentionTypeDTOS means all professional attention types, but with true or false attribute
     *                                   wich indicates if it is available or not
     */
    @PostMapping(value = "/tipo-atencion-disponible", produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public ResponseEntity<Void> postAvailableAttentionTypes(
            @PathVariable("idUsuarioPaciente") int idUsuarioPaciente,
            @RequestParam int idUsuarioProfesional,
            @RequestBody List<AvailableAttentionTypeDTO> availableAttentionTypeDTOS) {
        if (CollectionUtils.isEmpty(availableAttentionTypeDTOS)) {
            throw new EntityNotFoundException(AtencionDisponible.class.getSimpleName());
        }
        patientAttentionTypeService.createPatientAvailableAttentionTypes(
                availableAttentionTypeDTOS, idUsuarioProfesional, idUsuarioPaciente);
        return ResponseEntity.ok().build();
    }
}
