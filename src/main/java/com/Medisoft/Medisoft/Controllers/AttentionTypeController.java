package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.Services.ProfessionalAttentionTypeStorageService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping
public class AttentionTypeController {

    @Autowired
    private ProfessionalAttentionTypeStorageService professionalAttentionTypeStorageService;

    @GetMapping(value = "/profesional/tipo-atencion", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TipoAtencionProfesional> getTipoAtencionById(
            @RequestParam("id") int idTipoAtencionProfesional) {
        return ResponseEntity.ok(professionalAttentionTypeStorageService.findById(idTipoAtencionProfesional));
    }
}
