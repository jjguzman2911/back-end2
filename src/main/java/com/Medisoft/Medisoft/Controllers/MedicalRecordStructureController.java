package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordInitService;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordStructure;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordStructureService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.CompleteProfessionalRegistrationService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Exceptions.ProfessionalNotRegisteredException;
import com.Medisoft.Medisoft.Services.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("user/{userId}/professional/medical-record-structure")
public class MedicalRecordStructureController {

    @Autowired
    private MedicalRecordStructureService medicalRecordStructureService;

    @Autowired
    private MedicalRecordInitService medicalRecordInitService;

    @Autowired
    private UserService userService;

    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private CompleteProfessionalRegistrationService completeProfessionalRegistrationService;


    /**
     * when a professional has his own medical record saved, there should be another extra process to check if
     * he needs a new medical record or must get the one he has already saved.
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Section>> getMedicalRecordStructure(@PathVariable("userId") int userId) {
        User user = userService.findById(userId);
        if (user.isProfessional()) {
            Profesional profesional = user.getProfesional();
            if (profesional.hasMedicalRecord()) {
                MedicalRecordStructure medicalRecordStructure = medicalRecordStructureService.findById
                        (profesional.getMedicalRecordId());
                return ResponseEntity.ok(medicalRecordStructure.getSections());
            } else {
                return ResponseEntity.ok(medicalRecordInitService.getFirstMedicalRecordStructure
                        (profesional.getSpecialityId()));
            }
        }
        throw new ProfessionalNotRegisteredException();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public ResponseEntity<Profesional> postMedicalRecordStructure(@PathVariable("userId") int userId,
                                                                  @RequestBody List<Section> sections) {
        User user = userService.findById(userId);
        if (!user.isProfessional()) {
            throw new ProfessionalNotRegisteredException();
        }
        Profesional professional = user.getProfesional();
        MedicalRecordStructure medicalRecordStructure = professional.hasMedicalRecord() ?
                medicalRecordStructureService.findById(professional.getMedicalRecordId()) :
                MedicalRecordStructure.builder().speciality(professional.getEspecialidad())
                        .id(sequenceGeneratorService.generateSequence(MedicalRecordStructure.SEQUENCE_NAME))
                        .build();

        medicalRecordStructure.setSections(sections);
        medicalRecordStructure.setTimestamp(new Date().getTime());

        medicalRecordStructure = medicalRecordStructureService.save(medicalRecordStructure);
        professional.setMedicalRecordId(medicalRecordStructure.getId());
        user.setProfesional(professional);
        userService.save(user);
        completeProfessionalRegistrationService.isEnabledAsProfessional(user);

        return ResponseEntity.ok(professional);
    }
}
