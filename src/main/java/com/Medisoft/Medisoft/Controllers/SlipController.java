package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.SlipManagement.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.Medisoft.Medisoft.Utilities.ApplicationConstantsUtils.APPOINTMENT_SLIP_PATH;
import static com.Medisoft.Medisoft.Utilities.ApplicationConstantsUtils.PAYMENT_SLIP_PATH;


@RestController
@RequestMapping("/comprobante")
public class SlipController {

    @Autowired
    private FileService fileService;


    @GetMapping(value = "/turno", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getAppointmentSlip(@RequestParam("idTurno") long appointmentId,
                                                                   @RequestParam(value = "format", defaultValue = "pdf") String format,
                                                                   @RequestParam(value = "download", defaultValue = "false") boolean download) {
        String fileName = appointmentId + "." + format;
        InputStreamResource fileStreamResource = fileService.getFile(fileName, APPOINTMENT_SLIP_PATH);
        HttpHeaders headers = getHeaders(fileName, download);
        return new ResponseEntity<>(fileStreamResource, headers, HttpStatus.OK);
    }

    @GetMapping(value = "/cobro", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getPaymentSlip(@RequestParam("numeroTransaccion") long transactionId,
                                                                   @RequestParam(value = "format", defaultValue = "pdf") String format,
                                                                   @RequestParam(value = "download", defaultValue = "false") boolean download) {
        String fileName = transactionId + "." + format;
        InputStreamResource fileStreamResource = fileService.getFile(fileName, PAYMENT_SLIP_PATH);
        HttpHeaders headers = getHeaders(fileName, download);
        return new ResponseEntity<>(fileStreamResource, headers, HttpStatus.OK);
    }

    private HttpHeaders getHeaders(String fileName, boolean download){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        if (download){
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        }
        else {
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "filename=" + fileName);
        }
        headers.add(HttpHeaders.PRAGMA, "no-cache");
        headers.add(HttpHeaders.CACHE_CONTROL, "no-cache, no-store, must-revalidate");
        headers.add(HttpHeaders.EXPIRES, "0");
        return headers;
    }
}
