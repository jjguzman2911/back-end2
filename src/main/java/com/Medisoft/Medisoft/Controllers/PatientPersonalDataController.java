package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.PatientPersonalDataDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Services.PatientPersonalDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user/{patientUserId}/personal-data")
public class PatientPersonalDataController {

    @Autowired
    private PatientPersonalDataService patientPersonalDataService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PatientPersonalDataDTO> getPatientPersonalData(
            @PathVariable int patientUserId,
            @RequestParam(value = "attentionId", required = false) Long attentionId,
            @RequestParam(value = "appointmentId", required = false) Long appointmentId) {

        return ResponseEntity.ok(patientPersonalDataService.getPatientPersonalData(patientUserId, attentionId, appointmentId));
    }
}
