package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement.ProfessionalSearchResultDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement.ProfessionalSearchService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/search/professional")
public class ProfessionalSearchController {

    @Autowired
    private ProfessionalSearchService professionalSearchService;


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfessionalSearchResultDTO> searchProfessionals(
            @RequestParam(value = "fullName", required = false) String fullName,
            @RequestParam(value = "specialty", required = false) String specialty,
            @RequestParam(value = "orderBy", defaultValue = "relevance") String orderBy,
            HealthInsuranceDTO healthInsuranceDTO,
            @RequestParam(value = "maxDocs", defaultValue = "10") Integer maxDocs) {

        return StringUtils.isEmpty(specialty) ?
                ResponseEntity.ok(professionalSearchService.searchProfessionalsByAllSpecialties(maxDocs, fullName, orderBy, healthInsuranceDTO)) :
                ResponseEntity.ok(professionalSearchService.searchProfessionals(maxDocs, fullName, specialty, orderBy, healthInsuranceDTO));
    }
}
