package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AppointmentService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/appointment/{appointmentId}/assistance")
public class AppointmentAssistanceController {

    @Autowired
    private AppointmentService appointmentService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Turno> startAssistance(@PathVariable("appointmentId") long appointmentId) {
        return ResponseEntity.ok(appointmentService.startAssitance(appointmentId));
    }
}
