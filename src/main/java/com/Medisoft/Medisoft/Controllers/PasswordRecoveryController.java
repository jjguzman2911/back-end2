package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.SessionManagement.VerificationToken;
import com.Medisoft.Medisoft.Entities.UserManagement.DTO.PasswordRecoveryDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.EventManagement.OnPasswordRecoveryEvent;
import com.Medisoft.Medisoft.Entities.UserManagement.TokenService;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Entities.UserManagement.UserState;
import com.Medisoft.Medisoft.Exceptions.InvalidDataException;
import com.Medisoft.Medisoft.Exceptions.InvalidUserStateActionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequestMapping("/user")
public class PasswordRecoveryController {

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private TokenService tokenService;


    @PostMapping(value = "/password-recovery")
    public ResponseEntity<Void> recoverPassword(@RequestParam String userEmail) {
        User user = userService.findByEmail(userEmail);
        user.updateState(UserState.NO_PASSWORD);
        eventPublisher.publishEvent(new OnPasswordRecoveryEvent(user));

        userService.save(user);
        log.info("User {} in password recovery state", user.getId());
        return ResponseEntity.ok().build();
    }


    /**
     * Permite verificar el token de usuario para recuperar la contraseña.
     *
     * @param token representa el token.
     * @return devuelve un código HTTP:
     * - 200: token encontrado. Devuelve un Usuario.
     * - 404: token no encontrado.
     */
    @GetMapping(value = "/token-verification", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> verifyToken(@RequestParam("token") String token) {
        VerificationToken verificationToken = tokenService.findByToken(token);
        log.info("Token {} is valid", token);
        return ResponseEntity.ok(verificationToken.getUser().getId());
    }


    @PutMapping(value = "/password-update", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updatePassword(@RequestBody PasswordRecoveryDTO passwordRecoveryDTO) {
        String validate = passwordRecoveryDTO.validatePassword();
        if (StringUtils.isEmpty(validate)) { //Datos correctos
            User user = userService.findById(passwordRecoveryDTO.getUserId());

            if (user.isNoPassword()) {
                user.setContrasenia(passwordRecoveryDTO.getPassword());
                user.updateState(UserState.VERIFIED);

                log.info("Password recovered");
                return ResponseEntity.ok(userService.save(user));
            } else {
                throw new InvalidUserStateActionException(user.getActualState().getName());
            }
        } else { //Datos incorrectos
            throw new InvalidDataException(validate);
        }
    }
}
