package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.AttentionManagement.PatientMedicalRecord;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Services.PatientMedicalRecordService;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/patients-medical-record")
public class PatientMedicalRecordController {

    @Autowired
    private PatientMedicalRecordService patientMedicalRecordService;


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PatientMedicalRecord> postPatientMedicalRecord(@RequestBody PatientMedicalRecord patientMedicalRecord) {
        return ResponseEntity.ok(patientMedicalRecordService.postPatientMedicalRecord(patientMedicalRecord));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Section>> getPatientMedicalRecord(@RequestParam("medicalRecordId") long medicalRecordId,
                                                                 @RequestParam(value = "isPatient", defaultValue = "false") Boolean isPatient) {
        return ResponseEntity.ok(patientMedicalRecordService.getPatientMedicalRecord(medicalRecordId, isPatient));
    }
}
