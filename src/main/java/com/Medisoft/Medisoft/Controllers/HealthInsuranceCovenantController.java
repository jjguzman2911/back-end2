package com.Medisoft.Medisoft.Controllers;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceCovenantDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceCovenantService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalProfileDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(value = "/user/{professionalUserId}/professional/health-insurance-covenant", produces = MediaType.APPLICATION_JSON_VALUE)
public class HealthInsuranceCovenantController {

    @Autowired
    private HealthInsuranceCovenantService healthInsuranceCovenantService;


    @GetMapping
    public ResponseEntity<Set<HealthInsuranceCovenantDTO>> getProfessionalHealthInsuranceCovenants(@PathVariable int professionalUserId){
        return ResponseEntity.ok(healthInsuranceCovenantService.getProfessionalHealthInsuranceCovenants(professionalUserId));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProfessionalProfileDTO> postProfessionalHealthInsuranceCovenants(
            @PathVariable int professionalUserId, @RequestBody Set<HealthInsuranceDTO> healthInsuranceDTOS) {
        User professionalUser = healthInsuranceCovenantService.postProfessionalHealthInsuranceCovenants(
                professionalUserId, healthInsuranceDTOS);
        return ResponseEntity.ok(new ProfessionalProfileDTO(professionalUser));
    }
}
