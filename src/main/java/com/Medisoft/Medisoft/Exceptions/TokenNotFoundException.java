package com.Medisoft.Medisoft.Exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TokenNotFoundException extends RuntimeException {
    public TokenNotFoundException(String token) {
        super(token);
    }
}
