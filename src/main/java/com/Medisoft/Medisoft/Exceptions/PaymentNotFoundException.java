package com.Medisoft.Medisoft.Exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PaymentNotFoundException extends RuntimeException {
    public PaymentNotFoundException(String message) {
        super(message);
    }

}
