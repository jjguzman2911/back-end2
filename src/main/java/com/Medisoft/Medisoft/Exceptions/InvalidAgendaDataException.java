package com.Medisoft.Medisoft.Exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidAgendaDataException extends RuntimeException {

    public InvalidAgendaDataException(String message) {
        super(message);
    }

}
