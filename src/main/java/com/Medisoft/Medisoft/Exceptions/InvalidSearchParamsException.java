package com.Medisoft.Medisoft.Exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidSearchParamsException extends RuntimeException {
    public InvalidSearchParamsException(String message) {
        super(message);
    }

}
