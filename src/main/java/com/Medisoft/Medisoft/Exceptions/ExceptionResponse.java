package com.Medisoft.Medisoft.Exceptions;

import com.Medisoft.Medisoft.Utilities.DateFormatConverter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;

@Getter
@Setter
@NoArgsConstructor
public class ExceptionResponse {

    private String date;
    private String entity;
    private String message;
    private String resource;

    public ExceptionResponse(String message, String resource) {
        this.date = DateFormatConverter.calendarToString(Calendar.getInstance());
        this.message = message;
        this.resource = resource;
    }
}
