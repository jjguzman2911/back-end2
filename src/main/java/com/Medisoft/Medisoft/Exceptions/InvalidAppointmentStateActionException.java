package com.Medisoft.Medisoft.Exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidAppointmentStateActionException extends RuntimeException {

    public InvalidAppointmentStateActionException(String message) {
        super(message);
    }

}
