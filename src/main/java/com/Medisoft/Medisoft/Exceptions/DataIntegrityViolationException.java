package com.Medisoft.Medisoft.Exceptions;

public class DataIntegrityViolationException extends RuntimeException {
    private String className;

    public DataIntegrityViolationException() {
    }

    public DataIntegrityViolationException(String className) {
        this.className = className;
    }

    public DataIntegrityViolationException(String message, String className) {
        super(message);
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
