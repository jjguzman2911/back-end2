package com.Medisoft.Medisoft.Exceptions;


import com.Medisoft.Medisoft.Utilities.ApplicationConstantsUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = UserNotFoundException.class)
    protected ResponseEntity<Object> userNotFound(RuntimeException ex, WebRequest request) {
        String message = "Usuario " + ex.getMessage() + " no encontrado.";
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = LoginUserNotFoundException.class)
    protected ResponseEntity<Object> loginUserNotFound(RuntimeException ex, WebRequest request) {
        String message = "El usuario y/o contraseña son incorrectos";
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(UserDataIntegrityViolationException.class)
    protected ResponseEntity<Object> duplicatedDataUser(UserDataIntegrityViolationException ex, WebRequest request) {
        String message = "Ya existe un usuario registrado con el " + ex.getField() + " " + ex.getValue() +
                ". Por favor intente con un " + ex.getField() + " diferente";
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(value = ProfessionalNotRegisteredException.class)
    protected ResponseEntity<Object> professionalNotRegistered(RuntimeException ex, WebRequest request) {
        String message = "Lo sentimos, el profesional no está validado.";
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(UnauthorizedUserException.class)
    protected ResponseEntity<Object> unauthorizedUser(RuntimeException ex, WebRequest request) {
        String message = "El usuario no tiene permisos para acceder";
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler(TokenNotFoundException.class)
    protected ResponseEntity<Object> tokenNotFound(RuntimeException ex, WebRequest request) {
        String message = "El token " + ex.getMessage() + " es inválido";
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(InvalidUserStateActionException.class)
    protected ResponseEntity<Object> invalidUserStateActionPerformed(RuntimeException ex, WebRequest request) {
        String message = "El usuario está en estado " + ex.getMessage() +
                " y no tiene permitido realizar la acción solicitada";
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(InvalidDataException.class)
    protected ResponseEntity<Object> invalidData(RuntimeException ex, WebRequest request) {
        String message = "Los datos ingresados son incorrectos, por favor revisar " + ex.getMessage();
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(value = DataIntegrityViolationException.class)
    protected ResponseEntity<Object> dataIntegrityViolation(RuntimeException ex, WebRequest request) {
        String message = "Error al guardar los datos de " + ex.getMessage();
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    protected ResponseEntity<Object> entityNotFound(RuntimeException ex, WebRequest request) {
        String message = ex.getMessage();
        ExceptionResponse responseBody = new ExceptionResponse(
                message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = AppointmentNotFoundException.class)
    protected ResponseEntity<Object> appointmentNotFound(RuntimeException ex, WebRequest request) {
        String message = "Turno con id " + ex.getMessage() + " no encontrado.";
        ExceptionResponse responseBody = new ExceptionResponse(
                message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(InvalidAppointmentStateActionException.class)
    protected ResponseEntity<Object> invalidAppointmentStateAction(RuntimeException ex, WebRequest request) {
        String message = "No es posible realizar la acción solicitada para este turno";
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(IOException.class)
    protected ResponseEntity<Object> ioException(RuntimeException ex, WebRequest request) {
        String message = "Se produjo un error en nuestros servidores. Por favor intente nuevamente mas tarde";
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        log.warn(ex.getMessage());
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(InvalidSearchParamsException.class)
    protected ResponseEntity<Object> invalidSearchParams(RuntimeException ex, WebRequest request) {
        String message = "Para efectuar una búsqueda debe ingresar el nombre de un profesional o seleccionar una especialidad";
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(InvalidAgendaDataException.class)
    protected ResponseEntity<Object> invalidAgendaData(RuntimeException ex, WebRequest request) {
        String message = "Por favor revisa los horarios seleccionados. Recuerda que los mismos deben ser múltiplos de "
                + ApplicationConstantsUtils.getDurationInMinutesPretty();
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(PaymentNotFoundException.class)
    protected ResponseEntity<Object> paymentNotFound(RuntimeException ex, WebRequest request) {
        String message = "Lo sentimos, no pudimos encontrar el pago indicado";
        ExceptionResponse responseBody = new ExceptionResponse(message,
                ((ServletWebRequest) request).getRequest().getRequestURI());
        log.warn(message);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}
