package com.Medisoft.Medisoft.Exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidUserStateActionException extends RuntimeException {
    public InvalidUserStateActionException(String message) {
        super(message);
    }
}
