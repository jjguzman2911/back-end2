package com.Medisoft.Medisoft.Exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class UserDataIntegrityViolationException extends RuntimeException {
    private String field;
    private String value;
}
