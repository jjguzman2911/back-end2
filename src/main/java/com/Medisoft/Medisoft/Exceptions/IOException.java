package com.Medisoft.Medisoft.Exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class IOException extends RuntimeException {
    public IOException(String message) {
        super(message);
    }
}
