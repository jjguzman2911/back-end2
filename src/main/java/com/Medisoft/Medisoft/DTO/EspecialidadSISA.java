package com.Medisoft.Medisoft.DTO;

import java.util.Date;

public class EspecialidadSISA {
    private String especialidad;
    private Date fechaCertificacion;
    private int nroCertificacion;
    private String tipoCertificacion;
    private String ministerio;

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public Date getFechaCertificacion() {
        return fechaCertificacion;
    }

    public void setFechaCertificacion(Date fechaCertificacion) {
        this.fechaCertificacion = fechaCertificacion;
    }

    public int getNroCertificacion() {
        return nroCertificacion;
    }

    public void setNroCertificacion(int nroCertificacion) {
        this.nroCertificacion = nroCertificacion;
    }

    public String getTipoCertificacion() {
        return tipoCertificacion;
    }

    public void setTipoCertificacion(String tipoCertificacion) {
        this.tipoCertificacion = tipoCertificacion;
    }

    public String getMinisterio() {
        return ministerio;
    }

    public void setMinisterio(String ministerio) {
        this.ministerio = ministerio;
    }
}
