package com.Medisoft.Medisoft.DTO;

import java.util.Date;
import java.util.Set;

public class MatriculaSISA {

    private int matricula;
    private String profesion;
    private String jurisdiccion;
    private String estado;
    private Date fechaMatricula;
    private Date fechaRegistro;
    private Set<EspecialidadSISA> especialidades;

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getJurisdiccion() {
        return jurisdiccion;
    }

    public void setJurisdiccion(String jurisdiccion) {
        this.jurisdiccion = jurisdiccion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaMatricula() {
        return fechaMatricula;
    }

    public void setFechaMatricula(Date fechaMatricula) {
        this.fechaMatricula = fechaMatricula;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Set<EspecialidadSISA> getEspecialidades() {
        return especialidades;
    }

    public void setEspecialidades(Set<EspecialidadSISA> especialidades) {
        this.especialidades = especialidades;
    }
}
