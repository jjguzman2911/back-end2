package com.Medisoft.Medisoft.DTO;

import java.util.Date;
import java.util.Set;

public class ProfesionalSISA {

    private String apellido;
    private String nombre;
    private String tipoDocumento;
    private int numeroDocumento;
    private int codigo;
    private Set<MatriculaSISA> matriculas;
    private Date fechaRegistro;
    private Date fechaModificacion;

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public int getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(int numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Set<MatriculaSISA> getMatriculas() {
        return matriculas;
    }

    public void setMatriculas(Set<MatriculaSISA> matriculas) {
        this.matriculas = matriculas;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
}
