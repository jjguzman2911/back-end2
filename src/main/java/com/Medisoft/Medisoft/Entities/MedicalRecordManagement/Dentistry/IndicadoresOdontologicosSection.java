package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Dentistry;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;

import java.util.Arrays;

public class IndicadoresOdontologicosSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Indicadores odontológicos")
            .componentType(ComponentType.SECTION_PAGE).order(3).iconId(2000).build();

    @Override
    public Section init() {
        section.addComponent(antecedentesOdontologicos1());
        section.addComponent(antecedentesOdontologicos2());
        section.addComponent(antecedentesOdontologicos3());
        return section;
    }

    private Section antecedentesOdontologicos1() {
        Component medicacionActual = ListBox.builder().name("Medicación actual").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component detalleDeMedicacion = InputBox.builder().name("Detalle de medicación").componentType(ComponentType.TEXT_BOX)
                .build();
        Component dificultades = ListBox.builder().name("Dificultades").componentType(ComponentType.CHECK_BOX)
                .options(Arrays.asList("No posee", "Hablar", "Masticar", "Abrir la boca", "Tragar alimentos")).build();
        Component lesiones = ListBox.builder().name("Lesiones").componentType(ComponentType.CHECK_BOX)
                .options(Arrays.asList("No posee", "Manchas", "Abultamiento de tejido", "Ampollas", "Sangrado de encías",
                        "Ulceraciones")).build();
        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesOdontologicos1 = Section.builder().name("Antecedentes odontológicos 1")
                .componentType(ComponentType.SECTION_CARD).order(1).size(-1).build();
        antecedentesOdontologicos1.addComponent(medicacionActual);
        antecedentesOdontologicos1.addComponent(detalleDeMedicacion);
        antecedentesOdontologicos1.addComponent(dificultades);
        antecedentesOdontologicos1.addComponent(lesiones);
        antecedentesOdontologicos1.addComponent(observaciones);
        return antecedentesOdontologicos1;
    }

    private Section antecedentesOdontologicos2(){
        Component dolores = ListBox.builder().name("Dolores").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component tipoDeDolor = ListBox.builder().name("Tipo de dolor").componentType(ComponentType.CHECK_BOX)
                .options(Arrays.asList("No posee", "Suave", "Moderado", "Intenso", "Continuo", "Intermitente",
                        "Temporario", "Localizado", "Irradiado")).build();
        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();
        Component golpeEnDiente = ListBox.builder().name("Golpe en diente").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component comoYCuandoSeProdujo = InputBox.builder().name("Cómo y cuándo se produjo")
                .componentType(ComponentType.TEXT_BOX).build();
        Component fracturaDeDiente = ListBox.builder().name("Fractura de diente").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component observacionDeFractura = InputBox.builder().name("Observación de fractura (ubicación, fecha y tratamiento)")
                .componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesOdontologicos2 = Section.builder().name("Antecedentes odontológicos 2")
                .componentType(ComponentType.SECTION_CARD).order(2).size(1).build();
        antecedentesOdontologicos2.addComponent(dolores);
        antecedentesOdontologicos2.addComponent(tipoDeDolor);
        antecedentesOdontologicos2.addComponent(observaciones);
        antecedentesOdontologicos2.addComponent(golpeEnDiente);
        antecedentesOdontologicos2.addComponent(comoYCuandoSeProdujo);
        antecedentesOdontologicos2.addComponent(fracturaDeDiente);
        antecedentesOdontologicos2.addComponent(observacionDeFractura);

        return antecedentesOdontologicos2;
    }

    private Section antecedentesOdontologicos3(){
        Component movilidadEnDiente = ListBox.builder().name("Movilidad en diente").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component cuales = InputBox.builder().name("Cuáles").componentType(ComponentType.TEXT_BOX)
                .build();
        Component hinchazonEnCara = ListBox.builder().name("Hinchazón en cara").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component tratamientoDeHinchazon = ListBox.builder().name("Tratamiento de hinchazón").componentType(ComponentType.CHECK_BOX)
                .options(Arrays.asList("Ninguno", "Hielo", "Calor", "Otros")).build();
        Component supuracionBucal = ListBox.builder().name("Supuración bucal").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component ubicacionDeSupuracion = InputBox.builder().name("Ubicación de supuración")
                .componentType(ComponentType.TEXT_BOX).build();
        Component higieneBucal = ListBox.builder().name("Higiene bucal").componentType(ComponentType.CHECK_BOX)
                .options(Arrays.asList("Muy buena", "Buena", "Deficiente", "Mala")).build();

        Section antecedentesOdontologicos3 = Section.builder().name("Antecedentes odontológicos 3")
                .componentType(ComponentType.SECTION_CARD).order(3).size(-1).build();
        antecedentesOdontologicos3.addComponent(movilidadEnDiente);
        antecedentesOdontologicos3.addComponent(cuales);
        antecedentesOdontologicos3.addComponent(hinchazonEnCara);
        antecedentesOdontologicos3.addComponent(tratamientoDeHinchazon);
        antecedentesOdontologicos3.addComponent(supuracionBucal);
        antecedentesOdontologicos3.addComponent(ubicacionDeSupuracion);
        antecedentesOdontologicos3.addComponent(higieneBucal);
        return antecedentesOdontologicos3;
    }

}
