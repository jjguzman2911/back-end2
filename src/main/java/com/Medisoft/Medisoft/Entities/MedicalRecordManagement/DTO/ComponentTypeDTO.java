package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.DTO;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.ComponentType;
import lombok.Data;

@Data
public class ComponentTypeDTO {
    private final String name;
    private final int id;

    public ComponentTypeDTO(ComponentType componentType) {
        this.name = componentType.getName();
        this.id = componentType.getId();
    }
}
