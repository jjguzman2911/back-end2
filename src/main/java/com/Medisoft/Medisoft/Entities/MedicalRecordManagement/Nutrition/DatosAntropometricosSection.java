package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Nutrition;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class DatosAntropometricosSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Datos antropométricos").componentType(ComponentType.SECTION_PAGE)
            .order(4).iconId(3001).build();

    @Override
    public Section init() {
        section.addComponent(medidasTallaPeso());
        section.addComponent(perimetros());
        section.addComponent(plicometria());
        section.addComponent(composicionCorporal());
        section.addComponent(historialDePeso());
        section.addComponent(indicadoresBioquimicosYPresionArterial());
        return section;
    }

    private Section medidasTallaPeso() {
        Component pesoActual = InputBox.builder().name("Peso actual (kg)").componentType(ComponentType.INPUT_BOX)
                .order(1).build();
        Component estatura = InputBox.builder().name("Estatura (cm)").componentType(ComponentType.INPUT_BOX)
                .order(2).build();
        Component imc = InputBox.builder().name("Índice de masa corporal (IMC)").componentType(ComponentType.INPUT_BOX)
                .order(3).build();
        Component valoracionImc = InputBox.builder().name("Valoración IMC").componentType(ComponentType.TEXT_BOX)
                .order(4).build();

        Section medidaTallaPeso = Section.builder().name("Medida de talla, peso e IMC")
                .componentType(ComponentType.SECTION_CARD).size(-1).order(1).build();
        medidaTallaPeso.addComponent(pesoActual);
        medidaTallaPeso.addComponent(estatura);
        medidaTallaPeso.addComponent(imc);
        medidaTallaPeso.addComponent(valoracionImc);
        return medidaTallaPeso;
    }

    private Section perimetros() {
        Component circunferenciaCintura = InputBox.builder().name("Circunferencia de cintura (cm)")
                .componentType(ComponentType.INPUT_BOX).order(1).build();
        Component circunferenciaCadera = InputBox.builder().name("Circunferencia de cadera (cm)")
                .componentType(ComponentType.INPUT_BOX).order(2).build();
        Component circunferenciaMuneca = InputBox.builder().name("Circunferencia de muñeca (cm)")
                .componentType(ComponentType.INPUT_BOX).order(3).build();
        Component circunferenciaBrazo = InputBox.builder().name("Circunferencia de brazo (cm)")
                .componentType(ComponentType.INPUT_BOX).order(4).build();
        Component circunferenciaAbdomen = InputBox.builder().name("Circunferencia de abdomen (cm)")
                .componentType(ComponentType.INPUT_BOX).order(5).build();

        Section medidaDePerimetros = Section.builder().name("Medida de perímetros")
                .componentType(ComponentType.SECTION_CARD).size(1).order(2).build();
        medidaDePerimetros.addComponent(circunferenciaCintura);
        medidaDePerimetros.addComponent(circunferenciaCadera);
        medidaDePerimetros.addComponent(circunferenciaMuneca);
        medidaDePerimetros.addComponent(circunferenciaBrazo);
        medidaDePerimetros.addComponent(circunferenciaAbdomen);

        return medidaDePerimetros;
    }

    private Section plicometria() {
        Component pliegueCutaneoTricipital = InputBox.builder().name("Pliegue cutáneo tricipital (mm)")
                .componentType(ComponentType.INPUT_BOX).order(1).build();
        Component pliegueCutaneoBicipital = InputBox.builder().name("Pliegue cutáneo bicipital (mm)")
                .componentType(ComponentType.INPUT_BOX).order(2).build();
        Component pliegueCutaneoSubescapular = InputBox.builder().name("Pliegue cutáneo subescapular (mm)")
                .componentType(ComponentType.INPUT_BOX).order(3).build();
        Component pliegueCutaneoSuprailiaco = InputBox.builder().name("Pliegue cutáneo suprailiaco (mm)")
                .componentType(ComponentType.INPUT_BOX).order(4).build();

        Section plicometria = Section.builder().name("Plicometría")
                .componentType(ComponentType.SECTION_CARD).size(-1).order(3).build();
        plicometria.addComponent(pliegueCutaneoTricipital);
        plicometria.addComponent(pliegueCutaneoBicipital);
        plicometria.addComponent(pliegueCutaneoSubescapular);
        plicometria.addComponent(pliegueCutaneoSuprailiaco);

        return plicometria;
    }

    private Section composicionCorporal() {
        Component masaGrasa = InputBox.builder().name("Masa grasa (kg)")
                .componentType(ComponentType.INPUT_BOX).order(1).build();
        Component masaLibreDeGrasa = InputBox.builder().name("Masa libre de grasa (kg)")
                .componentType(ComponentType.INPUT_BOX).order(2).build();
        Component porcentajeMasaGrasa = InputBox.builder().name("Porcentaje de masa grasa (%)")
                .componentType(ComponentType.INPUT_BOX).order(3).build();
        Component porcentajeMasaLibreGrasa = InputBox.builder().name("Porcentaje de masa libre de grasa (%)")
                .componentType(ComponentType.INPUT_BOX).order(4).build();
        Component porcentajeAguaCorporal = InputBox.builder().name("Porcentaje de agua corporal (%)")
                .componentType(ComponentType.INPUT_BOX).order(5).build();

        Section composicionCorporal = Section.builder().name("Composición corporal")
                .componentType(ComponentType.SECTION_CARD).size(1).order(4).build();
        composicionCorporal.addComponent(masaGrasa);
        composicionCorporal.addComponent(masaLibreDeGrasa);
        composicionCorporal.addComponent(porcentajeMasaGrasa);
        composicionCorporal.addComponent(porcentajeMasaLibreGrasa);
        composicionCorporal.addComponent(porcentajeAguaCorporal);

        return composicionCorporal;
    }

    private Section historialDePeso() {
        Component pesoHabitual = InputBox.builder().name("Peso habitual (kg)")
                .componentType(ComponentType.INPUT_BOX).order(1).build();
        Component pesoMinimo = InputBox.builder().name("Peso mínimo (kg)")
                .componentType(ComponentType.INPUT_BOX).order(2).build();
        Component pesoDeseado = InputBox.builder().name("Peso deseado (kg)")
                .componentType(ComponentType.INPUT_BOX).order(3).build();
        Component pesoMaximo = InputBox.builder().name("Peso máximo (kg)")
                .componentType(ComponentType.INPUT_BOX).order(4).build();
        Component frecuenciaPesaje = InputBox.builder().name("Frecuencia de pesaje (veces por mes)")
                .componentType(ComponentType.INPUT_BOX).order(5).build();

        Section historialDePeso = Section.builder().name("Historial de peso")
                .componentType(ComponentType.SECTION_CARD).size(1).order(5).build();
        historialDePeso.addComponent(pesoHabitual);
        historialDePeso.addComponent(pesoMinimo);
        historialDePeso.addComponent(pesoDeseado);
        historialDePeso.addComponent(pesoMaximo);
        historialDePeso.addComponent(frecuenciaPesaje);

        return historialDePeso;
    }

    private Section indicadoresBioquimicosYPresionArterial() {
        List<Header> headers = new ArrayList<>();
        headers.add(new Header("Medida", false));
        headers.add(new Header("Valor", true));
        headers.add(new Header("Interpretación", true));

        List<List<String>> elements = new ArrayList<>();
        elements.add(Arrays.asList("Glucosa", "", ""));
        elements.add(Arrays.asList("Colesterol total", "", ""));
        elements.add(Arrays.asList("Colesterol HDL", "", ""));
        elements.add(Arrays.asList("Colesterol LDL", "", ""));
        elements.add(Arrays.asList("Triglicéridos", "", ""));
        elements.add(Arrays.asList("Hemoglobina", "", ""));
        elements.add(Arrays.asList("Hematocrocito", "", ""));
        elements.add(Arrays.asList("Presión arterial", "", ""));

        Table table = Table.builder().name("Indicadores bioquímicos y presión arterial").componentType(ComponentType.FIXED_TABLE)
                .headers(headers).elements(elements).build();

        Section indicadoresBioquimicosYPresionArterial = Section.builder().name("Indicadores bioquímicos y presión arterial")
                .componentType(ComponentType.SECTION_CARD).size(0).order(6).build();
        indicadoresBioquimicosYPresionArterial.addComponent(table);

        return indicadoresBioquimicosYPresionArterial;
    }
}
