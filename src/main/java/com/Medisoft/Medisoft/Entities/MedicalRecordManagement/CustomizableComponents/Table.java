package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
@NoArgsConstructor
@JsonDeserialize
public class Table extends Component {

    private List<Header> headers;
    private List<List<String>> elements;

    @Builder
    public Table(String name, ComponentType componentType, int order, List<Header> headers, List<List<String>> elements) {
        super(name, componentType, order);
        if (!(componentType.equals(ComponentType.FIXED_TABLE) ||
                componentType.equals(ComponentType.EDITABLE_TABLE))) {
            throw new IllegalArgumentException("component type must be compatible with Table");
        }
        this.headers = headers;
        this.elements = elements;
        if (componentType.equals(ComponentType.FIXED_TABLE)){
            checkTableIntegrity();
        }
    }

    private void checkTableIntegrity() {
        if (headers != null && elements != null) {
            int columnsLength = headers.size();
            if (elements.stream().anyMatch(element -> element.size() != columnsLength)) {
                throw new IllegalArgumentException("headers and elements must have equal length");
            }
        }
    }
}
