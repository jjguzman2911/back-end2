package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Quadrant {
    private Tooth[] teeth = new Tooth[8];
    private int id; //position in odontogram's array

    public void fulfillTeeth(int id) {
        this.id = id;
        for (int i = 0; i < teeth.length; i++) {
            if (teeth[i] == null) {
                teeth[i] = new Tooth();
            }
            teeth[i].setId(i + 1);
        }
    }
}
