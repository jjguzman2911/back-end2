package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import java.util.List;

/**
 * List box includes Combo Box, Radio Button y Check Box. Those three ones consist basically on options lists.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
@NoArgsConstructor
@JsonDeserialize
public class ListBox extends Component {

    private List<String> options;

    private List<String> selected;

    @Builder
    public ListBox(String name, ComponentType componentType, int order, List<String> options, List<String> selected) {
        super(name, componentType, order);
        if (!(componentType.equals(ComponentType.COMBO_BOX) ||
                componentType.equals(ComponentType.CHECK_BOX) ||
                componentType.equals(ComponentType.RADIO_BUTTON))) {
            throw new IllegalArgumentException("component type must be compatible with ListBox");
        }
        this.options = options;
        this.selected = selected;
    }
}
