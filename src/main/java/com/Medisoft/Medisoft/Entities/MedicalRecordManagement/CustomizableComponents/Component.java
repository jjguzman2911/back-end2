package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

@Data
@NoArgsConstructor
@JsonDeserialize(using = CustomComponentDeserializer.class)
@Document(collection = "components")
public abstract class Component implements Comparable<Component> {

    String name;
    ComponentType componentType;
    int order;


    public Component(String name, ComponentType componentType, int order) {
        if (StringUtils.isEmpty(name)) {
            throw new IllegalArgumentException("name can't be empty/null");
        }
        if (componentType == null) {
            throw new IllegalArgumentException("component type can't be null");
        }
        this.name = name;
        this.componentType = componentType;
        this.order = order;
    }

    @Override
    public int compareTo(Component o) {
        return Integer.compare(order, o.order);
    }
}
