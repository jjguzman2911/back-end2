package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.ClinicalMedicine;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.ComponentType;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.InputBox;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;

public class RevisionesPorSistemasSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Revisiones por sistemas")
            .componentType(ComponentType.SECTION_PAGE).order(6).iconId(1004).build();

    @Override
    public Section init() {
        section.addComponent(sistemasGenerales());
        section.addComponent(sistemaRespiratorio());
        section.addComponent(sistemaCardiovascular());
        section.addComponent(sistemaDigestivo());
        section.addComponent(sistemaGenitourinario());
        section.addComponent(sistemaNeurologico());

        return section;
    }

    private Section sistemasGenerales(){
        return createSistema("Sistemas generales", 1, -1);
    }

    private Section sistemaRespiratorio(){
        return createSistema("Sistema respiratorio", 2, 1);
    }

    private Section sistemaCardiovascular(){
        return createSistema("Sistema cardiovascular", 3, -1);
    }

    private Section sistemaDigestivo(){
        return createSistema("Sistema genitourinario", 4, 1);
    }

    private Section sistemaGenitourinario(){
        return createSistema("Sistema genitourinario", 5, -1);
    }

    private Section sistemaNeurologico(){
        return createSistema("Sistema neurológico", 6, 1);
    }


    private Section createSistema(String name, int order, int size){
        Section sistema =  Section.builder().name(name).componentType(ComponentType.SECTION_CARD)
                .order(order).size(size).build();
        sistema.addComponent(InputBox.builder().name(name).componentType(ComponentType.TEXT_BOX).build());
        return sistema;
    }
}
