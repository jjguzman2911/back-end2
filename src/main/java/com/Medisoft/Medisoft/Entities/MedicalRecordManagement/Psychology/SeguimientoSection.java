package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Psychology;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;

public class SeguimientoSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Seguimiento").componentType(ComponentType.SECTION_PAGE)
            .order(5).iconId(4002).build();

    @Override
    public Section init() {
        section.addComponent(seguimiento());
        return section;
    }

    private Section seguimiento() {
        Component comentarios = InputBox.builder().name("Comentarios").componentType(ComponentType.TEXT_BOX).build();
        Component file = File.builder().name("Archivo").componentType(ComponentType.FILE).build();

        Section seguimiento = Section.builder().name("Seguimiento").componentType(ComponentType.SECTION_CARD)
                .size(0).order(1).build();
        seguimiento.addComponent(comentarios);
        seguimiento.addComponent(file);
        return seguimiento;
    }

}
