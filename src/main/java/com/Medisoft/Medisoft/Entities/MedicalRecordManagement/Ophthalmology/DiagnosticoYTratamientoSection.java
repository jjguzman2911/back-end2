package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Ophthalmology;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.ComponentType;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.InputBox;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;

public class DiagnosticoYTratamientoSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Diagnóstico y tratamiento")
            .componentType(ComponentType.SECTION_PAGE).order(4).iconId(1002).build();

    @Override
    public Section init() {
        section.addComponent(diagnosticoYTratamiento());
        return section;
    }

    private Section diagnosticoYTratamiento() {
        Section diagnosticoYTratamiento = Section.builder().name("Diagnóstico y tratamiento").componentType(ComponentType.SECTION_CARD)
                .order(1).size(0).build();
        diagnosticoYTratamiento.addComponent(InputBox.builder().name("Diagnóstico").componentType(ComponentType.TEXT_BOX).build());
        diagnosticoYTratamiento.addComponent(InputBox.builder().name("Tratamiento").componentType(ComponentType.TEXT_BOX).build());
        return diagnosticoYTratamiento;
    }
}
