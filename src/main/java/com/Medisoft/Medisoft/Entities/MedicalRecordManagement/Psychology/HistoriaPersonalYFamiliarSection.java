package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Psychology;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public class HistoriaPersonalYFamiliarSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Historia personal y familiar").componentType(ComponentType.SECTION_PAGE)
            .order(2).iconId(4000).build();

    @Override
    public Section init() {
        section.addComponent(habitosPersonales());
        section.addComponent(estiloDeVida());
        section.addComponent(estructuraFamiliar());
        section.addComponent(genograma());
        return section;
    }

    private Section habitosPersonales() {
        Section habitosAlimenticios = Section.builder().name("Hábitos alimenticios").componentType(ComponentType.SECTION_SEPARATOR)
                .build();
        Component alimentosFavoritos = InputBox.builder().name("Alimentos favoritos").componentType(ComponentType.TEXT_BOX).build();
        Component alimentosRechazados = InputBox.builder().name("Alimentos rechazados").componentType(ComponentType.TEXT_BOX).build();
        habitosAlimenticios.addComponent(alimentosFavoritos);
        habitosAlimenticios.addComponent(alimentosRechazados);

        Section habitosDeSueno = Section.builder().name("Hábitos de sueño").componentType(ComponentType.SECTION_SEPARATOR)
                .build();
        Component horaHabitualDeLevantarse = InputBox.builder().name("Hora habitual de levantarse").componentType(ComponentType.INPUT_BOX).build();
        Component horaHabitualDeAcostarse = InputBox.builder().name("Hora habitual de acostarse").componentType(ComponentType.INPUT_BOX).build();
        habitosDeSueno.addComponent(horaHabitualDeLevantarse);
        habitosDeSueno.addComponent(horaHabitualDeAcostarse);

        Section habitosPersonales = Section.builder().name("Hábitos personales").componentType(ComponentType.SECTION_CARD)
                .size(-1).order(1).build();
        habitosPersonales.addComponent(habitosAlimenticios);
        habitosPersonales.addComponent(habitosDeSueno);
        return habitosPersonales;
    }

    private Section estiloDeVida() {
        Component realiza = ListBox.builder().name("Realiza").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component tipo = InputBox.builder().name("Tipo").componentType(ComponentType.TEXT_BOX).build();
        Component frecuencia = ListBox.builder().name("Frecuencia").componentType(ComponentType.COMBO_BOX)
                .options(MedicalRecordCommonUtils.frecuenciaOptions()).build();
        Component duracionTotal = InputBox.builder().name("Duración total").componentType(ComponentType.INPUT_BOX).build();

        Section actividadFisica = Section.builder().name("Actividad física").componentType(ComponentType.SECTION_SEPARATOR)
                .build();
        actividadFisica.addComponent(realiza);
        actividadFisica.addComponent(tipo);
        actividadFisica.addComponent(frecuencia);
        actividadFisica.addComponent(duracionTotal);

        Section actividadRecreativa = Section.builder().name("Actividad recreativa").componentType(ComponentType.SECTION_SEPARATOR)
                .build();
        actividadRecreativa.addComponent(realiza);
        actividadRecreativa.addComponent(tipo);
        actividadRecreativa.addComponent(frecuencia);
        actividadRecreativa.addComponent(duracionTotal);

        Section estiloDeVida = Section.builder().name("Estilo de vida").componentType(ComponentType.SECTION_CARD)
                .size(1).order(2).build();
        estiloDeVida.addComponent(actividadFisica);
        estiloDeVida.addComponent(actividadRecreativa);
        return estiloDeVida;
    }

    private Section estructuraFamiliar() {
        List<Header> headerList = Arrays.asList(
                new Header("Nombre y apellido", true),
                new Header("Parentesco", true),
                new Header("Sexo", true),
                new Header("Edad", true),
                new Header("Nivel educativo", true)
        );

        Table estructuraFamiliarTable = Table.builder().name("Estructura familiar").componentType(ComponentType.EDITABLE_TABLE)
                .headers(headerList).build();
        Section estructuraFamiliar = Section.builder().name("Estructura familiar").componentType(ComponentType.SECTION_CARD)
                .size(0).order(3).build();
        estructuraFamiliar.addComponent(estructuraFamiliarTable);
        return estructuraFamiliar;
    }

    private Section genograma() {
        Component file = File.builder().name("Genograma").componentType(ComponentType.FILE).build();
        Section genograma = Section.builder().name("Genograma").componentType(ComponentType.SECTION_CARD)
                .size(0).order(4).build();
        genograma.addComponent(file);
        return genograma;
    }

}
