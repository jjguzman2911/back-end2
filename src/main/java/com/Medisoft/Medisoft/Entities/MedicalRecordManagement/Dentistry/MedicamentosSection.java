package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Dentistry;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.ComponentType;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;

public class MedicamentosSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Medicamentos")
            .componentType(ComponentType.SECTION_PAGE).order(5).iconId(1003).build();

    @Override
    public Section init() {
        section.addComponent(medicamentos());
        return section;
    }

    private Section medicamentos(){
        return MedicalRecordCommonUtils.medicamentosSection(1, 0);
    }

}
