package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.ClinicalMedicine;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;

import java.util.Arrays;

public class IndicadoresClinicos2Section implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Indicadores clínicos 2")
            .componentType(ComponentType.SECTION_PAGE).order(3).iconId(1001).build();

    @Override
    public Section init() {
        section.addComponent(antecedentesEpidemiologicos());
        section.addComponent(antecedentesGinecoObstetricos());
        return section;
    }

    private Section antecedentesEpidemiologicos(){
        return MedicalRecordCommonUtils.antecedentesEpidemiologicos(1,-1);
    }

    private Section antecedentesGinecoObstetricos(){
        Section menstruacion = Section.builder().name("Menstruación").componentType(ComponentType.SECTION_SEPARATOR).build();
        menstruacion.addComponent(InputBox.builder().name("Edad de menarquia").componentType(ComponentType.INPUT_BOX).build());
        menstruacion.addComponent(InputBox.builder().name("Edad de menopausia").componentType(ComponentType.INPUT_BOX).build());
        menstruacion.addComponent(InputBox.builder().name("FUM").componentType(ComponentType.INPUT_BOX).build());
        menstruacion.addComponent(InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build());

        Section embarazo = Section.builder().name("embarazo").componentType(ComponentType.SECTION_SEPARATOR).build();
        embarazo.addComponent(ListBox.builder().name("Embarazo actual").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build());
        embarazo.addComponent(InputBox.builder().name("Semana de gestación").componentType(ComponentType.INPUT_BOX).build());
        embarazo.addComponent(ListBox.builder().name("Cantidad de embarazos").componentType(ComponentType.COMBO_BOX)
                .options(MedicalRecordCommonUtils.cantidadOptions()).build());
        embarazo.addComponent(ListBox.builder().name("Tipo de parto").componentType(ComponentType.RADIO_BUTTON)
                .options(Arrays.asList("Vaginal", "Cesárea")).build());
        embarazo.addComponent(ListBox.builder().name("Cantidad de hijos vivos").componentType(ComponentType.COMBO_BOX)
                .options(MedicalRecordCommonUtils.cantidadOptions()).build());
        embarazo.addComponent(ListBox.builder().name("Aborto").componentType(ComponentType.COMBO_BOX)
                .options(Arrays.asList("No revela", "Espontáneo", "Provocado")).build());
        embarazo.addComponent(InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build());

        Section anticonceptivos = Section.builder().name("Anticonceptivos").componentType(ComponentType.SECTION_SEPARATOR).build();
        anticonceptivos.addComponent(ListBox.builder().name("Cuáles").componentType(ComponentType.COMBO_BOX)
                        .options(Arrays.asList("Abstinencia en periodos fértiles", "Anticonceptivos orales", "DIU",
                                "preservativos", "otros")).build());
        anticonceptivos.addComponent(InputBox.builder().name("Fecha de último PAP").componentType(ComponentType.INPUT_BOX).build());
        anticonceptivos.addComponent(InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build());

        Section antecedentesGinecoObstetricos = Section.builder().name("Antecedentes gineco-obstétricos")
                .componentType(ComponentType.SECTION_CARD).order(2).size(1).build();
        antecedentesGinecoObstetricos.addComponent(menstruacion);
        antecedentesGinecoObstetricos.addComponent(embarazo);
        antecedentesGinecoObstetricos.addComponent(anticonceptivos);
        return antecedentesGinecoObstetricos;
    }
}
