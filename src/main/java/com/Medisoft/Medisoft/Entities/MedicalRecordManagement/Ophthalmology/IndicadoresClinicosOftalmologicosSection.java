package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Ophthalmology;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;

import java.util.Arrays;

public class IndicadoresClinicosOftalmologicosSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Indicadores clínicos oftalmológicos")
            .componentType(ComponentType.SECTION_PAGE).order(2).iconId(1001).build();

    @Override
    public Section init() {
        section.addComponent(antecedentesPersonales());
        section.addComponent(antecedentesOftalmologicos());
        section.addComponent(antecedentesFarmacologicos());
        section.addComponent(antecedentesInmunoalergicos());
        return section;
    }

    private Section antecedentesPersonales() {
        Component enfermedades = ListBox.builder().name("Enfermedades").componentType(ComponentType.CHECK_BOX)
                .options(Arrays.asList("No posee", "Hipertensión arterial", "Cardiopatia isquémica", "ACV hemorrágico",
                        "ACV isquémico", "Diabetes mellitus", "Toxoplasma", "Cáncer", "Enfermedad tiroidea",
                        "Nefropatías", "VIH", "Epoc", "Enfermedad autoinmune")).build();
        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();
        Component intervencionesQuirurgicas = InputBox.builder().name("Intervenciones quirúrgicas")
                .componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesPersonales = Section.builder().name("Antecedentes personales")
                .componentType(ComponentType.SECTION_CARD).order(1).size(-1).build();
        antecedentesPersonales.addComponent(enfermedades);
        antecedentesPersonales.addComponent(observaciones);
        antecedentesPersonales.addComponent(intervencionesQuirurgicas);
        return antecedentesPersonales;
    }

    private Section antecedentesOftalmologicos() {
        Component antecedentesPersonales = ListBox.builder().name("Antecedentes personales")
                .componentType(ComponentType.CHECK_BOX).options(Arrays.asList("No posee", "Miopía", "Astigmatismo",
                        "Presbicia", "Hipermetropía", "Cataratas", "Estrabismo", "Otros")).build();
        Component tratamientoParaDefectoDeRefraccion = InputBox.builder().name("Tratamiento para defecto de refracción")
                .componentType(ComponentType.TEXT_BOX).build();
        Component fechaDeUltimaRevisionOptica = InputBox.builder().name("Fecha de última revisión óptica")
                .componentType(ComponentType.INPUT_BOX).build();
        Component usoDeLentesDeContacto = ListBox.builder().name("Uso de lentes de contacto")
                .componentType(ComponentType.RADIO_BUTTON).options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component habitoUsoLentesContacto = InputBox.builder().name("Hábito de uso de lentes de contacto")
                .componentType(ComponentType.TEXT_BOX).build();
        Component usoDeGotasOftalmologicas = ListBox.builder().name("Uso de gotas oftalmológicas")
                .componentType(ComponentType.RADIO_BUTTON).options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component informacionDeUsoDeColirios = InputBox.builder().name("Información de uso de colirios")
                .componentType(ComponentType.TEXT_BOX).build();
        Component antecedentesFamiliares = ListBox.builder().name("Antecedentes familiares")
                .componentType(ComponentType.CHECK_BOX).options(Arrays.asList("No posee", "Glaucoma", "Queratocono",
                        "Miopía", "Cataratas", "Estrabismo", "Desprendimiento de retina", "Otros")).build();
        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesOftalmologicos = Section.builder().name("Antecedentes oftalmológicos")
                .componentType(ComponentType.SECTION_CARD).order(2).size(1).build();
        antecedentesOftalmologicos.addComponent(antecedentesPersonales);
        antecedentesOftalmologicos.addComponent(tratamientoParaDefectoDeRefraccion);
        antecedentesOftalmologicos.addComponent(fechaDeUltimaRevisionOptica);
        antecedentesOftalmologicos.addComponent(usoDeLentesDeContacto);
        antecedentesOftalmologicos.addComponent(habitoUsoLentesContacto);
        antecedentesOftalmologicos.addComponent(usoDeGotasOftalmologicas);
        antecedentesOftalmologicos.addComponent(informacionDeUsoDeColirios);
        antecedentesOftalmologicos.addComponent(antecedentesFamiliares);
        antecedentesOftalmologicos.addComponent(observaciones);
        return antecedentesOftalmologicos;
    }

    private Section antecedentesFarmacologicos() {
        Component farmacos = ListBox.builder().name("Fármacos").componentType(ComponentType.CHECK_BOX)
                .options(Arrays.asList("No posee", "Insulina", "ARTV",
                        "Antimaláricos", "Corticoides", "Amiodarona", "Otros")).build();
        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesFarmacologicos = Section.builder().name("Antecedentes farmacológicos")
                .componentType(ComponentType.SECTION_CARD).order(3).size(-1).build();
        antecedentesFarmacologicos.addComponent(farmacos);
        antecedentesFarmacologicos.addComponent(observaciones);
        return antecedentesFarmacologicos;
    }

    private Section antecedentesInmunoalergicos() {
        Component alergiasFarmacologicas = ListBox.builder().name("Alergias farmacológicas")
                .componentType(ComponentType.CHECK_BOX).options(Arrays.asList("No posee", "Antitiroideos", "Penicilina",
                        "Aspirinas")).build();
        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesInmunoalergicos = Section.builder().name("Antecedentes inmunoalérgicos")
                .componentType(ComponentType.SECTION_CARD).order(4).size(1).build();
        antecedentesInmunoalergicos.addComponent(alergiasFarmacologicas);
        antecedentesInmunoalergicos.addComponent(observaciones);
        return antecedentesInmunoalergicos;
    }
}
