package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Tooth {
    private boolean[] roots = new boolean[3];
    private boolean[] surfaces = new boolean[5];
    private int id;//position in quadrant's array
}
