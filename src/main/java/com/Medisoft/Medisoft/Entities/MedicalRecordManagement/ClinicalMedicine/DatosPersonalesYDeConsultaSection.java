package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.ClinicalMedicine;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Component;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.ComponentType;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.InputBox;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;

public class DatosPersonalesYDeConsultaSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Datos personales y de consulta")
            .componentType(ComponentType.SECTION_PAGE).order(1).iconId(1000).build();

    @Override
    public Section init() {
        section.addComponent(datosPersonales());
        section.addComponent(datosDeConsulta());
        return section;
    }

    private Section datosPersonales() {
        Section datosPersonales = MedicalRecordCommonUtils.datosPersonales(-1);
        Component residenciasAnteriores = InputBox.builder().name("Residencias anteriores")
                .componentType(ComponentType.TEXT_BOX).build();
        datosPersonales.addComponent(residenciasAnteriores);
        return datosPersonales;
    }

    private Section datosDeConsulta() {
        InputBox motivoDeConsulta = InputBox.builder().name("Motivo de consulta").componentType(ComponentType.TEXT_BOX).build();

        Section enfermedadActual = Section.builder().name("Enfermedad actual").componentType(ComponentType.SECTION_SEPARATOR)
                .build();

        InputBox fechaDeInicio = InputBox.builder().name("Fecha de inicio").componentType(ComponentType.INPUT_BOX).build();
        InputBox formaDeInicio = InputBox.builder().name("Forma de inicio").componentType(ComponentType.INPUT_BOX).build();
        InputBox estadoActualDeLosSintomas = InputBox.builder().name("Estado actual de los síntomas")
                .componentType(ComponentType.TEXT_BOX).build();
        InputBox tratamientosYEstudiosAnteriores = InputBox.builder().name("Tratamientos y estudios anteriores")
                .componentType(ComponentType.TEXT_BOX).build();
        enfermedadActual.addComponent(fechaDeInicio);
        enfermedadActual.addComponent(formaDeInicio);
        enfermedadActual.addComponent(estadoActualDeLosSintomas);
        enfermedadActual.addComponent(tratamientosYEstudiosAnteriores);

        Section datosDeConsulta = Section.builder().name("Datos de consulta").componentType(ComponentType.SECTION_CARD)
                .order(2).size(1).build();
        datosDeConsulta.addComponent(motivoDeConsulta);
        datosDeConsulta.addComponent(enfermedadActual);

        return datosDeConsulta;
    }

}
