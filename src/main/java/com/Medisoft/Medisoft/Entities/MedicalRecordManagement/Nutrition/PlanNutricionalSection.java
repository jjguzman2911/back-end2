package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Nutrition;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;


public class PlanNutricionalSection implements IMedicalRecordSection {
    private final Section section = Section.builder().name("Plan nutricional").componentType(ComponentType.SECTION_PAGE)
            .order(5).iconId(3002).build();

    @Override
    public Section init() {
        section.addComponent(horarios());
        section.addComponent(detalleDeAlimentos());
        return section;
    }

    private Section horarios() {
        Component desayuno = InputBox.builder().name("Desayuno").componentType(ComponentType.INPUT_BOX)
                .order(1).build();
        Component mediaManana = InputBox.builder().name("Media mañana").componentType(ComponentType.INPUT_BOX)
                .order(2).build();
        Component almuerzo = InputBox.builder().name("Almuerzo").componentType(ComponentType.INPUT_BOX)
                .order(3).build();
        Component merienda = InputBox.builder().name("Merienda").componentType(ComponentType.INPUT_BOX)
                .order(4).build();
        Component cena = InputBox.builder().name("Cena").componentType(ComponentType.INPUT_BOX)
                .order(5).build();
        Section horarios = Section.builder().name("Horarios").componentType(ComponentType.SECTION_CARD)
                .order(1).size(0).build();
        horarios.addComponent(desayuno);
        horarios.addComponent(mediaManana);
        horarios.addComponent(almuerzo);
        horarios.addComponent(merienda);
        horarios.addComponent(cena);

        return horarios;
    }

    private Section detalleDeAlimentos() {
        Table table = Table.builder().name("Detalle de alimentos").componentType(ComponentType.FIXED_TABLE)
                .headers(NutritionCommonUtils.headersDieta())
                .elements(NutritionCommonUtils.elementsDieta()).build();

        Section detalleDeAlimentos = Section.builder().name("Detalle de alimentos")
                .componentType(ComponentType.SECTION_CARD).size(0).order(2).build();
        detalleDeAlimentos.addComponent(table);

        return detalleDeAlimentos;
    }

}
