package com.Medisoft.Medisoft.Entities.MedicalRecordManagement;

import com.Medisoft.Medisoft.DAO.Mongo.IMedicalRecordDAO;
import com.Medisoft.Medisoft.Exceptions.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MedicalRecordStructureService {

    @Autowired
    private IMedicalRecordDAO medicalRecordDAO;


    public MedicalRecordStructure save(MedicalRecordStructure medicalRecordStructure) {
        try {
            MedicalRecordStructure savedMedicalRecordStructure = medicalRecordDAO.save(medicalRecordStructure);
            log.info("Medical record structure {} ", medicalRecordStructure.getId());
            return medicalRecordStructure;
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            throw new com.Medisoft.Medisoft.Exceptions.DataIntegrityViolationException(MedicalRecordStructure.class.getSimpleName());
        }
    }

    public MedicalRecordStructure findById(long medicalRecordId) {
        MedicalRecordStructure medicalRecordStructure = medicalRecordDAO.findById(medicalRecordId).orElse(null);
        if (medicalRecordStructure == null) {
            throw new EntityNotFoundException(MedicalRecordStructure.class.getSimpleName());
        }
        log.info("Medical record structure {} found", medicalRecordId);
        return medicalRecordStructure;
    }
}
