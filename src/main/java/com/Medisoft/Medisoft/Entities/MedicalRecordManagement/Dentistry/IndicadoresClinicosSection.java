package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Dentistry;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;

import java.util.Arrays;

public class IndicadoresClinicosSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Indicadores clínicos")
            .componentType(ComponentType.SECTION_PAGE).order(2).iconId(1001).build();

    @Override
    public Section init() {
        section.addComponent(antecedentesPersonales());
        section.addComponent(antecedentesHeredofamiliares());
        section.addComponent(antecedentesEpidemiologicos());
        section.addComponent(antecedentesInmunoalergicos());
        section.addComponent(antecedentesGinecologicos());
        return section;
    }

    private Section antecedentesPersonales() {
        Component enfermedades = ListBox.builder().name("Enfermedades").componentType(ComponentType.COMBO_BOX)
                .options(Arrays.asList("No posee", "Hipertensión arterial", "Cardiopatia isquemica", "ACV hemorrágico",
                        "ACV isquémico", "Diabetes mellitus", "Transtornos convulsivos", "Fiebre reumática", "Cáncer",
                        "Enfermedad tiroidea", "Litiasis biliar", "Gastritis", "IAM", "Enfermedad coronaria (No IAM)", "Epoc", "Asma")
                ).build();

        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX)
                .build();

        Section antecedentesPersonales = Section.builder().name("Antecedentes personales")
                .componentType(ComponentType.SECTION_CARD).order(1).size(-1).build();
        antecedentesPersonales.addComponent(enfermedades);
        antecedentesPersonales.addComponent(observaciones);
        return antecedentesPersonales;
    }

    private Section antecedentesHeredofamiliares(){
        Component enfermedades = ListBox.builder().name("Enfermedades").componentType(ComponentType.CHECK_BOX)
                .options(Arrays.asList("Asma", "HTA", "Diabetes", "IAM", "Cardiopatía isquémica", "ACV isquémico",
                        "ACV hemorrágico", "Cáncer", "Enfermedad tiroidea")).build();

        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesHeredofamiliares = Section.builder().name("Antecedentes heredofamiliares")
                .componentType(ComponentType.SECTION_CARD).order(2).size(1).build();
        antecedentesHeredofamiliares.addComponent(enfermedades);
        antecedentesHeredofamiliares.addComponent(observaciones);

        return antecedentesHeredofamiliares;
    }

    private Section antecedentesEpidemiologicos(){
        return MedicalRecordCommonUtils.antecedentesEpidemiologicos(3, -1);
    }

    private Section antecedentesInmunoalergicos(){
        Component alergiasFarmacologicas = ListBox.builder().name("Alergias farmacológicas")
                .componentType(ComponentType.CHECK_BOX).options(Arrays.asList("No posee", "AINES", "Anestésicos",
                        "Antibióticos", "Iodo")).build();

        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesInmunoalergicos = Section.builder().name("Antecedentes inmunoalérgicos")
                .componentType(ComponentType.SECTION_CARD).order(4).size(1).build();
        antecedentesInmunoalergicos.addComponent(alergiasFarmacologicas);
        antecedentesInmunoalergicos.addComponent(observaciones);
        return antecedentesInmunoalergicos;
    }

    private Section antecedentesGinecologicos(){
        ListBox embarazoActual = ListBox.builder().name("Embarazo actual").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();

        InputBox semanaGestacion = InputBox.builder().name("Semana de gestación")
                .componentType(ComponentType.TEXT_BOX).build();

        ListBox hijos = ListBox.builder().name("Hijos").componentType(ComponentType.COMBO_BOX)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();

        ListBox cantidad = ListBox.builder().name("Cantidad").componentType(ComponentType.COMBO_BOX)
                .options(MedicalRecordCommonUtils.cantidadOptions()).build();

        InputBox observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesGinecologicos = Section.builder().name("Antecedentes ginecológicos")
                .componentType(ComponentType.SECTION_CARD).order(5).size(-1).build();
        antecedentesGinecologicos.addComponent(embarazoActual);
        antecedentesGinecologicos.addComponent(semanaGestacion);
        antecedentesGinecologicos.addComponent(hijos);
        antecedentesGinecologicos.addComponent(cantidad);
        antecedentesGinecologicos.addComponent(observaciones);

        return antecedentesGinecologicos;

    }
}
