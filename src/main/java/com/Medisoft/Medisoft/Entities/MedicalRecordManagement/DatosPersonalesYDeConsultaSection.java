package com.Medisoft.Medisoft.Entities.MedicalRecordManagement;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.ComponentType;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.InputBox;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.ListBox;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;

import java.util.Arrays;
import java.util.List;

public class DatosPersonalesYDeConsultaSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Datos personales y de consulta")
            .componentType(ComponentType.SECTION_PAGE).order(1).iconId(1000).build();

    @Override
    public Section init() {
        section.addComponent(datosPersonales());
        section.addComponent(datosDeConsulta());
        return section;
    }

    private Section datosPersonales() {
        return MedicalRecordCommonUtils.datosPersonales(-1);
    }

    private Section datosDeConsulta() {
        InputBox motivoDeConsulta = InputBox.builder().name("Motivo de consulta").componentType(ComponentType.TEXT_BOX)
                .order(1).build();

        InputBox expectativaDeTratamiento = InputBox.builder().name("Expectativa de tratamiento").componentType(ComponentType.TEXT_BOX)
                .order(2).build();

        List<String> numbersList = Arrays.asList("1", "2", "3", "4", "5");
        ListBox nivelDeImportancia = ListBox.builder().name("Nivel de importancia").componentType(ComponentType.COMBO_BOX)
                .order(3).options(numbersList).build();

        ListBox gradoDeMotivacion = ListBox.builder().name("Grado de motivación").componentType(ComponentType.COMBO_BOX)
                .order(4).options(numbersList).build();

        Section datosDeConsulta = Section.builder().name("Datos de consulta").componentType(ComponentType.SECTION_CARD)
                .order(2).size(1).build();
        datosDeConsulta.addComponent(motivoDeConsulta);
        datosDeConsulta.addComponent(expectativaDeTratamiento);
        datosDeConsulta.addComponent(nivelDeImportancia);
        datosDeConsulta.addComponent(gradoDeMotivacion);

        return datosDeConsulta;
    }
}
