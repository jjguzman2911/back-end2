package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Ophthalmology;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExamenOftalmologicoSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Examen oftalmológico")
            .componentType(ComponentType.SECTION_PAGE).order(3).iconId(5000).build();

    @Override
    public Section init() {
        section.addComponent(agudezaVisual());
        section.addComponent(examenesYExploraciones());
        section.addComponent(biomicroscopiaYFondoDeOjo());
        return section;
    }

    private Section agudezaVisual() {
        List<Header> headers = new ArrayList<>();
        headers.add(new Header("Ojo", false));
        headers.add(new Header("Sin corrección", true));
        headers.add(new Header("Con corrección", true));
        headers.add(new Header("Corrección actual", true));
        List<List<String>> elements = new ArrayList<>();
        elements.add(Arrays.asList("Ojo izquierdo", "", "", ""));
        elements.add(Arrays.asList("Ojo derecho", "", "", ""));
        Table lejos = Table.builder().name("Lejos").componentType(ComponentType.FIXED_TABLE).headers(headers).elements(elements).build();
        Table cerca = Table.builder().name("Cerca").componentType(ComponentType.FIXED_TABLE).headers(headers).elements(elements).build();
        ;
        Section agudezaVisual = Section.builder().name("Agudeza visual").componentType(ComponentType.SECTION_CARD)
                .order(1).size(-1).build();
        agudezaVisual.addComponent(lejos);
        agudezaVisual.addComponent(cerca);
        return agudezaVisual;
    }

    private Section examenesYExploraciones() {
        Component examenDeReflejosPupilares = InputBox.builder().name("Examen de reflejos pupilares")
                .componentType(ComponentType.TEXT_BOX).build();
        Component exploracionDeMovimientosOculares = InputBox.builder().name("Exploración de movimientos oculares")
                .componentType(ComponentType.TEXT_BOX).build();
        Component tonometria = InputBox.builder().name("Tonometría")
                .componentType(ComponentType.TEXT_BOX).build();

        Section examenesYExploraciones = Section.builder().name("Exámenes y exploraciones")
                .componentType(ComponentType.SECTION_CARD).order(2).size(1).build();
        examenesYExploraciones.addComponent(examenDeReflejosPupilares);
        examenesYExploraciones.addComponent(exploracionDeMovimientosOculares);
        examenesYExploraciones.addComponent(tonometria);
        return examenesYExploraciones;
    }

    private Section biomicroscopiaYFondoDeOjo() {
        Component biomicroscopiaOD = InputBox.builder().name("Biomicroscopía O.D.")
                .componentType(ComponentType.TEXT_BOX).build();
        Component biomicroscopiaOI = InputBox.builder().name("Biomicroscopía O.I.")
                .componentType(ComponentType.TEXT_BOX).build();
        List<Header> headers = new ArrayList<>();
        headers.add(new Header("-", false));
        headers.add(new Header("Ojo derecho", true));
        headers.add(new Header("Ojo izquierdo", true));
        List<List<String>> elements = new ArrayList<>();
        elements.add(Arrays.asList("Papila", "", ""));
        elements.add(Arrays.asList("Vasos arteriolas", "", ""));
        elements.add(Arrays.asList("Retina", "", ""));
        elements.add(Arrays.asList("Vítreo", "", ""));
        elements.add(Arrays.asList("Fovea", "", ""));
        Table fondoDeOjo = Table.builder().name("Fondo de ojo").componentType(ComponentType.FIXED_TABLE)
                .headers(headers).elements(elements).build();

        Section biomicroscopiaYFondoDeOjo = Section.builder().name("Biomicroscopía y fondo de ojo")
                .componentType(ComponentType.SECTION_CARD).order(3).size(-1).build();
        biomicroscopiaYFondoDeOjo.addComponent(biomicroscopiaOD);
        biomicroscopiaYFondoDeOjo.addComponent(biomicroscopiaOI);
        biomicroscopiaYFondoDeOjo.addComponent(fondoDeOjo);
        return biomicroscopiaYFondoDeOjo;
    }
}
