package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Psychology;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;

import java.util.Arrays;

public class PsicodiagnosticoYPlanDeOrientacionSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Psicodiagnóstico y plan de orientación").componentType(ComponentType.SECTION_PAGE)
            .order(4).iconId(4001).build();

    @Override
    public Section init() {
        section.addComponent(testPsicodiagnostico());
        section.addComponent(planDeOrientacion());
        return section;
    }

    private Section testPsicodiagnostico() {

        Component pruebasProyectivas = ListBox.builder().name("Pruebas proyectivas").componentType(ComponentType.CHECK_BOX)
                .options(Arrays.asList("HTP", "Test de familia", "Test de Rorschach")).build();
        Component subirArchivo = File.builder().name("Subir archivo").componentType(ComponentType.FILE).build();
        Component evaluacion = InputBox.builder().name("Evaluación").componentType(ComponentType.TEXT_BOX).build();

        Section testPsicodiagnostico = Section.builder().name("Test psicodiagnóstico").componentType(ComponentType.SECTION_CARD)
                .size(-1).order(1).build();
        testPsicodiagnostico.addComponent(pruebasProyectivas);
        testPsicodiagnostico.addComponent(subirArchivo);
        testPsicodiagnostico.addComponent(evaluacion);
        return testPsicodiagnostico;
    }

    private Section planDeOrientacion() {
        Component objetivoDelTratamiento = InputBox.builder().name("Objetivo del tratamiento").componentType(ComponentType.TEXT_BOX).build();
        Component frecuenciaYDuracion = InputBox.builder().name("Frecuencia y duración de las sesiones")
                .componentType(ComponentType.TEXT_BOX).build();
        Component duracionDelTratamiento = InputBox.builder().name("Duración del tratamiento")
                .componentType(ComponentType.TEXT_BOX).build();
        Component recomendaciones = InputBox.builder().name("Recomendaciones").componentType(ComponentType.TEXT_BOX).build();

        Section planDeOrientacion = Section.builder().name("Plan de orientación").componentType(ComponentType.SECTION_CARD)
                .size(1).order(2).build();
        planDeOrientacion.addComponent(objetivoDelTratamiento);
        planDeOrientacion.addComponent(frecuenciaYDuracion);
        planDeOrientacion.addComponent(duracionDelTratamiento);
        planDeOrientacion.addComponent(recomendaciones);

        return planDeOrientacion;
    }
}
