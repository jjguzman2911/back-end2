package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

@Getter
public enum ComponentType {
    SECTION_PAGE("Section page", 0),
    CHECK_BOX("Check box", 1),
    COMBO_BOX("Combo box", 2),
    RADIO_BUTTON("Radio button", 3),
    TEXT_BOX("Text box", 4),
    SECTION_CARD("Section card", 5),
    SECTION_SEPARATOR("Section separator", 6),
    INPUT_BOX("Input box", 7),
    FIXED_TABLE("Fixed table", 8),
    FILE("File", 9),
    EDITABLE_TABLE("Editable table", 10),
    ODONTOGRAM("Odontogram", 11);

    private final String name;
    @JsonValue
    private final int id;

    ComponentType(String name, int id) {
        this.name = name;
        this.id = id;
    }
}
