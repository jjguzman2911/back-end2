package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;


@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
@NoArgsConstructor
@JsonDeserialize
public class Odontogram extends Component {

    private Quadrant[] quadrants = new Quadrant[4];

    @Builder
    public Odontogram(String name, ComponentType componentType, int order) {
        this.name = name;
        this.componentType = ComponentType.ODONTOGRAM;
        this.order = order;
        fulfillQuadrants();
    }

    private void fulfillQuadrants() {
        for (int i = 0; i < quadrants.length; i++) {
            if (quadrants[i] == null) {
                quadrants[i] = new Quadrant();
            }
            quadrants[i].fulfillTeeth(i + 1);
        }
    }
}
