package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
@NoArgsConstructor
@JsonDeserialize
public class Section extends Component {

    @Singular("componente")
    private List<Component> components;
    private int size; //-1 left, 0 center, 1 right
    private boolean visible;
    private int iconId;


    @Builder
    public Section(String name, ComponentType componentType, int order, int size, List<Component> components,
                   Boolean visible, int iconId) {
        super(name, componentType, order);
        if (!(componentType.equals(ComponentType.SECTION_CARD) ||
                componentType.equals(ComponentType.SECTION_PAGE) ||
                componentType.equals(ComponentType.SECTION_SEPARATOR))) {
            throw new IllegalArgumentException("component type must be compatible with Section");
        }
        if (size > 1 || size < -1) {
            throw new IllegalArgumentException("size must be between -1 and 1");
        }
        this.size = size;
        this.components = components;
        this.visible = visible != null && visible;
        this.iconId = iconId;
    }

    public boolean hasComponents() {
        return components != null;
    }

    public void addComponent(Component component) {
        if (components == null) {
            components = new ArrayList<>();
        }
        components.add(component);
    }

    public void addComponents(Set<Component> components) {
        if (this.components == null) {
            this.components = new ArrayList<>();
        }
        this.components.addAll(components);
    }
}