package com.Medisoft.Medisoft.Entities.MedicalRecordManagement;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.IProfessionalRegistration;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * This class models a medical record structure for every speciality
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "medical_record_structures")
public class MedicalRecordStructure implements IProfessionalRegistration {

    public transient static final String SEQUENCE_NAME = "medical_record_structure_sequence";

    @Id
    private long id;
    private long timestamp;
    @Singular("section")
    private List<Section> sections;
    private Especialidad speciality;


    @Override
    public boolean isComplete() {
        return id != 0;
    }
}
