package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
@NoArgsConstructor
@JsonDeserialize
public class File extends Component {

    private List<String> urls;


    @Builder
    public File(String name, ComponentType componentType, int order, List<String> urls) {
        super(name, componentType, order);
        if (!componentType.equals(ComponentType.FILE)) {
            throw new IllegalArgumentException("component type must be compatible with File");
        }
        this.urls = urls;
    }
}
