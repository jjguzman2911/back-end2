package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Nutrition;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.ComponentType;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.InputBox;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.ListBox;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public class IndicadoresClinicosSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Indicadores clínicos").componentType(ComponentType.SECTION_PAGE)
            .order(2).iconId(1001).build();

    @Override
    public Section init() {
        section.addComponent(antecedentesPersonales());
        section.addComponent(antecedentesPersonalesNoPatologicos());
        section.addComponent(antecedentesHeredofamiliares());
        section.addComponent(estiloDeVida());
        section.addComponent(antecedentesGinecologicos());
        return section;
    }

    private Section antecedentesPersonales() {
        return MedicalRecordCommonUtils.antecedentesPersonales(-1);
    }

    private Section antecedentesHeredofamiliares() {
        return MedicalRecordCommonUtils.antecedentesHeredofamiliares(-1);
    }

    private Section antecedentesPersonalesNoPatologicos() {
        return MedicalRecordCommonUtils.antecedentesPersonalesNoPatologicos(1);
    }

    private Section estiloDeVida() {
        List<String> setActividadFisica = Arrays.asList("No realiza", "Muy ligera", "Ligera", "Moderada", "Pesada");
        ListBox actividadFisica = ListBox.builder().name("Actividad física").componentType(ComponentType.COMBO_BOX)
                .order(1).options(setActividadFisica).build();

        InputBox tipo = InputBox.builder().name("Tipo").componentType(ComponentType.TEXT_BOX)
                .order(1).build();

        ListBox frecuencia = ListBox.builder().name("Frecuencia").componentType(ComponentType.COMBO_BOX)
                .order(2).options(MedicalRecordCommonUtils.frecuenciaOptions()).build();

        InputBox duracion = InputBox.builder().name("Duración").componentType(ComponentType.TEXT_BOX)
                .order(3).build();

        Section ejercicio = Section.builder().name("Ejercicio").componentType(ComponentType.SECTION_SEPARATOR)
                .order(1).build();
        ejercicio.addComponent(tipo);
        ejercicio.addComponent(frecuencia);
        ejercicio.addComponent(duracion);

        Section estiloDeVida = Section.builder().name("Estilo de vida").componentType(ComponentType.SECTION_CARD)
                .size(1).order(4).build();
        estiloDeVida.addComponent(actividadFisica);
        estiloDeVida.addComponent(ejercicio);
        return estiloDeVida;
    }

    private Section antecedentesGinecologicos() {
        ListBox embarazoActual = ListBox.builder().name("Embarazo actual").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();

        InputBox semanaGestacion = InputBox.builder().name("Semana de gestación")
                .componentType(ComponentType.TEXT_BOX).build();

        InputBox fechaUltimaMenstruacion = InputBox.builder().name("Fecha de la última menstruación")
                .componentType(ComponentType.TEXT_BOX).build();

        ListBox anticonceptivosOrales = ListBox.builder().name("Anticonceptivos orales").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();

        InputBox cuales = InputBox.builder().name("Cuáles").componentType(ComponentType.TEXT_BOX).build();

        InputBox dosis = InputBox.builder().name("Dosis").componentType(ComponentType.TEXT_BOX).build();

        ListBox climaterio = ListBox.builder().name("Climaterio").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();

        InputBox fecha = InputBox.builder().name("Fecha").componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesGinecologicos = Section.builder().name("Antecedentes ginecológicos")
                .componentType(ComponentType.SECTION_CARD).order(5).size(-1).build();
        antecedentesGinecologicos.addComponent(embarazoActual);
        antecedentesGinecologicos.addComponent(semanaGestacion);
        antecedentesGinecologicos.addComponent(fechaUltimaMenstruacion);
        antecedentesGinecologicos.addComponent(anticonceptivosOrales);
        antecedentesGinecologicos.addComponent(cuales);
        antecedentesGinecologicos.addComponent(dosis);
        antecedentesGinecologicos.addComponent(climaterio);
        antecedentesGinecologicos.addComponent(fecha);

        return antecedentesGinecologicos;
    }
}
