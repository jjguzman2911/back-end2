package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Dentistry;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;

public class DatosPersonalesYDeConsultaSection implements IMedicalRecordSection {
    private final Section section = Section.builder().name("Datos personales y de consulta")
            .componentType(ComponentType.SECTION_PAGE).order(1).iconId(1000).build();

    @Override
    public Section init() {
        section.addComponent(datosPersonales());
        section.addComponent(datosDeConsulta());
        return section;
    }

    private Section datosPersonales() {
        Component nivelDeInstruccion = InputBox.builder().name("Nivel de instrucción").componentType(ComponentType.TEXT_BOX)
                .build();

        Component ocupacion = InputBox.builder().name("Ocupación").componentType(ComponentType.TEXT_BOX)
                .build();

        Component domicilio = InputBox.builder().name("Domicilio").componentType(ComponentType.TEXT_BOX)
                .build();

        Component lugarDeNacimiento = InputBox.builder().name("Lugar de nacimiento").componentType(ComponentType.TEXT_BOX)
                .build();

        Component religion = InputBox.builder().name("Religión").componentType(ComponentType.TEXT_BOX)
                .build();

        Component residenciasAnteriores = InputBox.builder().name("Residencias anteriores")
                .componentType(ComponentType.TEXT_BOX).build();

        Section datosPersonales = Section.builder().name("Datos personales").componentType(ComponentType.SECTION_CARD)
                .order(1).size(-1).build();
        datosPersonales.addComponent(nivelDeInstruccion);
        datosPersonales.addComponent(ocupacion);
        datosPersonales.addComponent(domicilio);
        datosPersonales.addComponent(lugarDeNacimiento);
        datosPersonales.addComponent(religion);
        datosPersonales.addComponent(residenciasAnteriores);

        return datosPersonales;
    }

    private Section datosDeConsulta() {
        InputBox motivoDeConsulta = InputBox.builder().name("Motivo de consulta").componentType(ComponentType.TEXT_BOX).build();

        Section enfermedadActual = Section.builder().name("Enfermedad actual").componentType(ComponentType.SECTION_SEPARATOR)
                .build();

        InputBox fechaDeInicio = InputBox.builder().name("Fecha de inicio").componentType(ComponentType.INPUT_BOX).build();
        InputBox formaDeInicio = InputBox.builder().name("Forma de inicio").componentType(ComponentType.INPUT_BOX).build();
        InputBox observaciones = InputBox.builder().name("Observaciones")
                .componentType(ComponentType.TEXT_BOX).build();
        enfermedadActual.addComponent(fechaDeInicio);
        enfermedadActual.addComponent(formaDeInicio);
        enfermedadActual.addComponent(observaciones);

        Section datosDeConsulta = Section.builder().name("Datos de consulta").componentType(ComponentType.SECTION_CARD)
                .order(2).size(1).build();
        datosDeConsulta.addComponent(motivoDeConsulta);
        datosDeConsulta.addComponent(enfermedadActual);

        return datosDeConsulta;
    }
}
