package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
@NoArgsConstructor
@JsonDeserialize
public class InputBox extends Component {

    private String text;

    @Builder
    public InputBox(String name, ComponentType componentType, int order, String text) {
        super(name, componentType, order);
        if (!(componentType.equals(ComponentType.INPUT_BOX) ||
                componentType.equals(ComponentType.TEXT_BOX))) {
            throw new IllegalArgumentException("component type must be compatible with InputBox");
        }
        this.text = text;
    }
}
