package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Ophthalmology;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;


public class DatosPersonalesYDeConsultaSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Datos personales y de consulta")
            .componentType(ComponentType.SECTION_PAGE).order(1).iconId(1000).build();

    @Override
    public Section init() {
        section.addComponent(datosPersonales());
        section.addComponent(datosDeConsulta());
        return section;
    }

    private Section datosPersonales() {
        Component lugarDeNacimiento = InputBox.builder().name("Lugar de nacimiento")
                .componentType(ComponentType.INPUT_BOX).build();
        Component domicilio = InputBox.builder().name("Domicilio").componentType(ComponentType.INPUT_BOX).build();
        Component nivelDeInstruccion = InputBox.builder().name("Nivel de instrucción")
                .componentType(ComponentType.INPUT_BOX).build();
        Component ocupacion = InputBox.builder().name("Ocupación").componentType(ComponentType.INPUT_BOX).build();
        Component estadoCivil = ListBox.builder().name("Estado civil").componentType(ComponentType.COMBO_BOX)
                .options(MedicalRecordCommonUtils.estadoCivilOptions()).build();
        Component telefono = InputBox.builder().name("Teléfono").componentType(ComponentType.INPUT_BOX).build();

        Section datosPersonales = Section.builder().name("Datos personales").componentType(ComponentType.SECTION_CARD)
                .order(1).size(-1).build();
        datosPersonales.addComponent(lugarDeNacimiento);
        datosPersonales.addComponent(domicilio);
        datosPersonales.addComponent(nivelDeInstruccion);
        datosPersonales.addComponent(ocupacion);
        datosPersonales.addComponent(estadoCivil);
        datosPersonales.addComponent(telefono);

        return datosPersonales;
    }

    private Section datosDeConsulta() {
        Component motivoDeConsulta = InputBox.builder().name("Motivo de consulta")
                .componentType(ComponentType.INPUT_BOX).build();
        Section enfermedadActual = Section.builder().componentType(ComponentType.SECTION_SEPARATOR)
                .name("Enfermedad actual").build();
        Component fechaDeInicio = InputBox.builder().name("Fecha de inicio").componentType(ComponentType.INPUT_BOX)
                .build();
        Component observaciones = InputBox.builder().name("Observaciones")
                .componentType(ComponentType.TEXT_BOX).build();
        enfermedadActual.addComponent(fechaDeInicio);
        enfermedadActual.addComponent(observaciones);

        Section datosDeConsulta = Section.builder().name("Datos de consulta").componentType(ComponentType.SECTION_CARD)
                .order(2).size(1).build();
        datosDeConsulta.addComponent(motivoDeConsulta);
        datosDeConsulta.addComponent(enfermedadActual);

        return datosDeConsulta;
    }

}
