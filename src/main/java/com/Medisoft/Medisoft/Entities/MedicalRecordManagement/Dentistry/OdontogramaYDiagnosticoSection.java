package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Dentistry;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;

import java.util.ArrayList;
import java.util.List;

public class OdontogramaYDiagnosticoSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Odontograma y diagnóstico")
            .componentType(ComponentType.SECTION_PAGE).order(4).iconId(2001).build();

    @Override
    public Section init() {
        section.addComponent(odontograma());
        section.addComponent(diagnosticoYPlanDeTratamiento());
        return section;
    }

    private Section odontograma() {
        Component odontograma = Odontogram.builder().build();
        Section odontogramaSection = Section.builder().name("Odontograma").componentType(ComponentType.SECTION_CARD)
                .order(1).size(0).build();
        odontogramaSection.addComponent(odontograma);
        return odontogramaSection;
    }

    private Section diagnosticoYPlanDeTratamiento() {
        Section estadoBucal = Section.builder().name("Estado bucal").componentType(ComponentType.SECTION_SEPARATOR).build();
        Component presenciaDeSarro = ListBox.builder().name("Presencia de sarro").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component enfermedadPeriodontal = ListBox.builder().name("Enfermedad periodontal").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        estadoBucal.addComponent(presenciaDeSarro);
        estadoBucal.addComponent(enfermedadPeriodontal);

        Section diagnosticoYTratamiento = Section.builder().name("Diagnóstico y tratamiento")
                .componentType(ComponentType.SECTION_SEPARATOR).build();
        Table diagnosticoYTratamientoTable = Table.builder().name("Diagnóstico y tratamiento").componentType(ComponentType.EDITABLE_TABLE).build();
        buildTableRowsAndColumns(diagnosticoYTratamientoTable);
        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();
        diagnosticoYTratamiento.addComponent(diagnosticoYTratamientoTable);
        diagnosticoYTratamiento.addComponent(observaciones);

        Section diagnosticoYPlanDeTratamiento = Section.builder().name("Diagnóstico y plan de tratamiento")
                .componentType(ComponentType.SECTION_CARD).order(2).size(0).build();
        diagnosticoYPlanDeTratamiento.addComponent(estadoBucal);
        diagnosticoYPlanDeTratamiento.addComponent(diagnosticoYTratamiento);
        return diagnosticoYPlanDeTratamiento;
    }

    private void buildTableRowsAndColumns(Table table) {
        List<Header> headers = new ArrayList<>();
        headers.add(new Header("Pieza dental", true));
        headers.add(new Header("Cara", true));
        headers.add(new Header("Diagnóstico", true));
        headers.add(new Header("Tratamiento", true));
        table.setHeaders(headers);
    }

}
