package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.ClinicalMedicine;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.ComponentType;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.InputBox;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;

public class DiagnosticoYEvolucionesSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Diagnóstico y evoluciones")
            .componentType(ComponentType.SECTION_PAGE).order(4).iconId(1002).build();

    @Override
    public Section init() {
        section.addComponent(diagnostico());
        section.addComponent(estudiosSolicitados());
        section.addComponent(signosVitales());
        return section;
    }

    private Section diagnostico(){
        Section diagnostico = Section.builder().name("Diagnóstico").componentType(ComponentType.SECTION_CARD)
                .order(1).size(-1).build();
        diagnostico.addComponent(InputBox.builder().name("Diagnóstico presuntivo").componentType(ComponentType.INPUT_BOX).build());
        diagnostico.addComponent(InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build());
        return diagnostico;
    }

    private Section estudiosSolicitados(){
        Section estudiosSolicitados = Section.builder().name("Estudios solicitados").componentType(ComponentType.SECTION_CARD)
                .order(2).size(1).build();
        estudiosSolicitados.addComponent(InputBox.builder().name("Estudios").componentType(ComponentType.INPUT_BOX).build());
        estudiosSolicitados.addComponent(InputBox.builder().name("Resultados").componentType(ComponentType.TEXT_BOX).build());

        return estudiosSolicitados;
    }

    private Section signosVitales(){
        Section signosVitales = Section.builder().name("Signos vitales").componentType(ComponentType.SECTION_CARD)
                .order(1).size(-1).build();
        signosVitales.addComponent(InputBox.builder().name("Peso (kg)").componentType(ComponentType.INPUT_BOX).build());
        signosVitales.addComponent(InputBox.builder().name("Talla (cm)").componentType(ComponentType.INPUT_BOX).build());
        signosVitales.addComponent(InputBox.builder().name("IMC").componentType(ComponentType.INPUT_BOX).build());
        signosVitales.addComponent(InputBox.builder().name("Frecuencia cardíaca").componentType(ComponentType.INPUT_BOX).build());
        signosVitales.addComponent(InputBox.builder().name("T.A. sistólica").componentType(ComponentType.INPUT_BOX).build());
        signosVitales.addComponent(InputBox.builder().name("T.A. diastólica").componentType(ComponentType.INPUT_BOX).build());
        signosVitales.addComponent(InputBox.builder().name("Temperatura corporal (ºC)").componentType(ComponentType.INPUT_BOX).build());
        signosVitales.addComponent(InputBox.builder().name("Saturación de oxígeno (%)").componentType(ComponentType.INPUT_BOX).build());

        return signosVitales;
    }
}
