package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Nutrition;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Header;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NutritionCommonUtils {

    public static List<Header> headersDieta() {
        List<Header> headers = new ArrayList<>();
        headers.add(new Header("Comida", false));
        headers.add(new Header("Domingo", true));
        headers.add(new Header("Lunes", true));
        headers.add(new Header("Martes", true));
        headers.add(new Header("Miércoles", true));
        headers.add(new Header("Jueves", true));
        headers.add(new Header("Viernes", true));
        headers.add(new Header("Sábado", true));
        return headers;
    }

    public static List<List<String>> elementsDieta() {
        List<List<String>> elements = new ArrayList<>();
        elements.add(Arrays.asList("Desayuno", "", "", "", "", "", "", ""));
        elements.add(Arrays.asList("Media mañana", "", "", "", "", "", "", ""));
        elements.add(Arrays.asList("Almuerzo", "", "", "", "", "", "", ""));
        elements.add(Arrays.asList("Merienda", "", "", "", "", "", "", ""));
        elements.add(Arrays.asList("Cena", "", "", "", "", "", "", ""));
        return elements;
    }

    public static List<String> enfermedadesOptions() {
        return Arrays.asList("Diabetes", "Hipertensión arterial",
                "Cáncer", "Obesidad", "Enfermedad cardiovascular",
                "Alergias (antitiroideos, penicilina, aspirinas, anticonvulsivos)");
    }

    public static List<String> alergiasEIntoleranciasOptions() {
        return Arrays.asList("Ninguna", "Huevos", "Pescado", "Mariscos", "Maní", "Nuez", "Trigo", "Soja", "Otros");
    }

    public static List<String> rapidezParaComerOptions() {
        return Arrays.asList("Rápido (10 min)", "Medio (20 min)", "Lento (30 min");
    }

    public static List<String> cualitativoOptions() {
        return Arrays.asList("Bueno", "Regular", "Malo");
    }
}
