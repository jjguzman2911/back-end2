package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.HashMap;

public class CustomComponentDeserializer extends StdDeserializer<Component> {

    private final HashMap<Integer, Class<?>> componentTypeToClass;

    protected CustomComponentDeserializer() {
        super(Component.class);
        componentTypeToClass = new HashMap<>();
        componentTypeToClass.put(0, Section.class);
        componentTypeToClass.put(1, ListBox.class);
        componentTypeToClass.put(2, ListBox.class);
        componentTypeToClass.put(3, ListBox.class);
        componentTypeToClass.put(4, InputBox.class);
        componentTypeToClass.put(5, Section.class);
        componentTypeToClass.put(6, Section.class);
        componentTypeToClass.put(7, InputBox.class);
        componentTypeToClass.put(8, Table.class);
        componentTypeToClass.put(9, File.class);
        componentTypeToClass.put(10, Table.class);
        componentTypeToClass.put(11, Odontogram.class);
    }

    @Override
    public Component deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        ObjectMapper objectMapper = (ObjectMapper) p.getCodec();
        ObjectNode object = objectMapper.readTree(p);
        JsonNode valueNode = object.findValue("componentType");
        for (Integer componentType : componentTypeToClass.keySet()) {
            if (componentType.equals(valueNode.asInt())) {
                return deserialize(objectMapper, componentType, object);
            }
        }
        throw new IllegalArgumentException("error inferring to which class to deserialize " + object);
    }

    private Component deserialize(ObjectMapper objectMapper,
                                  Integer componentType,
                                  ObjectNode object) throws IOException {
        return (Component) objectMapper.treeToValue(object, componentTypeToClass.get(componentType));
    }
}
