package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Nutrition;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;

public class HabitosAlimenticiosSection implements IMedicalRecordSection {
    private final Section section = Section.builder().name("Hábitos alimenticios").componentType(ComponentType.SECTION_PAGE)
            .order(3).iconId(3000).build();

    @Override
    public Section init() {
        section.addComponent(historiaAlimentaria());
        section.addComponent(habitosAlimenticios());
        section.addComponent(dietaHabitual());
        return section;
    }

    private Section historiaAlimentaria() {
        Section suplementos = Section.builder().name("Suplementos")
                .componentType(ComponentType.SECTION_SEPARATOR).order(1).build();
        Component consume = ListBox.builder().name("Consume").componentType(ComponentType.RADIO_BUTTON)
                .order(1).options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component cual = InputBox.builder().name("Cuáles").componentType(ComponentType.TEXT_BOX).build();
        Component dosis = InputBox.builder().name("Dosis").componentType(ComponentType.TEXT_BOX).build();
        Component porque = InputBox.builder().name("Por qué").componentType(ComponentType.TEXT_BOX).build();
        suplementos.addComponent(consume);
        suplementos.addComponent(cual);
        suplementos.addComponent(dosis);
        suplementos.addComponent(porque);

        Section medicamentosParaAdelgazar = Section.builder().name("Medicamentos para adelgazar")
                .componentType(ComponentType.SECTION_SEPARATOR).order(2).build();
        medicamentosParaAdelgazar.addComponent(consume);
        medicamentosParaAdelgazar.addComponent(cual);
        medicamentosParaAdelgazar.addComponent(porque);

        Section dietasAnteriores = Section.builder().name("Dietas anteriores")
                .componentType(ComponentType.SECTION_SEPARATOR).order(3).build();
        Component tuvo = ListBox.builder().name("Tuvo").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        dietasAnteriores.addComponent(tuvo);
        dietasAnteriores.addComponent(cual);
        dietasAnteriores.addComponent(porque);

        Section consumoDeCafe = Section.builder().name("Consumo de café")
                .componentType(ComponentType.SECTION_SEPARATOR).order(4).build();
        Component frecuencia = ListBox.builder().name("Frecuencia").componentType(ComponentType.COMBO_BOX)
                .options(MedicalRecordCommonUtils.frecuenciaOptions()).build();
        Component cantidad = InputBox.builder().name("Cantidad").componentType(ComponentType.INPUT_BOX).build();
        consumoDeCafe.addComponent(consume);
        consumoDeCafe.addComponent(frecuencia);
        consumoDeCafe.addComponent(cantidad);

        Section alergiasEIntolerancia = Section.builder().name("Alergias e intolerancia")
                .componentType(ComponentType.SECTION_SEPARATOR).order(5).build();
        Component cuales = ListBox.builder().name("Cuáles")
                .componentType(ComponentType.CHECK_BOX).options(NutritionCommonUtils.alergiasEIntoleranciasOptions()).build();
        Component otros = InputBox.builder().name("Otros").componentType(ComponentType.TEXT_BOX).build();
        alergiasEIntolerancia.addComponent(cuales);
        alergiasEIntolerancia.addComponent(otros);

        Section historiaAlimentaria = Section.builder().name("Historia alimentaria").componentType(ComponentType.SECTION_CARD)
                .order(1).size(-1).build();
        historiaAlimentaria.addComponent(suplementos);
        historiaAlimentaria.addComponent(medicamentosParaAdelgazar);
        historiaAlimentaria.addComponent(dietasAnteriores);
        historiaAlimentaria.addComponent(consumoDeCafe);
        historiaAlimentaria.addComponent(alergiasEIntolerancia);

        return historiaAlimentaria;
    }

    private Section habitosAlimenticios() {
        Component compraAlimentos = InputBox.builder().name("Responsable de comprar los alimentos")
                .componentType(ComponentType.INPUT_BOX).order(1).build();

        Component preparaAlimentos = InputBox.builder().name("Responsable de preparar los alimentos")
                .componentType(ComponentType.INPUT_BOX).order(2).build();

        Component horaHabitualDeLevantarse = InputBox.builder().name("Hora habitual de levantarse")
                .componentType(ComponentType.INPUT_BOX).order(3).build();
        Component horaHabitualDeAcostarse = InputBox.builder().name("Hora habitual de acostarse")
                .componentType(ComponentType.INPUT_BOX).order(4).build();

        Section habitoDePicoteo = Section.builder().name("Hábito de picoteo").componentType(ComponentType.SECTION_SEPARATOR)
                .order(5).build();
        Component existe = ListBox.builder().name("Existe").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component alimentos = InputBox.builder().name("Alimentos que picotea").componentType(ComponentType.TEXT_BOX).build();
        habitoDePicoteo.addComponent(existe);
        habitoDePicoteo.addComponent(alimentos);

        Component rapidezParaComer = ListBox.builder().name("Rapidez para comer").componentType(ComponentType.COMBO_BOX)
                .options(NutritionCommonUtils.rapidezParaComerOptions()).order(6).build();

        Component apetito = ListBox.builder().name("Apetito").componentType(ComponentType.COMBO_BOX)
                .order(7).options(NutritionCommonUtils.cualitativoOptions()).build();

        Component horaDeMayorHambre = InputBox.builder().name("Hora de mayor hambre")
                .componentType(ComponentType.INPUT_BOX).order(8).build();
        Component alimentosFavoritos = InputBox.builder().name("Alimentos favoritos")
                .componentType(ComponentType.TEXT_BOX).order(9).build();
        Component alimentosRechazados = InputBox.builder().name("Alimentos rechazados")
                .componentType(ComponentType.TEXT_BOX).order(10).build();
        Component consumoDeAgua = InputBox.builder().name("Consumo de agua")
                .componentType(ComponentType.INPUT_BOX).order(11).build();
        Component consumoDeSal = InputBox.builder().name("Consumo de sal")
                .componentType(ComponentType.INPUT_BOX).order(12).build();


        Section habitosAlimenticios = Section.builder().name("Hábitos alimenticios").componentType(ComponentType.SECTION_CARD)
                .order(2).size(1).build();
        habitosAlimenticios.addComponent(compraAlimentos);
        habitosAlimenticios.addComponent(preparaAlimentos);
        habitosAlimenticios.addComponent(horaHabitualDeLevantarse);
        habitosAlimenticios.addComponent(horaHabitualDeAcostarse);
        habitosAlimenticios.addComponent(habitoDePicoteo);
        habitosAlimenticios.addComponent(rapidezParaComer);
        habitosAlimenticios.addComponent(apetito);
        habitosAlimenticios.addComponent(horaDeMayorHambre);
        habitosAlimenticios.addComponent(alimentosFavoritos);
        habitosAlimenticios.addComponent(alimentosRechazados);
        habitosAlimenticios.addComponent(consumoDeAgua);
        habitosAlimenticios.addComponent(consumoDeSal);

        return habitosAlimenticios;
    }

    private Section dietaHabitual() {
        Table table = Table.builder().name("Dieta habitual").componentType(ComponentType.FIXED_TABLE)
                .headers(NutritionCommonUtils.headersDieta())
                .elements(NutritionCommonUtils.elementsDieta()).build();

        Section dietaHabitual = Section.builder().name("Dieta habitual")
                .componentType(ComponentType.SECTION_CARD).size(0).order(3).build();
        dietaHabitual.addComponent(table);
        return dietaHabitual;
    }

}
