package com.Medisoft.Medisoft.Entities.MedicalRecordManagement;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.ClinicalMedicine.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Dentistry.IndicadoresOdontologicosSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Dentistry.OdontogramaYDiagnosticoSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Nutrition.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Ophthalmology.DiagnosticoYTratamientoSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Ophthalmology.ExamenOftalmologicoSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Ophthalmology.IndicadoresClinicosOftalmologicosSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Psychology.HistoriaPersonalYFamiliarSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Psychology.PsicodiagnosticoYPlanDeOrientacionSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Psychology.SeguimientoSection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class MedicalRecordInitService {

    public List<Section> getFirstMedicalRecordStructure(int specialityId) {
        switch (specialityId) {
            case 1:
                return getClinicalMedicineRecordStructure();
            case 2:
                return getDentistryMedicalRecordStructure();
            case 3:
                return getNutritionRecordStructure();
            case 4:
                return getPsychologyRecordStructure();
            case 5:
                return getOphthalmologyRecordStructure();
            default:
                return getDefault();
        }
    }

    private List<Section> getClinicalMedicineRecordStructure() {
        log.info("Clinical medicine record structure found");
        List<Section> clinicalMedicineRecordList = new ArrayList<>();
        clinicalMedicineRecordList.add(new com.Medisoft.Medisoft.Entities.MedicalRecordManagement.ClinicalMedicine.DatosPersonalesYDeConsultaSection().init());
        clinicalMedicineRecordList.add(new IndicadoresClinicos1Section().init());
        clinicalMedicineRecordList.add(new IndicadoresClinicos2Section().init());
        clinicalMedicineRecordList.add(new DiagnosticoYEvolucionesSection().init());
        clinicalMedicineRecordList.add(new MedicamentosSection().init());
        clinicalMedicineRecordList.add(new RevisionesPorSistemasSection().init());
        return clinicalMedicineRecordList;
    }

    private List<Section> getDentistryMedicalRecordStructure(){
        log.info("Dentistry record structure found");
        List<Section> dentistryMedicalRecordList = new ArrayList<>();
        dentistryMedicalRecordList.add(new com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Dentistry.DatosPersonalesYDeConsultaSection().init());
        dentistryMedicalRecordList.add(new com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Dentistry.IndicadoresClinicosSection().init());
        dentistryMedicalRecordList.add(new IndicadoresOdontologicosSection().init());
        dentistryMedicalRecordList.add(new OdontogramaYDiagnosticoSection().init());
        dentistryMedicalRecordList.add(new com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Dentistry.MedicamentosSection().init());
        return dentistryMedicalRecordList;
    }

    private List<Section> getNutritionRecordStructure() {
        log.info("Nutrition record structure found");
        List<Section> nutritionRecordList = new ArrayList<>();
        nutritionRecordList.add(new DatosPersonalesYDeConsultaSection().init());
        nutritionRecordList.add(new IndicadoresClinicosSection().init());
        nutritionRecordList.add(new HabitosAlimenticiosSection().init());
        nutritionRecordList.add(new DatosAntropometricosSection().init());
        nutritionRecordList.add(new PlanNutricionalSection().init());
        nutritionRecordList.add(new RecomendacionesFisicasNutricionalesSection().init());
        return nutritionRecordList;
    }

    private List<Section> getPsychologyRecordStructure() {
        log.info("Psychology record structure found");
        List<Section> psychologyRecordList = new ArrayList<>();
        psychologyRecordList.add(new DatosPersonalesYDeConsultaSection().init());
        psychologyRecordList.add(new HistoriaPersonalYFamiliarSection().init());
        psychologyRecordList.add(new com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Psychology.IndicadoresClinicosSection().init());
        psychologyRecordList.add(new PsicodiagnosticoYPlanDeOrientacionSection().init());
        psychologyRecordList.add(new SeguimientoSection().init());
        return psychologyRecordList;
    }

    public List<Section> getOphthalmologyRecordStructure() {
        log.info("Ophthalmology record structure found");
        List<Section> ophthalmologyRecordList = new ArrayList<>();
        ophthalmologyRecordList.add(new com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Ophthalmology.DatosPersonalesYDeConsultaSection().init());
        ophthalmologyRecordList.add(new IndicadoresClinicosOftalmologicosSection().init());
        ophthalmologyRecordList.add(new ExamenOftalmologicoSection().init());
        ophthalmologyRecordList.add(new DiagnosticoYTratamientoSection().init());
        return ophthalmologyRecordList;
    }

    private List<Section> getDefault() {
        log.warn("No record structure found");
        return new ArrayList<>();
    }
}
