package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Nutrition;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Component;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.ComponentType;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.InputBox;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import lombok.Data;

@Data
public class RecomendacionesFisicasNutricionalesSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Recomendaciones físicas nutricionales")
            .componentType(ComponentType.SECTION_PAGE).order(6).iconId(3003).build();

    @Override
    public Section init() {
        section.addComponent(recomendacionesFisicasNutricionales());
        return section;
    }

    private Section recomendacionesFisicasNutricionales() {
        Component actividadesFisicas = InputBox.builder().name("Actividades físicas")
                .componentType(ComponentType.TEXT_BOX).order(1).build();

        Component ingestaDeAgua = InputBox.builder().name("Ingesta de agua")
                .componentType(ComponentType.TEXT_BOX).order(2).build();

        Component alimentosAEvitar = InputBox.builder().name("Alimentos a evitar")
                .componentType(ComponentType.TEXT_BOX).order(3).build();

        Component recomendacionesAdicionales = InputBox.builder().name("Recomendaciones adicionales")
                .componentType(ComponentType.TEXT_BOX).order(4).build();

        Section recomendacionesFisicasNutricionales = Section.builder().name("Recomendaciones físicas nutricionales")
                .componentType(ComponentType.SECTION_CARD).order(1).size(-1).build();
        recomendacionesFisicasNutricionales.addComponent(actividadesFisicas);
        recomendacionesFisicasNutricionales.addComponent(ingestaDeAgua);
        recomendacionesFisicasNutricionales.addComponent(alimentosAEvitar);
        recomendacionesFisicasNutricionales.addComponent(recomendacionesAdicionales);

        return recomendacionesFisicasNutricionales;
    }
}
