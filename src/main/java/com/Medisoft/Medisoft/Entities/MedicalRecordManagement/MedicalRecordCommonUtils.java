package com.Medisoft.Medisoft.Entities.MedicalRecordManagement;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Nutrition.NutritionCommonUtils;

import java.util.Arrays;
import java.util.List;

public class MedicalRecordCommonUtils {

    public static List<String> frecuenciaOptions() {
        return Arrays.asList("Nunca", "Ocasional (1 vez por semana)", "Frecuente (3 veces por semana)",
                "Muy frecuente (5 vez por semana)", "Siempre");
    }

    public static List<String> siNoOptions() {
        return Arrays.asList("Si", "No");
    }

    public static Section antecedentesPersonalesNoPatologicos(int sectionSize) {
        ListBox frecuencias = ListBox.builder().name("Frecuencia").componentType(ComponentType.COMBO_BOX)
                .order(1).options(MedicalRecordCommonUtils.frecuenciaOptions()).build();

        InputBox cantidad = InputBox.builder().name("Cantidad").componentType(ComponentType.TEXT_BOX)
                .order(2).build();

        Section alcohol = Section.builder().name("Alcohol").componentType(ComponentType.SECTION_SEPARATOR)
                .order(1).build();
        alcohol.addComponent(frecuencias);
        alcohol.addComponent(cantidad);

        Section tabaco = Section.builder().name("Tabaco").componentType(ComponentType.SECTION_SEPARATOR)
                .order(2).build();
        tabaco.addComponent(frecuencias);
        tabaco.addComponent(cantidad);

        Section estupefacientes = Section.builder().name("Estupefacientes").componentType(ComponentType.SECTION_SEPARATOR)
                .order(3).build();
        estupefacientes.addComponent(frecuencias);
        estupefacientes.addComponent(cantidad);

        InputBox observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX)
                .order(4).build();

        Section antecedentesPersonalesNoPatologicos = Section.builder().name("Antecedentes personales no patológicos")
                .componentType(ComponentType.SECTION_CARD).order(3).size(sectionSize).build();
        antecedentesPersonalesNoPatologicos.addComponent(alcohol);
        antecedentesPersonalesNoPatologicos.addComponent(tabaco);
        antecedentesPersonalesNoPatologicos.addComponent(estupefacientes);
        antecedentesPersonalesNoPatologicos.addComponent(observaciones);

        return antecedentesPersonalesNoPatologicos;
    }

    public static Section antecedentesHeredofamiliares(int sectionSize) {
        ListBox enfermedades = ListBox.builder().name("Enfermedades").componentType(ComponentType.CHECK_BOX)
                .order(1).options(NutritionCommonUtils.enfermedadesOptions()).build();

        InputBox otrasEnfermedades = InputBox.builder().name("Otras enfermedades").componentType(ComponentType.TEXT_BOX)
                .order(2).build();

        Section antecedentesHeredofamiliares = Section.builder().name("Antecedentes heredofamiliares en primer grado")
                .componentType(ComponentType.SECTION_CARD).order(2).size(sectionSize).build();
        antecedentesHeredofamiliares.addComponent(enfermedades);
        antecedentesHeredofamiliares.addComponent(otrasEnfermedades);
        return antecedentesHeredofamiliares;
    }

    public static Section antecedentesPersonales(int sectionSize) {
        List<String> listFuncionesIntestinales = Arrays.asList("Diarrea", "Normal", "Estreñimiento");
        ListBox funcionIntestinal = ListBox.builder().name("Función intestinal").componentType(ComponentType.COMBO_BOX)
                .order(1).options(listFuncionesIntestinales).build();

        InputBox agregarComentario = InputBox.builder().name("Agregar comentario").componentType(ComponentType.TEXT_BOX)
                .order(2).build();

        ListBox enfermedades = ListBox.builder().name("Enfermedades").componentType(ComponentType.CHECK_BOX)
                .order(1).options(NutritionCommonUtils.enfermedadesOptions()).build();

        List<String> listTipoSanguineo = Arrays.asList("A+", "A-", "AB+", "AB-", "B+", "B-", "O+", "O-");
        ListBox tipoSanguineo = ListBox.builder().name("Tipo sanguíneo").componentType(ComponentType.COMBO_BOX)
                .order(2).options(listTipoSanguineo).build();

        Section enfermedadesSection = Section.builder().name("Enfermedades")
                .componentType(ComponentType.SECTION_SEPARATOR).order(3).size(0).build();
        enfermedadesSection.addComponent(enfermedades);
        enfermedadesSection.addComponent(tipoSanguineo);

        Section antecedentesPersonales = Section.builder().name("Antecedentes personales")
                .componentType(ComponentType.SECTION_CARD).order(1).size(sectionSize).build();
        antecedentesPersonales.addComponent(funcionIntestinal);
        antecedentesPersonales.addComponent(agregarComentario);
        antecedentesPersonales.addComponent(enfermedadesSection);
        return antecedentesPersonales;
    }

    public static Section datosPersonales(int sectionSize) {
        InputBox nivelDeInstruccion = InputBox.builder().name("Nivel de instrucción").componentType(ComponentType.TEXT_BOX)
                .order(1).build();

        InputBox ocupacion = InputBox.builder().name("Ocupación").componentType(ComponentType.TEXT_BOX)
                .order(2).build();

        ListBox estadoCivil = ListBox.builder().name("Estado civil").componentType(ComponentType.COMBO_BOX)
                .order(3).options(estadoCivilOptions()).build();

        ListBox hijos = ListBox.builder().name("Hijos").componentType(ComponentType.COMBO_BOX)
                .order(4).options(MedicalRecordCommonUtils.siNoOptions()).build();

        ListBox cantidad = ListBox.builder().name("Cantidad").componentType(ComponentType.COMBO_BOX)
                .order(5).options(cantidadOptions()).build();

        InputBox paisDeOrigen = InputBox.builder().name("País de origen").componentType(ComponentType.TEXT_BOX)
                .order(6).build();

        InputBox religion = InputBox.builder().name("Religión").componentType(ComponentType.TEXT_BOX)
                .order(7).build();

        Section datosPersonales = Section.builder().name("Datos personales").componentType(ComponentType.SECTION_CARD)
                .order(1).size(sectionSize).build();
        datosPersonales.addComponent(nivelDeInstruccion);
        datosPersonales.addComponent(ocupacion);
        datosPersonales.addComponent(estadoCivil);
        datosPersonales.addComponent(hijos);
        datosPersonales.addComponent(cantidad);
        datosPersonales.addComponent(paisDeOrigen);
        datosPersonales.addComponent(religion);

        return datosPersonales;
    }

    public static Section antecedentesEpidemiologicos(int order, int size){
        Component enfermedadesGeograficas = ListBox.builder().name("Enfermedades geograficas")
                .componentType(ComponentType.CHECK_BOX).options(Arrays.asList("No posee", "Chagas", "Amebiasis",
                        "Plaudismo", "Parasitosis intestinal", "Tuberculosis")).build();
        Component ets = ListBox.builder().name("ETS").componentType(ComponentType.CHECK_BOX)
                .options(Arrays.asList("No posee", "Sífilis", "Gonorrea", "Clamidiasis", "VIH", "HPV", "Hepatitis",
                        "Herpes genital", "Tricomoniasis")).build();

        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesEpidemiologicos = Section.builder().name("Antecedentes epidemiológicos")
                .componentType(ComponentType.SECTION_CARD).order(order).size(size).build();
        antecedentesEpidemiologicos.addComponent(enfermedadesGeograficas);
        antecedentesEpidemiologicos.addComponent(ets);
        antecedentesEpidemiologicos.addComponent(observaciones);
        return antecedentesEpidemiologicos;
    }

    public static List<String> cantidadOptions() {
        return Arrays.asList("0", "1", "2", "3", "4 o más");
    }

    public static List<String> estadoCivilOptions() {
        return Arrays.asList("Soltero", "Casado", "Divorciado", "Viudo");
    }

    public static Section medicamentosSection(int order, int size) {
        List<Header> headerList = Arrays.asList(
                new Header("Medicamento", true),
                new Header("Frecuencia", true),
                new Header("Duración", true),
                new Header("Vía de administración", true)
        );
        Table medicamentosTable = Table.builder().name("Medicamentos").componentType(ComponentType.EDITABLE_TABLE)
                .headers(headerList).build();

        Section medicamentos = Section.builder().name("Medicamentos").componentType(ComponentType.SECTION_CARD)
                .order(order).size(size).build();
        medicamentos.addComponent(medicamentosTable);
        return medicamentos;
    }
}
