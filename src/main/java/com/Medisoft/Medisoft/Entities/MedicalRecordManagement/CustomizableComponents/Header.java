package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Header {
    private String label;
    private boolean editable;
}
