package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.ClinicalMedicine;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Nutrition.NutritionCommonUtils;

import java.util.Arrays;


public class IndicadoresClinicos1Section implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Indicadores clínicos 1")
            .componentType(ComponentType.SECTION_PAGE).order(2).iconId(1001).build();

    @Override
    public Section init() {
        section.addComponent(antecedentesPersonales());
        section.addComponent(antecedentesPersonalesNoPatologicos());
        section.addComponent(antecedentesHeredofamiliares());
        section.addComponent(antecedentesInmunoalergicos());
        return section;
    }

    private Section antecedentesPersonales() {
        Component enfermedadesInfantiles = ListBox.builder().name("Enfermedades infantiles").componentType(ComponentType.COMBO_BOX)
                .options(Arrays.asList("No posee", "Sarampión", "Varicela", "Papera")).build();

        Component enfermedadesDeAdultos = ListBox.builder().name("Enfermedades de adultos").componentType(ComponentType.COMBO_BOX)
                .options(Arrays.asList("No posee", "IAM", "Enfermedad coronaria (IAM)", "HTA", "Arritmia", "Aneurisma",
                        "Aorta abdominal", "Enfermedad vascular periférica", "Insuficiencia cardíaca", "ACV isquémico",
                        "ACV hemorrágico", "Epoc", "Asma", "Prostatismo", "TEPA", "Cáncer", "Vasculitis")).build();

        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX)
                .build();

        Component transfusiones = ListBox.builder().name("Transfusiones").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();

        Component fechaDeUltimaTransfusion = InputBox.builder().name("Fecha de última tranfusión")
                .componentType(ComponentType.INPUT_BOX).build();

        Component unidadesTransfundidas = InputBox.builder().name("Unidades transfundidas")
                .componentType(ComponentType.INPUT_BOX).build();

        Component ingresosHospitalariosNoQuirurgicos = InputBox.builder().name("Ingresos hospitalarios no quirúrgicos")
                .componentType(ComponentType.TEXT_BOX).build();


        Section antecedentesPersonales = Section.builder().name("Antecedentes personales")
                .componentType(ComponentType.SECTION_CARD).order(1).size(-1).build();
        antecedentesPersonales.addComponent(enfermedadesInfantiles);
        antecedentesPersonales.addComponent(enfermedadesDeAdultos);
        antecedentesPersonales.addComponent(observaciones);
        antecedentesPersonales.addComponent(transfusiones);
        antecedentesPersonales.addComponent(fechaDeUltimaTransfusion);
        antecedentesPersonales.addComponent(unidadesTransfundidas);
        antecedentesPersonales.addComponent(ingresosHospitalariosNoQuirurgicos);
        return antecedentesPersonales;
    }

    private Section antecedentesPersonalesNoPatologicos(){
        return MedicalRecordCommonUtils.antecedentesPersonalesNoPatologicos(1);
    }

    private Section antecedentesHeredofamiliares(){
        Component enfermedades = ListBox.builder().name("Enfermedades").componentType(ComponentType.CHECK_BOX)
                .options(Arrays.asList("Asma", "HTA", "Diabetes", "IAM", "Cardiopatía isquémica", "ACV isquémico",
                        "ACV hemorrágico", "Cáncer", "Enfermedad tiroidea")).build();

        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();


        Section antecedentesHeredofamiliares = Section.builder().name("Antecedentes heredofamiliares")
                .componentType(ComponentType.SECTION_CARD).order(3).size(-1).build();
        antecedentesHeredofamiliares.addComponent(enfermedades);
        antecedentesHeredofamiliares.addComponent(observaciones);

        return antecedentesHeredofamiliares;
    }

    private Section antecedentesInmunoalergicos(){
        Component alergiasFarmacologicas = ListBox.builder().name("Alergias farmacológicas")
                .componentType(ComponentType.CHECK_BOX).options(Arrays.asList("No posee", "Antitiroideos", "Penicilina",
                        "Aspirina", "Antiinflamatorios", "Iodo")).build();

        Component alergiasAlimenticias = ListBox.builder().name("Alergias alimenticias")
                .componentType(ComponentType.CHECK_BOX).options(NutritionCommonUtils.alergiasEIntoleranciasOptions()).build();

        Component observaciones = InputBox.builder().name("Observaciones").componentType(ComponentType.TEXT_BOX).build();
        Component vacunaciones = InputBox.builder().name("Vacunaciones").componentType(ComponentType.TEXT_BOX).build();

        Section antecedentesInmunoalergicos = Section.builder().name("Antecedentes inmunoalérgicos")
                .componentType(ComponentType.SECTION_CARD).order(4).size(1).build();
        antecedentesInmunoalergicos.addComponent(alergiasFarmacologicas);
        antecedentesInmunoalergicos.addComponent(alergiasAlimenticias);
        antecedentesInmunoalergicos.addComponent(observaciones);
        antecedentesInmunoalergicos.addComponent(vacunaciones);
        return antecedentesInmunoalergicos;
    }
}
