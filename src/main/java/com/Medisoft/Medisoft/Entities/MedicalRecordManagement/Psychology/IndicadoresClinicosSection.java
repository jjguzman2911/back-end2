package com.Medisoft.Medisoft.Entities.MedicalRecordManagement.Psychology;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.*;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.IMedicalRecordSection;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordCommonUtils;

public class IndicadoresClinicosSection implements IMedicalRecordSection {

    private final Section section = Section.builder().name("Indicadores clínicos").componentType(ComponentType.SECTION_PAGE)
            .order(3).iconId(1001).build();

    @Override
    public Section init() {
        section.addComponent(antecedentesPersonalesNoPatologicos());
        section.addComponent(antecedentesPersonales());
        section.addComponent(antecedentesHeredofamiliares());
        return section;
    }

    private Section antecedentesPersonalesNoPatologicos() {
        return MedicalRecordCommonUtils.antecedentesPersonalesNoPatologicos(-1);
    }

    private Section antecedentesPersonales() {
        Component toma = ListBox.builder().name("Toma").componentType(ComponentType.RADIO_BUTTON)
                .options(MedicalRecordCommonUtils.siNoOptions()).build();
        Component informacionDeConsumo = InputBox.builder().name("Información de consumo")
                .componentType(ComponentType.TEXT_BOX).build();
        Section medicacionPsiquiatricaSeparator = Section.builder().name("Medicación psiquiátrica")
                .componentType(ComponentType.SECTION_SEPARATOR).order(4).build();
        medicacionPsiquiatricaSeparator.addComponent(toma);
        medicacionPsiquiatricaSeparator.addComponent(informacionDeConsumo);

        Section antecedentesPersonales = MedicalRecordCommonUtils.antecedentesPersonales(1);
        antecedentesPersonales.addComponent(medicacionPsiquiatricaSeparator);
        return antecedentesPersonales;
    }

    private Section antecedentesHeredofamiliares() {
        return MedicalRecordCommonUtils.antecedentesHeredofamiliares(-1);
    }

}
