package com.Medisoft.Medisoft.Entities.MedicalRecordManagement;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;

public interface IMedicalRecordSection {

    Section init();
}
