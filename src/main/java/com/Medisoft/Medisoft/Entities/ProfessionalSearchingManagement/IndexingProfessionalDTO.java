package com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement;

import com.Medisoft.Medisoft.Entities.UserManagement.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class IndexingProfessionalDTO {
    private Integer id; //user id
    private String fullName; //name + surname
    private String name;
    private String surname;
    private String specialty; //specialty name

    public IndexingProfessionalDTO(User user) {
        setId(user.getId());
        setFullName(user.getPersona().getFullName());
        setSpecialty(user.getProfesional().getEspecialidad().getNombre());
        setName(user.getPersona().getNombre());
        setSurname(user.getPersona().getApellido());
    }
}
