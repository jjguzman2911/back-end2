package com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement;


import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Paths;


@Slf4j
@Component
public class IndexEngine {

    @Autowired
    private IndexGuardian indexGuardian;

    private IndexWriter writer;

    private void createWriter() {
        try {
            FSDirectory dir = FSDirectory.open(Paths.get(indexGuardian.getINDEX_DIR()));
            IndexWriterConfig config = new IndexWriterConfig(new StandardAnalyzer());
            this.writer = new IndexWriter(dir, config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Indexa un documento a la base documental.
     *
     * @param document representa el documento a indexar.
     */
    public void index(Document document) {
        try {
            if (!this.writer.isOpen()) {
                this.createWriter();
            }
            this.writer.addDocument(document);
            this.writer.commit();
            this.writer.close();
            indexGuardian.setNewIndexChange(true);
            log.info("Document {} indexed", document.get("id"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Desindexa de la BD documental un documento por su ID.-
     *
     * @param document representa el documento a desindexa.-
     */
    public void deindex(Document document) {
        try {
            if (this.writer == null || !this.writer.isOpen()) {
                this.createWriter();
            }
            this.writer.deleteDocuments(new Term("id", document.get("id")));
            this.writer.commit();
            this.writer.close();
            indexGuardian.setNewIndexChange(true);
            log.info("Document {} deindexed", document.get("id"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Document createProfessionalDocument(Integer id, String name, String surname, String specialty) {
        Document document = new Document();
        document.add(new StringField("id", id.toString(), Field.Store.YES));
        document.add(new TextField("name", name, Field.Store.YES));
        document.add(new TextField("surname", surname, Field.Store.YES));
        document.add(new TextField("specialty", specialty, Field.Store.YES));

        return document;
    }
}
