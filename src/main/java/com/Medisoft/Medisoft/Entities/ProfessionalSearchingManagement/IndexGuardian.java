package com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class IndexGuardian {
    private final String INDEX_DIR = "./src/main/resources/index";
    private boolean newIndexChange = false;
}
