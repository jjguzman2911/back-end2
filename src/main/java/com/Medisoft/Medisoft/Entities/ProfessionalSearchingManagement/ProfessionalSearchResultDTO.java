package com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfessionalSearchResultDTO {
    private List<ProfessionalSearchDTO> professionals;
    private Set<HealthInsuranceFilterDTO> healthInsurances;
}
