package com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import com.Medisoft.Medisoft.Utilities.FilteringAndSortingProfessionalsUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProfessionalSearchService {

    @Autowired
    private SearchDocumentService searchDocumentService;

    @Autowired
    private MapperProfessionalScoreService mapperProfessionalScoreService;


    public ProfessionalSearchResultDTO searchProfessionals(int maxDocs, String fullName, String specialty, String orderBy, HealthInsuranceDTO healthInsuranceDTO) {
        log.info("Finding for [name={}, specialty={}, orderBy={}, healthInsurances={}]",
                fullName, specialty, orderBy, healthInsuranceDTO.toString());
        Map<Integer, Float> scoreMap = searchDocumentService.searchProfessionalByNameAndSpecialty(fullName, specialty, maxDocs);
        return findFilterAndSortProfessionals(scoreMap, healthInsuranceDTO, orderBy);
    }

    public ProfessionalSearchResultDTO searchProfessionalsByAllSpecialties(int maxDocs, String fullName, String orderBy, HealthInsuranceDTO healthInsuranceDTO) {
        log.info("Finding for [name={}, specialty=all, orderBy={}, healthInsurance={}]",
                fullName, orderBy, healthInsuranceDTO.toString());
        Map<Integer, Float> scoreMap = searchDocumentService.searchProfessionalByName(fullName, maxDocs);
        return findFilterAndSortProfessionals(scoreMap, healthInsuranceDTO, orderBy);
    }

    private ProfessionalSearchResultDTO findFilterAndSortProfessionals(Map<Integer, Float> scoreMap, HealthInsuranceDTO healthInsuranceDTO, String orderBy) {
        List<ProfessionalSearchDTO> professionalSearchDTOS = mapperProfessionalScoreService.findProfessionalsAndSortByRelevance(scoreMap);
        Set<HealthInsuranceFilterDTO> professionalsAmountByHealthInsurance =
                buildHealthInsuranceFilterSet(professionalSearchDTOS);
        FilteringAndSortingProfessionalsUtil.filterAndSortProfessionals(professionalSearchDTOS, healthInsuranceDTO, orderBy);
        log.info("{} professionals were found for selected criteria", professionalSearchDTOS.size());
        return new ProfessionalSearchResultDTO(professionalSearchDTOS, professionalsAmountByHealthInsurance);
    }

    private Set<HealthInsuranceFilterDTO> buildHealthInsuranceFilterSet(List<ProfessionalSearchDTO> professionalSearchDTOS) {
        Map<HealthInsuranceDTO, Integer> professionalsAmountByHealthInsurance = new HashMap<>();
        for (ProfessionalSearchDTO professionalSearchDTO : professionalSearchDTOS) {
            for (HealthInsuranceDTO healthInsuranceDTO : professionalSearchDTO.getHealthInsuranceDTOS()) {
                if (professionalsAmountByHealthInsurance.containsKey(healthInsuranceDTO)) {
                    int amount = professionalsAmountByHealthInsurance.get(healthInsuranceDTO);
                    professionalsAmountByHealthInsurance.replace(healthInsuranceDTO, amount, amount + 1);
                } else {
                    professionalsAmountByHealthInsurance.put(healthInsuranceDTO, 1);
                }
            }
        }
        return mapToHealthInsuranceFilterSet(professionalsAmountByHealthInsurance);
    }

    private Set<HealthInsuranceFilterDTO> mapToHealthInsuranceFilterSet(Map<HealthInsuranceDTO, Integer> professionalsAmountByHealthInsurance) {
        return professionalsAmountByHealthInsurance.entrySet().stream()
                .sorted(Map.Entry.<HealthInsuranceDTO, Integer>comparingByValue().reversed())
                .map(healthInsuranceDTOIntegerEntry -> new HealthInsuranceFilterDTO(
                        healthInsuranceDTOIntegerEntry.getKey(),
                        healthInsuranceDTOIntegerEntry.getValue()))
                .collect(Collectors.toSet());
    }
}
