package com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Paths;

@Component
public class SearchEngine {

    @Autowired
    private IndexGuardian indexGuardian;

    private IndexSearcher searcher;


    private void createSearcher() {
        try {
            Directory dir = FSDirectory.open(Paths.get(indexGuardian.getINDEX_DIR()));
            IndexReader reader = DirectoryReader.open(dir);
            searcher = new IndexSearcher(reader);
            indexGuardian.setNewIndexChange(false);
        } catch (IOException e) {
            e.printStackTrace();
            throw new com.Medisoft.Medisoft.Exceptions.IOException(this.getClass().getCanonicalName());
        }
    }

    private void updateSearcher() {
        if (searcher == null || indexGuardian.isNewIndexChange()) {
            this.createSearcher();
        }
    }


    /**
     * Permite buscar en la BD documental por ID de documento.
     *
     * @param id representa el ID del documento a buscar.
     * @return devuelve un TopDocs con los documentos que mejor coinciden.
     */
    public TopDocs searchById(Integer id) {
        if (this.searcher == null || indexGuardian.isNewIndexChange()) {
            this.createSearcher();
        }
        try {
            QueryParser qp = new QueryParser("id", new StandardAnalyzer());
            Query idQuery = qp.parse(id.toString());
            if (this.searcher == null) {
                this.createSearcher();
            }
            return this.searcher.search(idQuery, 10);
        } catch (ParseException | IOException e) {
            System.err.println(e);
        }
        return null;
    }


    /**
     * Permite buscar en la BD documental.
     * Por defecto se utiliza la wildcard *.
     *
     * @param fieldName  representa el nombre del campo en el que buscar.
     * @param fieldValue representa el valor del campo a buscar.
     * @return devuelve un TopDocs con los documentos que mejor coinciden.
     */
    public TopDocs wildcardSearch(String fieldName, String fieldValue, int maxDocs) {
        try {
            QueryParser qp = new QueryParser(fieldName, new StandardAnalyzer());
            Query descripcionQuery = qp.parse(fieldValue + "*");
            updateSearcher();

            return searcher.search(descripcionQuery, maxDocs);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Realiza una búsqueda multicampo en la BD documental.-
     *
     * @param fieldNames representa los nombres de los campos en los que buscar.
     * @param fieldValue representa la cadena a buscar.
     * @return devuelve un Top de Documentos con mayor relevancia para la búsqueda.-
     */
    public TopDocs multiFieldSearch(String[] fieldNames, String fieldValue, int maxDocs) {
        try {
            MultiFieldQueryParser queryParser = new MultiFieldQueryParser(fieldNames,
                    new StandardAnalyzer());
            Query query = queryParser.parse(fieldValue + "*");
            updateSearcher();

            return searcher.search(query, maxDocs);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Permite buscar en la BD documental.
     * Esta búsqueda es Fuzzy; busca similaritudes.
     *
     * @param fieldName  representa el nombre del campo en el que buscar.
     * @param fieldValue representa el valor del campo a buscar.
     * @return devuelve un TopDocs con los documentos que mejor coinciden.
     */
    public TopDocs fuzzySearch(String fieldName, String fieldValue, int maxDocs) {
        try {
            Term term = new Term(fieldName, fieldValue);
            Query fuzzy = new FuzzyQuery(term);
            updateSearcher();

            return searcher.search(fuzzy, maxDocs);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Document getDocumentById(int docId) {
        try {
            return searcher.doc(docId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
