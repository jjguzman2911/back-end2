package com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement;

import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ProfessionalProfileReviewDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.ReviewStatistic;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Services.ReviewStatisticStorageService;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class MapperProfessionalScoreService {

    @Autowired
    private UserService userService;

    @Autowired
    private SearchEngine searchEngine;

    @Autowired
    private ReviewStatisticStorageService reviewStatisticStorageService;


    public List<ProfessionalSearchDTO> findProfessionalsAndSortByRelevance(Map<Integer, Float> scoreMap) {
        Map<Integer, Float> sortedMap = scoreMap.entrySet().stream()
                .sorted(Map.Entry.<Integer, Float>comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (viejo, nuevo) -> viejo, LinkedHashMap::new));

        return findProfessionalsFromDocuments(sortedMap);
    }

    /**
     * Recupera un listado de Profesionales resultado de la búsqueda en la BD documental.
     * Este listado está ponderado según la puntuación obtenida por coincidencias de la búsqueda en la BD documental.
     *
     * @param sortedMap representa el Mapa ordenado por puntajes de búsqueda.-
     * @return devuelve la lista de Profesionales para mostrar en los resultados de la búsqueda.-
     */
    private List<ProfessionalSearchDTO> findProfessionalsFromDocuments(Map<Integer, Float> sortedMap) {
        List<ProfessionalSearchDTO> professionals = new ArrayList<>();
        sortedMap.forEach((docId, totalScore) -> {
            User user = getUserByDocument(searchEngine.getDocumentById(docId));
            ProfessionalProfileReviewDTO review = getReview(user.getId());
            ProfessionalSearchDTO professional = new ProfessionalSearchDTO(user, review);
            if (!professionals.contains(professional)) {
                professionals.add(professional);
            }
        });
        return professionals;
    }

    /**
     * Recupera un Usuario desde la BD relacional a partir de un documento de la BD documental.-
     *
     * @param document representa el documento de la BD documental.
     * @return devuele el usuario encontrado en base al ID.
     */
    private User getUserByDocument(Document document) {
        IndexableField field = document.getField("id");
        int id = Integer.parseInt(field.stringValue());
        return userService.findById(id);
    }

    private ProfessionalProfileReviewDTO getReview(int professionalUserId) {
        ReviewStatistic reviewStatistic = reviewStatisticStorageService.findByProfessionalUserId(professionalUserId);
        return new ProfessionalProfileReviewDTO(reviewStatistic);
    }
}
