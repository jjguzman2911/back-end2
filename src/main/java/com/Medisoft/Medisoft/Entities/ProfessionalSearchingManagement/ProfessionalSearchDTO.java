package com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement;

import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ProfessionalProfileReviewDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice.MedicalOfficeDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.Objects;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
public class ProfessionalSearchDTO {

    private int idUsuario;
    private String foto;
    private String nombre;
    private String apellido;
    private ProfessionalProfileReviewDTO review;
    private String especialidad;
    private MedicalOfficeDTO consultorio;
    @JsonIgnore
    private Set<HealthInsuranceDTO> healthInsuranceDTOS;


    public ProfessionalSearchDTO(User user, ProfessionalProfileReviewDTO review) {
        this.idUsuario = user.getId();
        this.foto = user.getFotoUrl();
        this.nombre = user.getPersona().getNombre();
        this.apellido = user.getPersona().getApellido();
        this.review = review;
        this.especialidad = user.getProfesional().getEspecialidad().getNombre();
        setConsultorio(new MedicalOfficeDTO(user.getProfesional().getConsultorio()));
        setHealthInsuranceDTOS(user.getProfesional().getHealthInsurancesDTO());
    }

    public boolean containsHealthInsurance(HealthInsuranceDTO healthInsuranceDTO) {
        if (CollectionUtils.isEmpty(healthInsuranceDTOS)) {
            return false;
        }
        return healthInsuranceDTOS.contains(healthInsuranceDTO);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfessionalSearchDTO that = (ProfessionalSearchDTO) o;
        return idUsuario == that.idUsuario;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUsuario);
    }
}
