package com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HealthInsuranceFilterDTO {
    private HealthInsuranceDTO healthInsurance;
    private int amountOfProfessionals;
}
