package com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement;

import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Service
public class SearchDocumentService {

    @Autowired
    private SearchEngine searchEngine;


    public Map<Integer, Float> searchProfessionalByNameAndSpecialty(String fullName, String specialty, int maxDocs){
        Map<Integer, Float> scoreMap = new HashMap<>();
        searchByFullName(fullName, scoreMap, maxDocs);
        searchBySpecialty(specialty, scoreMap, maxDocs);
        return scoreMap;
    }

    private void searchByFullName(String fullName, Map<Integer, Float> scoreMap, int maxDocs) {
        if (!StringUtils.isEmpty(fullName)) {
            fullName = fullName.toLowerCase().trim();
            TopDocs nameFuzzyScores = searchEngine.fuzzySearch("name", fullName, maxDocs);
            if (nameFuzzyScores != null) {
                loadScoreMap(nameFuzzyScores, scoreMap);
            }
            TopDocs surnameFuzzyScores = searchEngine.fuzzySearch("surname", fullName, maxDocs);
            if (surnameFuzzyScores != null) {
                loadScoreMap(surnameFuzzyScores, scoreMap);
            }
            TopDocs fullNameMultiFieldSearch = searchEngine.multiFieldSearch(new String[]{"name", "surname"}, fullName, maxDocs);
            if (fullNameMultiFieldSearch != null) {
                loadScoreMap(fullNameMultiFieldSearch, scoreMap);
            }
        }
    }

    private void searchBySpecialty(String specialty, Map<Integer, Float> scoreMap, int maxDocs) {
        if (!StringUtils.isEmpty(specialty)) {
            specialty = specialty.toLowerCase();
            TopDocs specialtyWildcardScores = searchEngine.wildcardSearch("specialty", specialty, maxDocs);
            if (specialtyWildcardScores != null) {
                loadScoreMap(specialtyWildcardScores, scoreMap);
            }
        }
    }

    /**
     * Permite la carga de HashMap con el nro de Documento y Puntaje.
     *
     * @param resultSearch representa el conjunto de documentos con mayor relevancia para buscar.
     * @param hashMap      representa el HashMap en donde se hace la carga.
     */
    private void loadScoreMap(TopDocs resultSearch, Map<Integer, Float> hashMap) {
        for (ScoreDoc scoreDoc : resultSearch.scoreDocs) {
            if (hashMap.containsKey(scoreDoc.doc)) {
                Float score = hashMap.get(scoreDoc.doc);
                score += scoreDoc.score;
                hashMap.replace(scoreDoc.doc, score);
            } else {
                hashMap.put(scoreDoc.doc, scoreDoc.score);
            }
        }
    }

    public Map<Integer, Float> searchProfessionalByName(String fullName, int maxDocs){
        Map<Integer, Float> scoreMap = new HashMap<>();
        if (StringUtils.isEmpty(fullName)) {
            maxDocs = 50;
        }
        searchByFullName(fullName, scoreMap, maxDocs);
        searchBySpecialty("psicología", scoreMap, maxDocs);
        searchBySpecialty("medicina clínica", scoreMap, maxDocs);
        searchBySpecialty("oftalmología", scoreMap, maxDocs);
        searchBySpecialty("nutrición", scoreMap, maxDocs);
        searchBySpecialty("odontología", scoreMap, maxDocs);
        return scoreMap;
    }
}
