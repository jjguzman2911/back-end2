package com.Medisoft.Medisoft.Entities.AppointmentManagement;

public enum EventoTurno {
    CREAR,
    ASIGNAR,
    ANULAR,
    CANCELAR,
    NO_ASIGNAR,
    REGISTRAR_INASISTENCIA,
    REGISTRAR_ASISTENCIA,
    COBRAR,
    ATENDER,
    TRASLADAR,
    REGISTRAR_ATENCION,
    REEMBOLSAR,
    PASAR_ANULADO,
    PASAR_CANCELADO,
    LEAVE
}