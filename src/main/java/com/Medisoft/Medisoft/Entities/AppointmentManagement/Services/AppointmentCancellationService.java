package com.Medisoft.Medisoft.Entities.AppointmentManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.EventoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.EmailManagement.Services.EmailNotificationService;
import com.Medisoft.Medisoft.Entities.PaymentManagement.Services.PaymentRefundService;
import com.Medisoft.Medisoft.Exceptions.InvalidAppointmentStateActionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class AppointmentCancellationService {

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private PaymentRefundService paymentRefundService;

    @Autowired
    private EmailNotificationService emailNotificationService;


    public List<Turno> cancelAppointment(long appointmentId) {
        Turno appointment = appointmentService.findById(appointmentId);
        if (!appointment.isCancelable())
            throw new InvalidAppointmentStateActionException();
        return refundAndSetAppointmentFree(appointment);
    }

    private List<Turno> refundAndSetAppointmentFree(Turno appointment) {
        refundPayment(appointment);
        return setAppointmentFree(appointment);
    }

    private void refundPayment(Turno appointment) {
        if (appointment.isCobrado()) {
            paymentRefundService.refundPayment(appointment);
            appointment.actualizarEstado(EventoTurno.REEMBOLSAR);
        }
    }

    private List<Turno> setAppointmentFree(Turno appointment) {
        List<Turno> freeAppointments = appointment.setFree();
        updateAppointmentsState(freeAppointments);
        log.info("Appointment {} cancelled, {} in total were cancelled", appointment.getId(), freeAppointments.size());
        return appointmentService.saveAll(freeAppointments);
    }

    private void updateAppointmentsState(List<Turno> freeAppointments){
        freeAppointments.forEach(freeAppointment -> {
            freeAppointment.actualizarEstado(EventoTurno.CANCELAR);
            if (freeAppointment.isReassignable()) freeAppointment.actualizarEstado(EventoTurno.TRASLADAR);
        });
    }


    public void nullify(long appointmentId) {
        log.info("Nullifying appointment {}", appointmentId);
        Turno appointment = appointmentService.findById(appointmentId);
        if (!appointment.isAnulable())
            throw new InvalidAppointmentStateActionException();
        refundAndNotify(appointment);
    }

    private void refundAndNotify(Turno appointment) {
        emailNotificationService.notifyAppointmentNullification(appointment);
        refundPayment(appointment);
        appointment.actualizarEstado(EventoTurno.ANULAR);
        appointmentService.save(appointment);
    }
}
