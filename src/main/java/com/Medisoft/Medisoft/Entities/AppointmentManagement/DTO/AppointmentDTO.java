package com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.EstadoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.ProfesionalTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class AppointmentDTO {
    private long id;
    private long startDateTimestamp;
    private long finishDateTimestamp;
    private ProfesionalTurno professional;
    private Especialidad specialty;
    private EstadoTurno currentState;


    public AppointmentDTO(Turno appointment) {
        setId(appointment.getId());
        setStartDateTimestamp(appointment.getStartDateInMillis());
        setFinishDateTimestamp(appointment.getFinishDateInMillis());
        setProfessional(appointment.getProfesional());
        setSpecialty(appointment.getEspecialidad());
        setCurrentState(appointment.getEstadoActual());
    }
}
