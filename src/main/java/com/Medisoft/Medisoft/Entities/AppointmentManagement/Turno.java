package com.Medisoft.Medisoft.Entities.AppointmentManagement;

import com.Medisoft.Medisoft.Entities.PatientManagement.PatientHealthInsurance;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice.Consultorio;
import com.Medisoft.Medisoft.Utilities.ApplicationConstantsUtils;
import com.Medisoft.Medisoft.Utilities.DateFormatConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.util.*;

@Data
@NoArgsConstructor
@Entity
public class Turno implements Comparable<Turno> {

    @Id
    private long id;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private PacienteTurno paciente;

    private String fechaHoraInicio;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private ProfesionalTurno profesional;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    private Especialidad especialidad;

    @Column
    @Enumerated(value = EnumType.STRING)
    private EstadoTurno estadoActual;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "id_turno", referencedColumnName = "id")
    private List<HistoriaEstadoTurno> historialEstadoTurno;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    private Consultorio consultorio;

    @JsonIgnore
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private List<Turno> turnosEnlazados;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private TipoAtencionProfesional tipoAtencion;

    private boolean padre; //por defecto ES padre (True), cuando es enlace es hijo (False)

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private PatientHealthInsurance patientHealthInsurance;

    public Turno(long id, String fechaHoraInicio, ProfesionalTurno profesional,
                 Especialidad especialidad, Consultorio consultorio) {
        setId(id);
        setFechaHoraInicio(fechaHoraInicio);
        setProfesional(profesional);
        setEspecialidad(especialidad);
        setEstadoActual(EstadoTurno.DISPONIBLE);
        actualizarHistorialEstadoTurno();
        setConsultorio(consultorio);
        setPadre(true);
    }


    public void actualizarEstado(EventoTurno evento) {
        estadoActual = estadoActual.siguienteEstado(evento, estadoActual);
        actualizarHistorialEstadoTurno();
        if (turnosEnlazados != null && !turnosEnlazados.isEmpty()) {
            turnosEnlazados.forEach(turno -> turno.actualizarEstado(evento));
        }
    }

    private void actualizarHistorialEstadoTurno() {
        if (historialEstadoTurno == null) {
            historialEstadoTurno = new ArrayList<>();
        }
        HistoriaEstadoTurno nuevaHistoria = new HistoriaEstadoTurno(estadoActual, new Date());
        historialEstadoTurno.add(nuevaHistoria);
    }

    public boolean addTurno(Turno turno) {
        if (turnosEnlazados == null) {
            turnosEnlazados = new ArrayList<>();
        }
        return turnosEnlazados.add(turno);
    }

    public int calcularDuracion() {
        if (tipoAtencion != null) {
            return tipoAtencion.calcularDuracion();
        }
        return ApplicationConstantsUtils.APPOINTMENT_MINIMUM_DURATION;
    }

    public long getDurationInMillis(){
        return (long) calcularDuracion() * 1000 * 60;
    }

    public long getStartDateInMillis(){
        return DateFormatConverter.getDateInMillis(fechaHoraInicio);
    }

    public long getFinishDateInMillis(){
        return getStartDateInMillis() + getDurationInMillis();
    }


    /**
     * Calcula una lista de los ID de los turnos que se deben enlazar con el turno actual para reservar la
     * franja horaria necesaria de acuerdo al tipo de atención.
     *
     * @return List<Long>, devuelve una lista con todos los IDs de los turnos que se van a enlazar con el principal.
     */
    public List<Long> determinarIdTurnosEnlazados() {
        //Se le resta uno porque es el contemplado por el turno principal
        int cantidadEnlaces = this.tipoAtencion.getUnidadMinimaTrabajo() - 1;
        List<Long> turnosEnlace = new ArrayList<>();
        long fechaHora = DateFormatConverter.stringDateToLongDate(this.getFechaHoraInicio());

        while (cantidadEnlaces > 0) {
            fechaHora += ApplicationConstantsUtils.APPOINTMENT_MINIMUM_DURATION;
            long idTurnoEnlace = DateFormatConverter.joinLongs(fechaHora, this.getProfesional().getIdUsuario());
            turnosEnlace.add(idTurnoEnlace);
            cantidadEnlaces--;
        }
        return turnosEnlace;
    }


    public boolean isFuture() {
        Calendar turno = DateFormatConverter.parseStringToCalendar(fechaHoraInicio);
        Calendar hoy = DateFormatConverter.parseStringToCalendar(DateFormatConverter.parseDateToString(new Date()));
        return turno.after(hoy);
    }

    public String getFechaHoraFin() {
        Calendar fechaHoraInicioCalendar = DateFormatConverter.parseStringToCalendar(fechaHoraInicio);
        fechaHoraInicioCalendar.add(Calendar.MINUTE, calcularDuracion());
        return DateFormatConverter.calendarToString(fechaHoraInicioCalendar);
    }

    @JsonIgnore
    public float getPrecio() {
        return tipoAtencion.getPrecio();
    }

    @JsonIgnore
    public int getPatientUserId() {
        return paciente != null ? paciente.getIdUsuario() : -1;
    }

    @JsonIgnore
    public int getProfessionalUserId() {
        return profesional != null ? profesional.getIdUsuario() : -1;
    }


    public boolean esAsignable() {
        return estadoActual.equals(EstadoTurno.DISPONIBLE);
    }

    public boolean isOnLeave(){
        return estadoActual.equals(EstadoTurno.ON_LEAVE);
    }

    public boolean isReassignable(){
        return estadoActual.equals(EstadoTurno.CANCELADO) && isReassignableByDate();
    }

    private boolean isReassignableByDate(){
        long startingDateInMillis = DateFormatConverter.parseStringToDate(fechaHoraInicio).getTime();
        long nowInMillis = new Date().getTime();
        long differenceInTime = startingDateInMillis - nowInMillis;
        long sixHoursInMillis = 1000 * 60 * 60 * 6;
        return differenceInTime > sixHoursInMillis;
    }

    @JsonIgnore
    public boolean isNotPayable() {
        return !estadoActual.equals(EstadoTurno.ASIGNADO);
    }

    @JsonIgnore
    public boolean isCobrado() {
        return estadoActual.equals(EstadoTurno.COBRADO);
    }

    public boolean esAsistible() {
        return estadoActual.equals(EstadoTurno.COBRADO);
    }

    @JsonIgnore
    public boolean isAtendible() {
        return estadoActual.equals(EstadoTurno.EN_ATENCION);
    }

    @JsonIgnore
    public boolean isAtencionEmpezable() {
        return estadoActual.equals(EstadoTurno.ASISTIDO);
    }

    @JsonIgnore
    public boolean isCancelable() {
        return estadoActual.equals(EstadoTurno.ASIGNADO) || estadoActual.equals(EstadoTurno.COBRADO);
    }

    @JsonIgnore
    public boolean isAttendable() {
        return (estadoActual.equals(EstadoTurno.ASIGNADO) ||
                estadoActual.equals(EstadoTurno.COBRADO) ||
                estadoActual.equals(EstadoTurno.ASISTIDO)) &&
                isPadre();
    }


    @JsonIgnore
    public boolean isAnulable() {
        return estadoActual.equals(EstadoTurno.ASIGNADO) ||
                estadoActual.equals(EstadoTurno.COBRADO) ||
                estadoActual.equals(EstadoTurno.ASISTIDO);
    }

    public boolean isAvailable() {
        return estadoActual.equals(EstadoTurno.DISPONIBLE);
    }

    public boolean isAtendido(){
        return estadoActual.equals(EstadoTurno.ATENDIDO);
    }

    @JsonIgnore
    public List<Turno> setFree() {
        setPaciente(null);
        setTipoAtencion(null);
        setPatientHealthInsurance(null);
        List<Turno> oldAppointments = new ArrayList<>();
        if (!CollectionUtils.isEmpty(turnosEnlazados)) {
            turnosEnlazados.forEach(appointment -> appointment.setPadre(true));
            oldAppointments.addAll(turnosEnlazados);
            setTurnosEnlazados(null);
        }
        oldAppointments.add(this);
        return oldAppointments;
    }

    @JsonIgnore
    public boolean isFullRefundable() {
        long startingDateInMillis = DateFormatConverter.parseStringToDate(fechaHoraInicio).getTime();
        long nowInMillis = new Date().getTime();
        long differenceInTime = startingDateInMillis - nowInMillis;
        long dayInMillis = 86400000;
        return differenceInTime > dayInMillis;
    }

    public boolean isAssignedOrAttendable() {
        return estadoActual.equals(EstadoTurno.ASIGNADO) || estadoActual.equals(EstadoTurno.COBRADO)
                || estadoActual.equals(EstadoTurno.ASISTIDO);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Turno turno = (Turno) o;
        return id == turno.getId() &&
                Objects.equals(fechaHoraInicio, turno.fechaHoraInicio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fechaHoraInicio);
    }

    @Override
    public int compareTo(Turno o) {
        Calendar fechaHoraYo = DateFormatConverter.parseStringToCalendar(fechaHoraInicio);
        Calendar fechaHoraOtro = DateFormatConverter.parseStringToCalendar(o.fechaHoraInicio);
        return fechaHoraYo.compareTo(fechaHoraOtro);
    }
}
