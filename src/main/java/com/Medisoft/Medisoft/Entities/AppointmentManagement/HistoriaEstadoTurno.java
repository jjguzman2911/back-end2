package com.Medisoft.Medisoft.Entities.AppointmentManagement;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
public class HistoriaEstadoTurno {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(value = EnumType.STRING)
    private EstadoTurno estadoTurno;

    private Date fechaHora;

    public HistoriaEstadoTurno(EstadoTurno estado, Date fechaHora) {
        setEstadoTurno(estado);
        setFechaHora(fechaHora);
    }
}
