package com.Medisoft.Medisoft.Entities.AppointmentManagement.Services;

import com.Medisoft.Medisoft.DAO.JPA.IAppointmentDAO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.EventoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Exceptions.AppointmentNotFoundException;
import com.Medisoft.Medisoft.Exceptions.InvalidAppointmentStateActionException;
import com.Medisoft.Medisoft.Services.ComprobanteMailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AppointmentService {

    @Autowired
    private IAppointmentDAO appointmentDAO;

    @Autowired
    private ComprobanteMailService comprobanteMailService;


    public Turno findById(long id) {
        Turno appointment = appointmentDAO.findById(id).orElse(null);
        if (appointment == null) {
            throw new AppointmentNotFoundException(id + "");
        }
        log.info("Appointment {} found", id);
        return appointment;
    }

    public Turno findByIdDummy(long id) {
        return appointmentDAO.findById(id).orElse(null);
    }


    public Turno save(Turno turno) {
        try {
            Turno savedAppointment = appointmentDAO.save(turno);
            log.info("Appointment {} saved", savedAppointment.getId());
            return savedAppointment;
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            throw new com.Medisoft.Medisoft.Exceptions.DataIntegrityViolationException(Turno.class.getSimpleName());
        }
    }

    public List<Turno> findAll() {
        return appointmentDAO.findAll();
    }

    public List<Turno> saveAll(List<Turno> turnos) {
        return appointmentDAO.saveAll(turnos);
    }

    public void deleteAll(Set<Turno> appointments) {
        appointmentDAO.deleteAll(appointments);
    }


    @Async
    public void generarYEnviarComprobanteTurno(Turno turno, boolean enviarAPaciente,
                                               boolean enviarAProfesional) {
        comprobanteMailService.generarYEnviarComprobanteTurno(turno, enviarAPaciente, enviarAProfesional);
    }

    public Turno attendAppointment(long appointmentId) {
        Turno appointment = findById(appointmentId);
        if (!appointment.isAtendible()) {
            throw new InvalidAppointmentStateActionException();
        }
        appointment.actualizarEstado(EventoTurno.REGISTRAR_ATENCION);
        log.info("Appointment {} assisted", appointment.getId());
        return save(appointment);
    }

    public List<Turno> findAllForProfessional(int professionalUserId, boolean future, boolean ascendingOrder, int maxSize) {
        return appointmentDAO.findAll().stream()
                .filter(turno -> turno.getProfessionalUserId() == professionalUserId)
                .filter(appointment -> {
                    if (future) {
                        return appointment.isFuture();
                    }
                    return true;
                })
                .sorted(ascendingOrder ? Collections.reverseOrder(Collections.reverseOrder()) : Collections.reverseOrder())
                .limit(maxSize)
                .collect(Collectors.toList());
    }

    //ascendingOrder means from the next following appointment to the last one (farest)
    public List<Turno> findAllForPatient(int patientUserId, boolean future, boolean ascendingOrder, int maxSize) {
        return appointmentDAO.findAll().stream()
                .filter(appointment -> appointment.getPatientUserId() == patientUserId)
                .filter(appointment -> {
                    if (future) {
                        return appointment.isFuture();
                    }
                    return true;
                })
                .filter(appointment -> !appointment.isAtendido())
                .sorted(ascendingOrder ? Collections.reverseOrder(Collections.reverseOrder()) : Collections.reverseOrder())
                .limit(maxSize)
                .collect(Collectors.toList());
    }

    public List<Turno> findFutureAppointments(int professionalUserId, int maxSize) {
        return appointmentDAO.findAll().stream()
                .filter(appointment -> appointment.getProfessionalUserId() == professionalUserId &&
                        appointment.isFuture() &&
                        appointment.isAttendable())
                .sorted(Collections.reverseOrder(Collections.reverseOrder()))
                .limit(maxSize)
                .collect(Collectors.toList());
    }

    public List<Turno> findAllForProfessionalAndPatient(int professionalUserId, int patientUserId,
                                                        boolean future, boolean ascendingOrder, int maxSize) {
        return appointmentDAO.findAll().stream()
                .filter(turno -> turno.getProfessionalUserId() == professionalUserId &&
                        turno.getPatientUserId() == patientUserId)
                .filter(appointment -> {
                    if (future) {
                        return appointment.isFuture();
                    }
                    return true;
                })
                .sorted(ascendingOrder ? Collections.reverseOrder(Collections.reverseOrder()) : Collections.reverseOrder())
                .limit(maxSize)
                .collect(Collectors.toList());
    }

    public Turno startAssitance(long appointmentId) {
        Turno appointment = findById(appointmentId);
        if (!appointment.isAtencionEmpezable()) {
            throw new InvalidAppointmentStateActionException();
        }
        appointment.actualizarEstado(EventoTurno.ATENDER);
        log.info("Appointment {} assistance started", appointmentId);
        return save(appointment);
    }

    public List<Turno> findAvailableAppointments(int professionalUserId) {
        return appointmentDAO.findAll().stream()
                .filter(turno -> turno.getProfessionalUserId() == professionalUserId)
                .filter(appointment -> appointment.isFuture() &&
                        appointment.isAvailable() &&
                        appointment.isPadre())
                .sorted(Collections.reverseOrder(Collections.reverseOrder()))
                .collect(Collectors.toList());
    }
}
