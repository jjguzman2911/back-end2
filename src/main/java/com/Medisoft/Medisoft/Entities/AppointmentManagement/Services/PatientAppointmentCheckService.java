package com.Medisoft.Medisoft.Entities.AppointmentManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO.PatientAppointmentDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PatientAppointmentCheckService {

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private UserService userService;


    public List<PatientAppointmentDTO> getAllPatientAppointments(int patientUserId, boolean future,
                                                                 int maxSize, boolean ascendingOrder) {
        List<Turno> patientAppointments = appointmentService.findAllForPatient(patientUserId, future, ascendingOrder, maxSize);
        log.info("{} appointments found for patient {}, with [future={}, ascendingOrder={}]",
                patientAppointments.size(), patientUserId, future, ascendingOrder);
        return convertToDTO(patientAppointments);
    }

    public List<PatientAppointmentDTO> getAllPatientAppointmentsForProfessional(int patientUserId, int professionalUserId,
                                                                                boolean future, int maxSize,
                                                                                boolean ascendingOrder) {
        List<Turno> patientAppointments = appointmentService.findAllForProfessionalAndPatient(
                professionalUserId, patientUserId, future, ascendingOrder, maxSize);
        log.info("{} appointments found for patient {} and professional {}, with [future={}, ascendingOrder={}]",
                patientAppointments.size(), patientUserId, professionalUserId, future, ascendingOrder);
        return convertToDTO(patientAppointments);
    }

    private List<PatientAppointmentDTO> convertToDTO(List<Turno> patientAppointments) {
        Map<Integer, User> userSet = new HashMap<>();
        return patientAppointments.stream()
                .map(appointment -> mapToPatientAppointmentDto(userSet, appointment))
                .collect(Collectors.toList());
    }

    private PatientAppointmentDTO mapToPatientAppointmentDto(Map<Integer, User> userSet, Turno appointment){
        int professionalUserId = appointment.getProfessionalUserId();
        User professionalUser = userSet.get(professionalUserId);
        if (professionalUser == null) {
            professionalUser = userService.findById(professionalUserId);
            userSet.put(professionalUserId, professionalUser);
        }
        return new PatientAppointmentDTO(appointment, professionalUser.getId(),
                professionalUser.getPersona().getNombre(), professionalUser.getPersona().getApellido(),
                professionalUser.getFotoUrl(), professionalUser.getProfesional().isWorkingWithMercadopago());
    }
}
