package com.Medisoft.Medisoft.Entities.AppointmentManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.AppointmentMakingDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.EventoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.PacienteTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Exceptions.InvalidAppointmentStateActionException;
import com.Medisoft.Medisoft.Scheduling.AppointmentTaskScheduler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Slf4j
@Service
public class AppointmentMakingService {

    @Autowired
    private UserService userService;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private AppointmentTaskScheduler appointmentTaskScheduler;


    public Turno makeAppointment(int professionalUserId, AppointmentMakingDTO appointmentMakingDto) {
        log.info("Making appointment {}", appointmentMakingDto);
        User professionalUser = userService.findProfessionalUserById(professionalUserId);
        Turno principalAppointment = buildPrincipalAppointment(appointmentMakingDto, professionalUser);
        updateAppointmentSchedule(professionalUser, principalAppointment);
        notify(principalAppointment);
        return principalAppointment;
    }

    private Turno buildPrincipalAppointment(AppointmentMakingDTO appointmentMakingDto, User professionalUser) {
        Turno principalAppointment = appointmentService.findById(appointmentMakingDto.getIdTurnoInicial());
        if (!principalAppointment.esAsignable()) {
            throw new InvalidAppointmentStateActionException();
        }
        User patientUser = userService.findById(appointmentMakingDto.getIdUsuarioPaciente());
        principalAppointment.setTipoAtencion(appointmentMakingDto.getTipoAtencionProfesional());
        principalAppointment.setPaciente(new PacienteTurno(patientUser));

        return principalAppointment;
    }

    private void updateAppointmentSchedule(User professionalUser, Turno principalAppointment) {
        log.info("Updating professional appointment schedule with appointment {}", principalAppointment);
        Profesional profesional = professionalUser.getProfesional();
        bindAndUpdateAppointments(principalAppointment, profesional);
        professionalUser.setProfesional(profesional);
        userService.save(professionalUser);
    }

    private void bindAndUpdateAppointments(Turno principalAppointment, Profesional profesional) {
        log.info("Binding appointments");
        List<Turno> bindedAppointments = this.findBindedAppointments(principalAppointment);
        principalAppointment.actualizarEstado(EventoTurno.ASIGNAR);
        for (Turno turno : bindedAppointments) {
            turno.actualizarEstado(EventoTurno.ASIGNAR);
        }
        profesional.getTurnero().bindToPrincipalAppointment(principalAppointment, bindedAppointments);
    }

    private List<Turno> findBindedAppointments(Turno principalAppointment) {
        List<Long> idsTurnosEnlazados = principalAppointment.determinarIdTurnosEnlazados();
        List<Turno> turnosEnlazados = new ArrayList<>();

        for (Long idTurnoEnlace : idsTurnosEnlazados) {
            Turno turno = appointmentService.findByIdDummy(idTurnoEnlace);
            if (turno != null) {
                turno.setPadre(false);
                turnosEnlazados.add(turno);
            }
        }
        return turnosEnlazados;
    }

    private void notify(Turno principalAppointment) {
        appointmentService.generarYEnviarComprobanteTurno(principalAppointment, true, true);
        appointmentTaskScheduler.createNonAttendanceAppointmentTask(principalAppointment, true);
    }
}
