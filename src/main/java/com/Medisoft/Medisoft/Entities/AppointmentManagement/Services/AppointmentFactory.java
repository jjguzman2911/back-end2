package com.Medisoft.Medisoft.Entities.AppointmentManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.*;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.Agenda;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.DiaAtencion;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.HorarioAtencion;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice.Consultorio;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Utilities.ApplicationConstantsUtils;
import com.Medisoft.Medisoft.Utilities.DateFormatConverter;
import com.Medisoft.Medisoft.Utilities.GlobalTimeSetting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class AppointmentFactory {
    private final static int MAX_WEEKS = 16;
    private User usuario;
    private Turnero turnero;
    private Agenda agenda;
    private Especialidad especialidad;
    private Consultorio consultorio;


    /**
     * Determina la cantidad de semanas de turnos que se deben generar. Delega el método al turnero.
     *
     * @return devuelve la cantidad de semanas de turnos para generar.
     * @see Turnero#cuantasSemanasParaGenerar()
     */
    private int howManyWeeksToAdd() {
        if (turnero == null) {
            return MAX_WEEKS;
        }
        return turnero.cuantasSemanasParaGenerar();
    }


    /**
     * Genera los turnos del profesional seteado para los próximos cuatro meses, o lo que es lo mismo, las próximas
     * 16 semanas.-
     *
     * @see AppointmentFactory#buildDates(DiaAtencion diaAtencion, int weeksToAdd)
     * @see AppointmentFactory#buildAppointment(String fechaHoraAtencion)
     **/
    public void createAppointments() {
        int weeksToAdd = howManyWeeksToAdd();
        if (turnero == null) {
            turnero = new Turnero();
        }
        if (weeksToAdd > 0) {
            log.info("Updating appointment schedule for {} more weeks", weeksToAdd);
            if (agenda != null && agenda.isComplete()) {
                createAppointmentsForWeeks(weeksToAdd);
            }
        }
    }

    private void createAppointmentsForWeeks(int weeksToAdd){
        for (DiaAtencion diaAtencion : agenda.getDiasAtencion()) {

            List<String> attentionDates = buildDates(diaAtencion, weeksToAdd);
            attentionDates.forEach(this::createAppointmentForDate);
        }
        turnero.setFechaHoraUltimaGeneracion(new Date());
        log.info("Appointment schedule generated");
    }

    private void createAppointmentForDate(String date){
        Turno appointment = buildAppointment(date);
        if (agenda.isOnLeaveRange(appointment.getStartDateInMillis())){
            log.info("Appointment {} is on leaving range", appointment);
            appointment.actualizarEstado(EventoTurno.LEAVE);
        }
        turnero.addTurno(appointment);
    }


    /**
     * Genera todos las fechas y hora de inicio de atención disponibles por profesional para cada uno de los próximos
     * 4 meses, o lo que es lo mismo, las próximas 16 semanas.
     *
     * @param diaAtencion representa el día de atención definido por el profesional.
     * @param weeksToAdd  representa la cantidad de semanas que se deben generar con turnos. Es un valor entre 0 y 16.
     * @return List<String>, devuelve una lista con todas las fechas y horas de atención del profesional.
     */
    private List<String> buildDates(DiaAtencion diaAtencion, int weeksToAdd) {
        List<String> appointmentDates = new ArrayList<>();

        Calendar firstCalendar = Calendar.getInstance(TimeZone.getTimeZone(GlobalTimeSetting.timeZone));
        Calendar secondCalendar = Calendar.getInstance(TimeZone.getTimeZone(GlobalTimeSetting.timeZone));
        //Medisoft -> Domingo = 0;
        //Calendar -> Domingo = 1;
        int dayOfWeek = diaAtencion.getDiaSemana().ordinal() + 1;

        firstCalendar.set(Calendar.DAY_OF_WEEK, dayOfWeek);
        secondCalendar.set(Calendar.DAY_OF_WEEK, dayOfWeek);

        for (HorarioAtencion horarioAtencion : diaAtencion.getHorarios()) {

            int horaInicio = DateFormatConverter.hourFromHour(horarioAtencion.getHoraInicio());
            int minutoInicio = DateFormatConverter.minuteFromHour(horarioAtencion.getHoraInicio());

            int horaFin = DateFormatConverter.hourFromHour(horarioAtencion.getHoraFin());
            int minutoFin = DateFormatConverter.minuteFromHour(horarioAtencion.getHoraFin());

            createAppointmentDates(weeksToAdd, firstCalendar, secondCalendar, horaInicio, minutoInicio, horaFin, minutoFin, appointmentDates);

        }
        return appointmentDates;
    }

    private void createAppointmentDates(int totalWeeksToAdd, Calendar firstCalendar, Calendar secondCalendar, int horaInicio, int minutoInicio, int horaFin, int minutoFin, List<String> appointmentDates) {
        //Crea los turnos desde el mas lejano al mas cercano
        int currentWeek = firstCalendar.get(Calendar.WEEK_OF_YEAR) + totalWeeksToAdd;
        int yearInPreviousIteration = 0;
        for (int currentWeeksToAdd = totalWeeksToAdd; currentWeeksToAdd >= 0; currentWeeksToAdd--) {
            int currentYear = firstCalendar.get(Calendar.YEAR);
            int weeksOfYear = firstCalendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
            if (currentWeek > weeksOfYear) {
                currentWeek -= weeksOfYear;
                currentYear++;
            }
            if (currentWeek <= 0) {
                if (yearInPreviousIteration == currentYear){
                    currentYear--;
                    firstCalendar.set(Calendar.YEAR, currentYear);
                }
                weeksOfYear = firstCalendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
                currentWeek = weeksOfYear;
            }

            setCalendarToSpecificDate(firstCalendar, horaInicio, minutoInicio, currentWeek, currentYear);
            setCalendarToSpecificDate(secondCalendar, horaFin, minutoFin, currentWeek, currentYear);

            addAppointmentDates(firstCalendar, secondCalendar, appointmentDates);
            currentWeek--;
            yearInPreviousIteration = currentYear;
        }
    }

    private void setCalendarToSpecificDate(Calendar calendar, int hour, int minute, int weekOfYear, int year) {
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.WEEK_OF_YEAR, weekOfYear);
        calendar.set(Calendar.YEAR, year);
        log.info("Calendar set to date {}", calendar.getTime());
    }

    private void addAppointmentDates(Calendar firstCalendar, Calendar secondCalendar, List<String> appointmentDates) {
        while (firstCalendar.before(secondCalendar)) {
            appointmentDates.add(DateFormatConverter.calendarToString(firstCalendar));
            firstCalendar.add(Calendar.MINUTE, ApplicationConstantsUtils.APPOINTMENT_MINIMUM_DURATION);
        }
    }


    /**
     * Crea un turno en base a su fecha y hora de atención.-
     *
     * @param fechaHoraAtencion representa la fecha y hora de atención del turno.
     * @return Turno, devuelve los datos del turno recién creado.
     */
    private Turno buildAppointment(String fechaHoraAtencion) {
        long id = buildAppointmentId(fechaHoraAtencion, usuario.getId());
        return new Turno(id, fechaHoraAtencion, new ProfesionalTurno(usuario), especialidad, consultorio);
    }

    private long buildAppointmentId(String date, int userId) {
        long fecha = DateFormatConverter.stringDateToLongDate(date);
        return DateFormatConverter.joinLongs(fecha, userId);
    }


    /**
     * Setea todos los turnos que estaban disponibles cuya fecha es pasada como turno no asignado.-
     *
     * @see Turno#actualizarEstado(EventoTurno)
     */
    public void unassignTickets() {
        for (Turno turno : turnero.getTurnos()) {
            if (turno.getEstadoActual().equals(EstadoTurno.DISPONIBLE) && !turno.isFuture()) {
                turno.actualizarEstado(EventoTurno.NO_ASIGNAR);
            }
        }
    }


    public void configureAppointmentFactory(User usuario) {
        this.usuario = usuario;
        this.agenda = usuario.getProfesional().getAgenda();
        this.especialidad = usuario.getProfesional().getEspecialidad();
        this.turnero = usuario.getProfesional().getTurnero();
        this.consultorio = usuario.getProfesional().getConsultorio();
    }

    /**
     * Permite conocer si se debe realizar una generación de Turnos. Para ello consulta cuántas semanas se deben generar.-
     *
     * @return boolean, retorna True si la cantidad de semanas es mayor a 0, False en caso contrario.-
     * @see #howManyWeeksToAdd()
     */
    public boolean isGenerationNeeded() {
        return howManyWeeksToAdd() > 0;
    }


    /**
     * Determina si existen turnos para setearse como No Asignados. Esta comprobación sólo es válida pasada
     * una semana de la última generación del turnero.-
     *
     * @return boolena, retorna True si la última generación del turnero fue hace más de una semana;
     * False en caso contrario.-
     * @see Turnero#generacionFueMasDeUnaSemana()
     */
    public boolean isUnassigmentNeeded() {
        return turnero.generacionFueMasDeUnaSemana();
    }

    public Turnero getTurnero() {
        return turnero;
    }


    public Set<Long> generateAppointmentIds(List<DiaAtencion> attentionDays, int professionalUserId) {
        Set<Long> appointmentIds = new HashSet<>();
        for (DiaAtencion attentionDay : attentionDays) {
            createAppointmentIdsPerAttentionDay(attentionDay, professionalUserId, appointmentIds);
        }
        return appointmentIds;
    }

    private void createAppointmentIdsPerAttentionDay(DiaAtencion attentionDay, int professionalUserId, Set<Long> appointmentIds) {
        List<String> fechasHoraAtencion = buildDates(attentionDay, MAX_WEEKS);
        for (String fechaHoraAtencion : fechasHoraAtencion) {
            long appointmentId = buildAppointmentId(fechaHoraAtencion, professionalUserId);
            appointmentIds.add(appointmentId);
        }
    }

    public boolean updateAppointmentScheduleIfNeeded(User professionalUser) {
        //Seteamos la Ticket Factory
        configureAppointmentFactory(professionalUser);
        boolean isUpdated = false;
        //Chequeamos si se deben generar turnos
        if (isGenerationNeeded()) {
            createAppointments();
            isUpdated = true;
        }
        //Chequeamos si hay que No Asignar turnos
        if (isUnassigmentNeeded()) {
            unassignTickets();
            isUpdated = true;
        }
        return isUpdated;
        //Guardamos en BD en caso de ser necesario
    }
}
