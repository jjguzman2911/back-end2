package com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.EstadoTurno;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConsultaTurnosProfesionalDTO {

    private Set<TurnoProximoDTO> proximosTurnos;
    private List<TipoAtencionProfesional> tiposAtencion;
    private List<EstadoTurno> estadosTurno;

}
