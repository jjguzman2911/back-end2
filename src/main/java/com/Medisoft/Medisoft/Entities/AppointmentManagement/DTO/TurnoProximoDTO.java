package com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.EstadoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.PacienteTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class TurnoProximoDTO implements Comparable<TurnoProximoDTO> {
    private long id;
    private long startDateInMillis;
    private long finishDateInMillis;
    private PacienteTurno paciente;
    private Especialidad especialidad;
    private TipoAtencionProfesional tipoAtencion;
    private EstadoTurno estadoActual;

    public TurnoProximoDTO(Turno turno) {
        setId(turno.getId());
        setStartDateInMillis(turno.getStartDateInMillis());
        setFinishDateInMillis(turno.getFinishDateInMillis());
        setPaciente(turno.getPaciente());
        setEspecialidad(turno.getEspecialidad());
        setTipoAtencion(turno.getTipoAtencion());
        setEstadoActual(turno.getEstadoActual());
    }

    @Override
    public int compareTo(TurnoProximoDTO o) {
        return Long.compare(startDateInMillis, o.startDateInMillis);
    }
}
