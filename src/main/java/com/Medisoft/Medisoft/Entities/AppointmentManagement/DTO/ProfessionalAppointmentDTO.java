package com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.EstadoTurno;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ProfessionalAppointmentDTO {
    private long appointmentId;
    private int patientUserId;
    private String patientFullName;
    private EstadoTurno currentAppointmentState;
    private long startDateTimestamp;
    private long finishDateTimestamp;
    private String attentionType;
}
