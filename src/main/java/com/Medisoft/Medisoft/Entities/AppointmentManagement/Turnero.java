package com.Medisoft.Medisoft.Entities.AppointmentManagement;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Data
@Entity
public class Turnero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date fechaHoraUltimaGeneracion;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("fechaHoraInicio desc")
    private Set<Turno> turnos;

    public Turnero() {
        turnos = new TreeSet<>();
    }


    /**
     * Determina la cantidad de semanas que se van a utilizar para crear nuevos turnos.
     *
     * @return int, devuelve un valor entre 0 y 16 representando el total de semanas en que un profesional puede tener definido su turnero.-
     */
    public int cuantasSemanasParaGenerar() {
        if (turnos.isEmpty() || fechaHoraUltimaGeneracion == null) {
            return 16;
        }
        long nowInMillis = new Date().getTime();
        long lastCreationInMillis = getFechaHoraUltimaGeneracion().getTime();
        long differenceInTime = nowInMillis - lastCreationInMillis;
        //Devuelve la diferencia en semanas que existe entre la ultima generacion y la fecha actual
        return (int) (differenceInTime / 604.8e6);
    }

    /**
     * Añade un turno al conjunto de turnos disponibles.-
     *
     * @param turno representa el turno a añadir.
     * @return boolean, devuelve el True si el turno se agrega a la colección, False en caso contrario.
     * @see Set#add(Object)
     */
    public boolean addTurno(Turno turno) {
        return turnos.add(turno);
    }


    public Turno bindToPrincipalAppointment(Turno turnoPrincipal, List<Turno> turnosEnlazados) {
        turnos.remove(turnoPrincipal);
        turnos.removeAll(turnosEnlazados);
        turnoPrincipal.setTurnosEnlazados(turnosEnlazados);

        turnos.add(turnoPrincipal);
        turnos.addAll(turnosEnlazados);
        return turnoPrincipal;
    }


    /**
     * Determina si la generación del turnero fue hace más de una semana.-
     *
     * @return boolena, retorna True si la última generación del turnero fue hace más de una semana;
     * False en caso contrario.-
     */
    public boolean generacionFueMasDeUnaSemana() {
        if (fechaHoraUltimaGeneracion == null) {
            return true;
        }
        long nowLessOneWeekInMillis = new Date().getTime() - (1000 * 60 * 60 * 24 * 7);
        long lastCreationInMillis = fechaHoraUltimaGeneracion.getTime();
        return lastCreationInMillis <= nowLessOneWeekInMillis;
    }

    private Set<Turno> getUnusedAppointments() {
        return turnos.stream().filter(appointment -> appointment.esAsignable() || appointment.isOnLeave()).collect(Collectors.toSet());
    }

    public int removeUnusedAppointments() {
        Set<Turno> availableAppointments = getUnusedAppointments();
        turnos.removeAll(availableAppointments);
        return availableAppointments.size();
    }
}
