package com.Medisoft.Medisoft.Entities.AppointmentManagement;

import com.Medisoft.Medisoft.Entities.UserManagement.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;


@Data
@NoArgsConstructor
@Entity
@Immutable
@Subselect(
        "SELECT user.id AS id_usuario, user.email, concat(persona.nombre, ' ', persona.apellido) AS nombre_apellido, tipo_documento.nombre AS tipo_documento, persona.documento AS numero_documento " +
        "FROM user JOIN persona ON user.persona_id = persona.id " +
            "JOIN tipo_documento ON persona.tipo_documento_id = tipo_documento.id")
public class PacienteTurno {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(insertable = false, updatable = false)
    private int idUsuario;

    @Column(insertable = false, updatable = false)
    private String email;

    @Column(insertable = false, updatable = false)
    private String nombreApellido;

    @Column(insertable = false, updatable = false)
    private String tipoDocumento;

    @Column(insertable = false, updatable = false)
    private String numeroDocumento;

    public PacienteTurno(User user) {
        setIdUsuario(user.getId());
        setEmail(user.getEmail());
        setNombreApellido(user.getPersona().getFullName());
        setTipoDocumento(user.getPersona().getTipoDocumento().getNombre());
        setNumeroDocumento(user.getPersona().getDocumento());
    }
}
