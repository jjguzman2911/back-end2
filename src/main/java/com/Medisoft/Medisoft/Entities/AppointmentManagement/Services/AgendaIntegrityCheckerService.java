package com.Medisoft.Medisoft.Entities.AppointmentManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turnero;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.DiaAtencion;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.Leave;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.LeaveRange;
import com.Medisoft.Medisoft.Exceptions.AgendaIntegrityCheckedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AgendaIntegrityCheckerService {

    @Autowired
    private AppointmentFactory appointmentFactory;


    public Set<Long> checkAgendaIntegrity(Turnero appointmentSchedule, List<DiaAtencion> attentionDays, List<LeaveRange> leaveRanges, int professionalUserId) throws AgendaIntegrityCheckedException {
        log.info("Checking agenda integrity");
        if (appointmentSchedule == null || CollectionUtils.isEmpty(appointmentSchedule.getTurnos())) {
            throw new AgendaIntegrityCheckedException();
        }
        Set<Turno> criticalAppointments = getCriticalAppointments(appointmentSchedule);
        Set<Long> newAppointmentIds = appointmentFactory.generateAppointmentIds(attentionDays, professionalUserId);
        return getAffectedAppointments(criticalAppointments, newAppointmentIds, leaveRanges);
    }

    private Set<Turno> getCriticalAppointments(Turnero appointmentSchedule) {
        return appointmentSchedule.getTurnos().stream()
                .filter(appointment ->
                        appointment.isAssignedOrAttendable() &&
                        appointment.isPadre())
                .collect(Collectors.toSet());
    }

    private Set<Long> getAffectedAppointments(Set<Turno> criticalAppointments, Set<Long> newAppointments, List<LeaveRange> leaveRanges) throws AgendaIntegrityCheckedException {
        Leave leave = Leave.builder().ranges(leaveRanges).build();
        Set<Long> affectedAppointments = criticalAppointments.stream()
                .filter(criticalAppointment ->
                        !newAppointments.contains(criticalAppointment.getId()) ||
                        leave.isInRange(criticalAppointment.getStartDateInMillis()))
                .map(Turno::getId)
                .collect(Collectors.toSet());
        if (!affectedAppointments.isEmpty()) {
            log.warn("{} appointments affected", affectedAppointments.size());
            affectedAppointments.forEach(appointmentId -> log.warn("Appointment {} affected", appointmentId));
            return affectedAppointments;
        }
        throw new AgendaIntegrityCheckedException();
    }
}
