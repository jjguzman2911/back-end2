package com.Medisoft.Medisoft.Entities.AppointmentManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO.AppointmentDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO.ConsultaTurnosProfesionalDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO.ProfessionalAppointmentDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO.TurnoProximoDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.EstadoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Utilities.DTOMapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProfessionalAppointmentCheckService {

    @Autowired
    private UserService userService;

    @Autowired
    private AppointmentFactory appointmentFactory;

    @Autowired
    private AppointmentService appointmentService;


    public List<AppointmentDTO> getAvailableAppointments(int professionalUserId) {
        log.info("Getting available appointments");
        User professionalUser = userService.findProfessionalUserById(professionalUserId);
        updateAppointmentScheduleIfNeeded(professionalUser);
        List<Turno> availableAppointments = appointmentService.findAvailableAppointments(professionalUserId);
        log.info("{} available appointments found for professional {}", availableAppointments.size(), professionalUserId);
        return DTOMapperUtils.toAppointmentDto(availableAppointments);
    }

    private void updateAppointmentScheduleIfNeeded(User professionalUser) {
        boolean needsUpdate = appointmentFactory.updateAppointmentScheduleIfNeeded(professionalUser);
        if (needsUpdate) {
            log.info("Updating appointment schedule");
            professionalUser.getProfesional().setTurnero(appointmentFactory.getTurnero());
            userService.save(professionalUser);
        }
    }


    public ConsultaTurnosProfesionalDTO getNextAppointments(int professionalUserId) {
        User professionalUser = userService.findProfessionalUserById(professionalUserId);
        updateAppointmentScheduleIfNeeded(professionalUser);
        Profesional profesional = professionalUser.getProfesional();

        Set<TurnoProximoDTO> proximosTurnos =
                profesional.getTurnero().getTurnos() //Obtengo la coleccion a recorrer
                        .stream() //Se convierte en un flujo
                        .filter(turno -> turno.isFuture() && turno.isPadre()) //equivale a la condicion del if
                        .map(TurnoProximoDTO::new) // El cuerpo del if
                        .collect(Collectors.toCollection(TreeSet::new)); //Lo pongo en una coleccion nueva

        return new ConsultaTurnosProfesionalDTO(
                proximosTurnos,
                profesional.getAtenciones().getAtenciones(),
                estadosTurnoConsultaTurnosProfesional());
    }

    private List<EstadoTurno> estadosTurnoConsultaTurnosProfesional() {
        return Arrays.stream(EstadoTurno.values()).
                filter(x -> !(
                        x.equals(EstadoTurno.CANCELADO) ||
                                x.equals(EstadoTurno.NO_ASIGNADO) ||
                                x.equals(EstadoTurno.NO_ASISTIDO) ||
                                x.equals(EstadoTurno.EN_ATENCION) ||
                                x.equals(EstadoTurno.REEMBOLSADO) ||
                                x.equals(EstadoTurno.ATENDIDO)))
                .collect(Collectors.toList());
    }

    public List<ProfessionalAppointmentDTO> getFutureAppointments(int professionalUserId, int maxSize) {
        log.info("Getting future appointments");
        List<Turno> todaysAppointments = appointmentService.findFutureAppointments(professionalUserId, maxSize);
        todaysAppointments.forEach(appointment -> log.info("Appointment {} starts at {}", appointment.getId(), appointment.getFechaHoraInicio()));
        return DTOMapperUtils.toProfessionalAppointmentDto(todaysAppointments);
    }

}
