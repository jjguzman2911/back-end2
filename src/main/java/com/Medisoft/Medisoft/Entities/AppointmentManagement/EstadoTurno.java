package com.Medisoft.Medisoft.Entities.AppointmentManagement;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public enum EstadoTurno {
    DISPONIBLE(EventoTurno.ASIGNAR, EventoTurno.NO_ASIGNAR, EventoTurno.LEAVE),
    ASIGNADO(EventoTurno.ANULAR, EventoTurno.CANCELAR,
            EventoTurno.COBRAR, EventoTurno.REGISTRAR_INASISTENCIA),
    ANULADO(),
    CANCELADO(EventoTurno.NO_ASIGNAR, EventoTurno.TRASLADAR),
    COBRADO(EventoTurno.REGISTRAR_ASISTENCIA, EventoTurno.ANULAR,
            EventoTurno.CANCELAR, EventoTurno.REGISTRAR_INASISTENCIA,
            EventoTurno.REEMBOLSAR),
    NO_ASIGNADO(),
    NO_ASISTIDO(),
    ASISTIDO(EventoTurno.ATENDER, EventoTurno.ANULAR),
    EN_ATENCION(EventoTurno.REGISTRAR_ATENCION),
    ATENDIDO(),
    REEMBOLSADO(EventoTurno.CANCELAR, EventoTurno.ANULAR),
    ON_LEAVE();


    final static Map<EventoTurno, EstadoTurno> map = new HashMap<>();

    //map.put(Evento disparable, Estado destino)
    static {
        map.put(EventoTurno.CREAR, EstadoTurno.DISPONIBLE);
        map.put(EventoTurno.NO_ASIGNAR, EstadoTurno.NO_ASIGNADO);
        map.put(EventoTurno.ASIGNAR, EstadoTurno.ASIGNADO);
        map.put(EventoTurno.TRASLADAR, EstadoTurno.DISPONIBLE);
        map.put(EventoTurno.COBRAR, EstadoTurno.COBRADO);
        map.put(EventoTurno.REEMBOLSAR, EstadoTurno.REEMBOLSADO);
        map.put(EventoTurno.ANULAR, EstadoTurno.ANULADO);
        map.put(EventoTurno.CANCELAR, EstadoTurno.CANCELADO);
        map.put(EventoTurno.ATENDER, EstadoTurno.EN_ATENCION);
        map.put(EventoTurno.REGISTRAR_ATENCION, EstadoTurno.ATENDIDO);
        map.put(EventoTurno.REGISTRAR_INASISTENCIA, EstadoTurno.NO_ASISTIDO);
        map.put(EventoTurno.REGISTRAR_ASISTENCIA, EstadoTurno.ASISTIDO);
        map.put(EventoTurno.LEAVE, EstadoTurno.ON_LEAVE);
    }

    final List<EventoTurno> eventosDisparables;


    EstadoTurno(EventoTurno... eventos) {
        eventosDisparables = Arrays.asList(eventos);
    }


    public EstadoTurno siguienteEstado(EventoTurno eventoDisparado, EstadoTurno estadoActual) {
        if (eventosDisparables.contains(eventoDisparado)) {
            //Si no hay ninguna transicion posible, se queda en el estado actual
            return map.getOrDefault(eventoDisparado, estadoActual);
        }
        return estadoActual;
    }
}
