package com.Medisoft.Medisoft.Entities.AppointmentManagement;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import lombok.Data;

@Data
public class AppointmentMakingDTO {
    private long idTurnoInicial;
    private TipoAtencionProfesional tipoAtencionProfesional;
    private int idUsuarioPaciente;

}
