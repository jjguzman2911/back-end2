package com.Medisoft.Medisoft.Entities.AppointmentManagement;

import com.Medisoft.Medisoft.Entities.UserManagement.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.util.Objects;


@Data
@NoArgsConstructor
@Entity
@Immutable
@Subselect(
        "SELECT user.id AS id_usuario, user.email, concat(persona.nombre, ' ', persona.apellido) AS nombre_apellido, profesional.matricula " +
        "FROM user JOIN persona ON user.persona_id = persona.id " +
            "JOIN profesional ON profesional.id = user.profesional_id")
public class ProfesionalTurno {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(insertable = false, updatable = false)
    private int idUsuario;

    @Column(insertable = false, updatable = false)
    private String email;

    @Column(insertable = false, updatable = false)
    private String nombreApellido;

    @Column(insertable = false, updatable = false)
    private String matricula;

    public ProfesionalTurno(User usuario) {
        setIdUsuario(usuario.getId());
        setEmail(usuario.getEmail());
        setNombreApellido(usuario.getPersona().getFullName());
        setMatricula(usuario.getProfesional().getMatricula());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfesionalTurno that = (ProfesionalTurno) o;
        return idUsuario == that.idUsuario;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUsuario);
    }
}
