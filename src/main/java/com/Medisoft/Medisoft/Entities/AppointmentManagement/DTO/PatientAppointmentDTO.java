package com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO;


import com.Medisoft.Medisoft.Entities.AppointmentManagement.EstadoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice.Consultorio;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class PatientAppointmentDTO {
    private long appointmentId;
    private int professionalUserId;
    private String name;
    private String surname;
    private String specialty;
    private EstadoTurno currentAppointmentState;
    private long startDate;
    private long finishDate;
    private Consultorio medicalOffice;
    private String photoUrl;
    private String attentionType;
    private boolean professionalWorksWithMercadopago;

    public PatientAppointmentDTO(Turno appointment, int professionalUserId, String name, String surname, String photoUrl,
                                 boolean professionalWorksWithMercadopago) {
        setAppointmentId(appointment.getId());
        setProfessionalUserId(professionalUserId);
        setName(name);
        setSurname(surname);
        setSpecialty(appointment.getEspecialidad().getNombre());
        setCurrentAppointmentState(appointment.getEstadoActual());
        setStartDate(appointment.getStartDateInMillis());
        setFinishDate(appointment.getFinishDateInMillis());
        setMedicalOffice(appointment.getConsultorio());
        setPhotoUrl(photoUrl);
        setAttentionType(appointment.getTipoAtencion().getName());
        setProfessionalWorksWithMercadopago(professionalWorksWithMercadopago);
    }
}
