package com.Medisoft.Medisoft.Entities.ProfessionalManagement.View;

import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ProfessionalProfileReviewDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.DiaAtencion;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice.MedicalOfficeDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class ProfessionalProfileForPatientDTO {

    private int idUsuario;
    private String foto;
    private String nombre;
    private String apellido;
    private ProfessionalProfileReviewDTO review;
    private String especialidad;
    private MedicalOfficeDTO medicalOffice;
    private Set<HealthInsuranceDTO> healthInsurances;
    private Set<AttentionCategoryProfileForPatientDTO> attentionCategories;
    private Set<DiaAtencion> attentionDays;
    private boolean worksWithMercadopago;

    @Builder
    private ProfessionalProfileForPatientDTO(User user, Set<AttentionCategoryProfileForPatientDTO> attentionCategories,
                                             Set<DiaAtencion> attentionDays, ProfessionalProfileReviewDTO review) {
        setIdUsuario(user.getId());
        setFoto(user.getFotoUrl());
        setNombre(user.getName());
        setApellido(user.getSurname());
        setReview(review);
        setEspecialidad(user.getProfesional().getEspecialidad().getNombre());
        setMedicalOffice(new MedicalOfficeDTO(user.getProfesional().getConsultorio()));
        setHealthInsurances(user.getProfesional().getHealthInsurancesDTO());
        setAttentionCategories(attentionCategories);
        setAttentionDays(attentionDays);
        setWorksWithMercadopago(user.getProfesional().isWorkingWithMercadopago());
    }
}
