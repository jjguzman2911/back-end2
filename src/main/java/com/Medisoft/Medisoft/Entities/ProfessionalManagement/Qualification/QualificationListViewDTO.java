package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class QualificationListViewDTO {
    private int qualificationRequestId;
    private String photoUrl;
    private String applicantName;
    private String applicantSurname;
    private String idNumber;
    private String provincialLicense;
    private String specialtyLicense;
    private long requestTimestamp;
    private Especialidad specialty;

    public QualificationListViewDTO(QualificationRequest request, User user){
        setQualificationRequestId(request.getId());
        setPhotoUrl(user.getFotoUrl());
        setApplicantName(user.getName());
        setApplicantSurname(user.getSurname());
        setIdNumber(user.getDocumento());
        setProvincialLicense(request.getProvincialLicense());
        setSpecialtyLicense(request.getSpecialtyLicense());
        setRequestTimestamp(request.getRequestTimestamp());
        setSpecialty(request.getSpecialty());
    }

}
