package com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencion;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import lombok.Data;

import java.util.Objects;


@Data
public class TipoAtencionDTO {
    private int id;
    private String nombre;
    private int unidadMinimaTrabajo;
    private float precio;
    private boolean mandatory;
    private boolean valid;

    public TipoAtencionDTO(TipoAtencionProfesional tipoAtencionProfesional, boolean valid) {
        setId(tipoAtencionProfesional.getId());
        setNombre(tipoAtencionProfesional.getTipoAtencion().getNombre());
        setUnidadMinimaTrabajo(tipoAtencionProfesional.getUnidadMinimaTrabajo());
        setPrecio(tipoAtencionProfesional.getPrecio());
        setMandatory(tipoAtencionProfesional.isMandatory());
        setValid(valid);
    }

    public TipoAtencionDTO(TipoAtencion attention) {
        setId(attention.getId());
        setNombre(attention.getNombre());
        setMandatory(attention.isObligatorio());
        setValid(attention.isObligatorio());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TipoAtencionDTO that = (TipoAtencionDTO) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
