package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@Entity
public class LeaveRange {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long startTimestamp;
    private long finishTimestamp;

    public boolean isComplete(){
        return startTimestamp > 0 && finishTimestamp > 0;
    }

    public boolean isInRange(long timestamp){
        return startTimestamp <= timestamp && timestamp <= finishTimestamp;
    }
}
