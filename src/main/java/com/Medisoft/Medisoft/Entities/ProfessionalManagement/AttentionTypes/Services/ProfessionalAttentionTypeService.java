package com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.Services;

import com.Medisoft.Medisoft.Entities.PatientManagement.PatientAttentionTypeService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.AtencionesProfesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO.CategoriaTipoAtencionDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO.ProfessionalAttentionTypeDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO.TipoAtencionDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.CompleteProfessionalRegistrationService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalProfileDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Utilities.DTOMapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProfessionalAttentionTypeService {

    @Autowired
    private UserService userService;

    @Autowired
    private PatientAttentionTypeService patientAttentionTypeService;

    @Autowired
    private AttentionTypeCategoryStorageService attentionTypeCategoryStorageService;

    @Autowired
    private CompleteProfessionalRegistrationService completeProfessionalRegistrationService;


    public Set<CategoriaTipoAtencionDTO> getAttentionTypesByPatient(int professionalUserId, int patientUserId) {
        Set<CategoriaTipoAtencionDTO> professionalAttentionTypes = getAttentionTypesByProfessional(professionalUserId);

        Set<CategoriaTipoAtencionDTO> patientAttentionTypes =
                patientAttentionTypeService.createDtoSet(
                        patientAttentionTypeService.findAvailableAttentionTypes(
                                patientUserId, professionalUserId).getAtencionesDisponibles());

        updateCategoriaTipoAtencionDTO(professionalAttentionTypes, patientAttentionTypes);
        log.info("{} categories retrieved", professionalAttentionTypes.size());
        return professionalAttentionTypes;
    }

    private Set<CategoriaTipoAtencionDTO> getAttentionTypesByProfessional(int professionalUserId) {
        User user = userService.findProfessionalUserById(professionalUserId);
        Profesional professional = user.getProfesional();
        return convertProfessionalAttentionTypeToDTO(professional.getAtenciones().getAtenciones());
    }

    private Set<CategoriaTipoAtencionDTO> convertProfessionalAttentionTypeToDTO(List<TipoAtencionProfesional> professionalAttentionTypes) {
        Set<CategoriaTipoAtencionDTO> categories = professionalAttentionTypes.stream()
                .map(CategoriaTipoAtencionDTO::new)
                .collect(Collectors.toSet());
        for (CategoriaTipoAtencionDTO categoriaTipoAtencionDTO : categories) {
            for (TipoAtencionProfesional professionalAttentionType : professionalAttentionTypes) {
                addTipoAtencionToCategoria(categoriaTipoAtencionDTO, professionalAttentionType);
            }
        }
        return categories;
    }

    private void addTipoAtencionToCategoria(CategoriaTipoAtencionDTO categoriaTipoAtencionDTO,
                                            TipoAtencionProfesional professionalAttentionType) {
        if (categoriaTipoAtencionDTO.isCategoriaTipoAtencion(professionalAttentionType.getCategoriaTipoAtencion())) {
            categoriaTipoAtencionDTO.addTipoAtencion(
                    new TipoAtencionDTO(professionalAttentionType, professionalAttentionType.isMandatory()));
        }
    }

    private void updateCategoriaTipoAtencionDTO(Set<CategoriaTipoAtencionDTO> professionalAttentionTypes,
                                                Set<CategoriaTipoAtencionDTO> patientAttentionTypes) {
        for (CategoriaTipoAtencionDTO professionalCategory : professionalAttentionTypes) {
            for (CategoriaTipoAtencionDTO patientCategory : patientAttentionTypes) {
                if (professionalCategory.equals(patientCategory)) {
                    professionalCategory.replaceAttentionTypes(patientCategory.getTiposAtencion());
                }
            }
        }
    }


    public Set<CategoriaTipoAtencionDTO> getAllAttentionTypes(int professionalUserId) {
        log.info("Getting categories and attentions for professional {}", professionalUserId);
        User user = userService.findProfessionalUserById(professionalUserId);
        Profesional professional = user.getProfesional();
        int specialtyId = professional.getEspecialidad().getId();
        return findCategoriesAndAttentionTypesBySpecialtyId(specialtyId, professional.getAtenciones());
    }

    private Set<CategoriaTipoAtencionDTO> findCategoriesAndAttentionTypesBySpecialtyId(int specialtyId, AtencionesProfesional professionalAttentions) {
        Set<CategoriaTipoAtencionDTO> categoriesDto = findCategoriesDtoBySpecialtyId(specialtyId);
        Set<TipoAtencionDTO> enabledAttentions = professionalAttentions != null ?
                mapEnabledAttentionTypes(professionalAttentions.getAtenciones()) :
                new HashSet<>();
        replaceDefaultBySavedAttentions(enabledAttentions, categoriesDto);
        return categoriesDto;
    }

    private Set<TipoAtencionDTO> mapEnabledAttentionTypes(List<TipoAtencionProfesional> professionalAttentions) {
        Set<TipoAtencionDTO> enabledAttentionTypes = professionalAttentions.stream()
                .map(attention -> {
                    TipoAtencionDTO attentionDto = new TipoAtencionDTO(attention, true);
                    attentionDto.setId(attention.getTipoAtencion().getId());
                    return attentionDto;
                })
                .collect(Collectors.toSet());
        log.info("{} enabled attention types found", enabledAttentionTypes.size());
        return enabledAttentionTypes;
    }

    private Set<CategoriaTipoAtencionDTO> findCategoriesDtoBySpecialtyId(int specialtyId) {
        return attentionTypeCategoryStorageService.findBySpecialtyId(specialtyId).stream()
                .map(CategoriaTipoAtencionDTO::new)
                .collect(Collectors.toSet());
    }

    private void replaceDefaultBySavedAttentions(Set<TipoAtencionDTO> attentionsDto, Set<CategoriaTipoAtencionDTO> categoriesDto) {
        for (CategoriaTipoAtencionDTO category : categoriesDto) {
            for (TipoAtencionDTO attention : attentionsDto) {
                if (category.containsAttentionType(attention)) {
                    category.replaceAttentionType(attention);
                }
            }
        }
    }


    public ProfessionalProfileDTO createProfessionalAttentionTypes(int professionalUserId,
                                                                   Set<ProfessionalAttentionTypeDTO> attentionsDto){
        log.info("Creating professional attention types for professional {}", professionalUserId);
        log.info(attentionsDto.toString());
        User user = userService.findProfessionalUserById(professionalUserId);
        createProfessionalAttentions(user, attentionsDto);
        userService.save(user);
        completeProfessionalRegistrationService.isEnabledAsProfessional(user);
        return new ProfessionalProfileDTO(user);
    }

    private void createProfessionalAttentions(User user, Set<ProfessionalAttentionTypeDTO> attentionsDto){
        Profesional professional = user.getProfesional();
        AtencionesProfesional professionalAttentions = mapToProfessionalAttention(professional, attentionsDto);
        professional.setAtenciones(professionalAttentions);
        user.setProfesional(professional);
    }

    private AtencionesProfesional mapToProfessionalAttention(Profesional professional, Set<ProfessionalAttentionTypeDTO> attentionsDto){
        AtencionesProfesional professionalAttentions = professional.getAtenciones() != null
                ? professional.getAtenciones()
                : new AtencionesProfesional();
        professionalAttentions.setAtenciones(DTOMapperUtils.fromProfessionalAttentionTypeDto(attentionsDto));
        professionalAttentions.setFechaRegistro(new Date());
        return professionalAttentions;
    }

}
