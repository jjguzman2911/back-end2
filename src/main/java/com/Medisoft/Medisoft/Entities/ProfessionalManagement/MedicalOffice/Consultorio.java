package com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.IProfessionalRegistration;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class Consultorio implements IProfessionalRegistration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String direccion;
    private String latitud;
    private String longitud;
    private String piso;
    private String departamento;
    private String torre;
    private Date fechaRegistro;

    @Override
    public boolean isComplete() {
        return latitud != null && longitud != null && direccion != null;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(direccion);
        sb.append("\t");
        if (!StringUtils.isEmpty(piso)) {
            sb.append(" Piso: ");
            sb.append(piso);
        }
        if (!StringUtils.isEmpty(departamento)) {
            sb.append(" Depto: ");
            sb.append(departamento);
        }
        if (!StringUtils.isEmpty(torre)) {
            sb.append(" Torre: ");
            sb.append(torre);
        }
        return sb.toString();
    }
}
