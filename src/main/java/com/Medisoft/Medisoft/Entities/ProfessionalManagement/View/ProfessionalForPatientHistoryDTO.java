package com.Medisoft.Medisoft.Entities.ProfessionalManagement.View;

import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ProfessionalProfileReviewDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ReviewDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfessionalForPatientHistoryDTO {
    private int professionalUserId;
    private String photoUrl;
    private String name;
    private String surname;
    private String license;
    private Especialidad specialty;
    private ProfessionalProfileReviewDTO review;
    private long lastAttentionTimestamp;

    public ProfessionalForPatientHistoryDTO(User professionalUser) {
        setProfessionalUserId(professionalUser.getId());
        setPhotoUrl(professionalUser.getFotoUrl());
        setName(professionalUser.getName());
        setSurname(professionalUser.getSurname());
        setLicense(professionalUser.getProfesional().getMatricula());
        setSpecialty(professionalUser.getProfesional().getEspecialidad());
    }

    public ProfessionalForPatientHistoryDTO(User professionalUser, ProfessionalProfileReviewDTO review, long lastAttentionTimestamp) {
        setProfessionalUserId(professionalUser.getId());
        setPhotoUrl(professionalUser.getFotoUrl());
        setName(professionalUser.getName());
        setSurname(professionalUser.getSurname());
        setLicense(professionalUser.getProfesional().getMatricula());
        setSpecialty(professionalUser.getProfesional().getEspecialidad());
        setReview(review);
        setLastAttentionTimestamp(lastAttentionTimestamp);
    }
}
