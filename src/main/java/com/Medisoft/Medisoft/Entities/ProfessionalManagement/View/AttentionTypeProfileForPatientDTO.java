package com.Medisoft.Medisoft.Entities.ProfessionalManagement.View;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AttentionTypeProfileForPatientDTO implements Comparable<AttentionTypeProfileForPatientDTO>{
    private String name;

    @Override
    public int compareTo(AttentionTypeProfileForPatientDTO o) {
        return name.compareTo(o.name);
    }
}
