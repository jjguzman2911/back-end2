package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda;

/**
 * Modela los días de la semana de acuerdo al calendario gregoriano.
 *
 * @author Misael
 * @version 2020-03-15
 */
public enum DiaSemana implements Comparable<DiaSemana>{
    DOMINGO,
    LUNES,
    MARTES,
    MIERCOLES,
    JUEVES,
    VIERNES,
    SABADO
}
