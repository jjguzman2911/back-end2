package com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.IProfessionalRegistration;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * Modela los tipos de atenciones realizados por un único profesional.
 *
 * @author Misael
 * @version 2020-01-27
 * @see IProfessionalRegistration
 */

@Entity
public class AtencionesProfesional implements IProfessionalRegistration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(cascade = CascadeType.ALL) //can't remove orphan rows
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<TipoAtencionProfesional> atenciones;
    private Date fechaRegistro;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<TipoAtencionProfesional> getAtenciones() {
        return atenciones;
    }

    public void setAtenciones(List<TipoAtencionProfesional> atenciones) {
        this.atenciones = atenciones;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public boolean isComplete() {
        for (TipoAtencionProfesional tipoAtencion : this.atenciones) {
            if (!tipoAtencion.esCompleto()) {
                return false;
            }
        }
        return true;
    }

    @JsonIgnore
    public TipoAtencionProfesional getTipoAtencionObligatoria() {
        return atenciones.stream()
                .filter(tipoAtencion -> tipoAtencion.esCompleto() && tipoAtencion.isMandatory())
                .findFirst()
                .orElse(null);
    }
}
