package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Services;

import com.Medisoft.Medisoft.DAO.JPA.IProfesionalTurnoDAO;
import com.Medisoft.Medisoft.DAO.JPA.IUserDAO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.ProfesionalTurno;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProfessionalStorageService {

    @Autowired
    private IProfesionalTurnoDAO profesionalTurnoDAO;

    @Autowired
    private IUserDAO usuarioDAO;


    public ProfesionalTurno getProfesionalTurnoByIdUsuario(int idUsuario) {
        ProfesionalTurno profesionalTurno = profesionalTurnoDAO.findById(idUsuario).orElse(null);
        if (profesionalTurno == null) {
            throw new EntityNotFoundException(ProfesionalTurno.class.getSimpleName());
        }
        return profesionalTurno;
    }

    public Profesional getProfesionalByIdUsuario(int idUsuario) {
        return usuarioDAO.findById(idUsuario)
                .map(User::getProfesional)
                .orElse(null);
    }

}
