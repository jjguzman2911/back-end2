package com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.Services;

import com.Medisoft.Medisoft.DAO.JPA.ICategoriaTipoAtencionDAO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.CategoriaTipoAtencion;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AttentionTypeCategoryStorageService {

    @Autowired
    private ICategoriaTipoAtencionDAO categoriaTipoAtencionDAO;


    public List<CategoriaTipoAtencion> findBySpecialtyId(int specialtyId) {
        log.info("Finding attention type categories by specialty id {}", specialtyId);
        return categoriaTipoAtencionDAO.findAll()
                .stream()
                .filter(x -> x.getEspecialidad().getId() == specialtyId)
                .collect(Collectors.toList());
    }
}
