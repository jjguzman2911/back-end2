package com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.CompleteProfessionalRegistrationService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
public class MedicalOfficeService {

    @Autowired
    private UserService userService;

    @Autowired
    private CompleteProfessionalRegistrationService completeProfessionalRegistrationService;


    public User postMedicalOffice(int professionalUserId, Consultorio medicalOffice) {
        User user = userService.findProfessionalUserById(professionalUserId);
        return updateMedicalOffice(user, medicalOffice);
    }

    private User updateMedicalOffice(User user, Consultorio medicalOffice) {
        Profesional professional = user.getProfesional();
        Consultorio professionalMedicalOffice = professional.getConsultorio() != null ?
                professional.getConsultorio()
                : new Consultorio();
        updateProfessionalMedicalOffice(professionalMedicalOffice, medicalOffice);
        professional.setConsultorio(professionalMedicalOffice);
        return completeProfessionalRegistrationService.updateAndIndexUser(user, professional);
    }

    private void updateProfessionalMedicalOffice(Consultorio professionalMedicalOffice, Consultorio medicalOffice) {
        professionalMedicalOffice.setDireccion(medicalOffice.getDireccion());
        professionalMedicalOffice.setLatitud(medicalOffice.getLatitud());
        professionalMedicalOffice.setLongitud(medicalOffice.getLongitud());
        professionalMedicalOffice.setDepartamento(medicalOffice.getDepartamento());
        professionalMedicalOffice.setPiso(medicalOffice.getPiso());
        professionalMedicalOffice.setTorre(medicalOffice.getTorre());
        professionalMedicalOffice.setFechaRegistro(new Date());
        log.info("Medical office registered");
    }
}
