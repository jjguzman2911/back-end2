package com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MedicalOfficeDTO {
    private String direccion;
    private String latitud;
    private String longitud;
    private String piso;
    private String departamento;
    private String torre;

    public MedicalOfficeDTO(Consultorio medicalOffice) {
        setDireccion(medicalOffice.getDireccion());
        setLatitud(medicalOffice.getLatitud());
        setLongitud(medicalOffice.getLongitud());
        setPiso(medicalOffice.getPiso());
        setDepartamento(medicalOffice.getDepartamento());
        setTorre(medicalOffice.getTorre());
    }
}
