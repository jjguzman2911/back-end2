package com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance;

import lombok.Builder;
import lombok.Data;

import java.util.Objects;

@Data
@Builder
public class HealthInsuranceCovenantDTO implements Comparable<HealthInsuranceCovenantDTO> {

    private HealthInsuranceDTO healthInsuranceDTO;
    private boolean enabled;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HealthInsuranceCovenantDTO that = (HealthInsuranceCovenantDTO) o;
        return Objects.equals(healthInsuranceDTO, that.healthInsuranceDTO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(healthInsuranceDTO);
    }

    @Override
    public int compareTo(HealthInsuranceCovenantDTO o) {
        return healthInsuranceDTO.compareTo(o.getHealthInsuranceDTO());
    }
}
