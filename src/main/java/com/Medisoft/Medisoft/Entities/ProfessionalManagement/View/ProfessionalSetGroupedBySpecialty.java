package com.Medisoft.Medisoft.Entities.ProfessionalManagement.View;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfessionalSetGroupedBySpecialty {
    private Especialidad specialty;
    private Set<ProfessionalForPatientHistoryDTO> professionals;
}
