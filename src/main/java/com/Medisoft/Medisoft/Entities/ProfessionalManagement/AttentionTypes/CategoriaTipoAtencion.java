package com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes;


import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class CategoriaTipoAtencion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;

    @OneToOne(fetch = FetchType.EAGER)
    private Especialidad especialidad;

    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<TipoAtencion> tiposAtencion;

    public CategoriaTipoAtencion(int id, String name) {
        setId(id);
        setNombre(name);
    }

}
