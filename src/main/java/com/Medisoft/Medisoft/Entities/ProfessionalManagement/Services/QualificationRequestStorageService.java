package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Services;

import com.Medisoft.Medisoft.DAO.JPA.QualificationRequestDAO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification.QualificationRequest;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification.QualificationRequestStatus;
import com.Medisoft.Medisoft.Exceptions.EntityNotFoundException;
import com.Medisoft.Medisoft.Exceptions.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class QualificationRequestStorageService {

    @Autowired
    private QualificationRequestDAO qualificationRequestDAO;

    public QualificationRequest save(QualificationRequest qualificationRequest){
        try {
            qualificationRequestDAO.save(qualificationRequest);
            log.info("Qualification request {} saved", qualificationRequest.getId());
            return qualificationRequest;
        } catch (Exception e) {
            log.error("", e.getCause());
            throw new IOException("Registro de profesional");
        }
    }

    public QualificationRequest findById(int id){
        QualificationRequest qualificationRequest = qualificationRequestDAO.findById(id).orElse(null);
        if (qualificationRequest == null) {
            throw new EntityNotFoundException(id + "");
        }
        log.info("Qualification request {} found", qualificationRequest);
        return qualificationRequest;
    }

    public QualificationRequest findLastByApplicantUserId(int applicantUserId){
        QualificationRequest qualificationRequest = qualificationRequestDAO.findAll()
                .stream()
                .filter(request -> request.getApplicantUserId() == applicantUserId)
                .max(Comparator.comparingLong(QualificationRequest::getLastActionTimestamp))
                .orElse(null);
        log.info("Last qualification request {} found", qualificationRequest);
        return qualificationRequest;
    }

    public List<QualificationRequest> findAllSortedByRequestTimestamp(){
        List<QualificationRequest> requests = qualificationRequestDAO.findAll()
                .stream()
                .filter(request -> request.getStatus() == QualificationRequestStatus.IN_VALIDATION)
                .sorted(Comparator.comparingLong(QualificationRequest::getRequestTimestamp))
                .collect(Collectors.toList());
        log.info("{} qualification requests found", requests.size());
        return requests;
    }

}
