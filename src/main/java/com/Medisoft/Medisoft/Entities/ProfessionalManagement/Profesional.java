package com.Medisoft.Medisoft.Entities.ProfessionalManagement;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turnero;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordStructure;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.Agenda;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.AtencionesProfesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceCovenant;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice.Consultorio;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.*;


@Entity
@Data
public class Profesional {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String matricula;
    private String specialtyLicense;

    @OneToOne(fetch = FetchType.EAGER)
    private Especialidad especialidad;

    private transient List<IProfessionalRegistration> requirements;
    private boolean habilitado;
    private Date fechaHabilitacion;
    private long qualificationTimestamp;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Consultorio consultorio;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private AtencionesProfesional atenciones;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Agenda agenda;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Turnero turnero;

    private boolean workingWithMercadopago;
    private long medicalRecordId;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private HealthInsuranceCovenant healthInsuranceCovenant;

    /**
     * Permite determinar si un profesional está habilitado al haber completado su proceso de registro.
     *
     * @return devuelve True en caso de estar habilitado, false en caso contrario.
     */
    public boolean isHabilitado() {
        if (requirements == null) {
            requirements = new ArrayList<>();
            requirements.add(atenciones);
            requirements.add(consultorio);
            requirements.add(agenda);
            requirements.add(MedicalRecordStructure.builder().id(medicalRecordId).build());
        }
        registrationStates();
        return habilitado;
    }

    /**
     * Determina el estado de la habilitación del profesional con respecto a los requisitos de registro.
     *
     * @return devuelve un listado con todos los requisitos de registro y su respectivo estado.
     */
    public List<RegistrationState> registrationStates() {
        habilitado = true;
        ArrayList<RegistrationState> states = new ArrayList<>();
        for (IProfessionalRegistration professionalQualification : requirements) {
            if (professionalQualification != null) {
                String qualificationName = professionalQualification.getClass().getSimpleName();
                if (professionalQualification.isComplete()) {
                    states.add(new RegistrationState(true, qualificationName));
                } else {
                    states.add(new RegistrationState(false, qualificationName));
                    this.habilitado = false;
                }
            } else {
                this.habilitado = false;
            }
        }
        return states;
    }


    @JsonIgnore
    public TipoAtencionProfesional getTipoAtencionObligatoria() {
        if (atenciones != null) {
            return atenciones.getTipoAtencionObligatoria();
        }
        return null;
    }

    @JsonIgnore
    public int getSpecialityId() {
        if (especialidad != null) {
            return especialidad.getId();
        }
        return -1;
    }

    public boolean hasMedicalRecord() {
        return medicalRecordId != 0;
    }

    public Set<HealthInsuranceDTO> getHealthInsurancesDTO() {
        return healthInsuranceCovenant != null ? healthInsuranceCovenant.getHealthInsurancesDTO() : new HashSet<>();
    }
}
