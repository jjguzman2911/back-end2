package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AgendaIntegrityCheckerService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AppointmentCancellationService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turnero;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.CompleteProfessionalRegistrationService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Exceptions.AgendaIntegrityCheckedException;
import com.Medisoft.Medisoft.Exceptions.InvalidAgendaDataException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class AgendaService {

    @Autowired
    private UserService userService;

    @Autowired
    private CompleteProfessionalRegistrationService completeProfessionalRegistrationService;

    @Autowired
    private AgendaIntegrityCheckerService agendaIntegrityCheckerService;

    @Autowired
    private AppointmentCancellationService appointmentCancellationService;


    public Set<Long> checkAndSaveAgenda(int professionalUserId, AgendaDto agendaDto) {
        log.info("Checking and saving agenda {}", agendaDto);
        List<DiaAtencion> attentionDays = agendaDto.getAttentionDays();
        List<LeaveRange> leaveRanges = agendaDto.getLeaveRanges();
        checkAgendaCompleteness(agendaDto);
        User user = userService.findProfessionalUserById(professionalUserId);
        Profesional profesional = user.getProfesional();
        Turnero appointmentSchedule = profesional.getTurnero();
        try {
            return agendaIntegrityCheckerService.checkAgendaIntegrity(appointmentSchedule, attentionDays, leaveRanges, professionalUserId);
        } catch (AgendaIntegrityCheckedException e) {
            updateProfessionalAgenda(user, attentionDays, leaveRanges);
            deleteAvailableAppointments(user);
            return new HashSet<>();
        }
    }

    private void checkAgendaCompleteness(AgendaDto agendaDto) {
        Agenda agenda = Agenda.builder()
                .diasAtencion(agendaDto.getAttentionDays())
                .leave(Leave.builder()
                        .ranges(agendaDto.getLeaveRanges())
                        .build())
                .build();
        if (!agenda.isComplete()) {
            log.warn("Agenda is not completed");
            throw new InvalidAgendaDataException();
        }
        log.info("Agenda is completed");
    }

    private void updateProfessionalAgenda(User user, List<DiaAtencion> attentionDays, List<LeaveRange> leaveRanges) {
        log.info("Updating professional agenda");
        Profesional professional = user.getProfesional();
        Agenda agenda = updateAgenda(professional.getAgenda(), attentionDays, leaveRanges);
        professional.setAgenda(agenda);
        completeProfessionalRegistrationService.updateAndIndexUser(user, professional);
    }

    private Agenda updateAgenda(Agenda agenda, List<DiaAtencion> attentionDays, List<LeaveRange> leaveRanges) {
        if (agenda == null) agenda = new Agenda();
        agenda.setFechaRegistro(new Date());
        agenda.setDiasAtencion(attentionDays);
        Leave leave = !CollectionUtils.isEmpty(leaveRanges) ?
                Leave.builder().ranges(leaveRanges)
                        .timestampCreated(new Date().getTime())
                        .build() :
                null;
        agenda.setLeave(leave);
        return agenda;
    }

    private void deleteAvailableAppointments(User user) {
        Profesional professional = user.getProfesional();
        Turnero appointmentSchedule = professional.getTurnero();
        int amountOfAppointmentsRemoved = appointmentSchedule != null ?
                appointmentSchedule.removeUnusedAppointments() :
                0;
        log.info("Deleting {} unused appointments", amountOfAppointmentsRemoved);
        if (amountOfAppointmentsRemoved > 0) {
            appointmentSchedule.setFechaHoraUltimaGeneracion(null);
            professional.setTurnero(appointmentSchedule);
            user.setProfesional(professional);
            userService.save(user);
        }
    }


    public User saveAgenda(int professionalUserId, AgendaCheckedDTO agendaCheckedDTO) {
        log.info("Saving agenda");
        User user = userService.findProfessionalUserById(professionalUserId);
        return updateAgendaAndNullifyAppointments(user, agendaCheckedDTO);
    }

    private User updateAgendaAndNullifyAppointments(User user, AgendaCheckedDTO agendaCheckedDTO) {
        log.info("Updating agenda and nullifying appointments");
        updateProfessionalAgenda(user, agendaCheckedDTO.getAttentionDays(), agendaCheckedDTO.getLeaveRanges());
        deleteAvailableAppointments(user);
        nullifyAffectedAppointments(agendaCheckedDTO.getAppointmentIdsToNullify());
        return user;
    }

    private void nullifyAffectedAppointments(Set<Long> appointmentsToNullify) {
        log.info("Nullifying {} appointments", appointmentsToNullify.size());
        appointmentsToNullify.forEach(appointmentId -> appointmentCancellationService.nullify(appointmentId));
    }
}
