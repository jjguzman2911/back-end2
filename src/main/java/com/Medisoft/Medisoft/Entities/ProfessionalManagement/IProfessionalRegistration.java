package com.Medisoft.Medisoft.Entities.ProfessionalManagement;


import com.fasterxml.jackson.annotation.JsonIgnore;

public interface IProfessionalRegistration {

    @JsonIgnore
    boolean isComplete();
}