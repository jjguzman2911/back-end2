package com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.Services;

import com.Medisoft.Medisoft.DAO.JPA.ITipoAtencionProfesionalDAO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.Medisoft.Medisoft.Exceptions.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ProfessionalAttentionTypeStorageService {

    @Autowired
    private ITipoAtencionProfesionalDAO tipoAtencionProfesionalDAO;


    public TipoAtencionProfesional findById(int professionalAttentionTypeId) {
        log.info("Finding professional attention type {}", professionalAttentionTypeId);
        TipoAtencionProfesional tipoAtencionProfesional = tipoAtencionProfesionalDAO.findById(professionalAttentionTypeId).orElse(null);
        if (tipoAtencionProfesional == null) {
            throw new EntityNotFoundException(TipoAtencionProfesional.class.getSimpleName());
        }
        return tipoAtencionProfesional;
    }
}
