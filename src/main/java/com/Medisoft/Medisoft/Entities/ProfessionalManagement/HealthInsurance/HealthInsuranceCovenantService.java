package com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.CompleteProfessionalRegistrationService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Exceptions.ProfessionalNotRegisteredException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Slf4j
@Service
public class HealthInsuranceCovenantService {

    @Autowired
    private HealthInsuranceStorageService healthInsuranceStorageService;

    @Autowired
    private UserService userService;

    @Autowired
    private CompleteProfessionalRegistrationService completeProfessionalRegistrationService;


    public Set<HealthInsuranceCovenantDTO> getProfessionalHealthInsuranceCovenants(int professionalUserId) {
        User professionalUser = userService.findById(professionalUserId);
        if (!professionalUser.isProfessional())
            throw new ProfessionalNotRegisteredException();

        return getHealthInsuranceCovenantsDTO(professionalUser.getProfesional().getHealthInsurancesDTO());
    }

    private Set<HealthInsuranceCovenantDTO> getHealthInsuranceCovenantsDTO(Set<HealthInsuranceDTO> healthInsuranceDTOS) {
        Set<HealthInsuranceCovenantDTO> healthInsuranceCovenantDTOSet = new TreeSet<>();
        healthInsuranceCovenantDTOSet.addAll(mapEnabledHealthInsuranceCovenant(healthInsuranceDTOS));
        healthInsuranceCovenantDTOSet.addAll(mapDisabledHealthInsuranceCovenant());

        return healthInsuranceCovenantDTOSet;
    }

    private Set<HealthInsuranceCovenantDTO> mapEnabledHealthInsuranceCovenant(Set<HealthInsuranceDTO> healthInsuranceDTOS) {
        Set<HealthInsuranceCovenantDTO> enabledHealthInsuranceCovenant = healthInsuranceDTOS.stream()
                .map(healthInsurance -> HealthInsuranceCovenantDTO.builder()
                        .healthInsuranceDTO(healthInsurance)
                        .enabled(true)
                        .build()
                )
                .collect(Collectors.toSet());
        log.info("{} enabled health insurance covenant found", enabledHealthInsuranceCovenant.size());
        return enabledHealthInsuranceCovenant;
    }

    private Set<HealthInsuranceCovenantDTO> mapDisabledHealthInsuranceCovenant() {
        return healthInsuranceStorageService.findAll().stream()
                .map(healthInsuranceDTO -> HealthInsuranceCovenantDTO.builder()
                        .healthInsuranceDTO(healthInsuranceDTO)
                        .enabled(false)
                        .build())
                .collect(Collectors.toSet());
    }


    public User postProfessionalHealthInsuranceCovenants(int professionalUserId, Set<HealthInsuranceDTO> healthInsuranceDTOS) {
        User professionalUser = userService.findProfessionalUserById(professionalUserId);
        return updateProfessionalHealthInsuranceCovenant(professionalUser, healthInsuranceDTOS);
    }

    private User updateProfessionalHealthInsuranceCovenant(User user, Set<HealthInsuranceDTO> healthInsuranceDTOS) {
        Profesional professional = user.getProfesional();
        professional.setHealthInsuranceCovenant(
                updateHealthInsuranceCovenant(professional.getHealthInsuranceCovenant(), healthInsuranceDTOS));
        return completeProfessionalRegistrationService.updateAndIndexUser(user, professional);
    }

    private HealthInsuranceCovenant updateHealthInsuranceCovenant(HealthInsuranceCovenant oldHealthInsuranceCovenant,
                                                                  Set<HealthInsuranceDTO> healthInsuranceDTOS) {
        if (oldHealthInsuranceCovenant == null)
            oldHealthInsuranceCovenant = new HealthInsuranceCovenant();
        oldHealthInsuranceCovenant.setRegisteredDate(new Date());
        oldHealthInsuranceCovenant.setHealthInsurances(mapHealthInsuranceDTOtoHealthInsurance(healthInsuranceDTOS));
        log.info("{} new health insurances are covenanted", healthInsuranceDTOS.size());
        return oldHealthInsuranceCovenant;
    }

    private Set<HealthInsurance> mapHealthInsuranceDTOtoHealthInsurance(Set<HealthInsuranceDTO> healthInsuranceDTOS) {
        return healthInsuranceDTOS.stream()
                .map(HealthInsurance::new)
                .collect(Collectors.toSet());
    }

}
