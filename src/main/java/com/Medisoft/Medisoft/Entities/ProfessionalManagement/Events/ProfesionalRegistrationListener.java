package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Events;

import com.Medisoft.Medisoft.Entities.EmailManagement.Services.MailSenderService;
import com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement.IndexEngine;
import com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement.IndexingProfessionalDTO;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Componente que escucha el evento de registro completo de profesional y realiza una acción en consecuencia.-
 * En este caso envía un email al profesional y realiza la indexación en la base documental. Modela al suscriptor.-
 * Patrón: Publish - Subscribe.-
 *
 * @author Misael
 * @version 2020-02-11
 */
@Component
public class ProfesionalRegistrationListener implements ApplicationListener<OnProfesionalRegistrationCompleteEvent> {

    @Autowired
    private IndexEngine indexEngine;

    @Autowired
    private MailSenderService mailSenderService;

    @Override
    public void onApplicationEvent(OnProfesionalRegistrationCompleteEvent event) {
        this.deindex(event);
        this.index(event);
        this.informCompleteRegistration(event);
    }


    private void deindex(OnProfesionalRegistrationCompleteEvent event) {
        Document document = new Document();
        document.add(new StringField("id", event.getProfesional().getId().toString(), Field.Store.YES));
        indexEngine.deindex(document);
    }


    private void index(OnProfesionalRegistrationCompleteEvent event) {
        IndexingProfessionalDTO professional = event.getProfesional();
        Document document = indexEngine.createProfessionalDocument(
                professional.getId(),
                professional.getName(),
                professional.getSurname(),
                professional.getSpecialty());

        indexEngine.index(document);
    }


    private void informCompleteRegistration(OnProfesionalRegistrationCompleteEvent event) {
        String mensaje = "\tTu registro como Profesional en MediSoft ha sido completado al 100%!" +
                "\n\tA partir de ahora empezás a figurar en el buscador " +
                "y el resto de usuarios podrá acceder a tu perfil para conocer los servicios que ofrecés.- " +
                "\n\n\nAtentamente te saluda, el equipo de MediSoft.";

        mailSenderService.sendRegularEmail(event.getEmail(), "Registro Profesional Completo - MEDISOFT", mensaje);
    }

}
