package com.Medisoft.Medisoft.Entities.ProfessionalManagement;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Events.OnProfesionalRegistrationCompleteEvent;
import com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement.IndexingProfessionalDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;

@Service
public class CompleteProfessionalRegistrationService {

    private static ApplicationEventPublisher staticEventPublisher;
    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private UserService userService;


    public void isEnabledAsProfessional(User user) {
        if (user.isEnabledAsProfessional()) {
            user.getProfesional().setFechaHabilitacion(new Date());
            IndexingProfessionalDTO indexingProfessionalDTO = new IndexingProfessionalDTO(user);

            staticEventPublisher.publishEvent(new OnProfesionalRegistrationCompleteEvent(
                    indexingProfessionalDTO, user.getEmail()));
        } else {
            user.getProfesional().setFechaHabilitacion(null);
        }
    }

    public User updateAndIndexUser(User user, Profesional professional) {
        user.setProfesional(professional);
        isEnabledAsProfessional(user);

        return userService.save(user);
    }

    @PostConstruct
    private void initStaticEventPublisher() {
        CompleteProfessionalRegistrationService.staticEventPublisher = eventPublisher;
    }
}
