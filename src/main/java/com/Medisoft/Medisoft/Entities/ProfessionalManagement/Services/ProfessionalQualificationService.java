package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Services;

import com.Medisoft.Medisoft.Entities.EmailManagement.Services.EmailNotificationService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification.*;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


@Slf4j
@Service
public class ProfessionalQualificationService {

    @Autowired
    private QualificationRequestStorageService qualificationRequestStorageService;

    @Autowired
    private EmailNotificationService emailNotificationService;

    @Autowired
    private UserService userService;


    public void requestQualificationAsProfessional(QualificationRequestDTO qualificationRequestDto){
        log.info("Processing qualification request {}", qualificationRequestDto);
        QualificationRequestStatus newStatus = QualificationRequestStatus.IN_VALIDATION;
        int applicantUserId = qualificationRequestDto.getApplicantUserId();
        sendEmail(applicantUserId, newStatus);
        QualificationRequest qualificationRequest = QualificationRequest.builder()
                .applicantUserId(applicantUserId)
                .frontalIdPhoto(qualificationRequestDto.getFrontalIdPhoto())
                .rearIdPhoto(qualificationRequestDto.getRearIdPhoto())
                .selfiePhoto(qualificationRequestDto.getSelfiePhoto())
                .specialty(qualificationRequestDto.getSpecialty())
                .provincialLicense(qualificationRequestDto.getProvincialLicense())
                .specialtyLicense(qualificationRequestDto.getSpecialtyLicense())
                .status(newStatus)
                .requestTimestamp(new Date().getTime())
                .lastActionTimestamp(new Date().getTime())
                .build();
        qualificationRequestStorageService.save(qualificationRequest);
    }

    private void sendEmail(int userId, QualificationRequestStatus status){
        User user = userService.findById(userId);
        String applicantEmail = user.getEmail();
        switch (status){
            case IN_VALIDATION:
                emailNotificationService.notifyQualificationRequestInitialized(applicantEmail, user.getPersona().getFullName());
                break;
            case ACCEPTED:
                emailNotificationService.notifyQualificationRequestAccepted(applicantEmail);
                break;
            case DECLINED:
                emailNotificationService.notifyQualificationRequestDeclined(applicantEmail);
                break;
        }
    }


    public List<QualificationListViewDTO> getAllQualificationRequests(){
        log.info("Getting all qualification requests");
        List<QualificationRequest> requests = qualificationRequestStorageService.findAllSortedByRequestTimestamp();
        return mapToListView(requests);
    }

    private List<QualificationListViewDTO> mapToListView(List<QualificationRequest> requests){
        Map<Integer, User> userCache = new ConcurrentHashMap<>();
        List<QualificationListViewDTO> listViewDtos = requests.stream()
                .map(request -> buildQualificationListViewDto(request, userCache))
                .collect(Collectors.toList());
        listViewDtos.forEach(qualification -> log.info("Qualification list view {}", qualification));
        return listViewDtos;
    }

    private QualificationListViewDTO buildQualificationListViewDto(QualificationRequest request, Map<Integer, User> userCache){
        int applicantUserId = request.getApplicantUserId();
        User user = userCache.get(applicantUserId);
        if (user == null){
            user = userService.findById(applicantUserId);
            userCache.put(applicantUserId, user);
        }
        return new QualificationListViewDTO(request, user);
    }

    public QualificationDetailedViewDTO getQualificationRequestById(int requestId){
        log.info("Getting qualification request {}", requestId);
        QualificationRequest request = qualificationRequestStorageService.findById(requestId);
        return new QualificationDetailedViewDTO(request);
    }

    public void acceptQualificationRequest(int requestId){
        log.info("Accepting qualification request {}", requestId);
        QualificationRequest request = qualificationRequestStorageService.findById(requestId);
        updateRequestStatus(request, QualificationRequestStatus.ACCEPTED);
        enableAsProfessional(request);
    }

    public void declineQualificationRequest(int requestId){
        log.info("Declining qualification request {}", requestId);
        QualificationRequest request = qualificationRequestStorageService.findById(requestId);
        updateRequestStatus(request, QualificationRequestStatus.DECLINED);
    }

    private void updateRequestStatus(QualificationRequest request, QualificationRequestStatus newStatus){
        request.updateStatus(newStatus);
        sendEmail(request.getApplicantUserId(), newStatus);
        qualificationRequestStorageService.save(request);
    }

    private void enableAsProfessional(QualificationRequest request){
        int professionalUserId = request.getApplicantUserId();
        log.info("Enabling user {} as professional {}", professionalUserId, request);
        User professionalUser = userService.findById(professionalUserId);
        Profesional professional = createProfessionalFromQualificationRequest(request);
        professionalUser.setProfesional(professional);
        userService.save(professionalUser);
    }

    private Profesional createProfessionalFromQualificationRequest(QualificationRequest request){
        Profesional professional = new Profesional();
        professional.setMatricula(request.getProvincialLicense());
        professional.setSpecialtyLicense(request.getSpecialtyLicense());
        professional.setEspecialidad(request.getSpecialty());
        professional.setQualificationTimestamp(request.getLastActionTimestamp());
        log.info("New professional created {}", professional);
        return professional;
    }
}
