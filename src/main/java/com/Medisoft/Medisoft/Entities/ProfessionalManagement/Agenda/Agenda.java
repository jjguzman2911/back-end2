package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.IProfessionalRegistration;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Builder
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Agenda implements IProfessionalRegistration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<DiaAtencion> diasAtencion;
    private Date fechaRegistro;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Leave leave;


    @JsonIgnore
    @Override
    public boolean isComplete() {
        if (diasAtencion == null) return false;
        for (DiaAtencion diaAtencion : diasAtencion) {
            if (!diaAtencion.isComplete()) {
                return false;
            }
        }
        if (leave != null){
            return leave.isComplete();
        }
        return true;
    }

    @JsonIgnore
    public boolean isOnLeaveRange(long appointmentStartTimestamp){
        if (leave == null){
            return false;
        }
        return leave.isInRange(appointmentStartTimestamp);
    }
}
