package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class QualificationRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int applicantUserId;
    private String provincialLicense;
    private String specialtyLicense;
    @OneToOne(fetch = FetchType.EAGER)
    private Especialidad specialty;
    private String frontalIdPhoto;
    private String rearIdPhoto;
    private String selfiePhoto;
    private QualificationRequestStatus status;
    private String statusDetail;
    private long requestTimestamp;
    private long lastActionTimestamp;


    public void updateStatus(QualificationRequestStatus status){
        this.status = status;
        lastActionTimestamp = new Date().getTime();
    }
}
