package com.Medisoft.Medisoft.Entities.ProfessionalManagement;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationState {
    private boolean state;
    private String registrationName;

}
