package com.Medisoft.Medisoft.Entities.ProfessionalManagement.View;

import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ProfessionalProfileReviewDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.ReviewStatistic;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Services.ReviewStatisticStorageService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.DiaAtencion;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification.QualificationRequest;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification.QualificationRequestStatus;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Services.QualificationRequestStorageService;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;


@Slf4j
@Service
public class ProfessionalProfileService {

    @Autowired
    private UserService userService;

    @Autowired
    private ReviewStatisticStorageService reviewStatisticStorageService;

    @Autowired
    private QualificationRequestStorageService qualificationRequestStorageService;


    public ProfessionalProfileDTO getProfessionalProfile(int professionalUserId) {
        log.info("Getting professional profile");
        QualificationRequest lastQualificationRequest = qualificationRequestStorageService.findLastByApplicantUserId(professionalUserId);
        if (lastQualificationRequest == null) return null;
        return getProfileByQualificationStatus(lastQualificationRequest.getStatus(), professionalUserId);
    }

    private ProfessionalProfileDTO getProfileByQualificationStatus(QualificationRequestStatus status, int professionalUserId){
        log.info("Current qualification status {}", status);
        if (status == QualificationRequestStatus.ACCEPTED) {
            User user = userService.findProfessionalUserById(professionalUserId);
            return new ProfessionalProfileDTO(user);
        }
        return new ProfessionalProfileDTO(status);
    }

    public ProfessionalProfileForPatientDTO getProfessionalProfileForPatient(int professionalUserId) {
        log.info("Getting professional profile for patient");
        User user = userService.findProfessionalUserById(professionalUserId);
        return createProfessionalProfileForPatient(user);
    }

    private ProfessionalProfileForPatientDTO createProfessionalProfileForPatient(User professionalUser){
        return ProfessionalProfileForPatientDTO.builder()
                .user(professionalUser)
                .attentionCategories(convertProfessionalAttentionTypeToDTO(professionalUser.getProfesional().getAtenciones().getAtenciones()))
                .attentionDays(getAndSortAttentionDays(professionalUser.getProfesional().getAgenda().getDiasAtencion()))
                .review(findProfessionalReview(professionalUser.getId()))
                .build();
    }

    private Set<AttentionCategoryProfileForPatientDTO> convertProfessionalAttentionTypeToDTO(List<TipoAtencionProfesional> professionalAttentionTypes) {
        log.info("Getting available attentions");
        Set<AttentionCategoryProfileForPatientDTO> categories = professionalAttentionTypes.stream()
                .map(professionalAttentionType -> new AttentionCategoryProfileForPatientDTO(professionalAttentionType.getCategoriaTipoAtencion()))
                .collect(Collectors.toCollection(TreeSet::new));

        fillCategories(categories, professionalAttentionTypes);
        return categories;
    }

    private void fillCategories(Set<AttentionCategoryProfileForPatientDTO> categories, List<TipoAtencionProfesional> professionalAttentionTypes){
        for (AttentionCategoryProfileForPatientDTO attentionCategory : categories) {
            addAllAttentionTypesToCategory(attentionCategory, professionalAttentionTypes);
        }
    }

    private void addAllAttentionTypesToCategory(AttentionCategoryProfileForPatientDTO attentionCategory ,
                                                List<TipoAtencionProfesional> professionalAttentionTypes){
        for (TipoAtencionProfesional professionalAttentionType : professionalAttentionTypes) {
            addAttentionTypeToCategory(attentionCategory, professionalAttentionType);
        }
    }

    private void addAttentionTypeToCategory(AttentionCategoryProfileForPatientDTO attentionCategory,
                                            TipoAtencionProfesional professionalAttentionType) {
        if (attentionCategory.isCategoryAttentionType(professionalAttentionType.getCategoriaTipoAtencion())) {
            attentionCategory.addAttentionType(new AttentionTypeProfileForPatientDTO(professionalAttentionType.getName()));
        }
    }

    private Set<DiaAtencion> getAndSortAttentionDays(List<DiaAtencion> attentionDays) {
        log.info("Getting attention days");
        Set<DiaAtencion> attentionDaysSet = new TreeSet<>(attentionDays);
        attentionDaysSet.forEach(attentionDay -> Collections.sort(attentionDay.getHorarios()));
        return attentionDaysSet;
    }


    public ProfessionalProfileReviewDTO findProfessionalReview(int professionalUserId) {
        log.info("Getting reviews");
        ReviewStatistic reviewStatistic = reviewStatisticStorageService.findByProfessionalUserId(professionalUserId);
        return new ProfessionalProfileReviewDTO(reviewStatistic);
    }
}
