package com.Medisoft.Medisoft.Entities.ProfessionalManagement;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Modela las diferentes especialidades que pueden tener los profesionales de la salud.
 * Las disponibles hasta el momento son:
 * - Medicina Clínica.
 * - Odontología.
 * - Oftalmología.
 * - Nutrición.
 * - Psicología.
 *
 * @author Misael
 * @version 2020-01-27
 */
@Data
@Entity
public class Especialidad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;
}
