package com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance;

import com.Medisoft.Medisoft.DAO.JPA.IHealthInsuranceDAO;
import com.Medisoft.Medisoft.Exceptions.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class HealthInsuranceStorageService {

    @Autowired
    private IHealthInsuranceDAO healthInsuranceDAO;

    public HealthInsuranceDTO findById(int id) {
        HealthInsuranceDTO healthInsuranceDTO = healthInsuranceDAO.findById(id).orElse(null);
        if (healthInsuranceDTO == null) {
            throw new EntityNotFoundException(HealthInsuranceDTO.class.getCanonicalName());
        }
        log.info("Health insurance {} found", id);
        return healthInsuranceDTO;
    }

    public List<HealthInsuranceDTO> findAll() {
        return healthInsuranceDAO.findAll();
    }
}
