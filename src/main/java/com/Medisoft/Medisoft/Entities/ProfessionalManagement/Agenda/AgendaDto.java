package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda;

import lombok.Data;

import java.util.List;

@Data
public class AgendaDto {
    private List<DiaAtencion> attentionDays;
    private List<LeaveRange> leaveRanges;
}
