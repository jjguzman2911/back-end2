package com.Medisoft.Medisoft.Entities.ProfessionalManagement.View;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda.Agenda;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.AtencionesProfesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.MedicalOffice.Consultorio;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification.QualificationRequestStatus;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.RegistrationState;
import com.Medisoft.Medisoft.Entities.UserManagement.Person.TipoDocumento;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
public class ProfessionalProfileDTO {

    private String nombre;
    private String apellido;
    private String matricula;
    private String specialtyLicense;
    private TipoDocumento tipoDocumento;
    private String documento;
    private Especialidad especialidad;
    private boolean estadoProfesional;
    private List<RegistrationState> estadosHabilitacion;
    private Consultorio consultorio;
    private Agenda agenda;
    private AtencionesProfesional atencionesProfesional;
    private long medicalRecordId;
    private boolean worksWithMercadopago;
    private Set<HealthInsuranceDTO> healthInsuranceDTOS;
    private QualificationRequestStatus qualificationStatus;


    public ProfessionalProfileDTO(User usuario) {
        setApellido(usuario.getPersona().getApellido());
        setNombre(usuario.getPersona().getNombre());
        setTipoDocumento(usuario.getPersona().getTipoDocumento());
        setDocumento(usuario.getPersona().getDocumento());
        setEspecialidad(usuario.getProfesional().getEspecialidad());
        setMatricula(usuario.getProfesional().getMatricula());
        setSpecialtyLicense(usuario.getProfesional().getSpecialtyLicense());
        setEstadoProfesional(usuario.getProfesional().isHabilitado());
        setEstadosHabilitacion(usuario.getProfesional().registrationStates());

        if (usuario.getProfesional().getAgenda() != null) {
            setAgenda(usuario.getProfesional().getAgenda());
        }
        if (usuario.getProfesional().getAtenciones() != null) {
            setAtencionesProfesional(usuario.getProfesional().getAtenciones());
            simplificarCategoriaTipoAtencion();
        }
        if (usuario.getProfesional().getConsultorio() != null) {
            setConsultorio(usuario.getProfesional().getConsultorio());
        }
        setMedicalRecordId(usuario.getProfesional().getMedicalRecordId());
        setWorksWithMercadopago(usuario.getProfesional().isWorkingWithMercadopago());
        if (usuario.getProfesional().getHealthInsuranceCovenant() != null){
            setHealthInsuranceDTOS(usuario.getProfesional().getHealthInsuranceCovenant().getHealthInsurancesDTO());
        }
        setQualificationStatus(QualificationRequestStatus.ACCEPTED);
    }

    public ProfessionalProfileDTO(QualificationRequestStatus qualificationStatus){
        setQualificationStatus(qualificationStatus);
    }

    private void simplificarCategoriaTipoAtencion() {
        for (TipoAtencionProfesional tipoAtencionProfesional : atencionesProfesional.getAtenciones()) {
            if (tipoAtencionProfesional.getCategoriaTipoAtencion() != null) {
                tipoAtencionProfesional.getCategoriaTipoAtencion().setTiposAtencion(null);
                tipoAtencionProfesional.getCategoriaTipoAtencion().setEspecialidad(null);
            }
        }
    }
}
