package com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO;

import lombok.Data;

@Data
public class ProfessionalAttentionTypeDTO {
    private String categoryName;
    private float price;
    private int attentionTypeId;
    private String attentionTypeName;
    private boolean mandatory;
    private int categoryId;
    private int minimumWorkingUnit;
}
