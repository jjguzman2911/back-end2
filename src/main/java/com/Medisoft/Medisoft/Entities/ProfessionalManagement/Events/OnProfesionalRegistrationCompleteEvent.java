package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Events;

import com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement.IndexingProfessionalDTO;
import org.springframework.context.ApplicationEvent;

/**
 * Evento que permite gestionar la finalización del registro de profesional. Modela al publicador.-
 *
 * @author Misael
 * @pattern Publish-Subscribe.-
 * @created 11/02/2020
 * @lastModification 11/02/2020
 */
public class OnProfesionalRegistrationCompleteEvent extends ApplicationEvent {

    private IndexingProfessionalDTO profesional;
    private String email;


    public OnProfesionalRegistrationCompleteEvent(
            IndexingProfessionalDTO profesional, String email) {
        super(profesional);

        this.profesional = profesional;
        this.email = email;
    }

    public IndexingProfessionalDTO getProfesional() {
        return profesional;
    }

    public void setProfesional(IndexingProfessionalDTO profesional) {
        this.profesional = profesional;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
