package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "leaves")
public class Leave {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "leave_id", referencedColumnName = "id")
    private List<LeaveRange> ranges;
    private long timestampCreated;

    public boolean isComplete(){
        for (LeaveRange range : ranges){
            if (!range.isComplete()){
                return false;
            }
        }
        return true;
    }

    public boolean isInRange(long appointmentStartTimestamp){
        for (LeaveRange range : ranges){
            if (range.isInRange(appointmentStartTimestamp)){
                return true;
            }
        }
        return false;
    }
}
