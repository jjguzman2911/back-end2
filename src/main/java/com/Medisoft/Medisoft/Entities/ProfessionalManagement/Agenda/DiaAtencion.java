package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda;

import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

/**
 * Modela un día específico de Atención con todos sus horarios asociados.
 *
 * @author Misael
 * @version 2020-02-05
 */
@Data
@Entity
public class DiaAtencion implements Comparable<DiaAtencion>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<HorarioAtencion> horarios;

    @Enumerated(value = EnumType.STRING)
    private DiaSemana diaSemana;


    /**
     * Determina si el horario de atención es completo en base a las fechas y horas de inicio y fin.
     *
     * @return devuelve True en caso que el horario sea completo, False en caso de que no.
     */
    public boolean isComplete() {
        if (horarios == null || diaSemana == null) {
            return false;
        }
        for (HorarioAtencion horario : horarios) {
            if (!horario.isValid()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int compareTo(DiaAtencion o) {
        return diaSemana.compareTo(o.diaSemana);
    }
}
