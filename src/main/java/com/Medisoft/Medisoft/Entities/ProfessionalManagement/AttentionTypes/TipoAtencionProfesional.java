package com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes;

import com.Medisoft.Medisoft.Utilities.ApplicationConstantsUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class TipoAtencionProfesional {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(fetch = FetchType.EAGER)
    private TipoAtencion tipoAtencion;

    @OneToOne(fetch = FetchType.EAGER)
    private CategoriaTipoAtencion categoriaTipoAtencion;
    private int unidadMinimaTrabajo;
    private float precio;


    public TipoAtencionProfesional(TipoAtencion tipoAtencion) {
        this.tipoAtencion = tipoAtencion;
    }


    /**
     * Permite determinar si el tipo de atencion del profesional es completa.
     *
     * @return boolean, devuelve True si es completa, False en caso contrario.
     */
    public boolean esCompleto() {
        return unidadMinimaTrabajo != 0 && categoriaTipoAtencion != null && tipoAtencion != null;
    }


    /**
     * Calcula la duración del tipo de atención en base a la duración mínima de trabajo.
     *
     * @return devuelve la duración total del tipo de atención expresada en minutos.
     */
    public int calcularDuracion() {
        return unidadMinimaTrabajo * ApplicationConstantsUtils.APPOINTMENT_MINIMUM_DURATION;
    }

    @JsonIgnore
    public String getName() {
        return tipoAtencion != null ? tipoAtencion.getNombre() : null;
    }

    @JsonIgnore
    public boolean isMandatory() {
        return tipoAtencion.isObligatorio();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TipoAtencionProfesional that = (TipoAtencionProfesional) o;
        return Objects.equals(tipoAtencion, that.tipoAtencion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tipoAtencion);
    }
}
