package com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.Services;

import com.Medisoft.Medisoft.Entities.AttentionManagement.Attention;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Services.AttentionService;
import com.Medisoft.Medisoft.Entities.PatientManagement.PatientAssistedDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.Person.Persona;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProfessionalAssistedPatientsService {

    @Autowired
    private UserService userService;

    @Autowired
    private AttentionService attentionService;


    public List<PatientAssistedDTO> getPatientsAssistedDTO(int professionalUserId, int maxSize) {
        Set<Integer> patientUserIds = getPatientsUserIds(professionalUserId, maxSize);
        List<PatientAssistedDTO> patientAssistedDTOS = new ArrayList<>();
        loadPatientAssistedList(patientUserIds, patientAssistedDTOS);
        log.info("{} patients found", patientAssistedDTOS.size());
        return patientAssistedDTOS;
    }

    private Set<Integer> getPatientsUserIds(int professionalUserId, int maxSize) {
        return attentionService.findAll().stream()
                .filter(attention -> attention.getProfessionalUserId() == professionalUserId)
                .map(Attention::getPatientUserId)
                .limit(maxSize)
                .collect(Collectors.toSet());
    }

    private void loadPatientAssistedList(Set<Integer> patientUserIds, List<PatientAssistedDTO> patientAssistedDTOS) {
        patientUserIds.forEach(patientUserId -> {
            User patientUser = userService.findById(patientUserId);
            Persona person = patientUser.getPersona();
            patientAssistedDTOS.add(
                    new PatientAssistedDTO(patientUserId, patientUser.getName(), patientUser.getSurname(),
                            person.getDocumento(), person.getTipoDocumento().getNombre(), patientUser.getFotoUrl()));
        });
    }
}
