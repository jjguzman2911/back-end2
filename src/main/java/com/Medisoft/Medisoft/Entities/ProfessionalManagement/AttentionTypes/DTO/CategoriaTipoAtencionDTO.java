package com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.CategoriaTipoAtencion;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.Medisoft.Medisoft.Utilities.DTOMapperUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
public class CategoriaTipoAtencionDTO {
    private int id;
    private String nombre;
    private Set<TipoAtencionDTO> tiposAtencion;

    public CategoriaTipoAtencionDTO(TipoAtencionProfesional tipoAtencionProfesional) {
        setNombre(tipoAtencionProfesional.getCategoriaTipoAtencion().getNombre());
        setId(tipoAtencionProfesional.getCategoriaTipoAtencion().getId());
    }

    public CategoriaTipoAtencionDTO(CategoriaTipoAtencion category) {
        setId(category.getId());
        setNombre(category.getNombre());
        setTiposAtencion(DTOMapperUtils.toAttentionTypeDto(category.getTiposAtencion()));
    }

    @JsonIgnore
    public boolean isCategoriaTipoAtencion(CategoriaTipoAtencion categoriaTipoAtencion) {
        return id == categoriaTipoAtencion.getId();
    }

    public void addTipoAtencion(TipoAtencionDTO tipoAtencionDTO) {
        if (tiposAtencion == null) {
            tiposAtencion = new HashSet<>();
        }
        tiposAtencion.add(tipoAtencionDTO);
    }

    public void replaceAttentionTypes(Set<TipoAtencionDTO> newAttentionTypes) {
        if (tiposAtencion.removeAll(newAttentionTypes)) {
            tiposAtencion.addAll(newAttentionTypes);
        }
    }

    public boolean containsAttentionType(TipoAtencionDTO attention) {
        return tiposAtencion.contains(attention);
    }

    public void replaceAttentionType(TipoAtencionDTO attention) {
        if (tiposAtencion.remove(attention)) {
            tiposAtencion.add(attention);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoriaTipoAtencionDTO that = (CategoriaTipoAtencionDTO) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
