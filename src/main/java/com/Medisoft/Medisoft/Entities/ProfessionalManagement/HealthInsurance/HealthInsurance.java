package com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "intermediate_health_insurance")
public class HealthInsurance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    private HealthInsuranceDTO healthInsuranceDTO;

    public HealthInsurance(HealthInsuranceDTO healthInsuranceDTO) {
        setHealthInsuranceDTO(healthInsuranceDTO);
    }
}
