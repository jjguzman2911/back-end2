package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import lombok.Data;

@Data
public class QualificationRequestDTO {
    private int applicantUserId;
    private String provincialLicense;
    private String specialtyLicense;
    private Especialidad specialty;
    private String frontalIdPhoto;
    private String rearIdPhoto;
    private String selfiePhoto;
}
