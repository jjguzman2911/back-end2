package com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "health_insurance")
public class HealthInsuranceDTO implements Comparable<HealthInsuranceDTO> {

    @Id
    private int id;
    private String name;


    @JsonIgnore
    public boolean isValid() {
        return id > 0 && !StringUtils.isEmpty(name);
    }

    @Override
    public int compareTo(HealthInsuranceDTO o) {
        return Integer.compare(id, o.id);
    }
}
