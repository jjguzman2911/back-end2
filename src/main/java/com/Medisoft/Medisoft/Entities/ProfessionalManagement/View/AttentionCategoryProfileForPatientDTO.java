package com.Medisoft.Medisoft.Entities.ProfessionalManagement.View;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.CategoriaTipoAtencion;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.TreeSet;

@NoArgsConstructor
@Data
public class AttentionCategoryProfileForPatientDTO implements Comparable<AttentionCategoryProfileForPatientDTO>{
    private int id;
    private String name;
    private Set<AttentionTypeProfileForPatientDTO> attentionTypeProfileForPatientDTOs;

    public AttentionCategoryProfileForPatientDTO(CategoriaTipoAtencion attentionCategory){
        setId(attentionCategory.getId());
        setName(attentionCategory.getNombre());
    }

    @Override
    public int compareTo(AttentionCategoryProfileForPatientDTO o) {
        return name.compareTo(o.name);
    }

    public boolean isCategoryAttentionType(CategoriaTipoAtencion categoriaTipoAtencion){
        return id == categoriaTipoAtencion.getId();
    }

    public void addAttentionType(AttentionTypeProfileForPatientDTO attentionType){
        if (attentionTypeProfileForPatientDTOs == null)
            attentionTypeProfileForPatientDTOs = new TreeSet<>();
        attentionTypeProfileForPatientDTOs.add(attentionType);
    }
}
