package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification;

public enum QualificationRequestStatus {
    IN_VALIDATION, ACCEPTED, DECLINED
}
