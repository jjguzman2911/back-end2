package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda;

import com.Medisoft.Medisoft.Utilities.ApplicationConstantsUtils;
import com.Medisoft.Medisoft.Utilities.DateFormatConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Modela los horarios de atención del profesional en un formato HH:MM.
 *
 * @author Misael
 * @version 2020-02-10
 */
@Data
@Entity
public class HorarioAtencion implements Comparable<HorarioAtencion>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String horaInicio;
    private String horaFin;


    @JsonIgnore
    public boolean isValid() {
        int horaInicio = DateFormatConverter.horaStringAInteger(this.horaInicio);
        int horaFin = DateFormatConverter.horaStringAInteger(this.horaFin);
        if (horaFin <= horaInicio) return false;
        return (horaFin - horaInicio) % ApplicationConstantsUtils.APPOINTMENT_MINIMUM_DURATION == 0;
    }

    @Override
    public int compareTo(HorarioAtencion o) {
        return Integer.compare(DateFormatConverter.horaStringAInteger(horaInicio),
                DateFormatConverter.horaStringAInteger(o.horaInicio));
    }
}
