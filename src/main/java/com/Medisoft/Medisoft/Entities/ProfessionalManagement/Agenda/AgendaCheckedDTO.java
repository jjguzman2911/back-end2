package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Agenda;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class AgendaCheckedDTO {
    private List<DiaAtencion> attentionDays;
    private Set<Long> appointmentIdsToNullify;
    private List<LeaveRange> leaveRanges;
}
