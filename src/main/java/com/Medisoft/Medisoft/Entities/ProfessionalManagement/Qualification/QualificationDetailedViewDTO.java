package com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class QualificationDetailedViewDTO {
    private String frontalIdPhoto;
    private String rearIdPhoto;
    private String selfiePhoto;

    public QualificationDetailedViewDTO(QualificationRequest request){
        setFrontalIdPhoto(request.getFrontalIdPhoto());
        setRearIdPhoto(request.getRearIdPhoto());
        setSelfiePhoto(request.getSelfiePhoto());
    }
}
