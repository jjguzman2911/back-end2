package com.Medisoft.Medisoft.Entities.ProfessionalManagement.View;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Especialidad;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class ProfessionalsGrouperBySpecialtyUtil {


    public static Set<ProfessionalSetGroupedBySpecialty> groupBySpecialty(Set<User> professionalUsers) {
        Map<Especialidad, Set<ProfessionalForPatientHistoryDTO>> specialtyGroupedMap = new HashMap<>();
        professionalUsers.forEach(professional -> addToMap(specialtyGroupedMap, professional));
        return mapToProfessionalSetGroupedBySpecialty(specialtyGroupedMap);
    }

    private static void addToMap(Map<Especialidad, Set<ProfessionalForPatientHistoryDTO>> specialtyGroupedMap, User professionalUser) {
        ProfessionalForPatientHistoryDTO professional = new ProfessionalForPatientHistoryDTO(professionalUser);
        Especialidad specialty = professional.getSpecialty();
        if (specialtyGroupedMap.containsKey(specialty)) {
            addProfessionalToSpecialtyGroup(specialtyGroupedMap, specialty, professional);
        } else {
            addSpecialtyGroupAndProfesional(specialtyGroupedMap, specialty, professional);
        }
    }

    private static void addProfessionalToSpecialtyGroup(Map<Especialidad, Set<ProfessionalForPatientHistoryDTO>> specialtyGroupedMap,
                                                        Especialidad specialty,
                                                        ProfessionalForPatientHistoryDTO professional) {
        Set<ProfessionalForPatientHistoryDTO> professionalsSet = specialtyGroupedMap.get(specialty);
        professionalsSet.add(professional);
        specialtyGroupedMap.replace(specialty, professionalsSet);
    }

    private static void addSpecialtyGroupAndProfesional(Map<Especialidad, Set<ProfessionalForPatientHistoryDTO>> specialtyGroupedMap,
                                                        Especialidad specialty,
                                                        ProfessionalForPatientHistoryDTO professional) {
        Set<ProfessionalForPatientHistoryDTO> professionalsSet = new HashSet<>();
        professionalsSet.add(professional);
        specialtyGroupedMap.put(specialty, professionalsSet);
    }

    private static Set<ProfessionalSetGroupedBySpecialty> mapToProfessionalSetGroupedBySpecialty(
            Map<Especialidad, Set<ProfessionalForPatientHistoryDTO>> specialtyGroupedMap) {
        log.info("Mapping map to professional set grouped by specialty");
        return specialtyGroupedMap.entrySet().stream()
                .map(specialtyEntrySet -> new ProfessionalSetGroupedBySpecialty(
                        specialtyEntrySet.getKey(),
                        specialtyEntrySet.getValue()))
                .collect(Collectors.toSet());
    }

}
