package com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.IProfessionalRegistration;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Data
@Entity
public class HealthInsuranceCovenant implements IProfessionalRegistration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date registeredDate;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<HealthInsurance> healthInsurances;


    @Override
    public boolean isComplete() {
        return true;
    }

    public Set<HealthInsuranceDTO> getHealthInsurancesDTO() {
        if (healthInsurances == null) {
            return new TreeSet<>();
        }
        return healthInsurances.stream()
                .map(HealthInsurance::getHealthInsuranceDTO)
                .collect(Collectors.toSet());
    }
}
