package com.Medisoft.Medisoft.Entities.EmailManagement.Appointments;

import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;
import com.Medisoft.Medisoft.Entities.EmailManagement.IAdjuntableEnEmail;

/**
 * Clase concreta que permite el envio de emails al Paciente cuand el turno fue asignado.-
 *
 * @author Misael
 * @pattern Strategy
 * @created 24/03/2020
 * @lastModification 24/03/2020
 */
public class EmailPacienteTurnoAsignado extends EmailNotification {


    public EmailPacienteTurnoAsignado() {
    }

    public EmailPacienteTurnoAsignado(String to, IAdjuntableEnEmail comprobanteTurno) {
        super(to, comprobanteTurno);
    }

    @Override
    public String getMessage() {
        return "Muchas gracias por utilizar los servicios de MediSoft. A continuación se adjunta " +
                "el comprobante de tu turno. Recordá que no es necesario presentarlo al momento de realizar la consulta." +
                "\n\nAtentamente te saluda, el equipo de MediSoft.";
    }

    @Override
    public String getSubject() {
        return "MEDISOFT - NUEVO TURNO ASIGNADO";
    }
}
