package com.Medisoft.Medisoft.Entities.EmailManagement.ProfessionalQualification;

import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;

public class ApplicantEmailQualificationRequestAccepted extends EmailNotification {

    public ApplicantEmailQualificationRequestAccepted(String to) {
        super(to, null);
    }

    @Override
    public String getMessage() {
        return "Hola,\n" +
                "\n" +
                "tus datos han sido validado exitosamente y tu solicitud ha sido aceptada.\n" +
                "\n" +
                "\n" +
                "Te invitamos a ingresar a tu nuevo perfil como profesional y registres los datos solicitados para atender a tus pacientes.\n" +
                "\n" +
                "\n" +
                "Ante cualquier consulta quedamos a su disposición.\n" +
                "\n" +
                "\n" +
                "\n" +
                "Atentamente te saluda, el equipo de MediSoft";
    }

    @Override
    public String getSubject() {
        return "MEDISOFT - SOLICITUD DE ALTA DE PROFESIONAL ACEPTADA";
    }
}
