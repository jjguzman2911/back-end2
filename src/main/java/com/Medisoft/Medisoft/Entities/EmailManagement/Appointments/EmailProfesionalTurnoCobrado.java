package com.Medisoft.Medisoft.Entities.EmailManagement.Appointments;

import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;
import com.Medisoft.Medisoft.Entities.EmailManagement.IAdjuntableEnEmail;

public class EmailProfesionalTurnoCobrado extends EmailNotification {

    public EmailProfesionalTurnoCobrado() {
    }

    public EmailProfesionalTurnoCobrado(String to, IAdjuntableEnEmail comprobanteTurno) {
        super(to, comprobanteTurno);
    }

    @Override
    public String getMessage() {
        return "Se ha registrado un nuevo cobro. A continuación se adjunta el comprobante del mismo. " +
                "Recordá que el mismo no es válido como factura, " +
                "y sirve únicamente para que tengas una constancia del pago." +
                "\n\nAtentamente te saluda, el equipo de MediSoft.";
    }

    @Override
    public String getSubject() {
        return "MEDISOFT - COBRO DE TURNO REGISTRADO";
    }
}
