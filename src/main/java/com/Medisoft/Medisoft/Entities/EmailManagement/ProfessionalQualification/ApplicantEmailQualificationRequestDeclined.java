package com.Medisoft.Medisoft.Entities.EmailManagement.ProfessionalQualification;

import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;

public class ApplicantEmailQualificationRequestDeclined extends EmailNotification {

    public ApplicantEmailQualificationRequestDeclined(String to) {
        super(to, null);
    }

    @Override
    public String getMessage() {
        return "Hola,\n" +
                "\n" +
                "hubo un inconveniente con el registro de tus datos.\n" +
                "\n" +
                "\n" +
                "Por eso mismo te pedimos que revise los datos solicitados e intentes nuevamente.\n" +
                "\n" +
                "\n" +
                "Una vez enviados los datos adjuntos estaremos procesándolos y le enviaremos la confirmación de su solicitud a este mismo mail.\n" +
                "\n" +
                "\n" +
                "Te pedimos disculpas por el inconveniente, queremos ayudarle y resolverlo lo más rápido posible.\n" +
                "\n" +
                "\n" +
                "Ante cualquier consulta quedamos a su disposición.\n" +
                "\n" +
                "\n" +
                "\n" +
                "Atentamente te saluda, el equipo de MediSoft";
    }

    @Override
    public String getSubject() {
        return "MEDISOFT - SOLICITUD DE ALTA DE PROFESIONAL RECHAZADA";
    }
}
