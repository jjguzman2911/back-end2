package com.Medisoft.Medisoft.Entities.EmailManagement;


import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase abstracta que define un conjunto de emails enviables para los diferentes cambios de estado de un turno.-
 */
@Data
@NoArgsConstructor
public abstract class EmailNotification {
    private String to;
    private String subject;
    private String message;
    private IAdjuntableEnEmail attachment;

    public EmailNotification(String to, IAdjuntableEnEmail comprobanteTurno) {
        setTo(to);
        setAttachment(comprobanteTurno);
    }

    public abstract String getMessage();

    public abstract String getSubject();

    public boolean hasAttachment() {
        return attachment != null;
    }
}
