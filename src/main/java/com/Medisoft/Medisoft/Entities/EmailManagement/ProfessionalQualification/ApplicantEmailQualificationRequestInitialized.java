package com.Medisoft.Medisoft.Entities.EmailManagement.ProfessionalQualification;

import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;

public class ApplicantEmailQualificationRequestInitialized extends EmailNotification {

    public ApplicantEmailQualificationRequestInitialized(String to){
        super(to, null);
    }

    @Override
    public String getMessage() {
        return "Hola,\n" +
                "\n" +
                "Te informamos que tu solicitud de alta de profesional ha sido registrada correctamente.\n" +
                "\n" +
                "\n" +
                "En las próximas 48 hs un moderador se encargará de evaluar tu solicitud y te enviaremos los resultados por este medio.\n" +
                "\n" +
                "\n" +
                "Ante cualquier consulta quedamos a su disposición.\n" +
                "\n" +
                "\n" +
                "\n" +
                "Atentamente te saluda, el equipo de MediSoft\n";
    }

    @Override
    public String getSubject() {
        return "MEDISOFT - ALTA DE PROFESIONAL";
    }
}
