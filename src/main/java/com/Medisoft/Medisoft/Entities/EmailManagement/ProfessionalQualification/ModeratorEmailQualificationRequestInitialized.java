package com.Medisoft.Medisoft.Entities.EmailManagement.ProfessionalQualification;

import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;


public class ModeratorEmailQualificationRequestInitialized extends EmailNotification {
    private final String aspirantFullName;

    public ModeratorEmailQualificationRequestInitialized(String aspirantFullName){
        super("proyectomedisoft@gmail.com", null);
        this.aspirantFullName = aspirantFullName;
    }

    @Override
    public String getMessage() {
        return "Se ha registrado una nueva solicitud de alta de profesional de " + aspirantFullName + "." +
                "\nRecuerda revisar su información en el apartado correspondiente.";
    }

    @Override
    public String getSubject() {
        return "MEDISOFT - ALTA DE PROFESIONAL";
    }
}
