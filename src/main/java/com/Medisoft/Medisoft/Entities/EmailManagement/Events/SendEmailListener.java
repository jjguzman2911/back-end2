package com.Medisoft.Medisoft.Entities.EmailManagement.Events;

import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;
import com.Medisoft.Medisoft.Entities.EmailManagement.Services.MailSenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class SendEmailListener implements ApplicationListener<SendEmailEvent> {

    @Autowired
    private MailSenderService mailSenderService;


    @Override
    public void onApplicationEvent(SendEmailEvent sendEmailEvent) {
        this.sendEmail(sendEmailEvent);
    }


    @Async
    void sendEmail(SendEmailEvent event) {
        EmailNotification email = event.getEmail();
        log.info("Sending email {}", email);
        String to = email.getTo();
        String subject = email.getSubject();
        String message = email.getMessage();
        if (email.hasAttachment()) {
            mailSenderService.sendEmailWithAttachment(to, subject, message, email.getAttachment().getFileName());
        } else {
            mailSenderService.sendRegularEmail(to, subject, message);
        }
    }
}
