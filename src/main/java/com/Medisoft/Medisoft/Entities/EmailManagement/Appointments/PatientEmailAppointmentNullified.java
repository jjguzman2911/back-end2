package com.Medisoft.Medisoft.Entities.EmailManagement.Appointments;

import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class PatientEmailAppointmentNullified extends EmailNotification {

    private String date;
    private String professionalFullName;
    private String specialty;

    public PatientEmailAppointmentNullified(String to, String date, String professionalFullName, String specialty) {
        super(to, null);
        this.date = date;
        this.professionalFullName = professionalFullName;
        this.specialty = specialty;
    }

    @Override
    public String getMessage() {
        StringBuilder builder = new StringBuilder();
        builder.append("Hola! Te informamos que tu turno para el día ");
        builder.append(date);
        builder.append(" ha sido anulado por tu profesional ");
        builder.append(professionalFullName);
        builder.append(", especialista en ");
        builder.append(specialty);
        builder.append(".");
        builder.append("\nEn caso de que hayas realizado el pago del mismo, se te hará el reintegro del monto correspondiente.");
        builder.append("\n\nAtentamente te saluda, el equipo de MediSoft.");
        return builder.toString();
    }

    @Override
    public String getSubject() {
        return "MEDISOFT - TURNO ANULADO";
    }

}
