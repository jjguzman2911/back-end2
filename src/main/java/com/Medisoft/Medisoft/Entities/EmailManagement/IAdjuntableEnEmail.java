package com.Medisoft.Medisoft.Entities.EmailManagement;


/**
 * Interfaz que determina si un archivo es adjuntable en un email o no.-
 *
 * @author Misael
 * @created 24/03/2020
 * @lastModification 24/03/2020
 */
public interface IAdjuntableEnEmail {

    String getFileName();
}
