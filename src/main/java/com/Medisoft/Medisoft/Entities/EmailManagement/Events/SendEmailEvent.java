package com.Medisoft.Medisoft.Entities.EmailManagement.Events;

import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class SendEmailEvent extends ApplicationEvent {
    private EmailNotification email;

    public SendEmailEvent(EmailNotification email) {
        super(email);
        this.email = email;
    }
}
