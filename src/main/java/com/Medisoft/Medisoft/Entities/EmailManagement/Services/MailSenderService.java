package com.Medisoft.Medisoft.Entities.EmailManagement.Services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;


@Slf4j
@Service
public class MailSenderService {
    private final String from;
    private final String userName;
    private final String password;
    private final Properties props;

    public MailSenderService() {
        this.from = "proyectomedisoft@gmail.com";
        this.userName = "proyectomedisoft";
        this.password = "klrmslnxrbvzudiw";
        this.props = new Properties();
        loadGmailProperties();
    }

    private void loadGmailProperties() {
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.ssl.trust", "smtp.gmail.com");
    }


    public void sendRegularEmail(String to, String subject, String message) {
        Session session = buildSession();
        Message mimeMessage = buildMessage(session, to, subject);

        setMessage(mimeMessage, message);
        sendMessage(session, mimeMessage, true);
        log.info("Email sent");
    }

    private Session buildSession() {
        return Session.getDefaultInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        });
    }

    private Message buildMessage(Session session, String to, String subject) {
        Message mimeMessage = new MimeMessage(session);
        setToFromAndSubject(mimeMessage, to, subject);
        return mimeMessage;
    }

    private void setToFromAndSubject(Message mimeMessage, String to, String subject) {
        try {
            mimeMessage.setFrom(new InternetAddress(from));
            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            mimeMessage.setSubject(subject);
        } catch (MessagingException e) {
            log.error("", e);
        }
    }

    private void setMessage(Message mimeMessage, String message) {
        try {
            mimeMessage.setText(message);
        } catch (MessagingException e) {
            log.error("", e);
        }
    }

    private void sendMessage(Session session, Message mimeMessage, boolean canRetry) {
        try {
            Transport transport = session.getTransport("smtp");
            transport.connect(from, password);
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
            transport.close();
        } catch (MessagingException e) {
            log.error("", e);
            if (canRetry) {
                retry(session, mimeMessage);
            }
        }
    }

    private void retry(Session session, Message mimeMessage) {
        log.info("Retrying to send message");
        int retries = 3;
        long timeInMillisToWait = 5000;
        double exponentialConstant = 1.7;
        while (retries > 0) {
            try {
                log.info("Waiting for {} secs", timeInMillisToWait);
                Thread.sleep(timeInMillisToWait);
                sendMessage(session, mimeMessage, false);
                retries--;
                timeInMillisToWait *= exponentialConstant;
            } catch (InterruptedException e) {
                log.error("", e);
            }
        }
    }


    public void sendEmailWithAttachment(String to, String subject, String message, String fileName) {
        Session session = buildSession();
        Message mimeMessage = buildMessage(session, to, subject);

        setContent(mimeMessage, message, fileName);
        sendMessage(session, mimeMessage, true);
        log.info("Email with attachment sent");
    }

    private void setContent(Message mimeMessage, String message, String fileName) {
        try {
            mimeMessage.setContent(createMultipart(message, fileName));
        } catch (MessagingException e) {
            log.error("", e);
        }
    }

    private Multipart createMultipart(String message, String fileName) throws MessagingException {
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(createMessageBodyPart(message));
        multipart.addBodyPart(createAttachmentBodyPart(fileName));
        return multipart;
    }

    private BodyPart createMessageBodyPart(String message) throws MessagingException {
        BodyPart bodyPart = new MimeBodyPart();
        bodyPart.setText(message);
        return bodyPart;
    }

    private BodyPart createAttachmentBodyPart(String fileName) throws MessagingException {
        BodyPart bodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(fileName);
        bodyPart.setDataHandler(new DataHandler(source));
        bodyPart.setFileName(fileName);
        return bodyPart;
    }
}