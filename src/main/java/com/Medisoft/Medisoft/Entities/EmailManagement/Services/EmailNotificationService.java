package com.Medisoft.Medisoft.Entities.EmailManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.EmailManagement.Appointments.*;
import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;
import com.Medisoft.Medisoft.Entities.EmailManagement.Events.SendEmailEvent;
import com.Medisoft.Medisoft.Entities.EmailManagement.IAdjuntableEnEmail;
import com.Medisoft.Medisoft.Entities.EmailManagement.ProfessionalQualification.ApplicantEmailQualificationRequestAccepted;
import com.Medisoft.Medisoft.Entities.EmailManagement.ProfessionalQualification.ApplicantEmailQualificationRequestDeclined;
import com.Medisoft.Medisoft.Entities.EmailManagement.ProfessionalQualification.ApplicantEmailQualificationRequestInitialized;
import com.Medisoft.Medisoft.Entities.EmailManagement.ProfessionalQualification.ModeratorEmailQualificationRequestInitialized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

@Service
public class EmailNotificationService {

    @Autowired
    private ApplicationEventPublisher eventPublisher;


    public void notifyAppointmentAssignedPatient(String to, IAdjuntableEnEmail comprobante) {
        EmailNotification tipoEmail = new EmailPacienteTurnoAsignado(to, comprobante);
        eventPublisher.publishEvent(new SendEmailEvent(tipoEmail));
    }

    public void notifyAppointmentAssignedProfessional(String to, IAdjuntableEnEmail comprobante) {
        EmailNotification tipoEmail = new EmailProfesionalTurnoAsignado(to, comprobante);
        eventPublisher.publishEvent(new SendEmailEvent(tipoEmail));
    }

    public void notifyAppointmentPaidPatient(String to, IAdjuntableEnEmail comprobante) {
        EmailNotification tipoEmail = new EmailPacienteTurnoCobrado(to, comprobante);
        eventPublisher.publishEvent(new SendEmailEvent(tipoEmail));
    }

    public void notifyAppointmentPaidProfessional(String to, IAdjuntableEnEmail comprobante) {
        EmailNotification tipoEmail = new EmailProfesionalTurnoCobrado(to, comprobante);
        eventPublisher.publishEvent(new SendEmailEvent(tipoEmail));
    }

    public void notifyAppointmentNullification(Turno appointment) {
        String to = appointment.getPaciente().getEmail();
        String date = appointment.getFechaHoraFin();
        String professionalFullName = appointment.getProfesional().getNombreApellido();
        String specialty = appointment.getEspecialidad().getNombre();
        sendEmailNullified(to, date, professionalFullName, specialty);
    }

    private void sendEmailNullified(String to, String date, String professionalFullName, String specialty) {
        EmailNotification email = new PatientEmailAppointmentNullified(to, date, professionalFullName, specialty);
        eventPublisher.publishEvent(new SendEmailEvent(email));
    }

    public void notifyQualificationRequestInitialized(String to, String aspirantFullName){
        EmailNotification applicantEmail = new ApplicantEmailQualificationRequestInitialized(to);
        eventPublisher.publishEvent(new SendEmailEvent(applicantEmail));

        EmailNotification moderatorEmail = new ModeratorEmailQualificationRequestInitialized(aspirantFullName);
        eventPublisher.publishEvent(new SendEmailEvent(moderatorEmail));
    }

    public void notifyQualificationRequestAccepted(String to){
        EmailNotification applicantEmail = new ApplicantEmailQualificationRequestAccepted(to);
        eventPublisher.publishEvent(new SendEmailEvent(applicantEmail));
    }

    public void notifyQualificationRequestDeclined(String to){
        EmailNotification applicantEmail = new ApplicantEmailQualificationRequestDeclined(to);
        eventPublisher.publishEvent(new SendEmailEvent(applicantEmail));
    }

}
