package com.Medisoft.Medisoft.Entities.EmailManagement.Appointments;


import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;
import com.Medisoft.Medisoft.Entities.EmailManagement.IAdjuntableEnEmail;

/**
 * Clase concreta que permite el envio de emails al Profesional cuand el turno fue asignado.-
 *
 * @author Misael
 * @pattern Strategy
 * @created 24/03/2020
 * @lastModification 24/03/2020
 */
public class EmailProfesionalTurnoAsignado extends EmailNotification {

    public EmailProfesionalTurnoAsignado() {
    }

    public EmailProfesionalTurnoAsignado(String to, IAdjuntableEnEmail comprobanteTurno) {
        setTo(to);
        setAttachment(comprobanteTurno);
    }

    @Override
    public String getMessage() {
        return "Se ha registrado un nuevo turno para su atención. A continuación se adjunta " +
                "el comprobante del mismo. Recordá que no es necesario que lo presenten al momento de realizar la " +
                "consulta." +
                "\n\nAtentamente te saluda, el equipo de MediSoft.";
    }

    @Override
    public String getSubject() {
        return "MEDISOFT - NUEVO TURNO ASIGNADO";
    }
}
