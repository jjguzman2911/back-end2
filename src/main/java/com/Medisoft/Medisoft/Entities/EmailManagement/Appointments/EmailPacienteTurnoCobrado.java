package com.Medisoft.Medisoft.Entities.EmailManagement.Appointments;

import com.Medisoft.Medisoft.Entities.EmailManagement.EmailNotification;
import com.Medisoft.Medisoft.Entities.EmailManagement.IAdjuntableEnEmail;

public class EmailPacienteTurnoCobrado extends EmailNotification {


    public EmailPacienteTurnoCobrado() {
    }

    public EmailPacienteTurnoCobrado(String to, IAdjuntableEnEmail comprobante) {
        super(to, comprobante);
    }


    @Override
    public String getMessage() {
        return "Muchas gracias por utilizar los servicios de MediSoft. A continuación se adjunta " +
                "el comprobante del pago de tu turno. Recordá que este comprobante no es válido como factura, " +
                "y sirve únicamente para que tengas una constancia de tu pago." +
                "\n\nAtentamente te saluda, el equipo de MediSoft.";
    }

    @Override
    public String getSubject() {
        return "MEDISOFT - COBRO DE TURNO REGISTRADO";
    }
}
