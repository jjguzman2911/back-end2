package com.Medisoft.Medisoft.Entities.SlipManagement.AppointmentSlip;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.SlipManagement.ComprobanteTableData;

/**
 * Modela los datos que se cargan en la tabla del cuerpo del comprobante del turno.-
 *
 * @author Misael
 * @created 22/03/2020
 * @lastModification 23/03/2020
 */
public class ComprobanteTurnoTableData implements ComprobanteTableData {
    private String fechaHora;
    private String duracion;
    private String tipoAtencion;
    private String especialidad;
    private String direccion;
    private String profesional;
    private String paciente;

    public ComprobanteTurnoTableData() {

    }

    public ComprobanteTurnoTableData(Turno turno) {
        setFechaHora(turno.getFechaHoraInicio());
        setDuracion(turno.calcularDuracion() + " min");
        setTipoAtencion(turno.getTipoAtencion().getTipoAtencion().getNombre());
        setEspecialidad(turno.getEspecialidad().getNombre());
        setDireccion(turno.getConsultorio().toString());
        setProfesional(turno.getProfesional().getNombreApellido());
        setPaciente(turno.getPaciente().getNombreApellido());
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getTipoAtencion() {
        return tipoAtencion;
    }

    public void setTipoAtencion(String tipoAtencion) {
        this.tipoAtencion = tipoAtencion;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getProfesional() {
        return profesional;
    }

    public void setProfesional(String profesional) {
        this.profesional = profesional;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }
}
