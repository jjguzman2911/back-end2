package com.Medisoft.Medisoft.Entities.SlipManagement.AppointmentSlip;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.SlipManagement.ComprobanteBuilderPDF;
import com.Medisoft.Medisoft.Entities.SlipManagement.ComprobanteTableSettings;
import com.Medisoft.Medisoft.Utilities.ApplicationConstantsUtils;


public class ComprobanteTurnoBuilderPDF extends ComprobanteBuilderPDF {

    public ComprobanteTurnoBuilderPDF() {
    }

    public ComprobanteTurnoBuilderPDF(Turno turno) {
        comprobanteTableSettings = new ComprobanteTableSettings(turno);
        documentName = turno.getId() + "";
        titulo = "Comprobante de turno";
        filePath = ApplicationConstantsUtils.APPOINTMENT_SLIP_PATH;
        leyenda = "Gracias por utilizar los servicios de Medisoft. Te recordamos que no es " +
                "necesario presentar este comprobante al momento de realizar la consulta.";
        espacioCuerpoPie = 3;
    }
}
