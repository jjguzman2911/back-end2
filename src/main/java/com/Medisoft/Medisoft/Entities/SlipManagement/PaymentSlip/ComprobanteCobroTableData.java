package com.Medisoft.Medisoft.Entities.SlipManagement.PaymentSlip;

import com.Medisoft.Medisoft.Entities.PaymentManagement.AppointmentPayment;
import com.Medisoft.Medisoft.Entities.SlipManagement.ComprobanteTableData;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ComprobanteCobroTableData implements ComprobanteTableData {
    private String fechaHora;
    private String numeroTransaccion;
    private String tipoAtencion;
    private String paciente;
    private String obraSocial;
    private String importe;

    public ComprobanteCobroTableData(AppointmentPayment appointmentPayment) {
        setFechaHora(appointmentPayment.getFechaCreacionPreety());
        setNumeroTransaccion(appointmentPayment.getNumeroTransaccion() + "");
        setTipoAtencion(appointmentPayment.getAppointment().getTipoAtencion().getTipoAtencion().getNombre());
        setImporte("$ " + appointmentPayment.getFinalPrice());
        setPaciente(appointmentPayment.getAppointment().getPaciente().getNombreApellido());
        setObraSocial(appointmentPayment.getHealthInsurance());
    }

}
