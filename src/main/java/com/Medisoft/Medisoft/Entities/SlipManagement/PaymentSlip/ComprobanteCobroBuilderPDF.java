package com.Medisoft.Medisoft.Entities.SlipManagement.PaymentSlip;

import com.Medisoft.Medisoft.Entities.PaymentManagement.AppointmentPayment;
import com.Medisoft.Medisoft.Entities.SlipManagement.ComprobanteBuilderPDF;
import com.Medisoft.Medisoft.Entities.SlipManagement.ComprobanteTableSettings;
import com.Medisoft.Medisoft.Utilities.ApplicationConstantsUtils;


public class ComprobanteCobroBuilderPDF extends ComprobanteBuilderPDF {

    public ComprobanteCobroBuilderPDF() {
    }

    public ComprobanteCobroBuilderPDF(AppointmentPayment appointmentPayment) {
        comprobanteTableSettings = new ComprobanteTableSettings(appointmentPayment);
        documentName = appointmentPayment.getNumeroTransaccion() + "";
        titulo = "Comprobante de cobro";
        filePath = ApplicationConstantsUtils.PAYMENT_SLIP_PATH;
        leyenda = "Gracias por utilizar los servicios de Medisoft. Te recordamos que este no es un comprobante legal," +
                " y sirve únicamente para que tengas una constancia de tu pago.";
        espacioCuerpoPie = 4;
    }
}
