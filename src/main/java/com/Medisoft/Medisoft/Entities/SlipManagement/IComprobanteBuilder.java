package com.Medisoft.Medisoft.Entities.SlipManagement;

public interface IComprobanteBuilder {

    void construirEncabezado();

    void construirCuerpo();

    void construirPiePagina();

    void agregarMetaData();

    void construirComprobante();

    IComprobante getComprobante();
}