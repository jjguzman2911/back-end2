package com.Medisoft.Medisoft.Entities.SlipManagement;

import com.Medisoft.Medisoft.Entities.EmailManagement.IAdjuntableEnEmail;
import lombok.Data;

/**
 * Modela un archivo de comprobante. Representa el producto en el patrón Builder.-
 *
 * @author Misael
 * @created 22/03/2020
 * @lastModification 23/03/2020
 */
@Data
public class Comprobante implements IComprobante, IAdjuntableEnEmail {
    private String fileName;
}
