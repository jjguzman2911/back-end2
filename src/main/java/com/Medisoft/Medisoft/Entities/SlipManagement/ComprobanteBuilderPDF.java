package com.Medisoft.Medisoft.Entities.SlipManagement;

import com.Medisoft.Medisoft.Utilities.DateFormatConverter;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Modela un constructor de comprobantes en PDF. Delega las implementaciones concretas a las clases hijas.-
 *
 * @author Misael
 * @pattern Builder
 * @created 22/03/2020
 * @lastModification 23/03/2020
 */
public abstract class ComprobanteBuilderPDF implements IComprobanteBuilder {

    protected static final Font footerFont = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
    protected static final Font bodyBoldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    protected static final Font bodyNormalFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
    protected static final Font titleFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, Font.BOLD | Font.UNDERLINE);
    protected static String filePath;
    protected static String documentName;
    protected static Document document;
    protected static IComprobante comprobante;
    protected static ComprobanteTableSettings comprobanteTableSettings;
    protected static String titulo;
    protected static String leyenda;
    protected static int espacioCuerpoPie;

    /**
     * Agrega n línea vacía a un párrafo.-
     *
     * @param paragraph representa el párrafo.-
     * @param number    representa la cantidad de líneas vacías para a agregar.-
     */
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    @Override
    public void construirEncabezado() {
        try {
            Paragraph encabezado = new Paragraph();

            addImagen(encabezado);
            addTitulo(encabezado);

            document.add(encabezado);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Añade una imagen al encabezado.-
     *
     * @param encabezado representa el encabezado.-
     */
    private void addImagen(Paragraph encabezado) {
        try {
            Image img = Image.getInstance("slips/Logo.png");
            img.scalePercent(6);
            img.setBorder(Image.BOX);
            img.setAlignment(Element.ALIGN_LEFT);
            encabezado.add(img);
        } catch (IOException | BadElementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Añade un título al encabezado.-
     *
     * @param encabezado representa el encabezado.-
     */
    private void addTitulo(Paragraph encabezado) {
        Paragraph title = new Paragraph(titulo, titleFont);
        title.setAlignment(Element.ALIGN_CENTER);
        encabezado.add(title);
    }

    @Override
    public void construirCuerpo() {
        try {
            Paragraph cuerpo = new Paragraph();
            addEmptyLine(cuerpo, 1);

            addTabla(cuerpo);
            addLeyenda(cuerpo);

            document.add(cuerpo);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Añade una tabla al cuerpo del comprobante.-
     *
     * @param cuerpo representa el cuerpo.-
     */
    private void addTabla(Paragraph cuerpo) {
        PdfPTable table = new PdfPTable(1);
        String[] datos = comprobanteTableSettings.loadData();
        String[] campos = comprobanteTableSettings.getCampos();

        for (int i = 0; i < campos.length; i++) {
            Phrase nombreFila = new Phrase();
            nombreFila.add(new Chunk(campos[i], bodyBoldFont));
            if (datos[i] != null) {
                nombreFila.add(new Chunk(datos[i], bodyNormalFont));
            }
            PdfPCell row = new PdfPCell(nombreFila);
            if (i % 2 == 0) {
                row.setBackgroundColor(BaseColor.LIGHT_GRAY);
            } else {
                row.setBackgroundColor(BaseColor.WHITE);
            }

            row.setBorderWidth(1);
            row.setBorderWidthTop(0);
            row.setBorderWidthBottom(0);
            row.setHorizontalAlignment(Element.ALIGN_LEFT);
            row.setVerticalAlignment(Element.ALIGN_MIDDLE);

            if (i == 0) {
                row.setBorderWidthTop(1);
            }

            if (i == campos.length - 1) {
                row.setBorderWidthBottom(1);
            }
            table.addCell(row);
        }

        cuerpo.add(table);
    }

    /**
     * Añade una leyenda al cuerpo del comprobante.-
     *
     * @param cuerpo representa el cuerpo.-
     */
    private void addLeyenda(Paragraph cuerpo) {
        PdfPTable table = new PdfPTable(1);

        Paragraph legend = new Paragraph(leyenda, footerFont);
        legend.setAlignment(Element.ALIGN_CENTER);

        PdfPCell cell = new PdfPCell(legend);
        cell.setBorderWidth(1);
        cell.setBorderWidthTop(0);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        table.addCell(cell);
        cuerpo.add(table);
    }


    @Override
    public void construirPiePagina() {
        try {
            Paragraph pie = new Paragraph();

            addEmptyLine(pie, espacioCuerpoPie);

            addDatosContacto(pie);
            addFechaHoraGeneracion(pie);

            document.add(pie);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Añade los datos de contacto al pie del comprobante.-
     *
     * @param pie representa el pie.-
     */
    private void addDatosContacto(Paragraph pie) {
        Paragraph datosContacto = new Paragraph("Datos de contacto",
                new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.UNDERLINE | Font.BOLD, BaseColor.BLACK));
        List list = new List();

        ListItem email = new ListItem("Email: proyectomedisoft@gmail.com", footerFont);
        list.add(email);

        ListItem telefono = new ListItem("Telefono: (+54) 9 3544 588818", footerFont);
        list.add(telefono);

        datosContacto.add(list);
        pie.add(datosContacto);
    }

    /**
     * Añade la fecha y hora de generación al pie del comprobante.-
     *
     * @param pie representa el pie.-
     */
    private void addFechaHoraGeneracion(Paragraph pie) {
        Paragraph fechaHoraGeneracion = new Paragraph("Documento generado por MediSoft el " +
                DateFormatConverter.parseDateToString(new Date()), footerFont);
        fechaHoraGeneracion.setAlignment(Element.ALIGN_RIGHT);
        pie.add(fechaHoraGeneracion);
    }


    @Override
    public void agregarMetaData() {
        document.addTitle("Comprobante de Turno");
        document.addSubject("Built with IText");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("MediSoft");
        document.addCreator("MediSoft");
    }


    @Override
    public void construirComprobante() {
        document = new Document(PageSize.A5.rotate());
        try {
            String fileName = filePath + "/" + documentName + ".pdf";

            PdfWriter.getInstance(document, new FileOutputStream(fileName));
            document.open();

            construirEncabezado();
            construirCuerpo();
            construirPiePagina();
            agregarMetaData();

            document.close();

            comprobante = new Comprobante();
            comprobante.setFileName(fileName);
        } catch (FileNotFoundException | DocumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public IComprobante getComprobante() {
        return comprobante;
    }
}
