package com.Medisoft.Medisoft.Entities.SlipManagement;

/**
 * Define una interfaz para la creación del producto ComprobanteTurno.-
 *
 * @author Misael
 * @pattern Builder
 * @created 22/03/2020
 * @lastModification 24/03/2020
 */
public interface IComprobante {

    void setFileName(String fileName);
}
