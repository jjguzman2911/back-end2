package com.Medisoft.Medisoft.Entities.SlipManagement;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.PaymentManagement.AppointmentPayment;
import com.Medisoft.Medisoft.Entities.SlipManagement.AppointmentSlip.ComprobanteTurnoTableData;
import com.Medisoft.Medisoft.Entities.SlipManagement.PaymentSlip.ComprobanteCobroTableData;
import lombok.NoArgsConstructor;

import java.lang.reflect.Field;

@NoArgsConstructor
public class ComprobanteTableSettings {
    private String[] campos;
    private ComprobanteTableData datos;


    public ComprobanteTableSettings(Turno turno) {
        campos = new String[]{
                "Fecha y hora: ",
                "Duración: ",
                "Tipo de atención: ",
                "Especialidad: ",
                "Consultorio: ",
                "Profesional: ",
                "Paciente: "
        };
        datos = new ComprobanteTurnoTableData(turno);
    }

    public ComprobanteTableSettings(AppointmentPayment appointmentPayment) {
        campos = new String[]{
                "Fecha y hora: ",
                "Nº de transacción: ",
                "Tipo de atención: ",
                "Paciente: ",
                "Obra social: ",
                "Importe: ",
        };
        datos = new ComprobanteCobroTableData(appointmentPayment);
    }


    public String[] getCampos() {
        return campos;
    }

    /**
     * Lee los atributos de la clase ComprobanteTurnoDatosTabla y los mapea como strings.-
     *
     * @return String[], devuelve un array de string con los valores de los atributos de ComprobanteTurnoDatosTabla.
     * @see ComprobanteTurnoTableData
     */
    public String[] loadData() {
        Field[] valores = this.datos.getClass().getDeclaredFields();
        String[] datos = new String[valores.length];
        try {
            for (int i = 0; i < datos.length; i++) {
                valores[i].setAccessible(true);
                if (valores[i].get(this.datos) != null) {
                    datos[i] = valores[i].get(this.datos).toString();
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return datos;
    }
}
