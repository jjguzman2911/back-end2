package com.Medisoft.Medisoft.Entities.SlipManagement;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Slf4j
@Service
public class FileService {

    public InputStreamResource getFile(String fileName, String fileBasePath) {
        String filePath = fileBasePath + "/" + fileName;
        log.info("Getting file in {}", filePath);
        try {
            File file = new File(filePath);
            return new InputStreamResource(FileUtils.openInputStream(file));
        } catch (IOException e) {
            log.error("", e);
            throw new com.Medisoft.Medisoft.Exceptions.IOException(FileService.log.getName());
        }
    }
}
