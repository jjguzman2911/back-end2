package com.Medisoft.Medisoft.Entities.UserManagement;

import com.Medisoft.Medisoft.DAO.JPA.IVerificationTokenDAO;
import com.Medisoft.Medisoft.Entities.SessionManagement.VerificationToken;
import com.Medisoft.Medisoft.Exceptions.TokenNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class TokenService {

    @Autowired
    private IVerificationTokenDAO verificationTokenDAO;


    public VerificationToken save(VerificationToken token) {
        try {
            VerificationToken savedToken = verificationTokenDAO.save(token);
            log.info("Token " + savedToken.getId() + " saved");
            return savedToken;
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            throw new com.Medisoft.Medisoft.Exceptions.DataIntegrityViolationException(VerificationToken.class.getSimpleName());
        }
    }

    public VerificationToken findByToken(String token) {
        VerificationToken verificationToken = verificationTokenDAO.findByToken(token);
        if (verificationToken == null) {
            throw new TokenNotFoundException(token);
        }
        return verificationToken;
    }

    public String buildToken() {
        return UUID.randomUUID().toString();
    }
}
