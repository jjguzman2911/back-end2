package com.Medisoft.Medisoft.Entities.UserManagement.EventManagement;

import com.Medisoft.Medisoft.Entities.EmailManagement.Services.MailSenderService;
import com.Medisoft.Medisoft.Entities.SessionManagement.VerificationToken;
import com.Medisoft.Medisoft.Entities.UserManagement.TokenService;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


/**
 * Componente que escucha el registro del usuario y realiza una acción en consecuencia.
 * En este caso, envía un email notificando al usuario. Modela al suscriptor
 * Patron: Publish-Subscribe
 *
 * @author Misael
 * @version 2019-11-17
 */
@Component
public class UserSignUpListener implements ApplicationListener<OnUserSignUpEvent> {

    @Autowired
    private MailSenderService mailSenderService;

    @Autowired
    private TokenService tokenService;

    @Override
    public void onApplicationEvent(OnUserSignUpEvent event) {
        confirmSignUp(event);
    }


    private void confirmSignUp(OnUserSignUpEvent event) {
        User user = event.getUsuario();
        String token = tokenService.buildToken();
        if (user.isNonVerified()) {
            VerificationToken verificationToken = VerificationToken.builder()
                    .token(token).user(user).build();
            tokenService.save(verificationToken);
        }
        sendMail(token, user.getEmail());
    }

    private void sendMail(String token, String email) {
        String confirmationUrl = "http://localhost:3000/cuenta/registro-confirmado&token=" + token;
        String message = "Gracias por unirte a MediSoft. Para completar tu registro haz click en el siguiente enlace: "
                + confirmationUrl +
                "\n\nAtentamente te saluda, el equipo de MediSoft.";
        mailSenderService.sendRegularEmail(email, "Confirmación de Usuario - MEDISOFT", message);
    }
}
