package com.Medisoft.Medisoft.Entities.UserManagement.DTO;

import lombok.Data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
public class PasswordRecoveryDTO {
    private int userId;
    private String password;

    public String validatePassword() {
        Pattern pattern2 = Pattern.compile("^((?=.[0-9])(?=.{8,})(?=.[A-Z])(?=.*[a-z]))$");
        Matcher matcher2 = pattern2.matcher(password);
        if (matcher2.find()) {
            return "- Contraseña\n";
        }
        return null;
    }
}
