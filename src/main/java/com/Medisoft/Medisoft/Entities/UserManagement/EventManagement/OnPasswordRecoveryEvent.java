package com.Medisoft.Medisoft.Entities.UserManagement.EventManagement;

import com.Medisoft.Medisoft.Entities.UserManagement.User;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;


@Getter
public class OnPasswordRecoveryEvent extends ApplicationEvent {
    private final User user;

    public OnPasswordRecoveryEvent(User user) {
        super(user);
        this.user = user;
    }
}
