package com.Medisoft.Medisoft.Entities.UserManagement.Person;

import com.Medisoft.Medisoft.Utilities.StringConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nombre;
    private String apellido;
    private Date fechaNacimiento;

    @OneToOne(fetch = FetchType.EAGER)
    private Sexo sexo;

    @OneToOne(fetch = FetchType.EAGER)
    private TipoDocumento tipoDocumento;
    private String documento;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Contacto contacto;


    public void setNombre(String nombre) {
        if (nombre != null) {
            this.nombre = StringConverter.parseStringToFirstCharacterUpperCaseString(nombre);
        }
    }

    public void setApellido(String apellido) {
        if (apellido != null) {
            this.apellido = StringConverter.parseStringToFirstCharacterUpperCaseString(apellido);
        }
    }

    public String validate() {
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isEmpty(nombre)) {
            stringBuilder.append("nombre, ");
        }
        if (StringUtils.isEmpty(apellido)) {
            stringBuilder.append("apellido");
        }
        return stringBuilder.toString();
    }

    @JsonIgnore
    public String getFullName() {
        return nombre + " " + apellido;
    }

}
