package com.Medisoft.Medisoft.Entities.UserManagement;

import com.Medisoft.Medisoft.Entities.SessionManagement.DTO.SessionUserDTO;
import com.Medisoft.Medisoft.Entities.SessionManagement.DTO.UserDTO;
import com.Medisoft.Medisoft.Entities.SessionManagement.Session;
import com.Medisoft.Medisoft.Exceptions.EntityNotFoundException;
import com.Medisoft.Medisoft.Exceptions.LoginUserNotFoundException;
import com.Medisoft.Medisoft.Exceptions.UnauthorizedUserException;
import com.Medisoft.Medisoft.Scheduling.SessionTaskScheduler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class SessionService {
    private final List<SessionUserDTO> moderatorUsers;

    @Autowired
    private SessionStorageService sessionStorageService;

    @Autowired
    private UserService userService;

    @Autowired
    private SessionTaskScheduler sessionTaskScheduler;

    public SessionService() {
        List<SessionUserDTO> moderators = new ArrayList<>();
        moderators.add(SessionUserDTO.builder().email("proyectomedisoft@gmail.com").contrasenia("m3d1s0ft.").build());
        this.moderatorUsers = moderators;
    }

    public UserDTO login(SessionUserDTO sessionUserDto) {
        log.info("Logging in session for user {}", sessionUserDto);
        boolean isModerator = isModerator(sessionUserDto);
        if (isModerator){
            log.info("Moderator user");
            return UserDTO.builder().email(sessionUserDto.getEmail())
                    .contrasenia(sessionUserDto.getContrasenia())
                    .isModerator(true).build();
        }
        return loginUser(sessionUserDto.getEmail(), sessionUserDto.getContrasenia());
    }

    private boolean isModerator(SessionUserDTO sessionUserDto){
        return moderatorUsers.contains(sessionUserDto);
    }

    private UserDTO loginUser(String email, String password){
        User user = userService.findLoginUser(email, password);
        if (user == null) {
            throw new LoginUserNotFoundException();
        }
        if (!user.isVerified()) {
            throw new UnauthorizedUserException();
        }
        Session session = sessionStorageService.save(new Session(user));
        sessionTaskScheduler.closeSession(session);
        return UserDTO.builder()
                .contrasenia(user.getContrasenia())
                .email(user.getEmail())
                .fotoUrl(user.getFotoUrl())
                .idUsuario(user.getId())
                .paciente(user.getPaciente())
                .persona(user.getPersona())
                .profesional(user.getProfesional())
                .sessionId(session.getId())
                .isModerator(false).build();
    }

    public void logout(int sessionId) {
        log.info("Logging out session {}", sessionId);
        Session userSession = sessionStorageService.findById(sessionId);
        if (userSession == null) {
            throw new EntityNotFoundException(Session.class.getSimpleName());
        }
        userSession.close();
        sessionStorageService.save(userSession);
    }
}
