package com.Medisoft.Medisoft.Entities.UserManagement.Person;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonalDataDTO {
    private List<Sexo> sexos;
    private List<TipoDocumento> tiposDocumento;
}
