package com.Medisoft.Medisoft.Entities.UserManagement.DTO;

import com.Medisoft.Medisoft.Entities.UserManagement.Person.Persona;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class UserProfileDTO {
    private Persona datosPersonales;
    private BasicUserDataDTO datosUsuario;
}
