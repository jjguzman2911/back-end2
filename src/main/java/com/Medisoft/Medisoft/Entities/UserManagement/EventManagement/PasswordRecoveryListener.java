package com.Medisoft.Medisoft.Entities.UserManagement.EventManagement;

import com.Medisoft.Medisoft.Entities.EmailManagement.Services.MailSenderService;
import com.Medisoft.Medisoft.Entities.SessionManagement.VerificationToken;
import com.Medisoft.Medisoft.Entities.UserManagement.TokenService;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


@Component
public class PasswordRecoveryListener implements ApplicationListener<OnPasswordRecoveryEvent> {

    @Autowired
    private MailSenderService mailSenderService;

    @Autowired
    private TokenService tokenService;

    @Override
    public void onApplicationEvent(OnPasswordRecoveryEvent event) {
        recoverPassword(event);
    }


    private void recoverPassword(OnPasswordRecoveryEvent event) {
        User user = event.getUser();
        String token = tokenService.buildToken();
        VerificationToken verificationToken = VerificationToken.builder()
                .token(token)
                .user(user).build();
        tokenService.save(verificationToken);
        sendMail(token, user.getEmail());
    }

    private void sendMail(String token, String email) {
        String confirmationUrl = "http://localhost:3000/cuenta/restablecimiento-contraseña&token=" + token;
        String message = "Para recuperar tu contraseña por favor hacé click en el siguiente enlace: "
                + confirmationUrl + "\n\nAtentamente te saluda, el equipo de MediSoft.";

        mailSenderService.sendRegularEmail(email, "Recuperación de Contraseña - MEDISOFT", message);
    }
}
