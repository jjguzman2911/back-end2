package com.Medisoft.Medisoft.Entities.UserManagement.EventManagement;

import com.Medisoft.Medisoft.Entities.UserManagement.User;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;


@Getter
public class OnUserSignUpEvent extends ApplicationEvent {
    private final User usuario;

    public OnUserSignUpEvent(User user) {
        super(user);
        this.usuario = user;
    }
}
