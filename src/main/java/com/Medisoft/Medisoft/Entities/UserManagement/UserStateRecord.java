package com.Medisoft.Medisoft.Entities.UserManagement;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Modela el historial de estados por los que pasa un Usuario.
 *
 * @author Misael
 * @version 2019-11-18
 */
@Entity
@Data
@NoArgsConstructor
public class UserStateRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated
    private UserState userState;

    private Date date;

    public UserStateRecord(UserState userState) {
        this.userState = userState;
        this.date = new Date();
    }
}
