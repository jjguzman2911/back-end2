package com.Medisoft.Medisoft.Entities.UserManagement;

import com.Medisoft.Medisoft.Entities.PatientManagement.Paciente;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.UserManagement.Person.Persona;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase que modela todos los datos relacionados a la entidad Usuario.
 *
 * @author juan
 * @author Misael
 * @version 2019-09-06
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String email;
    private String contrasenia;
    private String fotoUrl;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Paciente paciente;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Persona persona;

    @Enumerated
    private UserState actualState;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<UserStateRecord> userStateRecord;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Profesional profesional;

    @JsonIgnore
    public boolean isProfessional() {
        return profesional != null;
    }

    @JsonIgnore
    public boolean isEnabledAsProfessional() {
        return isProfessional() && profesional.isHabilitado();
    }

    @JsonIgnore
    public String getName() {
        return persona != null ? persona.getNombre() : "";
    }

    @JsonIgnore
    public String getSurname() {
        return persona != null ? persona.getApellido() : "";
    }

    @JsonIgnore
    public boolean isVerified() {
        return actualState.equals(UserState.VERIFIED);
    }

    @JsonIgnore
    public boolean isNonVerified() {
        return actualState.equals(UserState.NON_VERIFIED);
    }

    @JsonIgnore
    public boolean isNoPassword() {
        return actualState.equals(UserState.NO_PASSWORD);
    }

    public void updateState(UserState newState) {
        actualState = newState;
        updateUserStateRecord();
    }

    private void updateUserStateRecord() {
        UserStateRecord newUserStateRecord = new UserStateRecord(actualState);
        if (userStateRecord == null) {
            userStateRecord = new ArrayList<>();
        }
        userStateRecord.add(newUserStateRecord);
    }

    @JsonIgnore
    public String getDocumento() {
        if (persona != null) {
            return persona.getDocumento();
        }
        return "";
    }

    public String validate() {
        Pattern pattern = Pattern.compile("^/[a-zA-Z0-9.!#$%&’+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)/$");
        Matcher matcher = pattern.matcher(this.getEmail());
        StringBuilder cad = new StringBuilder();
        if (matcher.find()) {
            cad.append("- Email\n");
        }
        /*  Minimo 8 caracteres, Maximo 15
         *  Al menos una letra mayúscula, Al menos una letra minucula, Al menos un dígito
         *  No espacios en blanco, Al menos 1 caracter especial. */
        Pattern pattern2 = Pattern.compile("/(?=.[0-9])(?=.{8,})(?=.[A-Z])(?=.*[a-z])/");
        Matcher matcher2 = pattern2.matcher(this.getContrasenia());
        if (matcher2.find()) {
            cad.append("- Contraseña\n");
        }
        if (persona != null) {
            cad.append(persona.validate());
        }
        return cad.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User usuario = (User) o;
        return Objects.equals(this.email, usuario.email) &&
                Objects.equals(this.contrasenia, usuario.contrasenia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, contrasenia);
    }
}
