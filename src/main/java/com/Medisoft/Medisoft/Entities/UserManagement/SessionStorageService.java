package com.Medisoft.Medisoft.Entities.UserManagement;

import com.Medisoft.Medisoft.DAO.JPA.ISessionDAO;
import com.Medisoft.Medisoft.Entities.SessionManagement.Session;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SessionStorageService {

    @Autowired
    private ISessionDAO sessionDAO;


    public Session findById(int sessionId) {
        return sessionDAO.findById(sessionId).orElse(null);
    }

    public Session save(Session session) {
        try {
            Session savedSession = sessionDAO.save(session);
            log.info("Session " + savedSession.getId() + " saved");
            return savedSession;
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            throw new com.Medisoft.Medisoft.Exceptions.DataIntegrityViolationException(Session.class.getSimpleName());
        }
    }

    public List<Session> findAll() {
        return sessionDAO.findAll();
    }

    public List<Session> saveAll(List<Session> sessions) {
        try {
            List<Session> savedSessions = sessionDAO.saveAll(sessions);
            log.info("Sessions saved");
            return savedSessions;
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            throw new com.Medisoft.Medisoft.Exceptions.DataIntegrityViolationException(Session.class.getSimpleName());
        }
    }

}
