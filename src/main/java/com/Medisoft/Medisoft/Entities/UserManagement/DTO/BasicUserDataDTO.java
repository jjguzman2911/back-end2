package com.Medisoft.Medisoft.Entities.UserManagement.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BasicUserDataDTO {
    private String email;
    private String contrasenia;
    private String fotoUrl;

}
