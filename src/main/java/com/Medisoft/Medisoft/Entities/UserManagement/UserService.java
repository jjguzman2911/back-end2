package com.Medisoft.Medisoft.Entities.UserManagement;

import com.Medisoft.Medisoft.DAO.JPA.IUserDAO;
import com.Medisoft.Medisoft.Exceptions.IOException;
import com.Medisoft.Medisoft.Exceptions.ProfessionalNotRegisteredException;
import com.Medisoft.Medisoft.Exceptions.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
public class UserService {

    @Autowired
    private IUserDAO usuarioDAO;

    public User findById(int id) {
        User user = usuarioDAO.findById(id).orElse(null);
        if (user == null) {
            throw new UserNotFoundException(id + "");
        }
        log.info("User {} found", id);
        return user;
    }


    /**
     * Realiza una busqueda por email de usuario en la base de datos para detectar si existe o no.
     *
     * @param email representa el email del usuario.
     * @return devuelve un Optional de Usuario.
     */
    public User findByEmail(String email) {
        User user = usuarioDAO.findByEmail(email);
        if (user == null) {
            throw new UserNotFoundException(email + "");
        }
        log.info("User {} found", email);
        return user;
    }

    public boolean existsEmail(String email) {
        return usuarioDAO.findByEmail(email) != null;
    }

    public boolean existsDocumento(String documento) {
        return findByDocumento(documento) != null;
    }

    public User findLoginUser(String email, String password) {
        return usuarioDAO.findAll()
                .stream()
                .filter(user -> user.getEmail().equals(email) && user.getContrasenia().equals(password))
                .findFirst()
                .orElse(null);
    }

    @Transactional
    public User save(User user) {
        try {
            User savedUser = usuarioDAO.save(user);
            log.info("User {} saved", savedUser.getId());
            return savedUser;
        } catch (DataIntegrityViolationException e) {
            log.error("", e.getCause());
            throw new IOException(User.class.getCanonicalName());
        }
    }

    private User findByDocumento(String documento) {
        return usuarioDAO.findAll()
                .stream()
                .filter(x -> x.getDocumento().equals(documento))
                .findFirst()
                .orElse(null);
    }

    @Transactional
    public User findProfessionalUserById(int id){
        User professionalUser = findById(id);
        if (!professionalUser.isProfessional()){
            throw new ProfessionalNotRegisteredException();
        }
        return professionalUser;
    }
}
