package com.Medisoft.Medisoft.Entities.UserManagement;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum UserState {
    VERIFIED("verificado", 1),
    NON_VERIFIED("no verificado", 2),
    NO_PASSWORD("sin contraseña", 3);

    private final String name;
    private final int id;

    UserState(String name, int id) {
        this.name = name;
        this.id = id;
    }
}
