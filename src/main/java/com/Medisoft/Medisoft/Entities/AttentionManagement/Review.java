package com.Medisoft.Medisoft.Entities.AttentionManagement;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "reviews")
public class Review {
    public transient static String SEQUENCE_NAME = "review_sequence";
    private long id;
    private long attentionId;
    private int score;
    private String comment;
    private long timestamp;
    private int professionalUserId;
    private int patientUserId;
}
