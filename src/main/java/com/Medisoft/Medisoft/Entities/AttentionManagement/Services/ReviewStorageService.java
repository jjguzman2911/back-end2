package com.Medisoft.Medisoft.Entities.AttentionManagement.Services;

import com.Medisoft.Medisoft.DAO.Mongo.IReviewDAO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Review;
import com.Medisoft.Medisoft.Exceptions.IOException;
import com.Medisoft.Medisoft.Services.SequenceGeneratorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Slf4j
@Service
public class ReviewStorageService {

    @Autowired
    private IReviewDAO reviewDAO;

    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;


    public Review save(Review review) {
        try {
            log.info("Saving review");
            long reviewId = sequenceGeneratorService.generateSequence(Review.SEQUENCE_NAME);
            review.setId(reviewId);
            reviewDAO.save(review);
            log.info("Review saved with id {}", review.getId());
            return review;
        } catch (DataIntegrityViolationException e) {
            log.error("", e);
            throw new IOException(Review.class.getSimpleName());
        }
    }

    public List<Review> findAllByProfessionalUserIdAndSortByTimestamp(int professionalUserId) {
        log.info("Finding reviews for professional {}", professionalUserId);
        List<Review> reviews = reviewDAO.findAllByProfessionalUserId(professionalUserId);
        log.info("Sorting by timestamp {} results", reviews.size());
        reviews.sort(Comparator.comparingLong(Review::getTimestamp).reversed());
        return reviews;
    }
}
