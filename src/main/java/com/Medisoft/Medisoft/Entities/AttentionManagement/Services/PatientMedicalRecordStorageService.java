package com.Medisoft.Medisoft.Entities.AttentionManagement.Services;

import com.Medisoft.Medisoft.DAO.Mongo.IPatientMedicalRecordDAO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.PatientMedicalRecord;
import com.Medisoft.Medisoft.Exceptions.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PatientMedicalRecordStorageService {

    @Autowired
    private IPatientMedicalRecordDAO patientMedicalRecordDAO;


    public PatientMedicalRecord findById(long id) {
        PatientMedicalRecord patientMedicalRecord = patientMedicalRecordDAO.findById(id).orElse(null);
        if (patientMedicalRecord == null) {
            throw new EntityNotFoundException(PatientMedicalRecord.class.getSimpleName());
        }
        log.info("Patient medical record {} found", id);
        return patientMedicalRecord;
    }

    public PatientMedicalRecord save(PatientMedicalRecord patientMedicalRecord) {
        try {
            PatientMedicalRecord savedPatientMedicalRecord = patientMedicalRecordDAO.save(patientMedicalRecord);
            log.info("Patient medical record {} saved", savedPatientMedicalRecord.getId());
            return savedPatientMedicalRecord;
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            throw new com.Medisoft.Medisoft.Exceptions.DataIntegrityViolationException(PatientMedicalRecord.class.getSimpleName());
        }
    }

    public List<PatientMedicalRecord> findAllForPatient(int patientUserId) {
        return patientMedicalRecordDAO.findAll()
                .stream()
                .filter(x -> x.getPatientUserId() == patientUserId)
                .collect(Collectors.toList());
    }

}
