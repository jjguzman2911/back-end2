package com.Medisoft.Medisoft.Entities.AttentionManagement.DTO;

import com.Medisoft.Medisoft.Entities.AttentionManagement.ReviewStatistic;
import lombok.Data;

@Data
public class ProfessionalProfileReviewDTO {
    private float averageScore;
    private int totalReviewers;

    public ProfessionalProfileReviewDTO(ReviewStatistic reviewStatistic) {
        averageScore = reviewStatistic.getAverageScore();
        totalReviewers = reviewStatistic.getTotalReviewers();
    }
}
