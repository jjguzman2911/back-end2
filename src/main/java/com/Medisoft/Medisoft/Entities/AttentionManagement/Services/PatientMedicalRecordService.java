package com.Medisoft.Medisoft.Entities.AttentionManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AppointmentService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Attention;
import com.Medisoft.Medisoft.Entities.AttentionManagement.PatientMedicalRecord;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordStructure;
import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordStructureService;
import com.Medisoft.Medisoft.Entities.PatientManagement.AtencionesDisponiblesPaciente;
import com.Medisoft.Medisoft.Entities.PatientManagement.PatientAttentionTypeService;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Services.SequenceGeneratorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PatientMedicalRecordService {

    @Autowired
    private PatientMedicalRecordStorageService patientMedicalRecordStorageService;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private AttentionService attentionService;

    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private PatientAttentionTypeService patientAttentionTypeService;

    @Autowired
    private MedicalRecordStructureService medicalRecordStructureService;

    @Autowired
    private UserService userService;


    @Transactional
    public PatientMedicalRecord postPatientMedicalRecord(PatientMedicalRecord patientMedicalRecord) {
        Turno updatedAppointment = appointmentService.attendAppointment(patientMedicalRecord.getAppointmentId());
        long patientMedicalRecordId = sequenceGeneratorService.generateSequence(PatientMedicalRecord.SEQUENCE_NAME);
        int patientUserId = patientMedicalRecord.getPatientUserId();
        int professionalUserId = patientMedicalRecord.getProfessionalUserId();

        createAttention(patientUserId, professionalUserId, updatedAppointment, patientMedicalRecordId);
        disableAttentionType(patientUserId, professionalUserId, updatedAppointment.getTipoAtencion());
        patientMedicalRecord.setId(patientMedicalRecordId);

        return patientMedicalRecordStorageService.save(patientMedicalRecord);
    }

    @Async
    void createAttention(int patientUserId, int professionalUserId, Turno appointment, long patientMedicalRecordId) {
        Attention attention = Attention.builder()
                .patientUserId(patientUserId)
                .professionalUserId(professionalUserId)
                .appointment(appointment)
                .date(new Date())
                .patientMedicalRecordId(patientMedicalRecordId)
                .build();
        attentionService.save(attention);
    }

    @Async
    @Transactional
    void disableAttentionType(int patientUserId, int professionalUserId,
                              TipoAtencionProfesional professionalAttentionType) {
        AtencionesDisponiblesPaciente availablePatientAttentionTypes =
                patientAttentionTypeService.findAvailableAttentionTypes(patientUserId, professionalUserId);

        availablePatientAttentionTypes.disableProfessionalAttentionType(professionalAttentionType);

        patientAttentionTypeService.updateAvailablePatientAttentionTypes(patientUserId,
                availablePatientAttentionTypes.getAtencionesDisponibles(), professionalUserId);
    }


    public List<Section> getPatientMedicalRecord(long medicalRecordId, boolean isPatient) {
        log.info("Getting patient medical record {}", medicalRecordId);
        PatientMedicalRecord patientMedicalRecord = patientMedicalRecordStorageService.findById(medicalRecordId);
        List<Section> sections = patientMedicalRecord.getSections();
        if (isPatient) {
            hideInvisibleSections(sections);
        }
        return sections;
    }

    private void hideInvisibleSections(List<Section> sections) {
        log.info("Invisible sections hidden");
        List<Section> sectionsToHide = sections.stream()
                .filter(section -> !section.isVisible())
                .collect(Collectors.toList());
        sections.removeAll(sectionsToHide);
    }


    public List<Section> getNewestPatientMedicalRecord(int professionalUserId, int patientUserId) {
        User professionalUser = userService.findProfessionalUserById(professionalUserId);
        Profesional professional = professionalUser.getProfesional();
        Attention attention = attentionService.findNewest(patientUserId, professionalUserId);
        return getNewestSections(attention, professional.getMedicalRecordId());
    }

    private List<Section> getNewestSections(Attention attention, long professionalMedicalRecordId) {
        boolean patientHasBeenAttended = attention != null;
        if (patientHasBeenAttended) {
            return getPatientMedicalRecord(attention.getPatientMedicalRecordId(), professionalMedicalRecordId);
        }
        return getProfessionalMedicalRecord(professionalMedicalRecordId);
    }

    private List<Section> getPatientMedicalRecord(long patientMedicalRecordId, long professionalMedicalRecordId) {
        MedicalRecordStructure professionalMedicalRecord = medicalRecordStructureService.findById(professionalMedicalRecordId);
        PatientMedicalRecord patientMedicalRecord = patientMedicalRecordStorageService.findById(patientMedicalRecordId);
        boolean professionalMedicalRecordHasBeenModified = professionalMedicalRecord.getTimestamp() > patientMedicalRecord.getTimestamp();
        return professionalMedicalRecordHasBeenModified ?
                professionalMedicalRecord.getSections() :
                patientMedicalRecord.getSections();
    }


    private List<Section> getProfessionalMedicalRecord(long professionalMedicalRecordId) {
        return medicalRecordStructureService.findById(professionalMedicalRecordId).getSections();
    }

}
