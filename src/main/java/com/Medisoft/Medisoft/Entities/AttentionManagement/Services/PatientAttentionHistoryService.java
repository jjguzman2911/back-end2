package com.Medisoft.Medisoft.Entities.AttentionManagement.Services;

import com.Medisoft.Medisoft.Entities.AttentionManagement.Attention;
import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.AttentionDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ProfessionalAttentionDTO;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PatientAttentionHistoryService {

    @Autowired
    private AttentionService attentionService;

    @Autowired
    private UserService userService;


    public List<AttentionDTO> getPatientAttentionHistoryByProfessional(int patientUserId, int professionalUserId) {
        List<Attention> attentionList = attentionService.findAllSortedForPatientAndProfessional(patientUserId, professionalUserId, false);
        log.info("{} attentions for patient {} and professional {} have been found", attentionList.size(),
                patientUserId, professionalUserId);
        return mapAndFilterToAttentionListDto(attentionList, attentionList.size());
    }

    public List<AttentionDTO> getAllAttentions(int patientUserId, int size) {
        log.info("Getting all attentions for patient {} ", patientUserId);
        List<Attention> attentionList = attentionService.findAllByPatient(patientUserId, true);
        return mapAndFilterToAttentionListDto(attentionList, size);
    }

    private List<AttentionDTO> mapAndFilterToAttentionListDto(List<Attention> attentionList, int size) {
        Map<Integer, User> professionalCache = new ConcurrentHashMap<>();
        return attentionList.stream()
                .map(attention -> AttentionDTO.builder().attentionType(attention.getAttentionTypeString())
                        .patientMedicalRecordId(attention.getPatientMedicalRecordId())
                        .dateInMillis(attention.getDate().getTime())
                        .attentionId(attention.getId())
                        .appointmentId(attention.getAppointment().getId())
                        .specialtyName(attention.getSpecialtyName())
                        .professional(buildProfessionalAttentionDTO(attention.getProfessionalUserId(), professionalCache))
                        .isReviewed(attention.isReviewed())
                        .build())
                .limit(size)
                .collect(Collectors.toList());
    }

    private ProfessionalAttentionDTO buildProfessionalAttentionDTO(int professionalUserId, Map<Integer, User> professionalCache) {
        User professionalUser = professionalCache.get(professionalUserId);
        if (professionalUser == null){
            professionalUser = userService.findProfessionalUserById(professionalUserId);
            professionalCache.put(professionalUserId, professionalUser);
        }
        return ProfessionalAttentionDTO.builder()
                .name(professionalUser.getName())
                .surname(professionalUser.getSurname())
                .photoUrl(professionalUser.getFotoUrl())
                .professionalUserId(professionalUserId)
                .license(professionalUser.getProfesional().getMatricula())
                .build();
    }
}
