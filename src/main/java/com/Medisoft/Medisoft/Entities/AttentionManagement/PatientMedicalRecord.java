package com.Medisoft.Medisoft.Entities.AttentionManagement;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.CustomizableComponents.Section;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "patients_medical_records")
public class PatientMedicalRecord {

    public transient static final String SEQUENCE_NAME = "patients_medical_record_sequence";

    @Id
    private long id;
    private long appointmentId;
    private int patientUserId;
    private int professionalUserId;
    private List<Section> sections;
    private long timestamp;
}
