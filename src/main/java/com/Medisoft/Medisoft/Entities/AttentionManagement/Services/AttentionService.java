package com.Medisoft.Medisoft.Entities.AttentionManagement.Services;

import com.Medisoft.Medisoft.DAO.JPA.IAttentionDAO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Attention;
import com.Medisoft.Medisoft.Exceptions.EntityNotFoundException;
import com.Medisoft.Medisoft.Exceptions.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AttentionService {

    @Autowired
    private IAttentionDAO attentionDAO;


    public Attention findById(long id) {
        Attention attention = attentionDAO.findById(id).orElse(null);
        if (attention == null) {
            throw new EntityNotFoundException(Attention.class.getSimpleName());
        }
        log.info("Attention {} found", attention.getId());
        return attention;
    }

    public Attention save(Attention attention) {
        try {
            log.info("Saving attention");
            attentionDAO.save(attention);
            log.info("Attention {} saved", attention.getId());
            return attention;
        } catch (DataIntegrityViolationException e) {
            log.error("", e.getCause());
            throw new IOException(Attention.class.getSimpleName());
        }
    }

    public Attention findNewest(int patientUserId, int professionalUserId) {
        return attentionDAO.findAll().stream()
                .filter(x -> x.getPatientUserId() == patientUserId && x.getProfessionalUserId() == professionalUserId)
                .max((att1, att2) -> att1.getId() > att2.getId() ? 1 : -1)
                .orElse(null);
    }

    public List<Attention> findAll() {
        return attentionDAO.findAll();
    }

    public List<Attention> findAllSortedForPatientAndProfessional(int patientUserId, int professionalUserId, boolean ascendingSort) {
        return attentionDAO.findAll().stream()
                .filter(attention -> attention.getPatientUserId() == patientUserId &&
                        attention.getProfessionalUserId() == professionalUserId)
                .sorted(ascendingSort ? Comparator.naturalOrder() : Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    public List<Attention> findAllByPatient(int patientUserId) {
        return attentionDAO.findAll().stream()
                .filter(attention -> attention.getPatientUserId() == patientUserId)
                .collect(Collectors.toList());
    }

    public List<Attention> findAllByPatient(int patientUserId, boolean descendingOrder) {
        return attentionDAO.findAll().stream()
                .filter(attention -> attention.getPatientUserId() == patientUserId)
                .sorted(descendingOrder ? Comparator.reverseOrder() : Comparator.naturalOrder())
                .collect(Collectors.toList());
    }
}
