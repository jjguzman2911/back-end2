package com.Medisoft.Medisoft.Entities.AttentionManagement.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProfessionalAttentionDTO {
    private int professionalUserId;
    private String photoUrl;
    private String name;
    private String surname;
    private String license;
}
