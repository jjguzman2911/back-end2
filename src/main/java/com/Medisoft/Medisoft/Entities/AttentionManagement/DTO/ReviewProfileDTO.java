package com.Medisoft.Medisoft.Entities.AttentionManagement.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReviewProfileDTO {
    private ReviewStatisticDTO reviewStatistic;
    private List<ReviewDTO> reviews;
}
