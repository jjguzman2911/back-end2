package com.Medisoft.Medisoft.Entities.AttentionManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AppointmentService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Attention;
import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.PatientPersonalDataDTO;
import com.Medisoft.Medisoft.Entities.PatientManagement.PatientHealthInsurance;
import com.Medisoft.Medisoft.Entities.UserManagement.Person.Persona;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PatientPersonalDataService {

    @Autowired
    private UserService userService;

    @Autowired
    private AttentionService attentionService;

    @Autowired
    private AppointmentService appointmentService;

    public PatientPersonalDataDTO getPatientPersonalData(int patientUserId, Long attentionId, Long appointmentId) {
        log.info("Getting patient {} personal data", patientUserId);
        User patientUser = userService.findById(patientUserId);
        Persona person = patientUser.getPersona();
        return buildPatientPersonalData(person, patientUser.getFotoUrl(), attentionId, appointmentId);
    }

    private PatientPersonalDataDTO buildPatientPersonalData(Persona person, String imageUrl, Long attentionId, Long appointmentId) {
        PatientPersonalDataDTO patientPersonalData = PatientPersonalDataDTO.builder()
                .fullName(person.getFullName())
                .imageUrl(imageUrl).documentoType(person.getTipoDocumento().getNombre())
                .documentNumber(person.getDocumento() + "")
                .build();
        setAttentionDateAndMedicalRecordId(patientPersonalData, attentionId);
        setPatientHealthInsuranceData(patientPersonalData, appointmentId);
        log.info("Patient personal data {}", patientPersonalData);
        return patientPersonalData;
    }

    private void setAttentionDateAndMedicalRecordId(PatientPersonalDataDTO patientPersonalData, Long attentionId) {
        if (attentionId != null) {
            Attention attention = attentionService.findById(attentionId);
            long attentionDateInMillis = attention.getDate().getTime();
            long medicalRecordId = attention.getPatientMedicalRecordId();
            patientPersonalData.setAttentionDateInMillis(attentionDateInMillis);
            patientPersonalData.setMedicalRecordId(medicalRecordId);
            log.info("Medical record id {} and attention timestamp {} added", medicalRecordId, attentionDateInMillis);
        }
    }

    private void setPatientHealthInsuranceData(PatientPersonalDataDTO patientPersonalData, Long appointmentId){
        if (appointmentId != null) {
            Turno appointment = appointmentService.findById(appointmentId);
            PatientHealthInsurance patientHealthInsurance = appointment.getPatientHealthInsurance();
            patientPersonalData.setHealthInsurance(patientHealthInsurance.getHealthInsurance());
            patientPersonalData.setAffiliateNumber(patientHealthInsurance.getAffiliateNumber());
            log.info("Patient health insurance data added {}", patientHealthInsurance);
        }
    }
}
