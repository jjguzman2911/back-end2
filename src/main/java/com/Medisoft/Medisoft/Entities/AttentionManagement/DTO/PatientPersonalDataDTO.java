package com.Medisoft.Medisoft.Entities.AttentionManagement.DTO;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PatientPersonalDataDTO {
    private String fullName;
    private String imageUrl;
    private String documentoType;
    private String documentNumber;
    private String healthInsurance;
    private String affiliateNumber;
    private long attentionDateInMillis;
    private long medicalRecordId;
}
