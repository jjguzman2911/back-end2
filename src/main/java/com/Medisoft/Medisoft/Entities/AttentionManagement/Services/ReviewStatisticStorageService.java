package com.Medisoft.Medisoft.Entities.AttentionManagement.Services;

import com.Medisoft.Medisoft.DAO.JPA.IReviewStatisticDAO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.ReviewStatistic;
import com.Medisoft.Medisoft.Exceptions.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ReviewStatisticStorageService {

    @Autowired
    private IReviewStatisticDAO reviewStatisticDAO;


    public ReviewStatistic findByProfessionalUserId(int professionalUserId) {
        log.info("Finding review statistic for professional {}", professionalUserId);
        ReviewStatistic reviewStatistic = reviewStatisticDAO.findByProfessionalUserId(professionalUserId);
        return reviewStatistic != null ? reviewStatistic : new ReviewStatistic();
    }

    public ReviewStatistic save(ReviewStatistic reviewStatistic) {
        try {
            log.info("Saving review statistic {}", reviewStatistic.toString());
            return reviewStatisticDAO.save(reviewStatistic);
        } catch (DataIntegrityViolationException e) {
            log.error("", e);
            throw new IOException(ReviewStatistic.class.getSimpleName());
        }
    }
}
