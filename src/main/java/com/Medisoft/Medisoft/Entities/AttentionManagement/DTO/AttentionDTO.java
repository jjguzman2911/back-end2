package com.Medisoft.Medisoft.Entities.AttentionManagement.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AttentionDTO {
    private long attentionId;
    private long dateInMillis;
    private String attentionType;
    private String specialtyName;
    private long patientMedicalRecordId;
    private long appointmentId;
    private ProfessionalAttentionDTO professional;
    private boolean isReviewed;
}
