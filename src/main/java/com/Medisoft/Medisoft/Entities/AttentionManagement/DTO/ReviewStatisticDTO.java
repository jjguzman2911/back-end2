package com.Medisoft.Medisoft.Entities.AttentionManagement.DTO;

import com.Medisoft.Medisoft.Entities.AttentionManagement.ReviewStatistic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReviewStatisticDTO {
    private float averageScore;
    private int totalReviewers;
    private int oneStar;
    private int twoStars;
    private int threeStars;
    private int fourStars;
    private int fiveStars;

    public ReviewStatisticDTO(ReviewStatistic reviewStatistic) {
        setAverageScore(reviewStatistic.getAverageScore());
        setTotalReviewers(reviewStatistic.getTotalReviewers());
        setOneStar(reviewStatistic.getOneStar());
        setTwoStars(reviewStatistic.getTwoStars());
        setThreeStars(reviewStatistic.getThreeStars());
        setFourStars(reviewStatistic.getFourStars());
        setFiveStars(reviewStatistic.getFiveStars());
    }
}
