package com.Medisoft.Medisoft.Entities.AttentionManagement;

import com.Medisoft.Medisoft.Utilities.NumberRounderUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
public class ReviewStatistic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int professionalUserId;
    private long totalScore;
    private int totalReviewers;
    private long lastUpdateTimestamp;
    private int oneStar;
    private int twoStars;
    private int threeStars;
    private int fourStars;
    private int fiveStars;


    public ReviewStatistic(int professionalUserId, int initialScore) {
        this.professionalUserId = professionalUserId;
        totalScore = initialScore;
        totalReviewers = 1;
        lastUpdateTimestamp = new Date().getTime();
        addStar(initialScore);
    }

    public void addReviewScore(int score) {
        totalScore += score;
        totalReviewers++;
        lastUpdateTimestamp = new Date().getTime();
        addStar(score);
    }

    public boolean isEmpty(){
        return id == 0 || professionalUserId == 0;
    }

    @JsonIgnore
    public float getAverageScore() {
        float averageScore = 0f;
        if (totalScore > 0 && totalReviewers > 0) {
            averageScore =  (float) totalScore / (float) totalReviewers;
        }
        return NumberRounderUtils.roundToOneDecimal(averageScore);
    }

    private void addStar(int score) {
        switch (score) {
            case 1:
                oneStar++;
                break;
            case 2:
                twoStars++;
                break;
            case 3:
                threeStars++;
                break;
            case 4:
                fourStars++;
                break;
            case 5:
                fiveStars++;
                break;
        }
    }
}
