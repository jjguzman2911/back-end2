package com.Medisoft.Medisoft.Entities.AttentionManagement.DTO;

import lombok.Builder;
import lombok.Data;

import java.util.Objects;

@Builder
@Data
public class ProfessionalMapperAttentionDTO {
    private int profesionalUserId;
    private long lastAttentionTimestamp;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfessionalMapperAttentionDTO that = (ProfessionalMapperAttentionDTO) o;
        return profesionalUserId == that.profesionalUserId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(profesionalUserId);
    }
}
