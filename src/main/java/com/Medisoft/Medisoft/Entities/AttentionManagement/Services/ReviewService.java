package com.Medisoft.Medisoft.Entities.AttentionManagement.Services;

import com.Medisoft.Medisoft.Entities.AttentionManagement.Attention;
import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ReviewDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ReviewProfileDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ReviewStatisticDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Review;
import com.Medisoft.Medisoft.Entities.AttentionManagement.ReviewStatistic;
import com.Medisoft.Medisoft.Exceptions.InvalidDataException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ReviewService {

    @Autowired
    private ReviewStorageService reviewStorageService;

    @Autowired
    private AttentionService attentionService;

    @Autowired
    private ReviewStatisticService reviewStatisticService;


    public Review createReview(long attentionId, ReviewDTO reviewDTO) {
        log.info("Creating review");
        validateReview(reviewDTO);
        Review review = mapToReviewAndSave(attentionId, reviewDTO);
        updateAttention(attentionId, review.getId());
        updateReviewStatistics(review.getProfessionalUserId(), review.getScore());
        return review;
    }

    private void validateReview(ReviewDTO reviewDTO) {
        String message;
        if (reviewDTO.getScore() < 1 || reviewDTO.getScore() > 5) {
            message = "La calificación debe estar entre 1 y 5";
            log.warn(message);
            throw new InvalidDataException(message);
        }
        if (reviewDTO.getPatientUserId() < 1) {
            message = "El ID de paciente debe estar presente";
            log.warn(message);
            throw new InvalidDataException(message);
        }
        if (reviewDTO.getProfessionalUserId() < 1) {
            message = "El ID de profesional debe estar presente";
            log.warn(message);
            throw new InvalidDataException(message);
        }
    }

    private Review mapToReviewAndSave(long attentionId, ReviewDTO reviewDTO) {
        Review review = Review.builder()
                .timestamp(new Date().getTime())
                .score(reviewDTO.getScore())
                .comment(reviewDTO.getComment())
                .attentionId(attentionId)
                .professionalUserId(reviewDTO.getProfessionalUserId())
                .patientUserId(reviewDTO.getPatientUserId())
                .build();
        return reviewStorageService.save(review);
    }

    @Async
    void updateAttention(long attentionId, long reviewId) {
        log.info("Updating attention {} with review {}", attentionId, reviewId);
        Attention attention = attentionService.findById(attentionId);
        if (!attention.isReviewed()) {
            attention.setReviewId(reviewId);
            attentionService.save(attention);
        }
    }

    @Async
    void updateReviewStatistics(int professionalUserId, int score) {
        reviewStatisticService.updateOrCreateReviewStatistics(professionalUserId, score);
    }


    public ReviewProfileDTO getReviewProfile(int professionalUserId) {
        log.info("Getting review profile");
        List<Review> reviews = reviewStorageService.findAllByProfessionalUserIdAndSortByTimestamp(professionalUserId);
        ReviewStatistic reviewStatistic = reviewStatisticService.findByProfessionalUserId(professionalUserId);
        return buildReviewProfile(reviews, reviewStatistic);
    }

    private ReviewProfileDTO buildReviewProfile(List<Review> reviews, ReviewStatistic reviewStatistic) {
        List<ReviewDTO> reviewsDto = mapToDto(reviews);
        ReviewStatisticDTO reviewStatisticDto = new ReviewStatisticDTO(reviewStatistic);
        return new ReviewProfileDTO(reviewStatisticDto, reviewsDto);
    }

    private List<ReviewDTO> mapToDto(List<Review> reviews) {
        return reviews.stream()
                .map(ReviewDTO::new)
                .collect(Collectors.toList());
    }
}
