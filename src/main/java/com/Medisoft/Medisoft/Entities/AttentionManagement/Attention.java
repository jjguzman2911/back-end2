package com.Medisoft.Medisoft.Entities.AttentionManagement;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Attention implements Comparable<Attention> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int patientUserId;
    private int professionalUserId;
    @OneToOne(cascade = {CascadeType.REFRESH, CascadeType.PERSIST}, fetch = FetchType.EAGER, orphanRemoval = true)
    private Turno appointment;
    private Date date;
    private long patientMedicalRecordId;
    private long reviewId;


    @JsonIgnore
    public String getAttentionTypeString() {
        return appointment == null ? null : appointment.getTipoAtencion().getName();
    }

    public String getSpecialtyName() {
        return appointment == null ? null : appointment.getEspecialidad().getNombre();
    }

    @JsonIgnore
    public boolean isReviewed() {
        return reviewId > 0;
    }

    @Override
    public int compareTo(Attention o) {
        return date.compareTo(o.date);
    }
}
