package com.Medisoft.Medisoft.Entities.AttentionManagement.Services;

import com.Medisoft.Medisoft.Entities.AttentionManagement.Attention;
import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ProfessionalMapperAttentionDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ProfessionalProfileReviewDTO;
import com.Medisoft.Medisoft.Entities.AttentionManagement.ReviewStatistic;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalForPatientHistoryDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalSetGroupedBySpecialty;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.View.ProfessionalsGrouperBySpecialtyUtil;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Utilities.DTOMapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PatientProfessionalHistoryService {

    @Autowired
    private UserService userService;

    @Autowired
    private AttentionService attentionService;

    @Autowired
    private ReviewStatisticStorageService reviewStatisticStorageService;


    public Set<ProfessionalSetGroupedBySpecialty> getProfessionalHistoryByPatientGroupedBySpecialty(int patientUserId) {
        List<Attention> attentions = attentionService.findAllByPatient(patientUserId);
        log.info("Getting professionals for patient {}", patientUserId);
        return getProfessionalsByAttentions(attentions);
    }

    private Set<ProfessionalSetGroupedBySpecialty> getProfessionalsByAttentions(List<Attention> attentions) {
        Set<Integer> professionalUserIds = attentions.stream()
                .map(Attention::getProfessionalUserId)
                .collect(Collectors.toSet());
        log.info("{} professionals were consulted", professionalUserIds.size());
        return findProfessionalsAndGroupBySpecialty(professionalUserIds);
    }

    private Set<ProfessionalSetGroupedBySpecialty> findProfessionalsAndGroupBySpecialty(Set<Integer> professionalUserIds) {
        Set<User> professionalUsers = professionalUserIds.stream()
                .map(professionalUserId -> userService.findProfessionalUserById(professionalUserId))
                .collect(Collectors.toSet());
        return ProfessionalsGrouperBySpecialtyUtil.groupBySpecialty(professionalUsers);
    }

    public Set<ProfessionalForPatientHistoryDTO> getAllProfessionals(int patientUserId, int size) {
        log.info("Getting professionals for patient {}", patientUserId);
        List<Attention> attentions = attentionService.findAllByPatient(patientUserId, true);
        Set<ProfessionalMapperAttentionDTO> mapperAttentionDTOS = DTOMapperUtils.toProfessionalMapperAttentionDto(attentions, size);
        return buildProfessionalForPatientSet(mapperAttentionDTOS);
    }

    private Set<ProfessionalForPatientHistoryDTO> buildProfessionalForPatientSet(Set<ProfessionalMapperAttentionDTO> professionalUserIds) {
        log.info("{} professionals retrieved", professionalUserIds.size());
        return professionalUserIds.stream()
                .map(professionalMapperAttentionDTO -> {
                    User professionalUser = userService.findProfessionalUserById(professionalMapperAttentionDTO.getProfesionalUserId());
                    ReviewStatistic reviewStatistic = reviewStatisticStorageService.findByProfessionalUserId(professionalMapperAttentionDTO.getProfesionalUserId());

                    return new ProfessionalForPatientHistoryDTO(professionalUser,
                            new ProfessionalProfileReviewDTO(reviewStatistic),
                            professionalMapperAttentionDTO.getLastAttentionTimestamp());
                })
                .collect(Collectors.toSet());
    }
}
