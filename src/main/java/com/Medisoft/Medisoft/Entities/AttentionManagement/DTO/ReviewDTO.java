package com.Medisoft.Medisoft.Entities.AttentionManagement.DTO;

import com.Medisoft.Medisoft.Entities.AttentionManagement.Review;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ReviewDTO {
    private int score;
    private String comment;
    private int professionalUserId;
    private int patientUserId;

    public ReviewDTO(Review review) {
        setScore(review.getScore());
        setComment(review.getComment());
        setProfessionalUserId(review.getProfessionalUserId());
        setPatientUserId(review.getPatientUserId());
    }
}
