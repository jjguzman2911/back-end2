package com.Medisoft.Medisoft.Entities.AttentionManagement.Services;

import com.Medisoft.Medisoft.Entities.AttentionManagement.ReviewStatistic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ReviewStatisticService {
    @Autowired
    private ReviewStatisticStorageService reviewStatisticStorageService;


    public void updateOrCreateReviewStatistics(int professionalUserId, int score) {
        ReviewStatistic reviewStatistic = reviewStatisticStorageService.findByProfessionalUserId(professionalUserId);
        if (!reviewStatistic.isEmpty()) {
            log.info("Updating review statistics for professional {}", professionalUserId);
            reviewStatistic.addReviewScore(score);
        } else {
            log.info("Creating review statistics for professional {}", professionalUserId);
            reviewStatistic = new ReviewStatistic(professionalUserId, score);
        }
        reviewStatisticStorageService.save(reviewStatistic);
    }

    public ReviewStatistic findByProfessionalUserId(int professionalUserId) {
        return reviewStatisticStorageService.findByProfessionalUserId(professionalUserId);
    }

}
