package com.Medisoft.Medisoft.Entities.PaymentManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.EventoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AppointmentService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.PatientManagement.PatientHealthInsurance;
import com.Medisoft.Medisoft.Entities.PaymentManagement.OnlineAppointmentPayment;
import com.mercadopago.resources.MerchantOrder;
import com.mercadopago.resources.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Slf4j
@Service
public class OnlinePaymentService {

    @Autowired
    private PaymentStorageService paymentStorageService;

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private MercadopagoConnectionService mercadoPagoConnectionService;

    @Autowired
    private MercadopagoCredentialsService credentialsService;

    @Async
    @Transactional
    public void processPayment(String topic, String paymentId, long appointmentId) {
        switch (topic.toLowerCase()) {
            case "payment":
                processPayment(paymentId, appointmentId);
                break;
            case "chargebacks":
                break;
            case "merchant_order":
                processMerchantOrder(paymentId, appointmentId);
                break;
            default:
                log.warn("Invalid topic from mercadopago");
        }
    }

    private void processPayment(String paymentId, long appointmentId) {
        log.info("Updating payment {} for appointment {}", paymentId, appointmentId);
        OnlineAppointmentPayment appointmentPayment = (OnlineAppointmentPayment) paymentStorageService.findByAppointmentIdDummy(appointmentId);
        if (appointmentPayment != null) {
            Payment payment = mercadoPagoConnectionService.findPayment(paymentId, appointmentPayment.getAccessToken());
            updatePaymentStatus(appointmentPayment, payment.getStatus().name(), payment.getStatusDetail(), payment.getDateLastUpdated(), payment.getId());
            paymentStorageService.save(appointmentPayment);
        }
    }

    private void updatePaymentStatus(OnlineAppointmentPayment appointmentPayment, String status, String statusDetail, Date dateLastUpdate, String paymentId){
        log.info("Initial payment state {}", appointmentPayment);
        appointmentPayment.setStatus(status);
        appointmentPayment.setStatusDetail(statusDetail);
        appointmentPayment.setDateLastUpdate(dateLastUpdate);
        appointmentPayment.addPaymentId(paymentId);
        log.info("Current payment state {}", appointmentPayment);
    }

    private void processMerchantOrder(String merchantOrderId, long appointmentId) {
        log.info("Processing merchant order {} for appointment {}", merchantOrderId, appointmentId);
        OnlineAppointmentPayment appointmentPayment = createAppointmentPayment(appointmentId, merchantOrderId);
        paymentStorageService.save(appointmentPayment);
    }

    private OnlineAppointmentPayment createAppointmentPayment(long appointmentId, String merchantOrderId){
        Turno appointment = appointmentService.findById(appointmentId);
        String accessToken = credentialsService.getCredentials(appointment.getProfessionalUserId()).getAccessToken();
        MerchantOrder merchantOrder = mercadoPagoConnectionService.findMerchantOrder(merchantOrderId, accessToken);
        updateAppointmentStatus(appointment);
        OnlineAppointmentPayment appointmentPayment = OnlineAppointmentPayment.builder()
                .merchantOrderId(merchantOrderId)
                .dateCreated(merchantOrder.getDateCreated())
                .dateApproval(merchantOrder.getDateCreated())
                .dateLastUpdate(merchantOrder.getLastUpdate())
                .status(merchantOrder.getStatus())
                .statusDetail(merchantOrder.getStatus())
                .finalPrice(merchantOrder.getTotalAmount())
                .appointment(appointment)
                .accessToken(accessToken)
                .build();
        log.info("Appointment payment {} created", appointmentPayment);
        return appointmentPayment;
    }

    private void updateAppointmentStatus(Turno appointment){
        appointment.actualizarEstado(EventoTurno.COBRAR);
        appointment.setPatientHealthInsurance(PatientHealthInsurance.builder().affiliateNumber("-").healthInsurance("PARTICULAR").build());
    }

}
