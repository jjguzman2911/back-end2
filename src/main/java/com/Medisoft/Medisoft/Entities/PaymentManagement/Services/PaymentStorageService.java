package com.Medisoft.Medisoft.Entities.PaymentManagement.Services;

import com.Medisoft.Medisoft.DAO.JPA.IAppointmentPaymentDAO;
import com.Medisoft.Medisoft.Entities.PaymentManagement.AppointmentPayment;
import com.Medisoft.Medisoft.Entities.PaymentManagement.OnSiteAppointmentPayment;
import com.Medisoft.Medisoft.Exceptions.IOException;
import com.Medisoft.Medisoft.Exceptions.PaymentNotFoundException;
import com.Medisoft.Medisoft.Services.ComprobanteMailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class PaymentStorageService {

    @Autowired
    private IAppointmentPaymentDAO appointmentPaymentDAO;

    @Autowired
    private ComprobanteMailService comprobanteMailService;


    public AppointmentPayment findByIdDummy(long id) {
        return appointmentPaymentDAO.findById(id).orElse(null);
    }

    public AppointmentPayment save(AppointmentPayment appointmentPayment) {
        try {
            AppointmentPayment appointmentPaymentGuardado = appointmentPaymentDAO.save(appointmentPayment);
            log.info("Payment {} saved ", appointmentPaymentGuardado);
            return appointmentPaymentGuardado;
        } catch (DataIntegrityViolationException e) {
            log.error("", e);
            throw new IOException(AppointmentPayment.class.getSimpleName());
        }
    }

    public AppointmentPayment findByAppointmentIdDummy(long appointmentId) {
        AppointmentPayment appointmentPayment = appointmentPaymentDAO.findAll()
                .stream()
                .filter(x -> x.getAppointment().getId() == appointmentId)
                .findFirst()
                .orElse(null);
        log.info("Payment {} found by appointment id {}", appointmentPayment, appointmentId);
        return appointmentPayment;
    }

    public AppointmentPayment findByAppointmentId(long appointmentId) {
        AppointmentPayment appointmentPayment = appointmentPaymentDAO.findAll()
                .stream()
                .filter(x -> x.getAppointment().getId() == appointmentId)
                .findFirst()
                .orElse(null);
        if (appointmentPayment == null)
            throw new PaymentNotFoundException();
        log.info("Payment {} found by appointment id {}", appointmentPayment, appointmentId);
        return appointmentPayment;
    }

    @Async //TODO check if it is necessary or not -> generarYEnviarComprobanteCobro is also asynchronous
    public void generarYEnviarComprobanteCobro(OnSiteAppointmentPayment cobroTurno,
                                               boolean enviarAPaciente, boolean enviarAProfesional) {
        log.info("Notifying on site payment");
        comprobanteMailService.generarYEnviarComprobanteCobro(cobroTurno, enviarAPaciente, enviarAProfesional);
    }
}
