package com.Medisoft.Medisoft.Entities.PaymentManagement;


import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.SlipManagement.IComprobante;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
public class OnSiteAppointmentPayment extends AppointmentPayment {

    private float precioOriginal;
    private transient IComprobante comprobante;

    @Builder
    public OnSiteAppointmentPayment(long transactionNumber, Turno appointment, Date date, float finalPrice,
                                    String healthInsurance, float originalPrice, String affiliateNumber,
                                    IComprobante proof) {
        super(transactionNumber, appointment, date, finalPrice, healthInsurance);
        precioOriginal = originalPrice;
        comprobante = proof;
    }
}
