package com.Medisoft.Medisoft.Entities.PaymentManagement.Services;

import com.Medisoft.Medisoft.DAO.JPA.ProfessionalMercadopagoCredentialsDAO;
import com.Medisoft.Medisoft.Entities.PaymentManagement.ProfessionalMercadopagoCredentials;
import com.Medisoft.Medisoft.Exceptions.EntityNotFoundException;
import com.Medisoft.Medisoft.Exceptions.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ProfessionalMercadopagoCredentialsStorageService {

    @Autowired
    private ProfessionalMercadopagoCredentialsDAO mercadopagoCredentialsDAO;


    public ProfessionalMercadopagoCredentials findByProfessionalUserIdDummy(int professionalUserId){
        ProfessionalMercadopagoCredentials credentials = mercadopagoCredentialsDAO.findByProfessionalUserId(professionalUserId);
        log.info("Credentials found {}", credentials);
        return credentials;
    }

    public ProfessionalMercadopagoCredentials findByProfessionalUserId(int professionalUserId){
        ProfessionalMercadopagoCredentials credentials = mercadopagoCredentialsDAO.findByProfessionalUserId(professionalUserId);
        if (credentials == null){
            throw new EntityNotFoundException("El profesional no está habilitado para operar con Mercadopago");
        }
        log.info("Credentials found {}", credentials);
        return credentials;
    }


    public void save(ProfessionalMercadopagoCredentials credentials){
        try {
            mercadopagoCredentialsDAO.save(credentials);
            log.info("Credentials saved {}", credentials);
        } catch (Exception e){
            log.error("Error saving credentials: ", e);
            throw new IOException();
        }
    }
}
