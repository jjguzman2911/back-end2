package com.Medisoft.Medisoft.Entities.PaymentManagement.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AccessTokenDTO {
    private String accessToken;
}
