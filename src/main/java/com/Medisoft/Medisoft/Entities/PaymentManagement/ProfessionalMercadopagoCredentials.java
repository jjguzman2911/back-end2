package com.Medisoft.Medisoft.Entities.PaymentManagement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class ProfessionalMercadopagoCredentials {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String accessToken;
    private String publicKey;
    private String refreshToken;
    private String tokenType;
    private long mercadopagoUserId;
    private int professionalUserId;
    private long timestamp;

}
