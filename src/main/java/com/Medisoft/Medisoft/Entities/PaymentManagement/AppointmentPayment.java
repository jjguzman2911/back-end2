package com.Medisoft.Medisoft.Entities.PaymentManagement;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Utilities.DateFormatConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class AppointmentPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long numeroTransaccion;

    @OneToOne(cascade = {CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    protected Turno appointment;
    protected Date dateCreated;
    protected float finalPrice;
    protected String healthInsurance;


    @JsonIgnore
    public String getFechaCreacionPreety() {
        return DateFormatConverter.parseDateToString(dateCreated);
    }
}
