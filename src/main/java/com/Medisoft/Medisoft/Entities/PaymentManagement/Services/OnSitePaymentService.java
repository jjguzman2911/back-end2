package com.Medisoft.Medisoft.Entities.PaymentManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.EventoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AppointmentService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.PatientManagement.PatientHealthInsurance;
import com.Medisoft.Medisoft.Entities.PaymentManagement.DTO.OnSiteAppointmentPaymentDTO;
import com.Medisoft.Medisoft.Entities.PaymentManagement.DTO.PaymentInitDTO;
import com.Medisoft.Medisoft.Entities.PaymentManagement.OnSiteAppointmentPayment;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Exceptions.InvalidAppointmentStateActionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Set;

@Slf4j
@Service
public class OnSitePaymentService {

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private PaymentStorageService paymentStorageService;

    @Autowired
    private UserService userService;


    @Transactional
    public OnSiteAppointmentPayment payOnSite(OnSiteAppointmentPaymentDTO payment) {
        log.info("Paying appointment on site {}", payment);
        Turno appointment = appointmentService.findById(payment.getIdTurno());
        if (appointment.isNotPayable())
            throw new InvalidAppointmentStateActionException();
        return saveAndNotifyPayment(appointment, payment);
    }

    private OnSiteAppointmentPayment saveAndNotifyPayment(Turno appointment, OnSiteAppointmentPaymentDTO payment) {
        log.info("Saving and notifying payment");
        updateAppointment(appointment, payment.getHealthInsurance(), payment.getAffiliateNumber());
        OnSiteAppointmentPayment onSiteAppointmentPayment = createOnSitePayment(appointment, payment);
        saveAndNotify(onSiteAppointmentPayment);
        return onSiteAppointmentPayment;
    }

    private void updateAppointment(Turno appointment, String healthInsurance, String affiliateNumber) {
        appointment.actualizarEstado(EventoTurno.COBRAR);
        appointment.actualizarEstado(EventoTurno.REGISTRAR_ASISTENCIA);
        appointment.setPatientHealthInsurance(PatientHealthInsurance.builder()
                .healthInsurance(healthInsurance)
                .affiliateNumber(affiliateNumber).build());
    }

    private OnSiteAppointmentPayment createOnSitePayment(Turno appointment, OnSiteAppointmentPaymentDTO onSiteAppointmentPaymentDTO) {
        return OnSiteAppointmentPayment.builder()
                .appointment(appointment)
                .date(new Date())
                .originalPrice(appointment.getPrecio())
                .finalPrice(onSiteAppointmentPaymentDTO.getPrecioFinal())
                .affiliateNumber(onSiteAppointmentPaymentDTO.getAffiliateNumber())
                .healthInsurance(onSiteAppointmentPaymentDTO.getHealthInsurance()).build();
    }

    private void saveAndNotify(OnSiteAppointmentPayment onSiteAppointmentPayment) {
        log.info("Saving and notifying on site payment {}", onSiteAppointmentPayment);
        paymentStorageService.save(onSiteAppointmentPayment);
        paymentStorageService.generarYEnviarComprobanteCobro(onSiteAppointmentPayment, true, false);
    }


    public PaymentInitDTO getPaymentInit(long appointmentId) {
        log.info("Getting payment init for appointment {}", appointmentId);
        Turno appointment = appointmentService.findById(appointmentId);
        if (appointment.isNotPayable()) {
            throw new InvalidAppointmentStateActionException();
        }
        return buildPaymentInitDto(appointment);
    }

    private PaymentInitDTO buildPaymentInitDto(Turno appointment) {
        Set<HealthInsuranceDTO> healthInsurances = findHealthInsurances(appointment.getProfessionalUserId());
        return PaymentInitDTO.builder()
                .appointmentId(appointment.getId())
                .attentionType(appointment.getTipoAtencion().getName())
                .price(appointment.getPrecio())
                .cardIdNumber(appointment.getPaciente().getNumeroDocumento())
                .documentType(appointment.getPaciente().getTipoDocumento())
                .patientEmail(appointment.getPaciente().getEmail())
                .patientFullName(appointment.getPaciente().getNombreApellido())
                .healthInsurances(healthInsurances)
                .build();
    }

    private Set<HealthInsuranceDTO> findHealthInsurances(int professionalUserId) {
        User professionalUser = userService.findProfessionalUserById(professionalUserId);
        Profesional professional = professionalUser.getProfesional();
        return professional.getHealthInsurancesDTO();
    }
}
