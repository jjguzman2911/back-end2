package com.Medisoft.Medisoft.Entities.PaymentManagement.DTO;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentInitDTO {
    private long appointmentId;
    private String attentionType;
    private float price;
    private String patientEmail;
    private String cardIdNumber;
    private String documentType;
    private String patientFullName;
    private Set<HealthInsuranceDTO> healthInsurances;

}
