package com.Medisoft.Medisoft.Entities.PaymentManagement.DTO;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class OnSiteAppointmentPaymentDTO {
    @NotNull
    private long idTurno;
    private String healthInsurance;
    private String affiliateNumber;
    private float precioFinal;
}
