package com.Medisoft.Medisoft.Entities.PaymentManagement.Services;

import com.Medisoft.Medisoft.Entities.PaymentManagement.DTO.AccessTokenDTO;
import com.Medisoft.Medisoft.Entities.PaymentManagement.DTO.MercadopagoCredentialsDTO;
import com.Medisoft.Medisoft.Entities.PaymentManagement.ProfessionalMercadopagoCredentials;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
public class MercadopagoCredentialsService {

    @Autowired
    private ProfessionalMercadopagoCredentialsStorageService credentialsStorageService;

    @Autowired
    private UserService userService;


    public void createCredentials(int professionalUserId, MercadopagoCredentialsDTO credentialsDto){
        ProfessionalMercadopagoCredentials credentials = credentialsStorageService.findByProfessionalUserIdDummy(professionalUserId);
        if (credentials == null){
            credentials = initializeCredentials(professionalUserId, credentialsDto);
        } else {
            updateCredentials(credentials, credentialsDto);
        }
        credentialsStorageService.save(credentials);
        enableMercadopagoForProfessional(professionalUserId);
    }

    private ProfessionalMercadopagoCredentials initializeCredentials(int professionalUserId, MercadopagoCredentialsDTO credentialsDto) {
        log.info("Creating new credentials for professional {}", professionalUserId);
        return ProfessionalMercadopagoCredentials.builder()
                .accessToken(credentialsDto.getAccessToken())
                .publicKey(credentialsDto.getPublicKey())
                .refreshToken(credentialsDto.getRefreshToken())
                .tokenType(credentialsDto.getTokenType())
                .mercadopagoUserId(credentialsDto.getUserId())
                .professionalUserId(professionalUserId)
                .timestamp(new Date().getTime())
                .build();
    }

    private void updateCredentials(ProfessionalMercadopagoCredentials credentials, MercadopagoCredentialsDTO credentialsDto) {
        log.info("Updating credentials {}", credentials);
        credentials.setAccessToken(credentialsDto.getAccessToken());
        credentials.setPublicKey(credentialsDto.getPublicKey());
        credentials.setRefreshToken(credentialsDto.getRefreshToken());
        credentials.setTokenType(credentialsDto.getTokenType());
        credentials.setMercadopagoUserId(credentialsDto.getUserId());
        credentials.setTimestamp(new Date().getTime());
    }

    @Async
    void enableMercadopagoForProfessional(int professionalUserId){
        updateMercadopagoForProfessional(professionalUserId, true);
        log.info("Mercadopago enabled for professional {}", professionalUserId);
    }

    private void updateMercadopagoForProfessional(int professionalUserId, boolean isWorkingWithMercadopago){
        User professionalUser = userService.findProfessionalUserById(professionalUserId);
        professionalUser.getProfesional().setWorkingWithMercadopago(isWorkingWithMercadopago);
        userService.save(professionalUser);
    }


    public AccessTokenDTO getCredentials(int professionalUserId){
        log.info("Getting credentials for professional {}", professionalUserId);
        ProfessionalMercadopagoCredentials credentials = credentialsStorageService.findByProfessionalUserId(professionalUserId);
        return new AccessTokenDTO(credentials.getAccessToken());
    }


    public void disableCredentials(int professionalUserId){
        updateMercadopagoForProfessional(professionalUserId, false);
        log.info("Mercadopago disabled for professional {}", professionalUserId);
    }
}
