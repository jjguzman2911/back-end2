package com.Medisoft.Medisoft.Entities.PaymentManagement.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.PaymentManagement.OnlineAppointmentPayment;
import com.Medisoft.Medisoft.Exceptions.IOException;
import com.mercadopago.exceptions.MPException;
import com.mercadopago.resources.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

import static com.Medisoft.Medisoft.Utilities.ApplicationConstantsUtils.REFUNDABLE_PERCENTAGE;


@Slf4j
@Service
public class PaymentRefundService {

    @Autowired
    private PaymentStorageService paymentStorageService;

    @Autowired
    private MercadopagoConnectionService mercadoPagoConnectionService;


    public void refundPayment(Turno appointment) {
        log.info("Refunding payment for appointment {}", appointment);
        OnlineAppointmentPayment appointmentPayment =
                (OnlineAppointmentPayment) paymentStorageService.findByAppointmentId(appointment.getId());
        refundPayments(appointmentPayment, appointment.isFullRefundable());
    }

    private void refundPayments(OnlineAppointmentPayment appointmentPayment, boolean isFullRefundable){
        Set<String> paymentIds = appointmentPayment.getPaymentIds();
        paymentIds.forEach(paymentId -> refundPaymentById(paymentId, appointmentPayment.getAccessToken(), isFullRefundable));
    }

    private void refundPaymentById(String paymentId, String accessToken, boolean isFullRefundable){
        Payment payment = mercadoPagoConnectionService.findPayment(paymentId, accessToken);
        float refundableAmount = calculateRefundableAmount(payment.getTransactionAmount(), isFullRefundable);
        try {
            refund(payment, refundableAmount);
        } catch (MPException e) {
            handleException(e);
        }
    }

    private float calculateRefundableAmount(float finalPrice, boolean isFullRefundable){
        return isFullRefundable ?
                finalPrice :
                finalPrice * REFUNDABLE_PERCENTAGE;
    }

    private void refund(Payment payment, float refundableAmount) throws MPException {
        log.info("Refunding {} for payment {}", refundableAmount, payment.getId());
        payment.refund(refundableAmount);
        log.info("${} have been refunded", refundableAmount);
    }

    private void handleException(Exception e) {
        log.error("There was an error refunding payment ", e);
        throw new IOException();
    }
}
