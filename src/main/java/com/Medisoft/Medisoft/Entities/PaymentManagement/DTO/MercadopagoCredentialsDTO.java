package com.Medisoft.Medisoft.Entities.PaymentManagement.DTO;

import lombok.Data;

@Data
public class MercadopagoCredentialsDTO {
    private String accessToken;
    private long expiresIn;
    private boolean liveMode;
    private String publicKey;
    private String refreshToken;
    private String scope;
    private String tokenType;
    private long userId;
}
