package com.Medisoft.Medisoft.Entities.PaymentManagement;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
public class OnlineAppointmentPayment extends AppointmentPayment {
    private String merchantOrderId;
    private Date dateApproval;
    private Date dateLastUpdate;
    private String status;
    private String statusDetail;
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> paymentIds;
    private String accessToken;

    @Builder
    public OnlineAppointmentPayment(long transactionId, Turno appointment, Date dateCreated, float finalPrice,
                                    String merchantOrderId, Date dateApproval, Date dateLastUpdate,
                                    String status, String statusDetail, String accessToken) {
        super(transactionId, appointment, dateCreated, finalPrice, "Particular");
        setMerchantOrderId(merchantOrderId);
        setDateApproval(dateApproval);
        setDateLastUpdate(dateLastUpdate);
        setStatus(status);
        setStatusDetail(statusDetail);
        setAccessToken(accessToken);
    }

    public void addPaymentId(String paymentId){
        if (paymentIds == null){
            paymentIds = new HashSet<>();
        }
        paymentIds.add(paymentId);
    }
}
