package com.Medisoft.Medisoft.Entities.PaymentManagement.Services;

import com.Medisoft.Medisoft.Exceptions.IOException;
import com.mercadopago.MercadoPago;
import com.mercadopago.exceptions.MPException;
import com.mercadopago.resources.MerchantOrder;
import com.mercadopago.resources.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class MercadopagoConnectionService {


    public Payment findPayment(String paymentId, String accessToken) {
        try {
            configureSDK(accessToken);
            log.info("Finding payment {}", paymentId);
            return Payment.findById(paymentId);
        } catch (MPException e) {
            log.error("Error finding payment ", e);
            throw new IOException();
        }
    }

    public MerchantOrder findMerchantOrder(String merchantOrderId, String accessToken) {
        try {
            configureSDK(accessToken);
            log.info("Finding merchant order {}", merchantOrderId);
            return MerchantOrder.findById(merchantOrderId);
        } catch (MPException e) {
            log.error("Error finding merchant order ", e);
            throw new IOException();
        }
    }

    private void configureSDK(String accessToken){
        try {
            MercadoPago.SDK.cleanConfiguration();
            MercadoPago.SDK.setAccessToken(accessToken);
            log.info("Access token set {}", accessToken);
        } catch (MPException e) {
            log.error("Error setting access token {}: ", accessToken, e);
            throw new IOException();
        }
    }
}