package com.Medisoft.Medisoft.Entities.SessionManagement.DTO;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SessionUserDTO {
    private String email;
    private String contrasenia;
}
