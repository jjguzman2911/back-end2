package com.Medisoft.Medisoft.Entities.SessionManagement.DTO;

import com.Medisoft.Medisoft.Entities.PatientManagement.Paciente;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.UserManagement.Person.Persona;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDTO {

    private int sessionId;
    private int idUsuario;
    private String email;
    private String contrasenia;
    private String fotoUrl;
    private Paciente paciente;
    private Persona persona;
    private Profesional profesional;
    private boolean isModerator;

}
