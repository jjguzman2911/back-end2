package com.Medisoft.Medisoft.Entities.SessionManagement;


import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;


@Data
@NoArgsConstructor
@Entity
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Date loginDate;
    private Date logoutDate;

    @OneToOne(fetch = FetchType.EAGER)
    private User user;

    public Session(User user) {
        this.loginDate = new Date();
        this.user = user;
    }

    @JsonIgnore
    public boolean isOpen() {
        return logoutDate == null;
    }

    public void close() {
        logoutDate = new Date();
    }
}

