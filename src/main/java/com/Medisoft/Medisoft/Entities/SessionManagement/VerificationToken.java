package com.Medisoft.Medisoft.Entities.SessionManagement;

import com.Medisoft.Medisoft.Entities.UserManagement.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Modela el Token de identificacion para que un Usuario pueda pasar de No Verificado a Verificado.
 *
 * @author Misael
 * @version 2019-11-17
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VerificationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String token;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private User user;

}
