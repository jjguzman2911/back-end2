package com.Medisoft.Medisoft.Entities.PatientManagement;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.ProfesionalTurno;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Paciente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Singular("tipoAtencionDisponible")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<AtencionesDisponiblesPaciente> tiposAtencionDisponibles;


    @JsonIgnore
    public AtencionesDisponiblesPaciente getAtencionProfesional(ProfesionalTurno profesional) {
        if (tiposAtencionDisponibles == null) return null;
        return tiposAtencionDisponibles.stream()
                .filter(atencionesDisponiblesPaciente ->
                        atencionesDisponiblesPaciente.getProfesional().equals(profesional))
                .findFirst()
                .orElse(null);
    }

    public void updateAvailableAttentionTypes(ProfesionalTurno professional, List<AtencionDisponible> availableAttentionTypes) {
        tiposAtencionDisponibles.forEach(atencionesDisponiblesPaciente -> {
            if (atencionesDisponiblesPaciente.equalsProfessional(professional)) {
                atencionesDisponiblesPaciente.setAtencionesDisponibles(availableAttentionTypes);
                atencionesDisponiblesPaciente.setDate(new Date());
            }
        });
    }

    public boolean hasAtencionesProfesional(ProfesionalTurno profesional) {
        if (tiposAtencionDisponibles == null) return false;
        return tiposAtencionDisponibles
                .stream()
                .filter(AtencionesDisponiblesPaciente::esCompleto)
                .anyMatch(atencionesDisponiblesPaciente ->
                        atencionesDisponiblesPaciente.getProfesional().equals(profesional));
    }

    public void addTipoAtencionDisponible(AtencionesDisponiblesPaciente atencionesDisponiblesPaciente) {
        if (tiposAtencionDisponibles == null) {
            tiposAtencionDisponibles = new ArrayList<>();
        }
        tiposAtencionDisponibles.add(atencionesDisponiblesPaciente);
    }
}
