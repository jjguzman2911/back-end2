package com.Medisoft.Medisoft.Entities.PatientManagement;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AtencionDisponible {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Date date;

    @OneToOne
    private TipoAtencionProfesional tipoAtencionProfesional;

    private boolean valida; //se crea siempre en true, y una vez que es consumida la atencion se cambia a false;


    public boolean containsTipoAtencionProfesional(TipoAtencionProfesional atencionProfesional) {
        return tipoAtencionProfesional.equals(atencionProfesional);
    }

    public boolean containsValidProfessionalAttentionType(TipoAtencionProfesional atencionProfesional) {
        return containsTipoAtencionProfesional(atencionProfesional) && valida;
    }

    @JsonIgnore
    public boolean isMandatory() {
        return tipoAtencionProfesional.isMandatory();
    }
}
