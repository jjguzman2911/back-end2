package com.Medisoft.Medisoft.Entities.PatientManagement;

import lombok.Data;

@Data
public class AvailableAttentionTypeDTO {
    private int professionalAttentionTypeId;
    private boolean valid;
}
