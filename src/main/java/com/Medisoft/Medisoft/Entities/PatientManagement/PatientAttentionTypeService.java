package com.Medisoft.Medisoft.Entities.PatientManagement;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.ProfesionalTurno;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO.CategoriaTipoAtencionDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO.TipoAtencionDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Services.ProfessionalStorageService;
import com.Medisoft.Medisoft.Entities.UserManagement.User;
import com.Medisoft.Medisoft.Entities.UserManagement.UserService;
import com.Medisoft.Medisoft.Exceptions.EntityNotFoundException;
import com.Medisoft.Medisoft.Exceptions.InvalidDataException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PatientAttentionTypeService {

    @Autowired
    private UserService userService;

    @Autowired
    private ProfessionalStorageService professionalStorageService;


    public AtencionesDisponiblesPaciente findAvailableAttentionTypes(int idUsuarioPaciente, int idUsuarioProfesional) {
        User usuarioPaciente = userService.findById(idUsuarioPaciente);
        ProfesionalTurno profesionalTurno = professionalStorageService.getProfesionalTurnoByIdUsuario(idUsuarioProfesional);
        Paciente paciente = usuarioPaciente.getPaciente();
        AtencionesDisponiblesPaciente atencionesDisponiblesPaciente;
        if (paciente.hasAtencionesProfesional(profesionalTurno)) {
            atencionesDisponiblesPaciente = paciente.getAtencionProfesional(profesionalTurno);
        } else {
            Profesional profesional = professionalStorageService.getProfesionalByIdUsuario(idUsuarioProfesional);
            if (profesional == null) {
                throw new EntityNotFoundException(Profesional.class.getSimpleName());
            }
            TipoAtencionProfesional tipoAtencionObligatoria = profesional.getTipoAtencionObligatoria();
            if (tipoAtencionObligatoria == null) {
                throw new EntityNotFoundException(TipoAtencionProfesional.class.getSimpleName());
            }
            atencionesDisponiblesPaciente = createAvailablePatientAttentionTypes(
                    profesionalTurno, profesional.getAtenciones().getAtenciones());

            paciente.addTipoAtencionDisponible(atencionesDisponiblesPaciente);
            usuarioPaciente.setPaciente(paciente);
            userService.save(usuarioPaciente);
        }
        log.info("Available attention types retrieved");
        atencionesDisponiblesPaciente.getAvailableAttentions().forEach(attention -> log.info(attention.toString()));
        return atencionesDisponiblesPaciente;
    }


    private AtencionesDisponiblesPaciente createAvailablePatientAttentionTypes(ProfesionalTurno profesional,
                                                                               List<TipoAtencionProfesional> professionalAttentionTypes) {
        List<AtencionDisponible> availableAttentionTypes = professionalAttentionTypes.stream()
                .map(professionalAttentionType -> AtencionDisponible.builder()
                        .valida(professionalAttentionType.isMandatory())
                        .date(new Date())
                        .tipoAtencionProfesional(professionalAttentionType)
                        .build())
                .collect(Collectors.toList());
        return AtencionesDisponiblesPaciente.builder()
                .date(new Date())
                .profesional(profesional)
                .atencionesDisponibles(availableAttentionTypes)
                .build();
    }

    //@Transactional
    public AtencionesDisponiblesPaciente updateAvailablePatientAttentionTypes(int patientUserId,
                                                                              List<AtencionDisponible> newAttentionTypes,
                                                                              int professionalUserId) {
        User patientUser = userService.findById(patientUserId);
        ProfesionalTurno appointmentProfessional = professionalStorageService.getProfesionalTurnoByIdUsuario(professionalUserId);
        Paciente patient = patientUser.getPaciente();

        newAttentionTypes.forEach(atencionDisponible -> atencionDisponible.setDate(new Date()));
        patient.updateAvailableAttentionTypes(appointmentProfessional, newAttentionTypes);

        //patientService.save(patient);
        patientUser.setPaciente(patient);
        userService.save(patientUser);
        log.info("Available attention types saved");
        return patient.getAtencionProfesional(appointmentProfessional);
    }

    public Set<CategoriaTipoAtencionDTO> createDtoSet(List<AtencionDisponible> atencionesDisponible) {
        Set<CategoriaTipoAtencionDTO> categorias = atencionesDisponible.stream()
                .map(atencionDisponible -> new CategoriaTipoAtencionDTO(atencionDisponible.getTipoAtencionProfesional()))
                .collect(Collectors.toSet());

        for (CategoriaTipoAtencionDTO categoriaTipoAtencionDTO : categorias) {
            for (AtencionDisponible atencionDisponible : atencionesDisponible) {
                addTipoAtencionToCategoria(categoriaTipoAtencionDTO, atencionDisponible);
            }
        }
        log.info("{} categories retrieved", categorias.size());
        return categorias;
    }

    private void addTipoAtencionToCategoria(CategoriaTipoAtencionDTO categoriaTipoAtencionDTO,
                                            AtencionDisponible atencionDisponible) {
        if (categoriaTipoAtencionDTO.isCategoriaTipoAtencion(
                atencionDisponible.getTipoAtencionProfesional().getCategoriaTipoAtencion())) {
            categoriaTipoAtencionDTO.addTipoAtencion
                    (new TipoAtencionDTO(atencionDisponible.getTipoAtencionProfesional(), atencionDisponible.isValida()));
        }
    }

    public void createPatientAvailableAttentionTypes(List<AvailableAttentionTypeDTO> availableAttentionTypeDTOS,
                                                     int professionalUserId, int patientUserId) {
        User professionalUser = userService.findById(professionalUserId);
        User patientUser = userService.findById(patientUserId);
        Profesional professional = professionalUser.getProfesional();
        Paciente patient = patientUser.getPaciente();
        List<AtencionDisponible> patientAvailableAttentionTypes =
                buildPatientAvailableAttentionType(availableAttentionTypeDTOS,
                        professional.getAtenciones().getAtenciones());
        patient.updateAvailableAttentionTypes(professionalStorageService.getProfesionalTurnoByIdUsuario(professionalUserId),
                patientAvailableAttentionTypes);
        log.info("{} available attention types has been added for patient {} and professional {}",
                patientAvailableAttentionTypes.size(), patientUserId, professionalUserId);
    }

    private List<AtencionDisponible> buildPatientAvailableAttentionType(List<AvailableAttentionTypeDTO> availableAttentionTypeDTOS, List<TipoAtencionProfesional> professionalAttentionTypeList) {
        if (availableAttentionTypeDTOS.size() != professionalAttentionTypeList.size()) {
            throw new InvalidDataException("las atenciones del profesional");
        }
        Set<AtencionDisponible> patientAvailableAttentionTypes = new HashSet<>();
        professionalAttentionTypeList.forEach(professionalAttentionType -> availableAttentionTypeDTOS.forEach(availableAttentionTypeDTO -> {
            if (availableAttentionTypeDTO.getProfessionalAttentionTypeId() == professionalAttentionType.getId()) {
                patientAvailableAttentionTypes.add(AtencionDisponible.builder()
                        .date(new Date())
                        .tipoAtencionProfesional(professionalAttentionType)
                        .valida(availableAttentionTypeDTO.isValid())
                        .build());
            }
        }));
        return new ArrayList<>(patientAvailableAttentionTypes);
    }
}
