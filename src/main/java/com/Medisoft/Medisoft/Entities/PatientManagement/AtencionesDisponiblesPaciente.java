package com.Medisoft.Medisoft.Entities.PatientManagement;


import com.Medisoft.Medisoft.Entities.AppointmentManagement.ProfesionalTurno;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"atencionesDisponibles"})
@Entity
public class AtencionesDisponiblesPaciente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Singular("atencionDisponible")
    @OneToMany(cascade = CascadeType.ALL) //orphan removal is not working well here, don't add it again (30/07/20)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<AtencionDisponible> atencionesDisponibles;

    @OneToOne
    private ProfesionalTurno profesional;

    private Date date;


    public boolean esCompleto() {
        return atencionesDisponibles != null &&
                profesional != null &&
                date != null;
    }

    public void disableProfessionalAttentionType(TipoAtencionProfesional professionalAttentionType) {
        atencionesDisponibles.forEach(availableAttention -> {
            if (availableAttention.containsValidProfessionalAttentionType(professionalAttentionType)
                    && !availableAttention.isMandatory()) {
                availableAttention.setValida(false);
            }
        });
    }

    /**
     * return all valid attentions
     */
    @JsonIgnore
    public List<AtencionDisponible> getAvailableAttentions() {
        return atencionesDisponibles.stream()
                .filter(AtencionDisponible::isValida)
                .collect(Collectors.toList());
    }

    public boolean equalsProfessional(ProfesionalTurno professional) {
        return profesional.equals(professional);
    }
}
