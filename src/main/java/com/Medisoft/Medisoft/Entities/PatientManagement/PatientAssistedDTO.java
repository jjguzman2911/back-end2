package com.Medisoft.Medisoft.Entities.PatientManagement;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientAssistedDTO {
    private int userId;
    private String name;
    private String surname;
    private String cardIdNumber;
    private String documentType;
    private String photoUrl;

}
