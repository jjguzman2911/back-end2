package com.Medisoft.Medisoft.Entities.StatisticsManagement;

public interface AppointmentByDay {
    int getDayId();
    String getDayName();
    int getAmount();
}
