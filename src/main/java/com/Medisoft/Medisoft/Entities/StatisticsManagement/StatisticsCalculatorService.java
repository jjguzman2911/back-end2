package com.Medisoft.Medisoft.Entities.StatisticsManagement;

import com.Medisoft.Medisoft.DAO.JPA.IAppointmentDAO;
import com.Medisoft.Medisoft.DAO.JPA.IAppointmentPaymentDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class StatisticsCalculatorService {

    @Autowired
    private IAppointmentDAO appointmentDAO;

    @Autowired
    private IAppointmentPaymentDAO appointmentPaymentDAO;


    public List<AppointmentByState> getAppointmentsByState(int professionalUserId, List<String> stateNames, long fromTimestamp, long toTimestamp) {
        List<AppointmentByState> statistics = appointmentDAO.countAppointmentsByState(professionalUserId, stateNames, fromTimestamp, toTimestamp);
        statistics.forEach(statistic -> log.info("State {} with {} appointments", statistic.getState(), statistic.getAmount()));
        return statistics;
    }

    public List<AppointmentByHealthInsurance> getAppointmentsTreatedByHealthInsurance(int professionalUserId, long fromTimestamp,
                                                                                      long toTimestamp) {
        List<AppointmentByHealthInsurance> statistics = appointmentDAO.countAppointmentTreatedByHealthInsurance(professionalUserId, fromTimestamp, toTimestamp);
        statistics.forEach(statistic -> log.info("Health insurance {} with {} appointments", statistic.getHealthInsurance(), statistic.getAmount()));
        return statistics;
    }

    public List<PaymentByPaymentType> getPaymentsByPaymentType(int professionalUserId, long fromTimestamp, long toTimestamp){
        List<PaymentByPaymentType> statistics = appointmentPaymentDAO.countPaymentsByPaymentType(professionalUserId, fromTimestamp, toTimestamp);
        statistics.forEach(statistic -> log.info("Payment type {} with {} payments", statistic.getPaymentType(), statistic.getAmount()));
        return statistics;
    }

    public List<AppointmentByMonth> getAppointmentsTreatedByMonth(int professionalUserId, long fromTimestamp, long toTimestamp){
        List<AppointmentByMonth> statistics = appointmentDAO.countAppointmentsTreatedByMonth(professionalUserId, fromTimestamp, toTimestamp);
        statistics.forEach(statistic -> log.info("Month {} with {} appointments", statistic.getMonthId(), statistic.getAmount()));
        return statistics;
    }

    public List<AppointmentByDay> getAppointmentsTreatedByDay(int professionalUserId, long fromTimestamp, long toTimestamp){
        List<AppointmentByDay> statistics = appointmentDAO.countAppointmentsTreatedByDay(professionalUserId, fromTimestamp, toTimestamp);
        statistics.forEach(statistic -> log.info("Day {} with {} appointments", statistic.getDayName(), statistic.getAmount()));
        return statistics;
    }
}
