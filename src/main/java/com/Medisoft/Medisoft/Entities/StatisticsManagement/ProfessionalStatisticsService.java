package com.Medisoft.Medisoft.Entities.StatisticsManagement;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class ProfessionalStatisticsService {

    @Autowired
    private StatisticsCalculatorService statisticsCalculatorService;
    

    public List<AppointmentByState> getAppointmentsByState(int professionalUserId, List<String> stateNames, long fromTimestamp,
                                                           long toTimestamp) {
        log.info("Getting appointments for professional {} between {} and {}", professionalUserId, new Date(fromTimestamp), new Date(toTimestamp));
        return statisticsCalculatorService.getAppointmentsByState(professionalUserId, stateNames, fromTimestamp, toTimestamp);
    }

    public List<AppointmentByHealthInsurance> getAppointmentsTreatedByHealthInsurance(int professionalUserId,
                                                                                      long fromTimestamp, long toTimestamp) {
        log.info("Getting appointments treated by health insurance for professional {} between {} and {}", professionalUserId, new Date(fromTimestamp), new Date(toTimestamp));
        return statisticsCalculatorService.getAppointmentsTreatedByHealthInsurance(professionalUserId, fromTimestamp, toTimestamp);
    }

    public List<PaymentByPaymentType> getPaymentsByPaymentType(int professionalUserId, long fromTimestamp, long toTimestamp){
        log.info("Getting payments by payment type for professional {} between {} and {}", professionalUserId, new Date(fromTimestamp), new Date(toTimestamp));
        return statisticsCalculatorService.getPaymentsByPaymentType(professionalUserId, fromTimestamp, toTimestamp);
    }

    public List<AppointmentByMonth> getAppointmentsTreatedByMonth(int professionalUserId, long fromTimestamp, long toTimestamp){
        if (toTimestamp == Long.MAX_VALUE){
            toTimestamp = new Date().getTime();
            log.info("ToTimestamp changed to current timestamp {}", toTimestamp);
        }
        log.info("Getting appointments treated by month for professional {} between {} and {}", professionalUserId, new Date(fromTimestamp), new Date(toTimestamp));
        return statisticsCalculatorService.getAppointmentsTreatedByMonth(professionalUserId, fromTimestamp, toTimestamp);
    }

    public List<AppointmentByDay> getAppointmentsTreatedByDay(int professionalUserId, long fromTimestamp, long toTimestamp){
        log.info("Getting appointments treated by day for professional {} between {} and {}", professionalUserId, new Date(fromTimestamp), new Date(toTimestamp));
        return statisticsCalculatorService.getAppointmentsTreatedByDay(professionalUserId, fromTimestamp, toTimestamp);
    }
}
