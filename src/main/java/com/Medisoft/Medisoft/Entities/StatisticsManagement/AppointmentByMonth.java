package com.Medisoft.Medisoft.Entities.StatisticsManagement;

public interface AppointmentByMonth {
    int getMonthId();
    String getMonthName();
    int getAmount();
    int getYearId();
}
