package com.Medisoft.Medisoft.Entities.StatisticsManagement;

public interface AppointmentByState {
    String getState();
    int getAmount();
}
