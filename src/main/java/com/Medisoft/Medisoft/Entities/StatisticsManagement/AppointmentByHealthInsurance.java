package com.Medisoft.Medisoft.Entities.StatisticsManagement;

public interface AppointmentByHealthInsurance {
    String getHealthInsurance();
    int getAmount();
}
