package com.Medisoft.Medisoft.Entities.StatisticsManagement;

public interface PaymentByPaymentType {
    int getAmount();
    String getPaymentType();
}
