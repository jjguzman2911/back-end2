package com.Medisoft.Medisoft.Scheduling;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AppointmentService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public abstract class AppointmentTask implements Runnable {

    @Autowired
    protected AppointmentService appointmentService;
    protected Turno turno;

    @Override
    public void run() {
        if (!hasBeenModified() && executeAction()) {
            showLog();
        }
    }

    protected abstract boolean executeAction();

    protected abstract void showLog();

    private boolean hasBeenModified() {
        Turno turnoBD = appointmentService.findByIdDummy(turno.getId());
        if (turnoBD != null && turno.getEstadoActual().equals(turnoBD.getEstadoActual())) {
            turno = turnoBD;
            return false;
        }
        return true;
    }
}
