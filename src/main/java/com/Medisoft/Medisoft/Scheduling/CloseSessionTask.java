package com.Medisoft.Medisoft.Scheduling;


import com.Medisoft.Medisoft.Entities.SessionManagement.Session;
import com.Medisoft.Medisoft.Entities.UserManagement.SessionStorageService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Component("sessionTask")
public class CloseSessionTask implements Runnable {

    private Session session;

    @Autowired
    private SessionStorageService sessionStorageService;


    @Override
    public void run() {
        if (!hasBeenModified()) {
            session.close();
            sessionStorageService.save(session);
        }
    }

    private boolean hasBeenModified() {
        Session sessionDB = sessionStorageService.findById(session.getId());
        return sessionDB == null || !sessionDB.isOpen();
    }
}
