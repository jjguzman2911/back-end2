package com.Medisoft.Medisoft.Scheduling;

import com.Medisoft.Medisoft.Config.TaskConfig;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Utilities.DateFormatConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
public class AppointmentTaskScheduler {

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private TaskConfig taskConfig;

    @Async
    public void createNonAttendanceAppointmentTask(Turno turno, boolean showMessage) {
        AppointmentTask inasistenciaAppointmentTask = taskConfig.inasistenciaTurnoTask(turno);
        Date fechaHoraEjecucion = generarFechaHora(turno.getFechaHoraFin());
        scheduleTurnoTask(inasistenciaAppointmentTask, fechaHoraEjecucion, showMessage);
    }

    @Async
    public void createNoDisponibilidadTurnoTask(Turno turno, boolean showMessage) {
        AppointmentTask noDisponibilidadAppointmentTask = taskConfig.noDisponibilidadTurnoTask(turno);
        Date fechaHoraEjecucion = generarFechaHora(turno.getFechaHoraFin());
        scheduleTurnoTask(noDisponibilidadAppointmentTask, fechaHoraEjecucion, showMessage);
    }


    private void scheduleTurnoTask(AppointmentTask task, Date fechaHoraEjecucion, boolean showMessage) {
        taskScheduler.schedule(task, fechaHoraEjecucion);
        if (showMessage) {
            log.info("Task scheduled for {}", fechaHoraEjecucion);
        }
    }

    private Date generarFechaHora(String fechaHora) {
        return DateFormatConverter.parseStringToDate(fechaHora);
    }

}
