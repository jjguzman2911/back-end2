package com.Medisoft.Medisoft.Scheduling;

import com.Medisoft.Medisoft.Config.TaskConfig;
import com.Medisoft.Medisoft.Entities.SessionManagement.Session;
import com.Medisoft.Medisoft.Utilities.DateFormatConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
public class SessionTaskScheduler {

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private TaskConfig taskConfig;

    @Async
    public void closeSession(Session session) {
        Runnable closeSessionTask = taskConfig.closeSessionTask(session);
        long halfAnHour = 1000 * 60 * 30;
        Date when = DateFormatConverter.getDatePlusTime(halfAnHour);

        taskScheduler.schedule(closeSessionTask, when);
        log.info("Session will close at " + when);
    }
}
