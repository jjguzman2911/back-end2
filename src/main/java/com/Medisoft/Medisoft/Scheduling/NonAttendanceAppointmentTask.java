package com.Medisoft.Medisoft.Scheduling;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.EstadoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.EventoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NonAttendanceAppointmentTask extends AppointmentTask {


    public NonAttendanceAppointmentTask(Turno turno) {
        this.turno = turno;
    }

    @Override
    protected boolean executeAction() {
        if (turno.getEstadoActual().equals(EstadoTurno.ASIGNADO) ||
                turno.getEstadoActual().equals(EstadoTurno.COBRADO)) {
            turno.actualizarEstado(EventoTurno.REGISTRAR_INASISTENCIA);
            appointmentService.save(turno);
            return true;
        }
        return false;
    }

    @Override
    protected void showLog() {
        log.info("Appointment {} non attended ", turno.getId());
    }
}
