package com.Medisoft.Medisoft.Scheduling;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.EstadoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.EventoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UnassignedAppointmentTask extends AppointmentTask {

    public UnassignedAppointmentTask(Turno turno) {
        this.turno = turno;
    }


    @Override
    protected boolean executeAction() {
        if (turno.getEstadoActual().equals(EstadoTurno.DISPONIBLE)) {
            turno.actualizarEstado(EventoTurno.NO_ASIGNAR);
            appointmentService.save(turno);
            return true;
        }
        return false;
    }

    @Override
    protected void showLog() {
        log.info("Appointment {} unassigned", turno.getId());
    }
}
