package com.Medisoft.Medisoft.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.EmailManagement.IAdjuntableEnEmail;
import com.Medisoft.Medisoft.Entities.EmailManagement.Services.EmailNotificationService;
import com.Medisoft.Medisoft.Entities.PaymentManagement.OnSiteAppointmentPayment;
import com.Medisoft.Medisoft.Entities.SlipManagement.IComprobante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ComprobanteMailService {

    @Autowired
    private EmailNotificationService emailNotificationService;

    @Autowired
    private ComprobanteFactory comprobanteFactory;


    public void generarYEnviarComprobanteTurno(Turno turno, boolean enviarAPaciente,
                                               boolean enviarAProfesional) {
        IComprobante comprobanteTurno = comprobanteFactory.buildComprobanteTurnoPDF(turno);
        if (enviarAPaciente) {
            String emailPaciente = turno.getPaciente().getEmail();
            emailNotificationService.notifyAppointmentAssignedPatient(emailPaciente, (IAdjuntableEnEmail) comprobanteTurno);
        }
        if (enviarAProfesional) {
            String emailProfesional = turno.getProfesional().getEmail();
            emailNotificationService.notifyAppointmentAssignedProfessional(emailProfesional, (IAdjuntableEnEmail) comprobanteTurno);
        }
    }

    @Async
    public void generarYEnviarComprobanteCobro(OnSiteAppointmentPayment cobroTurno, boolean enviarAPaciente,
                                               boolean enviarAProfesional) {
        IComprobante comprobanteCobro = comprobanteFactory.buildComprobanteCobroPDF(cobroTurno);
        cobroTurno.setComprobante(comprobanteCobro);
        if (enviarAPaciente) {
            String emailPaciente = cobroTurno.getAppointment().getPaciente().getEmail();
            emailNotificationService.notifyAppointmentPaidPatient(emailPaciente, (IAdjuntableEnEmail) comprobanteCobro);
        }
        if (enviarAProfesional) {
            String emailProfesional = cobroTurno.getAppointment().getProfesional().getEmail();
            emailNotificationService.notifyAppointmentPaidProfessional(emailProfesional, (IAdjuntableEnEmail) comprobanteCobro);
        }
    }
}
