package com.Medisoft.Medisoft.Services;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.SlipManagement.PaymentSlip.ComprobanteCobroBuilderPDF;
import com.Medisoft.Medisoft.Entities.SlipManagement.AppointmentSlip.ComprobanteTurnoBuilderPDF;
import com.Medisoft.Medisoft.Entities.SlipManagement.IComprobante;
import com.Medisoft.Medisoft.Entities.SlipManagement.IComprobanteBuilder;
import com.Medisoft.Medisoft.Entities.PaymentManagement.AppointmentPayment;
import org.springframework.stereotype.Service;

/**
 * Es la fábrica que surge del patrón Builder y se encarga de coordinar
 * los servicios de creación de comprobantes de turnos.-
 *
 * @pattern Builder
 * @created 22/03/2020
 * @lastModification 23/03/2020
 */
@Service
public class ComprobanteFactory {

    private IComprobanteBuilder comprobanteBuilder;


    private IComprobante getComprobante() {
        return comprobanteBuilder.getComprobante();
    }

    private void buildComprobante() {
        comprobanteBuilder.construirComprobante();
    }

    private void setComprobanteBuilder(IComprobanteBuilder comprobanteBuilder) {
        this.comprobanteBuilder = comprobanteBuilder;
    }


    public IComprobante buildComprobanteTurnoPDF(Turno turno) {
        setComprobanteBuilder(new ComprobanteTurnoBuilderPDF(turno));
        buildComprobante();

        return getComprobante();
    }

    public IComprobante buildComprobanteCobroPDF(AppointmentPayment appointmentPayment) {
        setComprobanteBuilder(new ComprobanteCobroBuilderPDF(appointmentPayment));
        buildComprobante();

        return getComprobante();
    }
}
