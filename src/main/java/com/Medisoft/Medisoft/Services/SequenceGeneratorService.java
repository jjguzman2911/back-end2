package com.Medisoft.Medisoft.Services;

import com.Medisoft.Medisoft.Utilities.DatabaseSequence;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Slf4j
@Service
public class SequenceGeneratorService {

    @Autowired
    private MongoOperations mongoOperations;


    public long generateSequence(String sequenceName) {
        log.info("Generating sequence for {}", sequenceName);
        DatabaseSequence sequence = mongoOperations.findAndModify(query(where("_id").is(sequenceName)),
                new Update().inc("seq", 1), options().returnNew(true).upsert(true),
                DatabaseSequence.class);
        return !Objects.isNull(sequence) ? sequence.getSeq() : 1;
    }
}
