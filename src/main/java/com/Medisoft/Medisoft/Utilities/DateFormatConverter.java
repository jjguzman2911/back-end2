package com.Medisoft.Medisoft.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Esta clase realiza multiples conversiones de todos los tipos de datos.
 * Permite trabajar principalmente con horas y fechas en diferentes formatos.
 *
 * @author Misael
 * @version 2019-08-29
 */
public class DateFormatConverter {
    /**
     * Permite parsear la hora en formato HH:MM como un entero formato HHMM.
     *
     * @param hora representa la hora en formato HH:MM.
     * @return int devuelve la hora en formato HHMM.
     */
    public static int horaStringAInteger(String hora) {
        String horaParcial = hora.replaceAll(":", "");
        return Integer.parseInt(horaParcial);
    }


    /**
     * Convierte un Calendar a una fecha formato dd/MM/aaaa HH:mm.
     *
     * @param calendar representa el Calendar.
     * @return String, devuelve la fecha en formato dd/MM/aaaa HH:mm.
     */
    public static String calendarToString(Calendar calendar) {
        int anio = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH) + 1;
        int dia = calendar.get(Calendar.DAY_OF_MONTH);

        int hora = calendar.get(Calendar.HOUR_OF_DAY);
        int minuto = calendar.get(Calendar.MINUTE);
        return formatOneDigit(dia) + "/" +
                formatOneDigit(mes) + "/" +
                anio + " " +
                formatOneDigit(hora) + ":" +
                formatOneDigit(minuto);
    }


    /**
     * Formatea los valores de un solo dígito a dos digitos.-
     *
     * @param digit representa el dígito a formatear.
     * @return String, devuelve el dígito formateado.-
     */
    private static String formatOneDigit(int digit) {
        if (digit < 10) {
            return "0" + digit;
        }
        return "" + digit;
    }


    /**
     * Convierte una fecha en formato dd/MM/yyyy HH:mm a Date.
     *
     * @param fecha representa la fecha.
     * @return Date, devuelve la fecha en formato Date.
     */
    public static Date parseStringToDate(String fecha) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(GlobalTimeSetting.dateFormat);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone(GlobalTimeSetting.timeZone));
            return simpleDateFormat.parse(fecha);

        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return new Date();
    }


    /**
     * Parsea una fecha String a Calendar.-
     *
     * @param fecha representa la fecha en formato dd/MM/aaaa HH:mm.
     * @return Calendar, devuelve una istancia de Calendar con la fecha seteada.
     * @see #parseStringToDate(String fecha)
     */
    public static Calendar parseStringToCalendar(String fecha) {
        Date date = parseStringToDate(fecha);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        return calendar;
    }


    /**
     * Concatena los dígitos de una fecha en formato String dd/MM/yyyy HH:mm.-
     *
     * @param date representa la fecha a transformar.
     * @return long, devuelve la fecha como ddMMyyyyHHmm.-
     */
    public static long stringDateToLongDate(String date) {
        date = date.replaceAll("/", "");
        date = date.replaceAll(":", "");
        date = date.replaceAll(" ", "");

        return Long.parseLong(date);
    }


    /**
     * Permite unir dos longs en uno solo como una concatenación de ellos.-
     *
     * @param firstLong  representa el primer long.
     * @param secondLong representa el segundo long.
     * @return long, devuelve la concatenación de ambos longs.-
     */
    public static long joinLongs(long firstLong, long secondLong) {
        String firstString = "" + firstLong;
        String secondString = "" + secondLong;

        return Long.parseLong(firstString.concat(secondString));
    }


    /**
     * Devuelve los minutos de una hora en formato HH:MM.
     *
     * @param hora representa la hora.
     * @return int, devuelve MM.
     */
    public static int minuteFromHour(String hora) {
        String[] horaParcial = hora.split(":");
        return Integer.parseInt(horaParcial[1]);
    }


    /**
     * Devuelve las horas de una hora en formato HH:MM.
     *
     * @param hora representa la hora.
     * @return int, devuelve HH.
     */
    public static int hourFromHour(String hora) {
        String[] horaParcial = hora.split(":");
        return Integer.parseInt(horaParcial[0]);
    }


    /**
     * Parsea una fecha en formato Date a un formato String.-
     *
     * @param fecha representa la fecha a parsear.
     * @return String, devuelve una fecha en formato dd/MM/yyyy HH:mm.-
     */
    public static String parseDateToString(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat(GlobalTimeSetting.dateFormat);
        sdf.setTimeZone(TimeZone.getTimeZone(GlobalTimeSetting.timeZone));
        return sdf.format(fecha);
    }

    public static Date getDatePlusTime(long time) {
        long now = new Date().getTime();
        now += time;
        return new Date(now);
    }

    public static long getDateInMillis(String date) {
        Date parsedDate = parseStringToDate(date);
        return parsedDate.getTime();
    }
}
