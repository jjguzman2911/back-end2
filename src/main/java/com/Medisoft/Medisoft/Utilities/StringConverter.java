package com.Medisoft.Medisoft.Utilities;

import java.util.stream.IntStream;

public class StringConverter {

    public static String parseStringToFirstCharacterUpperCaseString(String string) {
        return IntStream.concat(IntStream.of(string.codePointAt(0))
                .map(Character::toUpperCase), string.codePoints().skip(1))
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

}
