package com.Medisoft.Medisoft.Utilities;

public class NumberRounderUtils {

    public static float roundToOneDecimal(float number){
        return roundToNDecimals(number, 1);
    }

    private static float roundToNDecimals(float number, int n){
        double scale = Math.pow(10, n);
        return (float) (Math.round(number * scale) / scale);
    }
}
