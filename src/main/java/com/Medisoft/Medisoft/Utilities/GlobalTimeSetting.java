package com.Medisoft.Medisoft.Utilities;

public class GlobalTimeSetting {

    public static final String dateFormat = "dd/MM/yyyy HH:mm";
    public static final String timeZone = "America/Buenos_Aires";
}
