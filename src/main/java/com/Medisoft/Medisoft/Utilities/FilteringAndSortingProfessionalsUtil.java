package com.Medisoft.Medisoft.Utilities;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalSearchingManagement.ProfessionalSearchDTO;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class FilteringAndSortingProfessionalsUtil {

    public static void filterAndSortProfessionals(List<ProfessionalSearchDTO> professionalSearchDTOS,
                                                  HealthInsuranceDTO healthInsuranceDTO,
                                                  String orderBy) {
        if (healthInsuranceDTO != null && healthInsuranceDTO.isValid()) {
            filterByHealthInsurance(professionalSearchDTOS, healthInsuranceDTO);
        }
        if (orderBy.equals("score")) {
            sortByScore(professionalSearchDTOS);
        }
    }

    private static void sortByScore(List<ProfessionalSearchDTO> professionalSearchDTOS) {
        log.info("Sorting professionals by score");
        professionalSearchDTOS.sort((o1, o2) -> Float.compare(
                o2.getReview().getAverageScore(),
                o1.getReview().getAverageScore())
        );
    }

    private static void filterByHealthInsurance(List<ProfessionalSearchDTO> professionalSearchDTOS, HealthInsuranceDTO healthInsuranceDTO) {
        log.info("Filtering by health insurance");
        List<ProfessionalSearchDTO> filteredProfessionals = professionalSearchDTOS.stream()
                .filter(professionalSearchDTO -> !professionalSearchDTO.containsHealthInsurance(healthInsuranceDTO))
                .collect(Collectors.toList());
        professionalSearchDTOS.removeAll(filteredProfessionals);
    }
}
