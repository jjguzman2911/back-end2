package com.Medisoft.Medisoft.Utilities;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO.AppointmentDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.DTO.ProfessionalAppointmentDTO;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.AttentionManagement.Attention;
import com.Medisoft.Medisoft.Entities.AttentionManagement.DTO.ProfessionalMapperAttentionDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.CategoriaTipoAtencion;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO.ProfessionalAttentionTypeDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.DTO.TipoAtencionDTO;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencion;
import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class DTOMapperUtils {

    public static Set<TipoAtencionDTO> toAttentionTypeDto(List<TipoAtencion> attentions) {
        log.info("Mapping to attention type dto");
        return attentions.stream()
                .map(TipoAtencionDTO::new)
                .collect(Collectors.toSet());
    }

    public static List<TipoAtencionProfesional> fromProfessionalAttentionTypeDto(Set<ProfessionalAttentionTypeDTO> attentionsDto) {
        log.info("Mapping from professional attention type dto");
        return attentionsDto.stream()
                .map(attentionDto -> TipoAtencionProfesional.builder()
                        .precio(attentionDto.getPrice())
                        .unidadMinimaTrabajo(attentionDto.getMinimumWorkingUnit())
                        .categoriaTipoAtencion(new CategoriaTipoAtencion(
                                attentionDto.getCategoryId(),
                                attentionDto.getCategoryName()))
                        .tipoAtencion(new TipoAtencion(
                                attentionDto.getAttentionTypeId(),
                                attentionDto.getAttentionTypeName(),
                                attentionDto.isMandatory()))
                        .build())
                .collect(Collectors.toList());
    }

    public static List<ProfessionalAppointmentDTO> toProfessionalAppointmentDto(List<Turno> appointments) {
        log.info("Mapping to professional appointment dto");
        return appointments.stream()
                .map(appointment -> ProfessionalAppointmentDTO.builder()
                        .appointmentId(appointment.getId())
                        .patientFullName(appointment.getPaciente().getNombreApellido())
                        .currentAppointmentState(appointment.getEstadoActual())
                        .attentionType(appointment.getTipoAtencion().getName())
                        .startDateTimestamp(appointment.getStartDateInMillis())
                        .finishDateTimestamp(appointment.getFinishDateInMillis())
                        .patientUserId(appointment.getPatientUserId())
                        .build())
                .collect(Collectors.toList());
    }

    public static List<AppointmentDTO> toAppointmentDto(List<Turno> appointments) {
        log.info("Mapping to appointment dto");
        List<AppointmentDTO> appointmentDTOS = appointments.stream()
                .map(AppointmentDTO::new)
                .collect(Collectors.toList());
        appointmentDTOS.forEach(appointment -> log.info(appointment.toString()));
        return appointmentDTOS;
    }

    public static Set<ProfessionalMapperAttentionDTO> toProfessionalMapperAttentionDto(List<Attention> attentions, int size) {
        log.info("Mapping to professional mapper attention dto");
        return attentions.stream()
                .map(attention -> ProfessionalMapperAttentionDTO.builder()
                        .profesionalUserId(attention.getProfessionalUserId())
                        .lastAttentionTimestamp(attention.getDate().getTime())
                        .build())
                .limit(size)
                .collect(Collectors.toSet());
    }
}
