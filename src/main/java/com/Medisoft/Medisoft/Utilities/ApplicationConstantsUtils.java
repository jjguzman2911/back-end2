package com.Medisoft.Medisoft.Utilities;

public class ApplicationConstantsUtils {
    public static final int APPOINTMENT_MINIMUM_DURATION = 20;
    public static final String PAYMENT_SLIP_PATH = "slips/Payment slips";
    public static final String APPOINTMENT_SLIP_PATH = "slips/Appointment slips";
    public static final float REFUNDABLE_PERCENTAGE = 0.7f;

    public static String getDurationInMinutesPretty() {
        return APPOINTMENT_MINIMUM_DURATION + " min";
    }
}
