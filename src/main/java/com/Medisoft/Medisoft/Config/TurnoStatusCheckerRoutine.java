package com.Medisoft.Medisoft.Config;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.EstadoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.EventoTurno;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Services.AppointmentService;
import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Scheduling.AppointmentTaskScheduler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@Order(0)
public class TurnoStatusCheckerRoutine implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private AppointmentTaskScheduler appointmentTaskScheduler;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        checkTurnosStatus();
    }

    private void checkTurnosStatus() {
        List<Turno> turnoList = appointmentService.findAll();
        if (checkState(turnoList)) {
            appointmentService.saveAll(turnoList);
            log.info("Appointments updated");
        } else {
            log.info("No appointments to update");
        }
    }

    private boolean checkState(List<Turno> turnos) {
        boolean needsUpdate = false;
        for (Turno turno : turnos) {
            if (esNoDisponible(turno) ||
                    esNoAsistido(turno)) {
                needsUpdate = true;
            }
        }
        return needsUpdate;
    }

    private boolean esNoDisponible(Turno turno) {
        if (turno.getEstadoActual().equals(EstadoTurno.DISPONIBLE)) {
            if (!turno.isFuture()) {
                turno.actualizarEstado(EventoTurno.NO_ASIGNAR);
                return true;
            } else {
                createNoDisponibilidadTurnoTask(turno);
            }
        }
        return false;
    }

    private boolean esNoAsistido(Turno turno) {
        if ((turno.getEstadoActual().equals(EstadoTurno.ASIGNADO) ||
                turno.getEstadoActual().equals(EstadoTurno.COBRADO))) {
            if (!turno.isFuture()) {
                turno.actualizarEstado(EventoTurno.REGISTRAR_INASISTENCIA);
                return true;
            } else {
                createInasistenciaTurnoTask(turno);
            }
        }
        return false;
    }

    private void createInasistenciaTurnoTask(Turno turno) {
        appointmentTaskScheduler.createNonAttendanceAppointmentTask(turno, false);
    }

    private void createNoDisponibilidadTurnoTask(Turno turno) {
        appointmentTaskScheduler.createNoDisponibilidadTurnoTask(turno, false);
    }
}
