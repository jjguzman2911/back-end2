package com.Medisoft.Medisoft.Config;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.SessionManagement.Session;
import com.Medisoft.Medisoft.Scheduling.AppointmentTask;
import com.Medisoft.Medisoft.Scheduling.CloseSessionTask;
import com.Medisoft.Medisoft.Scheduling.NonAttendanceAppointmentTask;
import com.Medisoft.Medisoft.Scheduling.UnassignedAppointmentTask;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan(basePackages = "com.Medisoft.Medisoft.Scheduling")
public class TaskConfig {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public AppointmentTask noDisponibilidadTurnoTask(Turno turno) {
        return new UnassignedAppointmentTask(turno);
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public AppointmentTask inasistenciaTurnoTask(Turno turno) {
        return new NonAttendanceAppointmentTask(turno);
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public CloseSessionTask closeSessionTask(Session session) {
        return CloseSessionTask.builder().session(session).build();
    }
}
