package com.Medisoft.Medisoft.Config;

import com.Medisoft.Medisoft.Entities.SessionManagement.Session;
import com.Medisoft.Medisoft.Entities.UserManagement.SessionStorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@Order(1)
public class CloseSessionsRoutine implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private SessionStorageService sessionStorageService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        closeOpenedSessions();
    }

    private void closeOpenedSessions() {
        List<Session> sessions = sessionStorageService.findAll();
        sessions.stream()
                .filter(Session::isOpen)
                .forEach(Session::close);
        sessionStorageService.saveAll(sessions);
    }
}
