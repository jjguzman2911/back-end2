package com.Medisoft.Medisoft.DAO.Mongo;

import com.Medisoft.Medisoft.Entities.AttentionManagement.PatientMedicalRecord;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPatientMedicalRecordDAO extends MongoRepository<PatientMedicalRecord, Long> {
}
