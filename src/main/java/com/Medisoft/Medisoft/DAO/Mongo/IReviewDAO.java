package com.Medisoft.Medisoft.DAO.Mongo;

import com.Medisoft.Medisoft.Entities.AttentionManagement.Review;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IReviewDAO extends MongoRepository<Review, Long> {

    List<Review> findAllByProfessionalUserId(int professionalUserId);
}
