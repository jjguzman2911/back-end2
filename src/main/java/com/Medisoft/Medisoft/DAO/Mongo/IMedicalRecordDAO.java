package com.Medisoft.Medisoft.DAO.Mongo;

import com.Medisoft.Medisoft.Entities.MedicalRecordManagement.MedicalRecordStructure;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IMedicalRecordDAO extends MongoRepository<MedicalRecordStructure, Long> {
}
