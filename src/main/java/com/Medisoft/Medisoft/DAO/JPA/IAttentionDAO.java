package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.AttentionManagement.Attention;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAttentionDAO extends JpaRepository<Attention, Long> {
}
