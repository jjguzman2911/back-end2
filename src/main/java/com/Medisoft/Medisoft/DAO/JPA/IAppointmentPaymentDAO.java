package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.PaymentManagement.AppointmentPayment;
import com.Medisoft.Medisoft.Entities.StatisticsManagement.PaymentByPaymentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IAppointmentPaymentDAO extends JpaRepository<AppointmentPayment, Long> {

    @Query(value =
            "SELECT COUNT(DISTINCT t.id) AS amount, ap.dtype AS paymentType " +
            "FROM appointment_payment ap JOIN turno t ON ap.appointment_id = t.id " +
            "WHERE t.profesional_id_usuario = :professionalUserId && " +
                "unix_timestamp(ap.date_created)*1000 BETWEEN :fromTimestamp AND :toTimestamp " +
            "GROUP BY ap.dtype",
            nativeQuery = true)
    List<PaymentByPaymentType> countPaymentsByPaymentType(@Param("professionalUserId") int professionalUserId,
                                                          @Param("fromTimestamp") long fromTimestamp,
                                                          @Param("toTimestamp") long toTimestamp);

}
