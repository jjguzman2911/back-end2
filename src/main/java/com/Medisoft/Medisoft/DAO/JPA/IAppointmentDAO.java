package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.Turno;
import com.Medisoft.Medisoft.Entities.StatisticsManagement.AppointmentByDay;
import com.Medisoft.Medisoft.Entities.StatisticsManagement.AppointmentByHealthInsurance;
import com.Medisoft.Medisoft.Entities.StatisticsManagement.AppointmentByMonth;
import com.Medisoft.Medisoft.Entities.StatisticsManagement.AppointmentByState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IAppointmentDAO extends JpaRepository<Turno, Long> {

    @Query(value =
            "SELECT COUNT(DISTINCT t.id) AS amount, het.estado_turno AS state " +
            "FROM turno t JOIN historia_estado_turno het ON t.id = het.id_turno " +
            "WHERE t.profesional_id_usuario = :professionalUserId && " +
                "(unix_timestamp(het.fecha_hora)*1000) BETWEEN :fromTimestamp AND :toTimestamp " +
            "GROUP BY het.estado_turno " +
            "HAVING het.estado_turno IN :stateList",
            nativeQuery = true)
    List<AppointmentByState> countAppointmentsByState(@Param("professionalUserId") int professionalUserId,
                                                      @Param("stateList") List<String> stateList,
                                                      @Param("fromTimestamp") long fromTimestamp,
                                                      @Param("toTimestamp") long toTimestamp);

    @Query(value =
            "SELECT COUNT(DISTINCT t.id) AS amount, phi.health_insurance AS healthInsurance " +
            "FROM turno t JOIN patient_health_insurance phi ON t.patient_health_insurance_id = phi.id " +
            "JOIN historia_estado_turno het ON t.id = het.id_turno " +
            "WHERE t.profesional_id_usuario = :professionalUserId && " +
                "(unix_timestamp(het.fecha_hora)*1000) BETWEEN :fromTimestamp AND :toTimestamp " +
                "&& t.estado_actual = 'ATENDIDO'" +
            "GROUP BY phi.health_insurance",
            nativeQuery = true)
    List<AppointmentByHealthInsurance> countAppointmentTreatedByHealthInsurance(@Param("professionalUserId") int professionalUserId,
                                                                                @Param("fromTimestamp") long fromTimestamp,
                                                                                @Param("toTimestamp") long toTimestamp);

    @Query(value =
            "SELECT COUNT(DISTINCT t.id) as amount, MONTH(het.fecha_hora) AS monthId, monthname(het.fecha_hora) AS monthName, YEAR(het.fecha_hora) AS yearId " +
            "FROM turno t JOIN historia_estado_turno het ON t.id = het.id_turno " +
            "WHERE t.profesional_id_usuario = :professionalUserId && " +
                "date_format(het.fecha_hora, '%Y-%m') BETWEEN " +
                "date_format(from_unixtime(:fromTimestamp/1000), '%Y-%m') AND " +
                "date_format(from_unixtime(:toTimestamp/1000), '%Y-%m') && " +
                "het.estado_turno = 'ATENDIDO' " +
            "GROUP BY MONTH(het.fecha_hora), YEAR(het.fecha_hora) " +
            "ORDER BY YEAR(het.fecha_hora), MONTH(het.fecha_hora)",
            nativeQuery = true)
    List<AppointmentByMonth> countAppointmentsTreatedByMonth(@Param("professionalUserId") int professionalUserId,
                                                             @Param("fromTimestamp") long fromTimestamp,
                                                             @Param("toTimestamp") long toTimestamp);


    @Query(value =
            "SELECT COUNT(t.id) AS amount, dayofweek(str_to_date(t.fecha_hora_inicio, '%d/%m/%Y %H:%i')) AS dayId, dayname(str_to_date(t.fecha_hora_inicio, '%d/%m/%Y %H:%i')) as dayName " +
            "FROM turno t JOIN historia_estado_turno het ON t.id = het.id_turno " +
            "WHERE t.profesional_id_usuario = :professionalUserId && " +
                "(unix_timestamp(str_to_date(t.fecha_hora_inicio, '%d/%m/%Y %H:%i'))*1000) BETWEEN " +
                ":fromTimestamp AND " +
                ":toTimestamp && " +
                "het.estado_turno = 'ASIGNADO' " +
            "GROUP BY dayofweek(str_to_date(t.fecha_hora_inicio, '%d/%m/%Y %H:%i'))",
            nativeQuery = true)
    List<AppointmentByDay> countAppointmentsTreatedByDay(@Param("professionalUserId") int professionalUserId,
                                                         @Param("fromTimestamp") long fromTimestamp,
                                                         @Param("toTimestamp") long toTimestamp);
}
