package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.TipoAtencionProfesional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITipoAtencionProfesionalDAO extends JpaRepository<TipoAtencionProfesional, Integer> {
}
