package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.HealthInsurance.HealthInsuranceDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IHealthInsuranceDAO extends JpaRepository<HealthInsuranceDTO, Integer> {
}
