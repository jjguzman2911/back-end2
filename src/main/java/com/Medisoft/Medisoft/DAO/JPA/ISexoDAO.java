package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.UserManagement.Person.Sexo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaz DAO para la persistencia de Sexo.
 *
 * @author Misael
 * @version 2019-11-15
 */
@Repository
public interface ISexoDAO extends JpaRepository<Sexo, Integer> {
}
