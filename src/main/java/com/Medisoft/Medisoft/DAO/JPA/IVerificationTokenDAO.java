package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.SessionManagement.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Interfaz que permite el mapeo a BD de los Token de Verificacion.
 *
 * @author Misael
 * @version 2019-11-17
 */
@Repository
public interface IVerificationTokenDAO extends JpaRepository<VerificationToken, Long> {

    VerificationToken findByToken(String token);
}
