package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Qualification.QualificationRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QualificationRequestDAO extends JpaRepository<QualificationRequest, Integer> {
}
