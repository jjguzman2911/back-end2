package com.Medisoft.Medisoft.DAO.JPA;


import com.Medisoft.Medisoft.Entities.UserManagement.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IUserDAO extends JpaRepository<User, Integer> {

    User findByEmail(String email);
}
