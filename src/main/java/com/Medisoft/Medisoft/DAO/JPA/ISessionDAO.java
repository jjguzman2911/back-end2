package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.SessionManagement.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaz DAO para la persistencia de la Sesion de Usuario.-
 *
 * @author Misael
 * @version 2019-11-20
 */
@Repository
public interface ISessionDAO extends JpaRepository<Session, Integer> {
}
