package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.UserManagement.Person.TipoDocumento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaz de Tipo de Documento para aplicacion de Patron DAO y Abstract Factory.
 *
 * @author misael
 * @version 2019-09-06
 */
@Repository
public interface ITipoDocumentoDAO extends JpaRepository<TipoDocumento, Integer> {


}
