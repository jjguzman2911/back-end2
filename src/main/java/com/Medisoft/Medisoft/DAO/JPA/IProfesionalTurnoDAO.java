package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.AppointmentManagement.ProfesionalTurno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IProfesionalTurnoDAO extends JpaRepository<ProfesionalTurno, Integer> {
}
