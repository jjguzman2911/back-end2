package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.PaymentManagement.ProfessionalMercadopagoCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessionalMercadopagoCredentialsDAO extends JpaRepository<ProfessionalMercadopagoCredentials, Integer> {

    ProfessionalMercadopagoCredentials findByProfessionalUserId(int professionalUserId);
}
