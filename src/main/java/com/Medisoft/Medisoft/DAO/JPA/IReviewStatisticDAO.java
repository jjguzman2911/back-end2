package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.AttentionManagement.ReviewStatistic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IReviewStatisticDAO extends JpaRepository<ReviewStatistic, Long> {

    ReviewStatistic findByProfessionalUserId(int professionalUserId);
}
