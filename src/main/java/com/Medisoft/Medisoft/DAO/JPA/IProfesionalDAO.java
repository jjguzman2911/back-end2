package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.Profesional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaz de Profesional para aplicacion de Patron DAO y Abstract Factory.
 *
 * @author Misael
 * @created 24/03/2020
 * @lastModification 24/03/2020
 */
@Repository
public interface IProfesionalDAO extends JpaRepository<Profesional, Integer> {
}
