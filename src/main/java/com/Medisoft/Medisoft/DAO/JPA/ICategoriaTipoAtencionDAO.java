package com.Medisoft.Medisoft.DAO.JPA;

import com.Medisoft.Medisoft.Entities.ProfessionalManagement.AttentionTypes.CategoriaTipoAtencion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interfaz DAO para la persistencia de las Categorias de Tipo de Atencion.
 *
 * @author Misael
 * @version 2020-01-27
 */
@Repository
public interface ICategoriaTipoAtencionDAO extends JpaRepository<CategoriaTipoAtencion, Integer> {
}
